var gulp = require('gulp');
var sass = require('gulp-sass');
var uglifycss = require('gulp-uglifycss');

gulp.task('sass', function() {
    gulp.src('./scss/styles.scss')
            .pipe(sass())
            .pipe(gulp.dest('../public/assets/webapp/css/'));
});

gulp.task('css', function () {
    gulp.src('./css/*.css')
        .pipe(uglifycss({
            "maxLineLen": 80,
            "uglyComments": true
        }))
        .pipe(gulp.dest('../public/assets/webapp/css/'));
});

gulp.task('watch', function() {
	gulp.watch('./scss/helpers/*.scss', ['sass','css']);
    gulp.watch('./scss/styles.scss', ['sass','css']);
});

gulp.task('compile', function() {
	gulp.src( './scss/*.scss' )
		.pipe(sass())
        .pipe(gulp.dest('./css/'));
});