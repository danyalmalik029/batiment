-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 31, 2018 at 03:58 AM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.2.3-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `etbdev`
--

-- --------------------------------------------------------

--
-- Table structure for table `etb01_building_permits`
--

CREATE TABLE `etb01_building_permits` (
  `id` int(10) UNSIGNED NOT NULL,
  `construction_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metadata` text COLLATE utf8_unicode_ci,
  `ground` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `building` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `access` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `aspect` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `petitionary_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `place_construction` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `characteristics_place` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `surrounding_landscape` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `concerned_land` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `neighboring_constructions` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `comments` text COLLATE utf8_unicode_ci,
  `status` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `etb01_building_permits`
--

INSERT INTO `etb01_building_permits` (`id`, `construction_type`, `metadata`, `ground`, `building`, `access`, `aspect`, `petitionary_name`, `place_construction`, `characteristics_place`, `surrounding_landscape`, `concerned_land`, `neighboring_constructions`, `user_id`, `created_at`, `updated_at`, `comments`, `status`) VALUES
(20, 'Maison ossature bois', '{\"ground_plane\":\"1\\/2MW8eZBFEDYMjv87wOGeJcshTIYALOsrNuUNphot.jpeg\",\"plan_facade\":\"1\\/gEh5kzEWbAF0v1CmOz8XMb3RSJq2dR9ieNTOP9Nb.jpeg\",\"photo_ground1\":\"1\\/maLitSM909c7cXN9Y1oLH7iN8l0E72JUYchWFXke.jpeg\",\"photo_ground2\":\"1\\/Ot2yeBg3LGpLRFKiaqvS6xaf48jZ80hXfLq3ECD4.jpeg\",\"situation_plan\":\"1\\/Q7toJFcsRUupBHWb3SdIZuvXpZY0CMPydwx58cN0.jpeg\",\"recipice\":\"1\\/cLOBKY8obBKVeGUfWGtc7gSoWYd7vJEKESanMqgw.jpeg\"}', 'rocailleux', 'immeuble', 'difficle', 'jolie', 'hervos', 'Douala', 'Zone Urbaine', 'Terrain tres vert', 'Végétation forte', 'Faible pente', 1, NULL, NULL, 'je pense que ceci n\'est pas complet il faut encore faire bcp de choses pour que cela marche pigé?', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `etb01_building_permits`
--
ALTER TABLE `etb01_building_permits`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `etb01_building_permits`
--
ALTER TABLE `etb01_building_permits`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
