var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
  // This function will display the specified tab of the form ...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  // ... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }

  // teste si lon est a la derniere page  et si la langue est l'anglais, affiche le bouton de soumission sinon affiche le bouton suivant
  if (n == (x.length - 1) &&  document.getElementById("prevBtn").innerHTML == "Previous") {
    document.getElementById("nextBtn").innerHTML = "Submit";
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }

  // teste si lon est a la derniere page et si la langue est le francais, affiche le bouton de soumission sinon affiche le bouton suivant
  if (n == (x.length - 1) &&  document.getElementById("prevBtn").innerHTML == "Précédent") {
    document.getElementById("nextBtn").innerHTML = "Soumettre";
  } 
  if (n != (x.length - 1) &&  document.getElementById("prevBtn").innerHTML == "Précédent")
  {
    document.getElementById("nextBtn").innerHTML = "Suivant";
  }

  // teste si le mode est la modification, la langue francaise et affichage le bouton de soumission avant le bouton paypal
  if(n == (x.length - 2) && document.getElementById("amount").value != 0 &&  document.getElementById("prevBtn").innerHTML == "Précédent" )
  {
    // document.getElementById("paypal-button").style.display = "none";
    document.getElementById("nextBtn").innerHTML = "Soumettre";
  } 
  if(n != (x.length - 2) && document.getElementById("amount").value != 0 &&  document.getElementById("prevBtn").innerHTML == "Précédent" )
   {
    document.getElementById("nextBtn").innerHTML = "Suivant";
  }

// teste si le mode est la modification, la langue anglaise et affichage le bouton de soumission avant le bouton paypal
   if(n == (x.length - 2) && document.getElementById("amount").value  != 0 &&  document.getElementById("prevBtn").innerHTML == "Previous" )
   {
     // document.getElementById("paypal-button").style.display = "none";
     document.getElementById("nextBtn").innerHTML = "Submit";
   } 
   if(n != (x.length - 2) && document.getElementById("amount").value != 0 &&  document.getElementById("prevBtn").innerHTML == "Previous" )
   {
     document.getElementById("nextBtn").innerHTML = "Next";
   }


  // teste si le mode est l'ajout et masque le bouton de soumission
  if(n == (x.length - 1) && document.getElementById("amount").value == 0 )
  {
    document.getElementById("nextBtn").style.display = "none";
  } else {
    document.getElementById("nextBtn").style.display = "inline";
  }
  // ... and run a function that displays the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form... :
  showTab(currentTab);
  if (currentTab == x.length) {
    //...the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
  }
  if(currentTab == (x.length - 1) && document.getElementById("amount").value != 0 )
  {
      
    document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  // for (i = 0; i < y.length; i++) {
  //   // If a field is empty...
  //   if (y[i].value == "") {
  //     // add an "invalid" class to the field:
  //     y[i].className += " invalid";
  //     // and set the current valid status to false:
  //     valid = false;
  //   }
  // }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class to the current step:
  x[n].className += " active";
}