<?php

$api = app('Dingo\Api\Routing\Router');

/*
|--------------------------------------------------------------------------
| API routes
|--------------------------------------------------------------------------
|
 |
 */
$api->version('v1', ['before' => 'csrf'], function ($api) {
    $api->group(['namespace' => 'App\Api\Controllers'], function ($api) {
        $api->group(['prefix' => 'classified', 'middleware' => ['web']], function ($api) {
            $api->get('{id}', 'AdsController@getDetails');

            $api->post('put', 'AdsController@disableProperty');

            $api->post('delete', 'AdsController@deleteProperty');

            $api->post('enable', 'AdsController@enableProperty');

            $api->post('compareLength', 'AdsController@compareLength');

            $api->post('addCompareProperty', 'AdsController@addCompareProperty');

            $api->post('remove', 'AdsController@remove');

            $api->get('map/properties', 'AdsController@mapProperties');
        });
        $api->group(['prefix' => 'plan', 'middleware' => ['web']], function ($api) {
            $api->get('{id}', 'PlanController@getDetails');

            $api->post('put', 'PlanController@disableProperty');

            $api->post('delete', 'PlanController@deleteProperty');

            $api->post('enable', 'PlanController@enableProperty');

            $api->post('compareLength', 'PlanController@compareLength');

            $api->post('addCompareProperty', 'PlanController@addCompareProperty');

            $api->post('remove', 'PlanController@remove');

            $api->get('map/properties', 'PlanController@mapProperties');
        });
    });
});

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::any('/', 'HomeController@index');

Auth::routes();
//For laravel lower than 5.3

// Build


Route::get('/non-authorized',function(){
    return view('errors.401');
})->name('401');
/* Properties */
Route::get('/view-all-plans', 'PlanController@viewAllPlans');
Route::get('/view-details-plan/{id}', 'PlanController@details');
Route::get('/estimate-calculation', 'CalculatorController@index');
Route::post('/estimate-calculation/add','CalculatorController@store');
Route::post('/estimate-calculation/receive/{id}','CalculatorController@sendMail');
Route::post('/estimate-calculation/cancel/{id}','CalculatorController@cancel');

Route::get('/view-all-properties', 'PropertiesController@viewAllProperties');
Route::get('/view-all-properties/{id}', 'PropertiesController@viewUserProperties');
Route::get('/view-rent-properties', 'PropertiesController@viewRentProperties');
Route::get('/view-sale-properties', 'PropertiesController@viewSaleProperties');
Route::get('/view-build-properties', 'PropertiesController@viewBuildProperties');
Route::get('/view-details-property/{id}', 'PropertiesController@details');
Route::post('/search-property', 'PropertiesController@search');
Route::get('/search-property', function () {
    return redirect('/');
});
Route::any('/compare-properties', 'PropertiesController@compareProperties');


Route::group(['middleware' => 'auth'], function () {
    Route::get('/submit-property', 'PropertiesController@propertyform');
    Route::post('/save-property', 'PropertiesController@save');
    Route::get('/my-properties', 'PropertiesController@myProperties');

    Route::any('/edit-property/{id}', 'PropertiesController@propertyform');
    Route::post('/update-property', 'PropertiesController@update');
    Route::post('/sendMessage', 'PropertiesController@sendMessage');
    Route::get('/sendMessage', function () {
        return redirect('/contact');
    });
    Route::get('/list-messages/{adsId}', 'PropertiesController@listMessages');
    Route::get('/list-plan-messages/{adsId}', 'PlanController@listMessages');
});

/* Plan */
Route::get('/notify_read/{not_id?}', 'HomeController@mark_noti_read')->name('mark_noti_read');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/create-plan', 'PlanController@createPlan');
    Route::post('/save-plan', 'PlanController@save');
    Route::get('/my-plans', 'PlanController@myPlans');
    Route::any('/edit-plan/{id}', 'PlanController@createPlan');
    Route::post('/update-plan', 'PlanController@update');
    Route::post('/sendPlanMessage', 'PlanController@sendMessage');
    Route::get('/sendPlanMessage', function (){
        return redirect('/view-all-plans');
    });

    Route::get('/upload-picture-to-plan/{id}', 'PlanController@uploadForm');
    Route::post('/save-upload-picture-to-plan', 'PlanController@upload');

    Route::get('/upload-zip-file', 'PlanController@uploadZipForm');
    Route::post('/upload-zip-file', 'PlanController@uploadZip');


    Route::get('/planfile/{plan_name}', 'PlanController@readSourceFile');
    // Route::get('/discussion', 'PlanController@list-messages');

    Route::get('/make-a-request','BuildingPermitController@index');
    Route::get('/permit-request','BuildingPermitController@make_request');
    Route::post('/permit-store','BuildingPermitController@store')->name('build_permit_store');
    Route::post('/permit-save','BuildingPermitController@save')->name('build_permit_save');
    Route::get('/permit-checkout', 'BuildingPermitController@checkout')->name('checkout');

    Route::get('/bank-financial', 'BankFinancialController@index');
    Route::get('/bank-financial-form', 'BankFinancialController@create');
    Route::post('/store-bank-financial', 'BankFinancialController@store');
    Route::get('/store-bank-financial', function () {
        return redirect('/bank-financial');
    });
    Route::post('/update-bank-financial', 'BankFinancialController@update');

    //build
    Route::get('/view-build-now', 'BuildController@index')->name('view-build-now');
    Route::get('/before-you-start', 'BuildController@build_now')->name('beforeStart');
    Route::get('/build-form','BuildController@show');

    Route::post('/build-form/add','BuildController@store');
//    Route::post('/build-form/add','BuildController@save-property');


});


Route::group(['middleware' => ['usercheck']],function(){
    Route::get('/build-table','BuildController@retrieve');
    Route::get('/downloadfile/{id}','BuildController@download');
    //calculator admin view
    Route::get('/calculator-table','CalculatorController@retrieve');
    Route::post('/calculator/delete/{id}','CalculatorController@delete');
    Route::post('/calculator/destroy','CalculatorController@destroy');
});

Route::get('/logout', 'Auth\LoginController@logout');

Route::any('/post/{slug}', 'PostController@show');
Route::get('/contact', 'ContactController@show');
Route::post('/contact', 'ContactController@sendMessage');

Route::group(['middleware' => ['web']], function () {
    // Language selection
    Route::get('/language/{locale}', function ($locale) {
        // This route is reached only when the user wants to switch the current language
        App::setLocale($locale);
        session(['language' => $locale]);

        $source = URL::previous();
        $isPost = strrpos($source, "/post/") >= 0;

        // route to the translated URL if switching language from post page
        if ($isPost) {
            // TODO, redirect to the macthing translated URL instead of the home page
            return redirect('/');
        } else {
            return redirect()->back();
        }
    });
});

Route::group(['prefix' => 'admin'], function () {

    Voyager::routes();


    Route::get('/zip_download_build_files/{id}','BuildingPermitController@zip_download')->name('zip_buildingpermit');

    Route::get('/zip_download_bank_files/{id}','BankFinancialController@zip_download')->name('zip_financial');

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/agents', 'UserController@agents')->name('viewagents');
Route::get('/userprofile', 'UserController@index')->name('userprofile')->middleware('auth');
Route::post('/userprofile', 'UserController@updateprofile')->name('userUpdateProfil');
Route::post('/userprofile/create', 'UserController@createprofile')->name('userCreateProfil');
Route::get('/userprofile/create', 'UserController@createprofile')->name('userCreateProfil');
Route::get('/agents/{id}', 'UserController@agencyAgents')->name('viewagencyAgents');

Route::get('/agencies', 'UserController@agencies')->name('viewagencies');

Route::get('/blog', 'BlogController@index');
Route::get('/blog/{slug}', 'BlogController@show');

Route::get('/guide', 'BlogController@guide');
Route::get('/guide/{slug}', 'BlogController@show');


Route::get('login/facebook', 'Auth\LoginController@redirectToFacebook');
Route::get('login/facebook/callback', 'Auth\LoginController@handleFacebookCallback');

Route::get('login/google', 'Auth\LoginController@redirectToGoogle');
Route::get('login/google/callback', 'Auth\LoginController@handleGoogleCallback');