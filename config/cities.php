<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Cities Table Name
    |--------------------------------------------------------------------------
    |
    | Below, you may change the default table cities name.
    |
    */

    'table' => 'etb01_cities',

];
