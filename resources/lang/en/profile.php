<?php

return [

    //PROFILE UPDATE
    'email'=> 'E-mail',
    'new_password'=>'new password',
    'retype_new_password'=>'retype new password',
    'name'=>'Name',
    'phone1'=>'Phone 1',
    'phone2'=>'Phone 2',
    'gender'=>'Gender',
    'birth_date'=>'Birth date',
    'profile_photo'=>'Profile photo',
    'website_url'=>'Websit url',
    'about_me'=>'About me',
    'language'=>'Language',
    'address'=>'Address',
    'city'=>'City',
    'region'=>'Region',
    'country'=>'COUNTRY',
    'post_code'=>'Post code',
    'update_profile'=>'update profile',
    'my_profile'=>'My profile',
    'select_country'=>'Please select the country',
    'update_success'=>'Updated Successfully',
    'user_profile'=>'User Profile',
    'update_profile_link'=>'Update my profile',

   //SOCIAL LOGIN
   'facebook_error'=>'You tried to sign in with [facebook] so you are already registered with this email from [Google]. Please use [Google] to log in.',
   'google_error'=>'You tried to sign in with [google] so you are already registered with this email from [facebook]. Please use [Google] to log in.',
    
    //REGISTER BLADE
    'register_sign_up'=>'Sign Up',
    'register_full_name'=>'Full Name',
    'register_email'=>'E-mail',
    'register_password'=>'Password',
    'register_confirm_password'=>'Confirm password',
    'register_register'=>'Register',
    'register_sign_up_with'=>'Sign Up with',
    'register_already_have_account'=>'Already have account?',
    'register_sign_in'=>'Sign In',

    //LOGIN BLADE
    'login_sign_in'=>'Sign In',
    'login_email_address'=>'E-mail Address',
    'login_password'=>'Password',
    'login_log_in'=>'Log In',
    'login_forgot_password'=>'Forgot you password?',
    'login_sign_in_with'=>'Sign in with',
    'login_dont_have_account'=>'Don\'t have an account?',
    'login_sign_up'=>'Sign Up',


    // Agents and agencies page
    'my_properties' => 'My properties',
    'user_properties' => 'Agent/Agency\'s properties',
    'agencies_title' => 'All agencies',
    'agents_title' => 'Agents',
    'agents_of_title' => 'Agents of',
    'view_agent_properties' => 'View agent properties',
    'view_agency_properties' => 'View agency properties',
    'view_agency_agents' => 'View agency agents',

];