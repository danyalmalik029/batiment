<?php

return [

    'your_name'=> 'Your name',
    'your_email'=> 'Your email',
    'phone'=> 'Your phone',
    'city'=> 'City and country',
    'subject'=> 'Subject',
    'message'=> 'Message',
    'message_placeholder'=> 'Please keep your message as simple as possible',
    'form'=> 'Contact form',
    'submit'=> 'Send message',
    'email'=> 'E-mail',
    'skyoe'=> 'Skype',
    'follow_us'=> 'Follow us on',
    'page_title'=> 'Contact us',
    'page_heading'=> 'Qoutation',
    'information'=> 'Contact information',
    'information_text'=>'Determining the budget of your dream home construction is often complicated. Many parameters come into play and it is difficult to find a price range for your project.',
    'information_body'=>'Archionline accompanies thousands of individuals in their housing project and is now making available its works costing tool. With some information about your home, the online calculator tells you the price of your individual home, be it a modern home, a wood house or a traditional home. The calculator will also offer to receive free by email the detailed price batch by lot.',
    'message_sent'=> 'Message sent successfully',

];