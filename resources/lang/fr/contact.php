<?php

return [

    'your_name'=> 'Votre nom',
    'your_email'=> 'Votre courriel',
    'phone'=> 'Téléphone',
    'city'=> 'Ville et pays',
    'subject'=> 'Objet',
    'message'=> 'Message',
    'message_placeholder'=> 'SVP veuillez être le plus précis possible',
    'form'=> 'Formulaire de contact',
    'submit'=> 'Envoyer le message',
    'email'=> 'Courriel',
    'skyoe'=> 'Skype',
    'follow_us'=> 'Suivez nous sur',
    'page_title'=> 'Contactez nous',
    'page_heading'=> 'Devis',
    'information'=> 'Informations de contact',
    'information_text'=>'Déterminer le budget de votre construction de maison de rêve est souvent compliqué. Beaucoup de paramètres entrent en jeu et il est difficile de trouver une fourchette de prix pour votre projet.',
    'information_body'=>'Archionline accompagne des milliers de particuliers dans leur projet d’habitat et met aujourd’hui à disposition son outil de chiffrage travaux. Avec quelques informations sur votre maison, la calculatrice en ligne vous indique le prix de votre maison individuelle, que ce soit une maison moderne, une maison bois ou une maison traditionnelle. La calculette vous proposera également de recevoir gratuitement par email le prix détaillé lot par lot.',
    'message_sent'=> 'Message envoyé avec succès',

];