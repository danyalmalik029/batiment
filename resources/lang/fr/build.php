<?php

return [
    'build_now_title'=>'Avant de commencer',
    'build_now_text_1'=>'Vous allez bientôt commencer notre nouveau processus de construction. De cette façon, vous pourrez obtenir une estimation précise du montant requis pour la construction de votre nouvelle propriété.',
    'build_now_text_2'=>'Au cours de ce processus, il est essentiel que les informations personnelles fournies (noms, adresses, dates de naissance) soient aussi précises que possible: cela nous permettra de vous identifier avec certitude avec nos partenaires bancaires.',
    'build_now_text_3'=>'Soyez toujours rassuré, un de nos experts vous accompagne tout au long du processus.',
    'lets_go_button'=>'Allons-y',
	'build_title' => 'Notre équipe vous accompagne dans toutes les étapes pour mener à bien votre projet. Pour vos travaux, nous sélectionnons des professionnels passionnés et des entreprises  de confiance avec des prix et délais négociés à l’avance et défiant toute concurrence.',
    'typepropriete'=>'1- Qu’est-ce qui vous correspond le mieux ?',
    'typeland'=>'2. Sélectionner votre terrain   ',
    'typeland_option'=>'Sélectionner votre terrain   ',
    'typeplan'=>'3. Sélectionner votre plan  ',
    'typeplan_option'=>'Sélectionner votre plan  ',
    'typebudget'=> '5. Veuillez sélectionner le type de contrat qui vous convient le mieux ?',
    'financing_team'=> '6. Souhaitez-vous solliciter un financement auprès de notre équipe?',
    'buildingpermit'=> 'Avez-vous un permis de construire ?',
    'oui'=>'YES',
    'non'=>'NON',
    'suivant'=> 'NEXT',
    'funding'=> 'Souhaitez-vous faire une demande de financement avec notre équipe ?',
    'notefunding'=> 'Votre demande de financement sera effectué sur le montant total  de votre ',
    'soumettre' => 'SOUMETTRE',
    'fermer'=>'CLOSE',
    'license' => 'Televerser son permis ',
    'income' => 'Avez-vous un apport personnel pour votre projet?',
    'noincome' => 'Cliquer sur suivant si vous n’avez pas d’apport personnel ',
    'success_modal' => 'Nous vous remercions d’avoir soumis votre demande de construire. Un de nos experts vous contactera vous peu.',
    'askland' => 'Avez-vous un terrain ?',
    'askplan' => 'Avez-vous un plan ?',
    'askuploadplan' => "S'il vous plaît, téléchargez votre plan ici",
    'askarea' => 'Quelle est la surface (area) de votre projet ',
    'askarea_note' => 'Vous pouvez ignorer la surface de votre projet en cliquant sur suivant',
    'ask_upload' => 'televerser son permis ',
    'submit_income' => 'Soumettez vos revenus',
    'budget_1' => 'Je ne sais pas',
    'budget_2' => 'Moins de 100,000 €',
    'budget_3' => 'Entre 101,000 € et 150,000 €',
    'budget_4' => 'Entre 151,000 € et 200,000 €',
    'budget_5' => 'Entre 201,000 € et 300,000 €',
    'budget_6' => 'Plus de 300,000 €',
    'body_success' => 'Nous vous remercions d’avoir soumis votre demande de construire. Un de nos experts vous contactera vous peu.',
    'title_success' =>'Notification',
    'close' => 'Close' ,
    'proceedmodal' => 'Oui je le veux',
    'applybuildingpermit' => 'Voulez-vous demander un permis de construire avec nous?',
    'applyfund' => 'Voulez-vous appliquer un financement avec nous?' ,
    'closemodal' => 'Non, je ne veux pas',
    'construction_project_message'=>'Votre projet de construction est prêt à être soumis. S\'il vous plaît cliquer sur le bouton soumettre pour examen par notre équipe',
    'checkbox_agreement_message'=>'Je conviens qu\'un membre de l\'équipe ETB me contacte pour suivre mon projet de construction.',
    'summary_project_title'=>'Résumé de votre projet',
    'summary_project_question'=>'Avez-vous un acompte pour votre projet?',
    'summary_table_heading'=>'Résumé',
    'down_payment_label'=>'Acompte: ',
    'summary_amount_of_land'=>'Montant du terrain',
    'summary_amount_of_plan'=>'Montant du plan',
    'summary_notary_fees'=>'Frais de notaire',
    'summary_bank_charge'=>'Frais de bancaires',
    'summary_sub_total'=>'Sous total',
    'summary_grand_total'=>'Total des frais',
    'contract1'=>'CONTRAT DE MARCHE DE TRAVAUX PAR CORPS D’ETAT',
    'contract2'=>'CONTRAT CCMI',
    'contract3'=>'CONTRAT D’ENTREPRISE GENERALE',

];
