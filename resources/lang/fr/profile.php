<?php

return [

    //PROFIL PAGE
    'email'=> 'E-mail',
    'new_password'=>'Nouveau mot de passe',
    'retype_new_password'=>'Retaper le nouveau mot de passe',
    'name'=>'Nom',
    'phone1'=>'Téléphone 1',
    'phone2'=>'Téléphone 2',
    'gender'=>'Sexe',
    'birth_date'=>'Date de naissance',
    'profile_photo'=>'Photo de profile',
    'website_url'=>'Url du site web',
    'about_me'=>'A propos de moi',
    'language'=>'Langue',
    'address'=>'Adresse',
    'city'=>'Ville',
    'region'=>'Region',
    'country'=>'Pays',
    'post_code'=>'Code postal',
    'update_profile'=>'modifier le profile',
    'my_profile'=>'Mon profil',
    'select_country'=>'Veuillez choisir le pays',
    'update_success'=>'modification réussie',
    'user_profile'=>'Profile de L\'utilisateur',
    'update_profile_link'=>'Mettre mon profil à jour',

    //SOCIAL LOGIN
    'facebook_error'=>'Vous avez tenté de vous connecter avec [facebook] alors vous êtes déjà enregistré avec ce courriel depuis [Google]. SVP Veuillez utiliser [Google] pour vous connecter.',
    'google_error'=>'Vous avez tenté de vous connecter avec [google] alors vous êtes déjà enregistré avec ce courriel depuis [facebook]. SVP Veuillez utiliser [facebook] pour vous connecter.',

    //REGISTER BLADE
    'register_sign_up'=>'S\'inscrire',
    'register_full_name'=>'Nom Complet',
    'register_email'=>'E-mail',
    'register_password'=>'Mot de Passe',
    'register_confirm_password'=>'Confirmer Mot de Passe',
    'register_register'=>'Sauvegarder',
    'register_sign_up_with'=>'S\'inscrire avec',
    'register_already_have_account'=>'Avez-vous déjà un compte?',
    'register_sign_in'=>'Se connecter',

     //LOGIN BLADE
     'login_sign_in'=>'Se Connecter',
     'login_email_address'=>'Adresse E-mail',
     'login_password'=>'Mot de Passe',
     'login_log_in'=>'S\'identifier',
     'login_forgot_password'=>'Mot de passe oublié?',
     'login_sign_in_with'=>'Se connecter avec',
     'login_dont_have_account'=>'Vous n\'avez pas de compte?',
     'login_sign_up'=>'S\'inscrire',


    // Agents and agencies page
    'my_properties' => 'Mes annonces',
    'user_properties' => 'Annonces de l\'agent/agence',
    'agencies_title' => 'Les agences',
    'agents_title' => 'Les agents',
    'agents_of_title' => 'Agents de',
    'view_agent_properties' => 'Voir les propriétes',
    'view_agency_properties' => 'Voir les propriétes',
    'view_agency_agents' => 'Voir les agents',

];