@extends("webapp.layouts.default")

@section('content')
    <div class="account-pages">
        <div class="wrapper-page">
            <div class="account-bg">
                <div class="card-box m-b-0">
                    <div class="text-xs-center m-t-20">
                        <span class="logo">@lang('profile.login_sign_in')</span>
                    </div>
                    <div class="m-t-30 m-b-20">
                        <form  action="{{ url('/login') }}" method="POST" class="form-horizontal m-t-20" role="form" >
                            {{csrf_field()}}
                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                <div class="full-width col-xs-12">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="@lang('profile.login_email_address')" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                <div class="full-width col-xs-12">
                                    <input id="password" type="password" class="form-control" name="password" placeholder="@lang('profile.login_password')" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <!--
                            <div class="form-group ">
                                <div class="full-width col-xs-12">
                                    <div class="checkbox checkbox-custom">
                                        <input name="remember" id="checkbox-signup" type="checkbox">
                                        <label for="checkbox-signup">
                                            Remember me
                                        </label>
                                    </div>
                                </div>
                            </div>
                            -->

                            <div class="form-group text-center m-t-30">
                                <div class="full-width col-xs-12">
                                    <button class="btn btn-success btn-block waves-effect waves-light" type="submit">@lang('profile.login_log_in')
                                    </button>
                                </div>
                            </div>

                            <div class="form-group m-t-30 m-b-0">
                                <div class="full-width col-sm-12">
                                    <a href="{{url('/password/reset')}}" class="text-muted"><i class="fa fa-lock m-r-5"></i> @lang('profile.login_forgot_password')</a>
                                </div>
                            </div>

                            <div class="form-group m-t-30 m-b-0">
                                <div class="full-width col-sm-12 text-xs-center">
                                    <h5 class="text-muted"><b>@lang('profile.login_sign_in_with')</b></h5>
                                </div>
                            </div>

                            @include('webapp.includes.social')

                            <div class="form-group m-b-0 text-xs-center">
                                <div class="col-sm-12 text-xs-center">
                                    <p class="text-more">@lang('profile.login_dont_have_account')<a href="{{url('register')}}" class="text-white m-l-5"><b>@lang('profile.login_sign_up')</b></a></p>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
        <!-- end wrapper page -->
    </div>
@endsection