@extends("webapp.layouts.default")

@section('content')
    <div class="account-pages">
        <div class="wrapper-page">

            <div class="account-bg">
                <div class="card-box m-b-0">
                    <div class="text-xs-center m-t-20">
                        <span class="logo">Reset Password</span>
                    </div>
                    <div class="m-t-30 m-b-20">
                        <div class="col-xs-12 text-xs-center">
                            <p class="text-muted m-b-0 font-13 m-t-20">Enter your email address and we'll send you an email with
                                instructions to reset your password. </p>
                        </div>
                        <form class="form-horizontal m-t-30">
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <input class="form-control" type="email" required="" placeholder="Enter email">
                                </div>
                            </div>

                            <div class="form-group text-center m-t-20 m-b-0">
                                <div class="col-xs-12">
                                    <button class="btn btn-success btn-block waves-effect waves-light" type="submit">Send
                                        Email
                                    </button>
                                </div>
                            </div>

                            <div class="form-group m-b-0 text-xs-center">
                                <div class="row m-t-20">
                                    <div class="col-sm-12 text-xs-center">
                                        <p class="text-more">Return to<a href="{{url('/login')}}" class="text-white m-l-5"><b>Sign In</b></a></p>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
            <!-- end card-box-->

        </div>
        <!-- end wrapper page -->
    </div>
@endsection