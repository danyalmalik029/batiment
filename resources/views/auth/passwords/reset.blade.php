@extends("webapp.layouts.default")

@section('content')



    <div class="account-pages">
        <div class="wrapper-page">
            <div class="account-bg">
                <div class="card-box m-b-0">
                    <div class="text-xs-center m-t-20">
                        <span class="logo">Reset Password</span>
                    </div>
                    <div class="m-t-30 m-b-20">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form role="form" method="POST" action="{{ url('/password/reset') }}" method="POST" class="form-horizontal m-t-20" >
                            {{csrf_field()}}

                            <input type="hidden" name="token" value="{{ $token }}">
                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                <div class="full-width col-xs-12">
                                    <input class="form-control" name="email" type="email" required="" placeholder="Email">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                <div class="full-width col-xs-12">
                                    <input class="form-control" name="password" type="password" required="" placeholder="Password">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <div class="full-width col-xs-12">
                                    <input id="password-confirm" type="password" class="form-control"  placeholder="Confirm Password" name="password_confirmation" required>

                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group text-center m-t-30">
                                <div class="full-width col-xs-12">
                                    <button class="btn btn-success btn-block waves-effect waves-light" type="submit">Reset Password
                                    </button>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
        <!-- end wrapper page -->
    </div>

@endsection
