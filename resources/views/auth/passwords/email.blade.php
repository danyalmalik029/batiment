@extends("webapp.layouts.default")

@section('content')
    <div class="account-pages">
        <div class="wrapper-page">
            <div class="account-bg">
                <div class="card-box m-b-0">
                    <div class="text-xs-center m-t-20">
                        <span class="logo">Reset Password</span>
                    </div>
                    <div class="m-t-30 m-b-20">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form  action="{{ url('/password/email') }}" method="POST" class="m-t-20" role="form" >
                            {{csrf_field()}}
                            <label for="email">E-Mail Address</label>
                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                <div class="full-width col-xs-12">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                <div class="full-width col-xs-12">
                                    <button type="submit" class="btn btn-primary btn-block ">
                                        Send Password Reset Link
                                    </button>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
        <!-- end wrapper page -->
    </div>

@endsection
