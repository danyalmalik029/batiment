@extends("webapp.layouts.default")

@section('content')
    <div class="account-pages">
        <div class="wrapper-page">
            <div class="account-bg">
                <div class="card-box m-b-0">
                    <div class="text-xs-center m-t-20">
                        <span class="logo">@lang('profile.register_sign_up')</span>
                    </div>
                    <div class="m-t-30 m-b-20">
                        @if(count($errors))
                        @endif
                        <form action="{{ url('register') }}" method="POST" class="form-horizontal m-t-20" >
                            {{csrf_field()}}
                            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                <div class="full-width col-xs-12">
                                    <input class="form-control" name="name" type="text" required="" value="{{ old('name') }}" placeholder="@lang('profile.register_full_name')">
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                <div class="full-width col-xs-12">
                                    <input class="form-control" name="email" type="email" required="" placeholder="@lang('profile.register_email')">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                <div class="full-width col-xs-12">
                                    <input class="form-control" name="password" type="password" required="" placeholder="@lang('profile.register_password')">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <div class="full-width col-xs-12">
                                    <input id="password-confirm" type="password" class="form-control"  placeholder="@lang('profile.register_confirm_password')" name="password_confirmation" required>

                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group text-center m-t-30">
                                <div class="full-width col-xs-12">
                                    <button class="btn btn-success btn-block waves-effect waves-light" type="submit">@lang('profile.register_register')
                                    </button>
                                </div>
                            </div>

                            <div class="form-group m-t-30 m-b-0">
                                <div class="full-width col-sm-12 text-xs-center">
                                    <h5 class="text-muted"><b>@lang('profile.register_sign_up_with')</b></h5>
                                </div>
                            </div>

                            @include('webapp.includes.social')


                            <div class="form-group m-b-0 text-xs-center">
                                <div class="col-sm-12 text-xs-center">
                                    <p class="text-more">@lang('profile.register_already_have_account')<a href="{{url('login')}}" class="text-white m-l-5"><b>@lang('profile.register_sign_in')</b></a></p>
                                </div>
                            </div>


                        </form>

                    </div>
                </div>
            </div>
            <!-- end card-box-->
        </div>
        <!-- end wrapper page -->
    </div>
@endsection