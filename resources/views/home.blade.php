@extends("webapp.layouts.default")


@section('content')

    <div class="content-title">
        <div class="content-title-inner">
            <div class="container">
                <h1>@lang('profile.my_profile')</h1>
            </div><!-- /.container -->
        </div><!-- /.content-title-inner -->
    </div>

    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">
                <div class="content">
                    <div class="container maincontent">

                        @if (session('message'))
                            <div class="alert alert-success">
                                {{ session('message') }}
                            </div>
                        @endif

                        <div class="col-xs-12">
                            <form method="post" action="{{ url('userprofile') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <input type="hidden" name="iduser" id="iduser" value="{{ $profile->iduser }}">
                                                <div class="form-group ">
                                                    <label for="email">@lang('profile.email')</label>
                                                    <input type="text" id="emailcantchange" name="emailcantchange" class="form-control" value="{{old('email', $user->email) }}" disabled="disabled">
                                                    @if ($errors->has('email'))
                                                        <span class="has-error">
                                                        {{ $errors->first('email') }}
                                                        </span>
                                                    @endif
                                                </div><!-- /.form-group -->
                                                <!--
                                                <div class="form-group ">
                                                    <label for="username">User name</label>
                                                    <input type="text" id="username" name="username" class="form-control" value="{{old('username', $user->username) }}">
                                                    @if ($errors->has('username'))
                                                        <span class="has-error">
                                                        {{ $errors->first('username') }}
                                                        </span>
                                                    @endif
                                                </div>
                                                -->
                                                <!-- /.form-group -->
                                                <div class="form-group @if($errors->has('password')) error @endif">
                                                    <label for="password">@lang('profile.new_password')</label>
                                                    <input type="password" id="password" name="password" class="form-control" >
                                                    @if ($errors->has('password'))
                                                        <span class="has-error">
                                                        {{ $errors->first('password') }}
                                                        </span>
                                                    @endif
                                                </div><!-- /.form-group -->
                                                <div class="form-group @if($errors->has('password_confirmation')) error @endif">
                                                    <label for="">@lang('profile.retype_new_password')</label>
                                                    <input type="password" id="password_confirmation" name="password_confirmation" class="form-control" >
                                                    @if ($errors->has('password_confirmation'))
                                                        <span class="has-error">
                                                        {{ $errors->first('password_confirmation') }}
                                                        </span>
                                                    @endif
                                                </div><!-- /.form-group -->
                                                <div class="form-group @if($errors->has('name')) error @endif">
                                                    <label for="">@lang('profile.name')</label>
                                                    <input type="text" id="name" name="name" class="form-control" value="{{old('name', $user->name) }}">
                                                    @if ($errors->has('name'))
                                                        <span class="has-error">
                                                        {{ $errors->first('name') }}
                                                        </span>
                                                    @endif
                                                </div><!-- /.form-group -->
                                                <div class="form-group @if($errors->has('phone_1')) error @endif">
                                                    <label for="">@lang('profile.phone1')</label>
                                                    <input type="text" id="phone_1" name="phone_1" class="form-control" value="{{old('phone_1', $profile->phone_1) }}">
                                                    @if ($errors->has('phone_1'))
                                                        <span class="has-error">
                                                        {{ $errors->first('phone_1') }}
                                                        </span>
                                                    @endif
                                                </div><!-- /.form-group -->
                                                <div class="form-group @if($errors->has('phone_2')) error @endif">
                                                    <label for="">@lang('profile.phone2')</label>
                                                    <input type="text"  name="phone_2" id="phone_2" class="form-control" value="{{ old('phone2', $profile->phone_2) }}">
                                                    @if ($errors->has('phone_2'))
                                                        <span class="has-error">
                                                        {{ $errors->first('phone_2') }}
                                                        </span>
                                                    @endif
                                                </div><!-- /.form-group -->
                                                <div class="form-group">
                                                    <label for="">@lang('profile.gender')</label>
                                                    <select class="form-control"  name="gender" id="gender">
                                                        @if( $profile->gender==="M")
                                                            <option value="M" selected>Male</option>
                                                            <option value="F" >Female</option>
                                                        @elseif( $profile->gender==="F")
                                                            <option value="M" >Male</option>
                                                            <option value="F" selected>Female</option> 
                                                        @endif
                                                        @if($profile->gender!=="M" && $profile->gender!=="F")
                                                            <option value="M" selected>Male</option>
                                                            <option value="F" >Female</option>
                                                        @endif
                                                    </select>
                                                    @if ($errors->has('email'))
                                                        <span class="has-error">
                                                        {{ $errors->first('email') }}
                                                        </span>
                                                    @endif
                                                </div><!-- /.form-group -->
                                                <div class="form-group @if($errors->has('birth_date')) error @endif">
                                                    <label for="">@lang('profile.birth_date')</label>

                                                    <div class="input-group input-append date" id="dpYears" data-date="{{ old('birth_date', $profile->birth_date) }}" data-date-format="@lang("general.general_language_format")" data-date-viewmode="years">
                                                        <input name="birth_date" id="birth_date" class="form-control" size="16" type="text" value="{{ old('birth_date', $profile->birth_date) }}" readonly="">
                                                        <span class="add-on input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                    </div>

                                                    @if ($errors->has('birth_date'))
                                                        <span class="has-error">
                                                        {{ $errors->first('birth_date') }}
                                                        </span>
                                                    @endif
                                                </div><!-- /.form-group -->
                                                <div class="form-group @if($errors->has('avatar')) error @endif">
                                                    <label for="avatar">@lang('profile.profile_photo')</label>
                                                    <input type="file" class="form-control" name="avatar" id="avatar">
                                                    @if ($errors->has('avatar'))
                                                        <span class="has-error">
                                                        {{ $errors->first('avatar') }}
                                                        </span>
                                                    @endif
                                                </div><!-- /.form-group -->

                                                @if(is_file('storage/' . $user->avatar))
                                                <div class="form-group">
                                                    <img src="{{ asset('storage/' . $user->avatar) }}" style="max-width: 300px; margin: 10px auto;">
                                                </div>
                                                @endif

                                            </div><!-- /.col-* -->

                                            <div class="col-sm-6">
                                                <div class="form-group @if($errors->has('website_url')) error @endif">
                                                    <label for="">@lang('profile.website_url')</label>
                                                    <input type="text" class="form-control"  name="website_url" id="website_url" value="{{ old('website_url', $profile->website_url) }}">
                                                    @if ($errors->has('website_url'))
                                                        <span class="has-error">
                                                        {{ $errors->first('website_url') }}
                                                        </span>
                                                    @endif
                                                </div><!-- /.form-group -->

                                                <div class="form-group @if($errors->has('about_me')) error @endif">
                                                    <label for="">@lang('profile.about_me')</label>
                                                    <textarea name="about_me" class="form-control" rows="4">{{ old('about_me', $profile->about_me) }}</textarea>
                                                    @if ($errors->has('about_me'))
                                                        <span class="has-error">
                                                        {{ $errors->first('about_me') }}
                                                        </span>
                                                    @endif
                                                </div><!-- /.form-group -->
                                                <div class="form-group">
                                                <label for="">@lang('profile.language')</label>
                                                    <select class="form-control"  name="language" id="language" value="{{ old('language', $profile->language) }}">
                                                        @if( $profile->language==="EN")
                                                            <option value="EN" selected>English</option>
                                                            <option value="FR">Français</option>
                                                        @elseif( $profile->language==="FR")
                                                            <option value="FR" selected>Français</option>
                                                            <option value="EN" >English</option>
                                                        @endif
                                                        @if( $profile->language!=="EN" && $profile->language!=="FR" )
                                                            <option value="EN" selected>English</option>
                                                            <option value="FR">Français</option>
                                                        @endif


                                                    </select>
                                                    @if ($errors->has('language'))
                                                        <span class="has-error">
                                                        {{ $errors->first('language') }}
                                                        </span>
                                                    @endif
                                                </div><!-- /.form-group -->

                                                <div class="form-group @if($errors->has('address')) error @endif">
                                                    <label for="">@lang('profile.address')</label>
                                                    <input type="text" class="form-control"  name="address" id="address" value="{{ old('address ', $profile->address) }}">
                                                    @if ($errors->has('address'))
                                                        <span class="has-error">
                                                        {{ $errors->first('address') }}
                                                        </span>
                                                    @endif
                                                </div><!-- /.form-group -->
                                                <div class="form-group @if($errors->has('city')) error @endif">
                                                    <label for="">@lang('profile.city')</label>
                                                    <input type="text" class="form-control"  name="city" id="city" value="{{ old('city', $profile->city) }}">
                                                    @if ($errors->has('city'))
                                                        <span class="has-error">
                                                        {{ $errors->first('city') }}
                                                        </span>
                                                    @endif
                                                </div><!-- /.form-group -->

                                                <div class="form-group @if($errors->has('region')) error @endif">
                                                    <label for="">@lang('profile.region')</label>
                                                    <input type="text" class="form-control"  name="region" id="region" value="{{ old('region', $profile->region) }}">
                                                    @if ($errors->has('region'))
                                                        <span class="has-error">
                                                        {{ $errors->first('region') }}
                                                        </span>
                                                    @endif
                                                </div><!-- /.form-group -->

                                                <div class="form-group @if($errors->has('country')) error @endif">
                                                    <label for="">@lang('profile.country')</label>
                                                    <select id="country"  class="form-control" name="country">
                                                    <option value="" selected>@lang('profile.select_country')</option>
                                                    @foreach($countries as $country)
                                                        <option value="{{ $country['alpha-3'] }}" @if(isset($countriesPrefixes[0])) @if($countriesPrefixes[0] == $country['alpha-3']) selected @endif @endif>{{ $country['name'] }}</option>
                                                    @endforeach
                                                    </select>
                                                </div><!-- /.form-group -->

                                                <div class="form-group @if($errors->has('postcode')) error @endif">
                                                    <label for="">@lang('profile.post_code')</label>
                                                    <input type="text" class="form-control"  name="postcode" id="postcode" value="{{ old('postcode', $profile->postcode) }}">
                                                    @if ($errors->has('postcode'))
                                                        <span class="has-error">
                                                        {{ $errors->first('postcode') }}
                                                        </span>
                                                    @endif
                                                </div><!-- /.form-group -->
                                            </div><!-- /.col-* -->
                                        </div><!-- /.row -->

                                        <div class="center">

                                            <div class="form-group-btn">
                                                <button type="submit" class="btn btn-primary">@lang('profile.update_profile')</button>
                                            </div><!-- /.form-group-btn -->
                                        </div><!-- /.center -->

                                    </div><!-- /.col-* -->
                                </div><!-- /.row -->
                            </form>
                        </div>

                    </div>
                </div>
            </div><!-- /.main-inner -->
        </div><!-- /.main -->
    </div><!-- /.main-wrapper -->






    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">

                <div class="content-title">
                    <div class="content-title-inner">
                        <div class="container">
                            <h1>@lang('profile.user_profile')</h1>
                        </div><!-- /.container -->
                    </div><!-- /.content-title-inner -->
                </div><!-- /.content-title -->


                <div class="content">
                    <div class="container">

                    </div><!-- /.container -->
                </div><!-- /.content -->
            </div>
        </div><!-- /.main -->
    </div><!-- /.main-wrapper -->

    @include("webapp.includes.call-to-action-footer")

@endsection


@section('script')

    <script type="text/javascript">
        $('#dpYears').datepicker();
    </script>

@endsection