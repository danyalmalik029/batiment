<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
</head>
<body>

<table width="80%" bgcolor="white">
    <tr>
        <td width="20%"><center><img src="https://etb-batiment.com/assets/webapp/img/logo_etb.png" height="60" width="50"></center></td>
        <td width="60%">
            <font face="Times New Roman" size="5" color="black">ETB Batiment</font> <br />
        </td>
    </tr>

</table>
<table width="80%">
    <tr>
        <td width="20%"><font face="Times New Roman" size="5" color="black"><a>WWW.ETB-BATIMENT.COM</a></font></td>
        <td width="20%"><font face="Times New Roman" size="5" color="black">{{$total * 0.05}} € </font></td>
    </tr>
    <tr>
        <td width="20%"><font face="Times New Roman" size="5" color="black">COORDINATION PILOTAGE ARCHITECTE ET INGENIEUR</font></td>
        <td width="20%"><font face="Times New Roman" size="5" color="black">{{$total * 0.04}} € </font></td>
    </tr>
    <tr>
        <td width="20%"><font face="Times New Roman" size="5" color="black">TERRASSEMENT</font></td>
        <td width="20%"><font face="Times New Roman" size="5" color="black">{{$total * 0.04}} € </font></td>
    </tr>
    <tr>
        <td width="20%"><font face="Times New Roman" size="5" color="black">FONDATION</font></td>
        <td width="20%"><font face="Times New Roman" size="5">{{$total * 0.17}} € </font></td>
    </tr>
    <tr>
        <td width="20%"><font face="Times New Roman" size="5" color="black">MACONNERIE</font></td>
        <td width="20%"><font face="Times New Roman" size="5">{{$total * 0.13}} € </font></td>
    </tr>
    <tr>
        <td width="20%"><font face="Times New Roman" size="5" color="black">CHARPENTE</font></td>
        <td width="20%"><font face="Times New Roman" size="5">{{$total * 0.05}} € </font></td>
    </tr>
    <tr>
        <td width="20%"><font face="Times New Roman" size="5" color="black">COUVERTURE</font></td>
        <td width="20%"><font face="Times New Roman" size="5" color="black">{{$total * 0.04}} € </font></td>
    </tr>
    <tr>
        <td width="20%"><font face="Times New Roman" size="5" color="black">ELECTRICITE</font></td>
        <td width="20%"><font face="Times New Roman" size="5" color="black">{{$total * 0.06}} € </font></td>
    </tr>
    <tr>
        <td width="20%"><font face="Times New Roman" size="5" color="black">PLOMBERIE</font></td>
        <td width="20%"><font face="Times New Roman" size="5" color="black">{{$total * 0.04}} € </font></td>
    </tr>
    <tr>
        <td width="20%"><font face="Times New Roman" size="5" color="black">SANITAIRE</font></td>
        <td width="20%"><font face="Times New Roman" size="5" color="black">{{$total * 0.02}} € </font></td>
    </tr>
    <tr>
        <td width="20%"><font face="Times New Roman" size="5" color="black">MURS ET CLOISON</font></td>
        <td width="20%"><font face="Times New Roman" size="5" color="black">{{$total * 0.05}} € </font></td>
    </tr>
    <tr>
        <td width="20%"><font face="Times New Roman" size="5" color="black">PLAFONDS</font></td>
        <td width="20%"><font face="Times New Roman" size="5" color="black">{{$total * 0.03}} €  </font></td>
    </tr>
    <tr>
        <td width="20%"><font face="Times New Roman" size="5" color="black">REVETEMENT SOL ET MUR</font></td>
        <td width="20%"><font face="Times New Roman" size="5" color="black">{{$total * 0.05}} €  </font></td>
    </tr>
    <tr>
        <td width="20%"><font face="Times New Roman" size="5" color="black">CLIMATISATION CHAUFFAGE</font></td>
        <td width="20%"><font face="Times New Roman" size="5" color="black">{{$total * 0.01}} €  </font></td>
    </tr>
    <tr>
        <td width="20%"><font face="Times New Roman" size="5" color="black">MENUISERIE INTERIEURE</font></td>
        <td width="20%"><font face="Times New Roman" size="5" color="black">{{$total * 0.01}} €  </font></td>
    </tr>
    <tr>
        <td width="20%"><font face="Times New Roman" size="5" color="black">MENUISERIE EXTERIEURE</font></td>
        <td width="20%"><font face="Times New Roman" size="5" color="black">{{$total * 0.09}} €  </font></td>
    </tr>
    <tr>
        <td width="20%"><font face="Times New Roman" size="5" color="black">PEINTURE INTERIEURE </font></td>
        <td width="20%"><font face="Times New Roman" size="5" color="black">{{$total * 0.04}} €  </font></td>
    </tr>
    <tr>
        <td width="20%"><font face="Times New Roman" size="5" color="black">PEINTURE EXTERIEURE</font></td>
        <td width="20%"><font face="Times New Roman" size="5" color="black">{{$total * 0.02}} €  </font></td>
    </tr>
    <tr>
        <td width="20%"><font face="Times New Roman" size="5" color="black">CUISINE TYPE CATALOGUE ETB</font></td>
        <td width="20%"><font face="Times New Roman" size="5" color="black">{{$total * 0.06}} €  </font></td>
    </tr>

    <tr>
        <td width="20%"><font face="Times New Roman" bold size="5" color="black">Total</font></td>
        <td width="20%"><font face="Times New Roman" bold size="5" color="black">{{$total}} € </font></td>
    </tr>

</table>


        <p>Ceci est une estimation non contractuelle basée sur une moyenne de construction française via.</p>
        <p> Elle n'inclut pas :</p>
        <p> La conception et le permis de construire ; </p>
         Le terrassement, en supposant que les fondations sont faites sur un terrain ordinaire (pas de pente et la qualité du sol ne nécessite pas de fondation particulière) ;</p>
        <p> Le raccordement ; la </p>
        <p> L’assainissement privatif.</p>
        <p> cuisine</p>
        <p> Nous restons à votre disposition pour effectuer un devis ferme pour votre projet sur www.etb-batiment.com.</p>


</body>
</html>