<br>
<div class="alert alert-success " role="alert">
  <button class="close" data-dismiss="alert"></button>
  {!! $message !!}
</div>