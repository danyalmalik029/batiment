<br>
<div class="alert alert-success " role="alert" style="border-color: #39B54A !important; background: #39B54A; border-radius: 4px; color: white;  padding: 5px; font-weight: 700;">
  <button class="close" data-dismiss="alert"></button>
  <i class="fa fa-thumbs-up fa-2x"></i> {!! $message !!}
</div>