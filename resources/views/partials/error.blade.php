<br>
<div class="alert alert-{{ $type }} " role="alert">
  <button class="close" data-dismiss="alert"></button>
  <i class="fa fa-warning fa-2x"></i> {!! $message !!}
</div>