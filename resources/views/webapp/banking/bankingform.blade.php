@extends("webapp.layouts.default")

@section('content')


    <div class="content-title">
        <div class="content-title-inner">
            <div class="container">
                <h1>@lang('banking.submit_inquiry')</h1>
            </div><!-- /.container -->
        </div><!-- /.content-title-inner -->
    </div>

    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">
                <div class="content">
                    <div class="container maincontent">
                        <div class="col-xs-12">

                            <?php

                            /*
                            @if(isset($editing))
                                <form method="post" id="regForm" action="{{ url('/update-bank-financial') }}" enctype="multipart/form-data">
                                    @else
                                        <form method="post" id="regForm" action="{{ url('/store-bank-financial') }}" enctype="multipart/form-data">
                                            @endif

                            */

                            ?>


                                <form method="post" id="regForm" action="{{ url('/store-bank-financial') }}" enctype="multipart/form-data">

                                     {{ csrf_field() }}

                                    <!--        Validation des Types de Fichiers        -->

                                         @if ($errors->has('cni'))
                                             <span class="has-error"  style="font-weight: bold; color:red;">
                                                 {{ $errors->first('cni') }}
                                             </span>
                                         @endif

                                         @if ($errors->has('livret_de_mariage'))
                                             <span class="has-error"  style="font-weight: bold; color:red;">
                                                {{ $errors->first('livret_de_mariage') }}
                                             </span>
                                         @endif

                                         @if ($errors->has('certificat_marie'))
                                             <span class="has-error"  style="font-weight: bold; color:red;">
                                                {{ $errors->first('certificat_marie') }}
                                             </span>
                                         @endif

                                         @if ($errors->has('facture_eau'))
                                             <span class="has-error"  style="font-weight: bold; color:red;">
                                                {{ $errors->first('facture_eau') }}
                                             </span>
                                         @endif

                                         @if ($errors->has('releves_de_compte'))
                                             <span class="has-error"  style="font-weight: bold; color:red;">
                                                {{ $errors->first('releves_de_compte') }}
                                             </span>
                                         @endif

                                         @if ($errors->has('prets_familiaux'))
                                             <span class="has-error"  style="font-weight: bold; color:red;">
                                                 {{ $errors->first('prets_familiaux') }}
                                             </span>
                                         @endif

                                         @if ($errors->has('donation'))
                                             <span class="has-error"  style="font-weight: bold; color:red;">
                                                {{ $errors->first('donation') }}
                                             </span>
                                         @endif

                                         @if ($errors->has('placements'))
                                             <span class="has-error"  style="font-weight: bold; color:red;">
                                                {{ $errors->first('placements') }}
                                             </span>
                                         @endif

                                         @if ($errors->has('surfaces_habitables_reno'))
                                             <span class="has-error"  style="font-weight: bold; color:red;">
                                                {{ $errors->first('surfaces_habitables_reno') }}
                                             </span>
                                         @endif

                                         @if ($errors->has('dossier_diagnostics_techniques'))
                                             <span class="has-error"  style="font-weight: bold; color:red;">
                                                {{ $errors->first('dossier_diagnostics_techniques') }}
                                             </span>
                                         @endif

                                         @if ($errors->has('passeport'))
                                             <span class="has-error"  style="font-weight: bold; color:red;">
                                                {{ $errors->first('passeport') }}
                                             </span>
                                         @endif

                                         @if ($errors->has('livret_de_famille'))
                                             <span class="has-error"  style="font-weight: bold; color:red;">
                                                {{ $errors->first('livret_de_famille') }}
                                             </span>
                                         @endif

                                         @if ($errors->has('quittances_edf'))
                                             <span class="has-error"  style="font-weight: bold; color:red;">
                                                {{ $errors->first('quittances_edf') }}
                                             </span>
                                         @endif

                                         @if ($errors->has('contrat_de_bail'))
                                             <span class="has-error"  style="font-weight: bold; color:red;">
                                                 {{ $errors->first('contrat_de_bail') }}
                                             </span>
                                         @endif

                                         @if ($errors->has('avis_imposition'))
                                             <span class="has-error"  style="font-weight: bold; color:red;">
                                                {{ $errors->first('avis_imposition') }}
                                             </span>
                                         @endif

                                         @if ($errors->has('epargne_logement'))
                                             <span class="has-error"  style="font-weight: bold; color:red;">
                                                {{ $errors->first('epargne_logement') }}
                                             </span>
                                         @endif

                                         @if ($errors->has('benefice_entreprise'))
                                             <span class="has-error"  style="font-weight: bold; color:red;">
                                                {{ $errors->first('benefice_entreprise') }}
                                             </span>
                                         @endif

                                         @if ($errors->has('contrat_de_construction'))
                                             <span class="has-error"  style="font-weight: bold; color:red;">
                                                {{ $errors->first('contrat_de_construction') }}
                                             </span>
                                         @endif

                                         @if ($errors->has('titre_de_propriete'))
                                             <span class="has-error"  style="font-weight: bold; color:red;">
                                                {{ $errors->first('titre_de_propriete') }}
                                             </span>
                                         @endif


                                     <!--        Fin de la validation et debut du Formulaire     -->

                                     <input type="hidden" rows="8" name="editing" value="{{ old('editing', $editing) }}" id="fname" class="form-control"  oninput="this.className = ''">

                                            <div class="tab"><h2>@lang('banking.identity')</h2><br />

                                                <h5>@lang('banking.informations')</h5>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <p> @lang('banking.civility')</p>
                                                                <p></p>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <p></p>
                                                                <p><input type="radio" name="civility" value="M" <?php if(isset($inquiry->civility) and $inquiry->civility=="M") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''" checked > <label for="">@lang('banking.mister')</label></p>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <p></p>
                                                                <p><input type="radio" name="civility" value="Mme" <?php if(isset($inquiry->civility) and $inquiry->civility=="Mme") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''" > <label for="">@lang('banking.mistress')</label></p>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <p></p>
                                                                <p><input type="radio" name="civility" value="Mlle" <?php if(isset($inquiry->civility) and $inquiry->civility=="Mlle") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''" > <label for="">@lang('banking.miss')</label></p>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <?php
                                                        if(isset($inquiry->email))
                                                            $email= $inquiry->email;
                                                        else
                                                            $email=session('email');
                                                    ?>


                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <p><label for="">@lang('banking.email') </label> <input type="text" rows="8" name="email" value="{{ old('email', $email) }}" id="email" class="form-control"  oninput="this.className = ''"></p>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                                if(isset($inquiry->fname))
                                                    $name= $inquiry->fname;
                                                else
                                                    $name=session('name');
                                                ?>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <p><label for="">@lang('banking.firstname') </label> <input type="text" rows="8" name="fname" value="{{ old('fname', $name) }}" id="fname" class="form-control"  oninput="this.className = ''"></p>
                                                                <p><label for="">@lang('banking.lastname') </label> <input type="text" name="lname" id="lname" value="{{ old('lname', $name) }}" class="form-control"  oninput="this.className = ''"></p>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-sm-12">

                                                                <p>   @lang('banking.instructions') </p>
                                                                <p>   @lang('banking.question') </p>
                                                                <p><input type="radio" name="rep" value="O" <?php if($inquiry->rep=="O") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''" checked> <label for="">@lang('banking.oui')</label>
                                                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <input type="radio" name="rep" value="N" <?php if($inquiry->rep=="N") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''" > <label for="">@lang('banking.non')</label></p>


                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                                if(isset($inquiry->datenais))
                                                    $datenais= $inquiry->datenais;
                                                else
                                                    $datenais=session('birth_date');
                                                ?>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <p><label for="">@lang('banking.datenais') </label> <input type="date" rows="8" name="datenais" value="{{ old('datenais', $datenais) }}" id="datenais" class="form-control"  oninput="this.className = ''"></p>
                                                                <p><label for="">@lang('banking.nationalite') </label> <input type="text" name="nat" id="nat" value="{{ old('nat', $inquiry->nat) }}" class="form-control"  oninput="this.className = ''"></p>


                                                            </div>

                                                        </div>
                                                    </div>
                                                    <?php
                                                    if(isset($inquiry->paysnais))
                                                        $paysnais= $inquiry->paysnais;
                                                    else
                                                        $paysnais=session('country');
                                                    ?>

                                                    <?php

                                                    if(isset($inquiry->vilcp))
                                                        $vilcp= $inquiry->vilcp;
                                                    elseif(session('city'))
                                                        $vilcp=session('city');
                                                    elseif(session('postcode'))
                                                        $vilcp=session('postcode');

                                                    ?>
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <p><label for="">@lang('banking.paysnais') </label> <input type="text" name="paysnais" id="paysnais"  value="{{ old('paysnais', $paysnais) }}" class="form-control"  oninput="this.className = ''"></p>
                                                                <p><label for="">@lang('banking.vilcp') </label> <input type="text" name="vilcp" id="vilcp"  value="{{ old('vilcp', $vilcp) }}" class="form-control"  oninput="this.className = ''"></p>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">


                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <p><label for="">@lang('banking.sitfamil') </label>

                                                                    <?php       /*
                                                                    <input type="text" rows="8" name="sitfamil" value="{{ old('sitfamil', $inquiry->sitfamil) }}" id="sitfamil" class="form-control"  oninput="this.className = ''"></p>
                                                                    */
                                                                ?>

                                                                    <select id="sitfamil" class="c-select form-control boxed" autocomplete="off" name="sitfamil" required>
                                                                        <option value="" selected>@lang('general.general_select_option')...</option>
                                                                        <option value="celibataire" @if(old('sitfamil', $inquiry->sitfamil) == 'celibataire') selected @endif>@lang('banking.celibataire') </option>
                                                                        <option value="marie" @if(old('sitfamil', $inquiry->sitfamil) == 'marie') selected @endif>@lang('banking.marie') </option>
                                                                        <option value="veuf" @if(old('sitfamil', $inquiry->sitfamil) == 'veuf') selected @endif>@lang('banking.veuf') </option>
                                                                        <option value="veuve" @if(old('sitfamil', $inquiry->sitfamil) == 'veuve') selected @endif>@lang('banking.veuve') </option>
                                                                    </select>

                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <p><label for="">@lang('banking.nbenfcharg') </label> <input type="number" name="nbenfcharg" id="nbenfcharg" value="{{ old('nbenfcharg', $inquiry->nbenfcharg) }}" class="form-control"  oninput="this.className = ''"></p>

                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>


                                                <h5>@lang('banking.infoconjoint')</h5>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <p> @lang('banking.civility')</p>
                                                                <p></p>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <p></p>
                                                                <p><input type="radio" name="civilityconj" value="M" <?php if($inquiry->civilityconj=="M") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''" checked> <label for="">@lang('banking.mister')</label></p>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <p></p>
                                                                <p><input type="radio" name="civilityconj" value="Mme" <?php if($inquiry->civilityconj=="Mme") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''" > <label for="">@lang('banking.mistress')</label></p>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <p></p>
                                                                <p><input type="radio" name="civilityconj" value="Mlle" <?php if($inquiry->civilityconj=="Mlle") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''" > <label for="">@lang('banking.miss')</label></p>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <p><label for="">@lang('banking.nationalite') </label> <input type="text" rows="8" name="natconj" value="{{ old('natconj', $inquiry->natconj) }}" id="natconj" class="form-control"  oninput="this.className = ''"></p>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <p><label for="">@lang('banking.firstname') </label> <input type="textarea" rows="8" name="fnameconj" value="{{ old('fnameconj', $inquiry->fnameconj) }}" id="fnameconj" class="form-control"  oninput="this.className = ''"></p>
                                                                <p><label for="">@lang('banking.lastname') </label> <input type="text" name="lnameconj" id="lnameconj" value="{{ old('lnameconj', $inquiry->lnameconj) }}" class="form-control"  oninput="this.className = ''"></p>

                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-sm-12">

                                                                <p><label for="">@lang('banking.paysnais') </label> <input type="text" name="paysnaisconj" id="paysnaisconj"  value="{{ old('paysnaisconj', $inquiry->paysnaisconj) }}" class="form-control"  oninput="this.className = ''"></p>
                                                                <p><label for="">@lang('banking.vilcp') </label> <input type="text" name="vilcpconj" id="vilcpconj"  value="{{ old('vilcpconj', $inquiry->vilcpconj) }}" class="form-control"  oninput="this.className = ''"></p>


                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <p><label for="">@lang('banking.datenais') </label> <input type="date" rows="8" name="datenaisconj" value="{{ old('datenaisconj', $inquiry->datenaisconj) }}" id="datenaisconj" class="form-control"  oninput="this.className = ''"></p>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <p> @lang('banking.emprunteur')</p>
                                                                <p></p>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <p></p>
                                                                <p><input type="radio" name="coemprunt" value="O" <?php if($inquiry->coemprunt=="O") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''" checked> <label for="">@lang('banking.oui')</label></p>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <p></p>
                                                                <p><input type="radio" name="coemprunt" value="N" <?php if($inquiry->coemprunt=="N") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''" > <label for="">@lang('banking.non')</label></p>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                            <!--    Ecran 2     -->

                                            <div class="tab"><h2>@lang('banking.adresse')</h2>

                                                <h5>@lang('banking.votreadresse')</h5>

                                                <div class="row">



                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <p><label for="">@lang('banking.nomvoie') </label> <input type="text" rows="8" name="nomvoie" value="{{ old('nomvoie', $inquiry->nomvoie) }}" id="nomvoie" class="form-control" placeholder=""  oninput="this.className = ''"></p>
                                                            </div>

                                                        </div>
                                                    </div>


                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-sm-5">
                                                                <p> @lang('banking.resprin')</p>
                                                                <p></p>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <p></p>
                                                                <p><input type="radio" name="resprin" value="M" <?php if($inquiry->resprin=="M") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''" checked> <label for="">@lang('banking.maison')</label></p>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <p></p>
                                                                <p><input type="radio" name="resprin" value="A" <?php if($inquiry->resprin=="A") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''" > <label for="">@lang('banking.appart')</label></p>
                                                            </div>


                                                        </div>
                                                    </div>


                                                </div>

                                                <?php
                                                if(isset($inquiry->address))
                                                $address= $inquiry->address;
                                                else
                                                $address=session('address');
                                                ?>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <p><label for="">@lang('banking.adrcompl') </label> <input type="text" name="adrcompl" value="{{ old('adrcompl', $address) }}" id="adrcompl" class="form-control"  oninput="this.className = ''"></p>
                                                                <p><label for="">@lang('banking.vilcpadr') </label> <input type="text" name="vilcpadr" id="vilcpadr"  value="{{ old('vilcpadr', $inquiry->vilcpadr) }}" class="form-control"  oninput="this.className = ''"></p>

                                                                <p><label for="">@lang('banking.telfixe') </label> <input type="text" rows="8" name="telfixe" value="{{ old('telfixe', $inquiry->telfixe) }}" id="telfixe" class="form-control"  oninput="this.className = ''"></p>

                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <p><label for="">@lang('banking.mdhab') </label> <input type="text" name="mdhab" id="mdhab" value="{{ old('mdhab', $inquiry->mdhab) }}" class="form-control"  oninput="this.className = ''"></p>
                                                                <p><label for="">@lang('banking.dateresidence') </label> <input type="date" rows="8" name="dateresidence" value="{{ old('dateresidence', $inquiry->dateresidence) }}" id="dateresidence" class="form-control"  oninput="this.className = ''"></p>
                                                                <p><label for="">@lang('banking.telmobile') </label> <input type="text" rows="8" name="telmobile" value="{{ old('telmobile', $inquiry->telmobile) }}" id="telmobile" class="form-control"  oninput="this.className = ''"></p>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <!--    Ecran 3     -->

                                            <div class="tab"><h2>@lang('banking.situation')</h2>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <h5>@lang('banking.vous')</h5>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <p><label for="">@lang('banking.vtprofession') </label> <input type="text" name="vtprofession" value="{{ old('vtprofession', $inquiry->vtprofession) }}" id="vtprofession" class="form-control"  oninput="this.className = ''"></p>
                                                                <p><label for="">@lang('banking.vtcontrat') </label> <input type="text" name="vtcontrat" id="vtcontrat"  value="{{ old('vtcontrat', $inquiry->vtcontrat) }}" class="form-control"  oninput="this.className = ''"></p>
                                                                <p><label for="">@lang('banking.vtancactivite') </label> <input type="date" rows="8" name="vtancactivite" value="{{ old('vtancactivite', $inquiry->vtancactivite) }}" id="vtancactivite" class="form-control"  oninput="this.className = ''"></p>
                                                                <p><label for="">@lang('banking.vtemployeur') </label> <input type="text" name="vtemployeur" id="vtemployeur"  value="{{ old('vtemployeur', $inquiry->vtemployeur) }}" class="form-control"  oninput="this.className = ''"></p>
                                                                <p><label for="">@lang('banking.vtvilcpemployeur') </label> <input type="text" rows="8" name="vtvilcpemployeur" value="{{ old('vtvilcpemployeur', $inquiry->vtvilcpemployeur) }}" id="vtvilcpemployeur" class="form-control"  oninput="this.className = ''"></p>

                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <h5>@lang('banking.votre_conjoint')</h5>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <p><label for="">@lang('banking.saprofession') </label> <input type="text" name="saprofession" id="saprofession" value="{{ old('saprofession', $inquiry->saprofession) }}" class="form-control"  oninput="this.className = ''"></p>
                                                                <p><label for="">@lang('banking.sncontrat') </label> <input type="text" rows="8" name="sncontrat" value="{{ old('sncontrat', $inquiry->sncontrat) }}" id="sncontrat" class="form-control"  oninput="this.className = ''"></p>
                                                                <p><label for="">@lang('banking.snancactivite') </label> <input type="date" rows="8" name="snancactivite" value="{{ old('snancactivite', $inquiry->snancactivite) }}" id="snancactivite" class="form-control"  oninput="this.className = ''"></p>
                                                                <p><label for="">@lang('banking.snemployeur') </label> <input type="text" rows="8" name="snemployeur" value="{{ old('snemployeur', $inquiry->snemployeur) }}" id="snemployeur" class="form-control"  oninput="this.className = ''"></p>
                                                                <p><label for="">@lang('banking.snvilcpemployeur') </label> <input type="text" rows="8" name="snvilcpemployeur" value="{{ old('snvilcpemployeur', $inquiry->snvilcpemployeur) }}" id="snvilcpemployeur" class="form-control"  oninput="this.className = ''"></p>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>


                                            </div>

                                            <!--    Ecran 4     -->

                                            <div class="tab"><h2>@lang('banking.budget') (&euro;)</h2>

                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <h5>@lang('banking.foyer')</h5>
                                                        <div class="row">
                                                            <div class="col-sm-12">

                                                                <p>
                                                                    <label for="">@lang('banking.vtrevmens')</label>
                                                                    <input type="text" name="vtrevmens" value="{{ old('vtrevmens', $inquiry->vtrevmens) }}" id="vtrevmens" class="form-control"  oninput="this.className = ''"></p>
                                                            </div>
                                                            <div class="">

                                                                <input type="hidden" name="vtperioderev" id="vtperioderev"  value="{{ old('vtperioderev', $inquiry->vtperioderev) }}" class="form-control"  oninput="this.className = ''">
                                                                <input type="hidden" name="saperioderev" id="saperioderev"  value="{{ old('saperioderev', $inquiry->saperioderev) }}" class="form-control"  oninput="this.className = ''">
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <h5>@lang('banking.banque')</h5>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <p>
                                                                    <label for="">@lang('banking.namebanq') </label>
                                                                    <input type="text" name="namebanq" id="namebanq" value="{{ old('namebanq', $inquiry->namebanq) }}" class="form-control"  oninput="this.className = ''">
                                                                </p>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <p>
                                                                <label for="">@lang('banking.snrevmens')</label>
                                                                <input type="text" name="snrevmens" value="{{ old('snrevmens', $inquiry->snrevmens) }}" id="snrevmens" class="form-control"  oninput="this.className = ''">
                                                                </p>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <p>
                                                                    <label for="">@lang('banking.ancbanque') </label>
                                                                    <input type="date" rows="8" name="ancbanque" value="{{ old('ancbanque', $inquiry->ancbanque) }}" id="ancbanque" class="form-control"  oninput="this.className = ''">
                                                                </p>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-sm-12">

                                                                <p>
                                                                    <label for="">@lang('banking.autrerevmens') </label>
                                                                    <input type="text" name="autrerevmens" value="{{ old('autrerevmens', $inquiry->autrerevmens) }}" id="autrerevmens" class="form-control"  oninput="this.className = ''">
                                                                </p>

                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-sm-12">

                                                                <p>
                                                                    <label for="">@lang('banking.vtcartbanc') </label>
                                                                    <select class="c-select form-control boxed" name="vtcartbanc" id="vtcartbanc">
                                                                        <option value="">@lang('general.general_select_option')...</option>
                                                                        <option value="bleu" @if(old('vtcartbanc', $inquiry->vtcartbanc) == "bleu") selected @endif>Bleue</option>
                                                                        <option value="visa_master" @if(old('vtcartbanc', $inquiry->vtcartbanc) == "visa_master") selected @endif>Visa/Mastercard</option>
                                                                        <option value="premier_gold" @if(old('vtcartbanc', $inquiry->vtcartbanc) == "premier_gold") selected @endif>Premier/Gold</option>
                                                                        <option value="sans_carte" @if(old('vtcartbanc', $inquiry->vtcartbanc) == "sans_carte") selected @endif>@lang("banking.nocard")</option>
                                                                    </select>
                                                                </p>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-sm-8">
                                                                <p> @lang('banking.changebanq')</p>
                                                                <p></p>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <p></p>
                                                                <p><input type="radio" name="changbanq" value="O" <?php if($inquiry->changbanq=="O") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''" checked> <label for="">@lang('banking.oui')</label></p>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <p></p>
                                                                <p><input type="radio" name="changbanq" value="N" <?php if($inquiry->changbanq=="N") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''" > <label for="">@lang('banking.non')</label></p>
                                                            </div>


                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <p>
                                                                    <label for="">@lang('banking.fichagebanc') </label>
                                                                    <select class="c-select form-control boxed" name="fichagebanc" id="fichagebanc">
                                                                        <option value="">@lang('general.general_select_option')...</option>
                                                                        <option value="fichagebanc_non" @if(old('fichagebanc', $inquiry->fichagebanc) == "fichagebanc_non") selected @endif>@lang("banking.fichagebanc_non")</option>
                                                                        <option value="fichagebanc_surendettement" @if(old('fichagebanc', $inquiry->fichagebanc) == "fichagebanc_surendettement") selected @endif>@lang("banking.fichagebanc_surendettement")</option>
                                                                        <option value="fichagebanc_incidentompte" @if(old('fichagebanc', $inquiry->fichagebanc) == "fichagebanc_incidentompte") selected @endif>@lang("banking.fichagebanc_incidentompte")</option>
                                                                        <option value="fichagebanc_incidentcredit" @if(old('fichagebanc', $inquiry->fichagebanc) == "fichagebanc_incidentcredit") selected @endif>@lang("banking.fichagebanc_incidentcredit")</option>
                                                                    </select>
                                                                </p>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>

                                                <h5>@lang('banking.chargesfoyer')</h5>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <p><label for="">@lang('banking.loyerresprinc') </label> <input type="text" name="loyerresprinc" value="{{ old('loyerresprinc', $inquiry->loyerresprinc) }}" id="loyerresprinc" class="form-control"  oninput="this.className = ''"></p>
                                                                <p><label for="">@lang('banking.creditconso') </label> <input type="text" name="creditconso" id="creditconso"  value="{{ old('creditconso', $inquiry->creditconso) }}" class="form-control"  oninput="this.className = ''"></p>
                                                                <p><label for="">@lang('banking.pensaliment') </label> <input type="text" name="pensaliment" value="{{ old('pensaliment', $inquiry->pensaliment) }}" id="pensaliment" class="form-control"  oninput="this.className = ''"></p>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-sm-8">
                                                                <p> @lang('banking.regroupcredit')</p>
                                                                <p></p>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <p></p>
                                                                <p><input type="radio" name="regroupcredit" value="O" <?php if($inquiry->regroupcredit=="O") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''" checked> <label for="">@lang('banking.oui')</label></p>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <p></p>
                                                                <p><input type="radio" name="regroupcredit" value="N" <?php if($inquiry->regroupcredit=="N") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''" > <label for="">@lang('banking.non')</label></p>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                            <div class="tab"><h2>@lang('banking.typepropriete')</h2><br />

                                                <div class="row">
                                                    <input type="hidden" id="id" name="id" value="{{ old('id', $inquiry->id) }}" >
                                                    <div class="table-responsive" id="list_de_trucs">
                                                        <table class="tableaupm">
                                                            <tr>
                                                                <td class="td_color"><img class="imagetb" src="{{ asset('assets/webapp/img/maison_contemporaine.jpg') }}" /></td>
                                                                <td><img class="imagetb" src="{{ asset('assets/webapp/img/maison_traditionnelle.jpeg') }}" /></td>
                                                                <td class="td_color"><img class="imagetb" src="{{ asset('assets/webapp/img/maison_en_bois.jpg') }}" /></td>
                                                                <td><img class="imagetb" src="{{ asset('assets/webapp/img/maison_ecologique.jpg') }}" /></td>
                                                                <td class="td_color"><img class="imagetb" src="{{ asset('assets/webapp/img/maison_plain_pieds.jpg') }}" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_color"><p>@lang('general.permit_submit_house_cont')  <br /> @lang('general.permit_submit_house_cont_text')   </p></td>
                                                                <td><p>@lang('general.permit_submit_house_trad')  <br /> @lang('general.permit_submit_house_trad_text') </p></td>
                                                                <td class="td_color"><p>@lang('general.permit_submit_house_bois')  <br /> @lang('general.permit_submit_house_bois_text') </p></td>
                                                                <td><p>@lang('general.permit_submit_house_eco') <br /> @lang('general.permit_submit_house_eco_text')  </p></td>
                                                                <td class="td_color"><p>@lang('general.permit_submit_house_plai') <br /> @lang('general.permit_submit_house_plai_text') </p></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_color"> <p> <label for="house_const"> @lang('general.permit_submit_house_cont') </label> <br /><input type="radio" name="house" id="house_const" value="Maison contemporaine" <?php if(isset($inquiry->house) and $inquiry->house=="Maison contemporaine") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''" ></p></td>
                                                                <td> <p> <label for="house_trad">@lang('general.permit_submit_house_trad') </label><br /> <input type="radio" name="house" id="house_trad" value="Maison traditionnelle" <?php if(isset($inquiry->house) and $inquiry->house=="Maison traditionnelle") {?> checked <?php }  ?> class="form-control" oninput="this.className = ''"></p></td>
                                                                <td class="td_color"> <p> <label for="house_bois">@lang('general.permit_submit_house_bois') </label><br /> <input type="radio" name="house" id="house_bois" value="Maison ossature bois" class="form-control" <?php if(isset($inquiry->house) and $inquiry->house=="Maison ossature bois") {?> checked <?php }  ?> oninput="this.className = ''"></p></td>
                                                                <td> <p> <label for="house_eco">@lang('general.permit_submit_house_eco') </label> <br /><input type="radio" name="house" id="house_eco"  value="Maison écologique" class="form-control" <?php if(isset($inquiry->house) and $inquiry->house=="Maison écologique") {?> checked <?php }  ?> oninput="this.className = ''"></p></td>
                                                                <td class="td_color"> <p> <label for="house_plai">@lang('general.permit_submit_house_plai') </label> <br /><input type="radio" name="house" id="house_plai" value="Maison de plain-pieds" class="form-control" <?php if(isset($inquiry->house) and $inquiry->house=="Maison de plain-pieds") {?> checked <?php }  ?> oninput="this.className = ''"></p></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab"><h2>@lang('banking.pieces')</h2>

                                                <h5>@lang('banking.notespieces')</h5>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-sm-12">

    @if(isset($files['cni']))
        <?php
        $thumbSrc_cni = (is_file($files['cni']))?
        asset($files['cni']):
        asset('');
        $name_cni = explode('/', $thumbSrc_cni);
        ?>
    @endif


    @if(isset($files['livret_de_mariage']))
        <?php
        $thumbSrc_livret_de_mariage = (is_file($files['livret_de_mariage']))?
        asset($files['livret_de_mariage']):
        asset('');
        $name_livret_de_mariage = explode('/', $thumbSrc_livret_de_mariage);
        ?>
    @endif


    @if(isset($files['certificat_marie']))
        <?php
        $thumbSrc_certificat_marie = (is_file($files['certificat_marie']))?
        asset($files['certificat_marie']):
        asset('');
        $name_certificat_marie = explode('/', $thumbSrc_certificat_marie);
        ?>
    @endif


    @if(isset($files['facture_eau']))
        <?php
        $thumbSrc_facture_eau = (is_file($files['facture_eau']))?
        asset($files['facture_eau']):
        asset('');
        $name_facture_eau = explode('/', $thumbSrc_facture_eau);
        ?>
    @endif


    @if(isset($files['releves_de_compte']))
        <?php
        $thumbSrc_releves_de_compte = (is_file($files['releves_de_compte']))?
        asset($files['releves_de_compte']):
        asset('');
        $name_releves_de_compte = explode('/', $thumbSrc_releves_de_compte);
        ?>
    @endif


    @if(isset($files['prets_familiaux']))
        <?php
        $thumbSrc_prets_familiaux = (is_file($files['prets_familiaux']))?
        asset($files['prets_familiaux']):
        asset('');
        $name_prets_familiaux = explode('/', $thumbSrc_prets_familiaux);
        ?>
    @endif


    @if(isset($files['donation']))
        <?php
        $thumbSrc_donation = (is_file($files['donation']))?
        asset($files['donation']):
        asset('');
        $name_donation = explode('/', $thumbSrc_donation);
        ?>
    @endif


    @if(isset($files['placements']))
        <?php
        $thumbSrc_placements = (is_file($files['placements']))?
        asset($files['placements']):
        asset('');
        $name_placements = explode('/', $thumbSrc_placements);
        ?>
    @endif


    @if(isset($files['surfaces_habitables_reno']))
        <?php
        $thumbSrc_surfaces_habitables_reno = (is_file($files['surfaces_habitables_reno']))?
        asset($files['surfaces_habitables_reno']):
        asset('');
        $name_surfaces_habitables_reno = explode('/', $thumbSrc_surfaces_habitables_reno);
        ?>
    @endif


    @if(isset($files['dossier_diagnostics_techniques']))
        <?php
        $thumbSrc_dossier_diagnostics_techniques = (is_file($files['dossier_diagnostics_techniques']))?
        asset($files['dossier_diagnostics_techniques']):
        asset('');
        $name_dossier_diagnostics_techniques = explode('/', $thumbSrc_dossier_diagnostics_techniques);
        ?>
    @endif


    @if(isset($files['passeport']))
        <?php
        $thumbSrc_passeport = (is_file($files['passeport']))?
        asset($files['passeport']):
        asset('');
        $name_passeport = explode('/', $thumbSrc_passeport);
        ?>
    @endif


    @if(isset($files['livret_de_famille']))
        <?php
        $thumbSrc_livret_de_famille = (is_file($files['livret_de_famille']))?
        asset($files['livret_de_famille']):
        asset('');
        $name_livret_de_famille = explode('/', $thumbSrc_livret_de_famille);
        ?>
    @endif


    @if(isset($files['quittances_edf']))
        <?php
        $thumbSrc_quittances_edf = (is_file($files['quittances_edf']))?
        asset($files['quittances_edf']):
        asset('');
        $name_quittances_edf = explode('/', $thumbSrc_quittances_edf);
        ?>
    @endif


    @if(isset($files['contrat_de_bail']))
        <?php
        $thumbSrc_contrat_de_bail = (is_file($files['contrat_de_bail']))?
        asset($files['contrat_de_bail']):
        asset('');
        $name_contrat_de_bail = explode('/', $thumbSrc_contrat_de_bail);
        ?>
    @endif


    @if(isset($files['avis_imposition']))
        <?php
        $thumbSrc_avis_imposition = (is_file($files['avis_imposition']))?
        asset($files['avis_imposition']):
        asset('');
        $name_avis_imposition = explode('/', $thumbSrc_avis_imposition);
        ?>
    @endif


    @if(isset($files['epargne_logement']))
        <?php
        $thumbSrc_epargne_logement = (is_file($files['epargne_logement']))?
        asset($files['epargne_logement']):
        asset('');
        $name_epargne_logement = explode('/', $thumbSrc_epargne_logement);
        ?>
    @endif


    @if(isset($files['benefice_entreprise']))
        <?php
        $thumbSrc_benefice_entreprise = (is_file($files['benefice_entreprise']))?
        asset($files['benefice_entreprise']):
        asset('');
        $name_benefice_entreprise = explode('/', $thumbSrc_benefice_entreprise);
        ?>
    @endif


    @if(isset($files['contrat_de_construction']))
        <?php
        $thumbSrc_contrat_de_construction = (is_file($files['contrat_de_construction']))?
        asset($files['contrat_de_construction']):
        asset('');
        $name_contrat_de_construction = explode('/', $thumbSrc_contrat_de_construction);
        ?>
    @endif


    @if(isset($files['titre_de_propriete']))
        <?php
        $thumbSrc_titre_de_propriete = (is_file($files['titre_de_propriete']))?
        asset($files['titre_de_propriete']):
        asset('');
        $name_titre_de_propriete = explode('/', $thumbSrc_titre_de_propriete);
        ?>
    @endif



    <p>  <label for="">@lang('banking.cni')</label>@if(isset($files['cni']) and isset($name_cni[9]))<a href="#" >{{  $name_passeport[9]    }}</a>  <input type="hidden" name="cni1"  @if(isset($files['cni'])) value="{{ old('cni', $files['cni']) }} " @endif class="form-control">@endif<input type="file" name="cni"  class="form-control"></p>
@if ($errors->has('cni'))
            <span class="has-error"  style="font-weight: bold; color:red;">
                {{ $errors->first('cni') }}
            </span>
        @endif
        {{--    <p>  <label for="">@lang('banking.wedding_booklet')</label>  @if(isset($files['livret_de_mariage']) and isset($name_livret_de_mariage[6]))<a href="{{  $thumbSrc_livret_de_mariage  }}" target="_blank">{{  $name_livret_de_mariage[6]    }}</a>  <input type="hidden" name="livret_de_mariage"  @if(isset($files['livret_de_mariage'])) value="{{ old('livret_de_mariage', $files['livret_de_mariage']) }} " @endif class="form-control">@endif<input type="file" name="livret_de_mariage"  class="form-control"></p>--}}
    <p>  <label for="">@lang('banking.marriage_certificate')</label>@if(isset($files['certificat_marie']) and isset($name_certificat_marie[9]))<a href="#" >{{  $name_certificat_marie[9]    }}</a>  <input type="hidden" name="certificat_marie1"  @if(isset($files['certificat_marie'])) value="{{old('certificat_marie', $name_certificat_marie[9])}} " @endif class="form-control">@endif<input type="file" name="certificat_marie"  class="form-control"></p>
        @if ($errors->has('certificat_marie'))
            <span class="has-error"  style="font-weight: bold; color:red;">
                {{ $errors->first('certificat_marie') }}
            </span>
        @endif
        <p>  <label for="">@lang('banking.water_bill')</label>@if(isset($files['facture_eau']) and isset($name_facture_eau[9]))<a href="#"  >{{  $name_facture_eau[9]    }}</a>  <input type="hidden" name="facture_eau1"  @if(isset($files['facture_eau'])) value="{{ old('facture_eau', $files['facture_eau']) }} " @endif class="form-control">@endif<input type="file" name="facture_eau"  class="form-control"></p>
        @if ($errors->has('facture_eau'))
            <span class="has-error"  style="font-weight: bold; color:red;">
                {{ $errors->first('facture_eau') }}
            </span>
        @endif
        <p>  <label for="">@lang('banking.account_statements') </label>@if(isset($files['releves_de_compte']) and isset($name_releves_de_compte[9]))<a href="#" >{{  $name_releves_de_compte[9]    }}</a>  <input type="hidden" name="releves_de_compte1"  @if(isset($files['releves_de_compte'])) value="{{ old('releves_de_compte', $files['releves_de_compte']) }} " @endif class="form-control">@endif<input type="file" name="releves_de_compte"  class="form-control"></p>
        @if ($errors->has('releves_de_compte'))
            <span class="has-error"  style="font-weight: bold; color:red;">
                {{ $errors->first('releves_de_compte') }}
            </span>
        @endif
        <p>  <label for="">@lang('banking.family_loans')</label>@if(isset($files['prets_familiaux']) and isset($name_prets_familiaux[9]))<a href="#"  >{{  $name_prets_familiaux[9]    }}</a>  <input type="hidden" name="prets_familiaux1"  @if(isset($files['prets_familiaux'])) value="{{ old('prets_familiaux', $files['prets_familiaux']) }} " @endif class="form-control">@endif<input type="file" name="prets_familiaux"  class="form-control"></p>
        @if ($errors->has('prets_familiaux'))
            <span class="has-error"  style="font-weight: bold; color:red;">
                {{ $errors->first('prets_familiaux') }}
            </span>
        @endif
        <p>  <label for="">@lang('banking.donation')</label>@if(isset($files['donation']) and isset($name_donation[9]))<a href="#" >{{  $name_donation[9]    }}</a>  <input type="hidden" name="donation1"  @if(isset($files['donation'])) value="{{ old('donation', $files['donation']) }} " @endif class="form-control">@endif<input type="file" name="donation"  class="form-control"></p>
        @if ($errors->has('donation'))
            <span class="has-error"  style="font-weight: bold; color:red;">
                {{ $errors->first('donation') }}
            </span>
        @endif
        <p>  <label for="">@lang('banking.investments')</label>@if(isset($files['placements']) and isset($name_placements[9]))<a href="#"  >{{  $name_placements[9]    }}</a>  <input type="hidden" name="placements1"  @if(isset($files['placements'])) value="{{ old('placements', $files['placements']) }} " @endif class="form-control">@endif<input type="file" name="placements"  class="form-control"></p>
        @if ($errors->has('placements'))
            <span class="has-error"  style="font-weight: bold; color:red;">
                {{ $errors->first('placements') }}
            </span>
        @endif
        <p>  <label for="">@lang('banking.reno_living')</label>@if(isset($files['surfaces_habitables_reno']) and isset($name_surfaces_habitables_reno[9]))<a href="#" >{{  $name_surfaces_habitables_reno[9]    }}</a>  <input type="hidden" name="surfaces_habitables_reno1"  @if(isset($files['surfaces_habitables_reno'])) value="{{ old('surfaces_habitables_reno', $files['surfaces_habitables_reno']) }} " @endif class="form-control">@endif<input type="file" name="surfaces_habitables_reno"  class="form-control"></p>
        @if ($errors->has('surfaces_habitables_reno'))
            <span class="has-error"  style="font-weight: bold; color:red;">
                {{ $errors->first('surfaces_habitables_reno') }}
            </span>
        @endif
        <p>  <label for="">@lang('banking.folder_technical')</label>@if(isset($files['dossier_diagnostics_techniques']) and isset($name_dossier_diagnostics_techniques[6]))<a href="#" >{{  $name_dossier_diagnostics_techniques[9]    }}</a>  <input type="hidden" name="dossier_diagnostics_techniques1"  @if(isset($files['dossier_diagnostics_techniques'])) value="{{ old('dossier_diagnostics_techniques', $files['dossier_diagnostics_techniques']) }} " @endif class="form-control">@endif<input type="file" name="dossier_diagnostics_techniques"  class="form-control"></p>
        @if ($errors->has('dossier_diagnostics_techniques'))
            <span class="has-error"  style="font-weight: bold; color:red;">
                {{ $errors->first('dossier_diagnostics_techniques') }}
            </span>
        @endif

                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-sm-12">

                                                                <p>  <label for="">@lang('banking.passport')</label>@if(isset($files['passeport']) and isset($name_passeport[9]))<a href="#" >{{  $name_passeport[9]    }}</a>  <input type="hidden" name="passeport1"  @if(isset($files['passeport'])) value="{{ old('passeport', $files['passeport']) }} " @endif class="form-control">@endif<input type="file" name="passeport"  class="form-control"></p>
                                                                @if ($errors->has('passeport'))
                                                                    <span class="has-error"  style="font-weight: bold; color:red;">
                                                                        {{ $errors->first('passeport') }}
                                                                    </span>
                                                                @endif
                                                                <p> <label for="">@lang('banking.family_record_book')</label>@if(isset($files['livret_de_famille']) and isset($name_livret_de_famille[9]))<a href="#" >{{  $name_livret_de_famille[9]    }}</a>  <input type="hidden" name="livret_de_famille1"  @if(isset($files['livret_de_famille'])) value="{{ old('livret_de_famille', $files['livret_de_famille']) }} " @endif class="form-control">@endif<input type="file" name="livret_de_famille"  class="form-control"></p>
                                                                @if ($errors->has('livret_de_famille'))
                                                                    <span class="has-error"  style="font-weight: bold; color:red;">
                                                                        {{ $errors->first('livret_de_famille') }}
                                                                    </span>
                                                                @endif
                                                                <p>  <label for="">@lang('banking.edf_receipts')</label> @if(isset($files['quittances_edf']) and isset($name_quittances_edf[9]))<a href="#" >{{  $name_quittances_edf[9]    }}</a>  <input type="hidden" name="quittances_edf1"  @if(isset($files['quittances_edf'])) value="{{ old('quittances_edf', $files['quittances_edf']) }} " @endif class="form-control">@endif<input type="file" name="quittances_edf"  class="form-control"></p>
                                                                @if ($errors->has('quittances_edf'))
                                                                    <span class="has-error"  style="font-weight: bold; color:red;">
                                                                        {{ $errors->first('quittances_edf') }}
                                                                    </span>
                                                                @endif
                                                                <p>  <label for="">@lang('banking.lease_contract')</label>@if(isset($files['contrat_de_bail']) and isset($name_contrat_de_bail[9]))<a href="#"  >{{  $name_contrat_de_bail[9]    }}</a>  <input type="hidden" name="contrat_de_bail1"  @if(isset($files['contrat_de_bail'])) value="{{ old('contrat_de_bail', $files['contrat_de_bail']) }} " @endif class="form-control">@endif<input type="file" name="contrat_de_bail"  class="form-control"></p>
                                                                @if ($errors->has('contrat_de_bail'))
                                                                    <span class="has-error"  style="font-weight: bold; color:red;">
                                                                        {{ $errors->first('contrat_de_bail') }}
                                                                    </span>
                                                                @endif
                                                                <p>  <label for="">@lang('banking.imposition_reviews') </label>@if(isset($files['avis_imposition']) and isset($name_avis_imposition[9]))<a href="#"  >{{  $name_avis_imposition[9]    }}</a>  <input type="hidden" name="avis_imposition1"  @if(isset($files['avis_imposition'])) value="{{ old('avis_imposition', $files['avis_imposition']) }} " @endif class="form-control">@endif<input type="file" name="avis_imposition"  class="form-control"></p>
                                                                @if ($errors->has('avis_imposition'))
                                                                    <span class="has-error"  style="font-weight: bold; color:red;">
                                                                        {{ $errors->first('avis_imposition') }}
                                                                    </span>
                                                                @endif
                                                                <p>  <label for="">@lang('banking.housing_savings')</label>@if(isset($files['epargne_logement']) and isset($name_epargne_logement[9]))<a href="#" target="_blank">{{  $name_epargne_logement[9]    }}</a>  <input type="hidden" name="epargne_logement1"  @if(isset($files['epargne_logement'])) value="{{ old('epargne_logement', $files['epargne_logement']) }} " @endif class="form-control">@endif<input type="file" name="epargne_logement"  class="form-control"></p>
                                                                @if ($errors->has('epargne_logement'))
                                                                    <span class="has-error"  style="font-weight: bold; color:red;">
                                                                        {{ $errors->first('epargne_logement') }}
                                                                    </span>
                                                                @endif
                                                                <p>  <label for="">@lang('banking.benefice_company')</label>@if(isset($files['benefice_entreprise']) and isset($name_benefice_entreprise[9]))<a href="#"  >{{  $name_benefice_entreprise[9]    }}</a>  <input type="hidden" name="benefice_entreprise1"  @if(isset($files['benefice_entreprise'])) value="{{ old('benefice_entreprise', $files['benefice_entreprise']) }} " @endif class="form-control">@endif<input type="file" name="benefice_entreprise"  class="form-control"></p>
                                                                @if ($errors->has('benefice_entreprise'))
                                                                    <span class="has-error"  style="font-weight: bold; color:red;">
                                                                        {{ $errors->first('benefice_entreprise') }}
                                                                    </span>
                                                                @endif
                                                                <p>  <label for="">@lang('banking.construction_contract') </label>@if(isset($files['contrat_de_construction']) and isset($name_contrat_de_construction[9]))<a href="#"  >{{  $name_contrat_de_construction[9]    }}</a>  <input type="hidden" name="contrat_de_construction1"  @if(isset($files['contrat_de_construction'])) value="{{ old('contrat_de_construction', $files['contrat_de_construction']) }} " @endif class="form-control">@endif<input type="file" name="contrat_de_construction"  class="form-control"></p>
                                                                @if ($errors->has('contrat_de_construction'))
                                                                    <span class="has-error"  style="font-weight: bold; color:red;">
                                                                        {{ $errors->first('contrat_de_construction') }}
                                                                    </span>
                                                                @endif
                                                                <p>  <label for="">@lang('banking.property_title') </label> @if(isset($files['titre_de_propriete']) and isset($name_titre_de_propriete[9]))<a href="#"  >{{  $name_titre_de_propriete[9]    }}</a>  <input type="hidden" name="titre_de_propriete1"  @if(isset($files['titre_de_propriete'])) value="{{ old('titre_de_propriete', $files['titre_de_propriete']) }} " @endif class="form-control">@endif<input type="file" name="titre_de_propriete"  class="form-control"></p>
                                                                @if ($errors->has('titre_de_propriete'))
                                                                    <span class="has-error"  style="font-weight: bold; color:red;">
                                                                        {{ $errors->first('titre_de_propriete') }}
                                                                    </span>
                                                                @endif
                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>

                                            </div>

                                            <div style="overflow:auto;">
                                                <div style="float:right;">
                                                    <button type="button" id="prevBtn" class="btn btn-primary" onclick="nextPrev(-1)">@lang('general.permit_submit_bt_previous')</button>
                                                    <button type="button" id="nextBtn"  class="btn btn-primary"onclick="nextPrev(1)">@lang('general.permit_submit_bt_next')</button>
                                                </div>
                                            </div>

                                            <!-- Circles which indicates the steps of the form: -->
                                            <div style="text-align:center;margin-top:40px;">
                                                <span class="step"></span>
                                                <span class="step"></span>
                                                <span class="step"></span>
                                                <span class="step"></span>
                                                <span class="step"></span>
                                                <span class="step"></span>

                                            </div>
                                        </form>
                        </div>

                    </div>
                </div>
            </div><!-- /.main-inner -->
        </div><!-- /.main -->
    </div><!-- /.main-wrapper -->

    @include("webapp.includes.call-to-action-footer")

@endsection


@section('script')
<script type="text/javascript" src="{{ asset('assets/webapp/js/buildingPermit.js') }}"></script>
@endsection