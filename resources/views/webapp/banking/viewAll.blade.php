@extends("webapp.layouts.default")

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">

@endsection

@section('content')

	<div class="main-wrapper">
	    <div class="main">
	        <div class="main-inner">
	        		        
					<div class="content-title">
						<div class="content-title-inner">
							<div class="container">		
								<h1>{{$title}}</h1>
							</div><!-- /.container -->
						</div><!-- /.content-title-inner -->
					</div><!-- /.content-title -->
				
				<br>
	            <div class="content">
	                <div class="container">

                        <div class="col-xs-12">
                            <form method="post" action="{{ url('userprofile') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <fieldset>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <input type="hidden" name="iduser" id="iduser">
                                                    <div class="form-group ">
                                                        <label for="email">@lang('profile.email')</label>
                                                        <input type="text" id="emailcantchange" name="emailcantchange" class="form-control" value="{{ session('email') }}" disabled="disabled">
                                                    </div><!-- /.form-group -->

                                                    <div class="form-group @if($errors->has('name')) error @endif">
                                                        <label for="">@lang('profile.name')</label>
                                                        <input type="text" id="name" name="name" class="form-control" value="{{ session('name')}}" disabled="disabled">
                                                    </div><!-- /.form-group -->

                                                    <div class="form-group @if($errors->has('phone_1')) error @endif">
                                                        <label for="">@lang('profile.phone1')</label>
                                                        <input type="text" id="phone_1" name="phone_1" class="form-control" value="{{ session('phone_1') }}" disabled="disabled">

                                                    </div><!-- /.form-group -->
                                                    <div class="form-group @if($errors->has('phone_2')) error @endif">
                                                        <label for="">@lang('profile.phone2')</label>
                                                        <input type="text" id="phone_1" name="phone_1" class="form-control" value="{{ session('phone_2')}}" disabled="disabled">
                                                    </div><!-- /.form-group -->
                                                    <div class="form-group">
                                                        <label for=""><a href="{{ url('userprofile') }}">@lang('profile.update_profile_link')</a></label>
                                                    </div><!-- /.form-group -->
                                                </div><!-- /.col-* -->

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="">@lang('profile.gender')</label>
                                                        <input type="text" id="phone_1" name="phone_1" class="form-control" value="{{ session('gender')}}" disabled="disabled">
                                                    </div><!-- /.form-group -->

                                                    <div class="form-group">
                                                        <label for="">@lang('general.building_permit_reserved_admin')</label>
                                                        <textarea name="comment" class="form-control" rows="10"  disabled="disabled">{{ old('comment', $inquiries->comments) }}</textarea>
                                                    </div><!-- /.form-group -->
                                                </div><!-- /.col-* -->
                                            </div><!-- /.row -->
                                            <!-- <div class="center"> -->
                                            <!-- <div class="form-group-btn"> -->
                                        <!-- <button type="submit" class="btn btn-primary">@lang('profile.update_profile')</button> -->
                                            <!-- </div>/.form-group-btn -->
                                            <!-- </div>/.center -->

                                        </div><!-- /.col-* -->
                                    </div><!-- /.row -->
                                </fieldset>
                            </form>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-md-18 col-lg-18">
                                <div>
                                    {!! \ContentHook::get_content(trans('financial_header_slug')) !!}
                                </div>
                                <article>
                                    <a href="{{ url('/bank-financial-form') }}" class="btn btn-primary center-block">@lang('banking.create')</a>
                                </article>
                            </div>
                        </div>

                        <div style="margin-top: 30px;"></div>
						<div class="row">		
							<div class="col-md-12">

                                @if($sucess != "null")

                                    @include('partials.alert',['type'=>'error','message'=>$sucess])

                                @endif

                                @if($error != "null")

                                    @include('partials.error',['type'=>'error','message'=>$error])

                                @endif
                                    <ol>
                                        @foreach($inquiries as $inquiry)
                                            @if(isset($inquiry->metadata))
                                            <?php
                                            $files = json_decode($inquiry->metadata, true); ?>
                                            <li>
                                                Files submitted:
                                                <ul>
                                                    @foreach($files as $key=>$file)
                                                    <li>@lang('banking.file_'.$key): $file</li>
                                                    @endforeach
                                                </ul>
                                            </li>
                                            @endif
                                        @endforeach
                                    </ol>

							</div><!-- /.col-sm-* -->
						</div><!-- /.row -->
					</div><!-- /.container -->
	            </div><!-- /.content -->
	        </div><!-- /.main-inner -->
	    </div><!-- /.main -->
    </div><!-- /.main-wrapper -->

@include("webapp.includes.call-to-action-footer")

@endsection

@section('script')

<script type="text/javascript">

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $(".disableProperty").click(function(){

        var id = $(this).attr("value");
        var name = $(this).attr("name");

        swal({
                title: "Are you sure?",
                text: "The property : " + name + " will be disable. It will not be visible for the other users.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#39B54A",
                confirmButtonText: "Yes, disable it!",
                closeOnConfirm: false
            },
            function(){
                $.ajax({
                    type: 'POST',
                    cache:false,
                    headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
                    url: 'api/classified/put',
                    data: "id=" + id,
                    dataType: 'json',

                    success : function(data, statut){
                        //console.log(data.statut);
                        if(data.statut == "done")
                            swal({
                                    title: "Disabled!",
                                    text: "Your property : " + name + " has been disabled.",
                                    type: "success",
                                    showCancelButton: false,
                                    confirmButtonColor: "#39B54A",
                                    confirmButtonText: "OK",
                                    closeOnConfirm: false
                                },
                                function(){
                                    location.reload();
                                });
                        //swal("Disabled!", "Your property : " + $(this).attr("name") + " has been disabled.", "success");
                    },

                    error : function(resultat, statut, erreur){
                        swal("Warning!", "Sorry. An error occured while disabling : " + name + ". Please retry later", "error");
                    }
                });


            });

        return false;
    });

    $(".deleteProperty").click(function(){

        var id = $(this).attr("value");
        var name = $(this).attr("name");

        swal({
                title: "Are you sure?",
                text: "The property : " + name + " will be delete.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#39B54A",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function(){
                $.ajax({
                    type: 'POST',
                    cache:false,
                    headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
                    url: 'api/classified/delete',
                    data: "id=" + id,
                    dataType: 'json',

                    success : function(data, statut){
                        //console.log(data.statut);
                        if(data.statut == "done")
                            swal({
                                    title: "Deleted!",
                                    text: "Your property : " + name + " has been deleted.",
                                    type: "success",
                                    showCancelButton: false,
                                    confirmButtonColor: "#39B54A",
                                    confirmButtonText: "OK",
                                    closeOnConfirm: false
                                },
                                function(){
                                    location.reload();
                                });
                        //swal("Disabled!", "Your property : " + $(this).attr("name") + " has been disabled.", "success");
                    },

                    error : function(resultat, statut, erreur){
                        swal("Warning!", "Sorry. An error occured while deleting : " + name + ". Please retry later", "error");
                    }
                });


            });

        return false;
    });

    $(".enableProperty").click(function(){

        var id = $(this).attr("value");
        var name = $(this).attr("name");

        swal({
                title: "Are you sure?",
                text: "The property : " + name + " will be visible for all users.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#39B54A",
                confirmButtonText: "Yes, enable it!",
                closeOnConfirm: false
            },
            function(){
                $.ajax({
                    type: 'POST',
                    cache:false,
                    headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
                    url: 'api/classified/enable',
                    data: "id=" + id,
                    dataType: 'json',

                    success : function(data, statut){
                        //console.log(data.statut);
                        if(data.statut == "done")
                            swal({
                                    title: "Enabled!",
                                    text: "Your property : " + name + " has been enabled.",
                                    type: "success",
                                    showCancelButton: false,
                                    confirmButtonColor: "#39B54A",
                                    confirmButtonText: "OK",
                                    closeOnConfirm: false
                                },
                                function(){
                                    location.reload();
                                });
                        //swal("Disabled!", "Your property : " + $(this).attr("name") + " has been disabled.", "success");
                    },

                    error : function(resultat, statut, erreur){
                        swal("Warning!", "Sorry. An error occured while enabling : " + name + ". Please retry later", "error");
                    }
                });


            });

        return false;
    });

    $(".compareProperty").click(function(event){

        event.preventDefault();

        var id = $(this).attr("value");
        var name = $(this).attr("name");
        console.log(id);

        swal({
                title: "Add to compare",
                text: "Add '" + name + "' to comparison grid?",
                type: "info",
                showCancelButton: true,
                confirmButtonText: "Yes, add!",
                closeOnConfirm: false
            },

            function(){

                $.ajax({
                    url: 'api/classified/addCompareProperty',
                    type: 'POST',
                    data: {_token: CSRF_TOKEN, id: id},
                    dataType: 'JSON',
                    success: function (data) {

                        console.log(data);

                        location.reload();

                        //swal("Added!", "The property.", "success");
                    },

                    error : function(resultat, statut, erreur){
                        swal("Warning!", "Sorry. An error occured while enabling : " + name + ". Please retry later", "error");
                    }
                });
            });

    });
</script>

@endsection

