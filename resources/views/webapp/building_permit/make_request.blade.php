@extends("webapp.layouts.default")

@section('content')


    <div class="content-title">
        <div class="content-title-inner">
            <div class="container">
                <h1>{{ (isset($content['title']))?$content['title']:"ETB Batiment: " . trans("general.menu_building_permit") }}</h1>
            </div><!-- /.container -->
        </div><!-- /.content-title-inner -->
    </div>

    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">
                <div class="content">
                    <div class="container maincontent">
                        <div class="col-xs-12">
                            <form method="post" action="{{ url('userprofile') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <fieldset>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <input type="hidden" name="iduser" id="iduser">
                                                    <div class="form-group ">
                                                        <label for="email">@lang('profile.email')</label>
                                                        <input type="text" id="emailcantchange" name="emailcantchange" class="form-control" value="{{ session('email') }}" disabled="disabled">
                                                    </div><!-- /.form-group -->

                                                    <div class="form-group @if($errors->has('name')) error @endif">
                                                        <label for="">@lang('profile.name')</label>
                                                        <input type="text" id="name" name="name" class="form-control" value="{{ session('name')}}" disabled="disabled">
                                                    </div><!-- /.form-group -->

                                                    <div class="form-group @if($errors->has('phone_1')) error @endif">
                                                        <label for="">@lang('profile.phone1')</label>
                                                        <input type="text" id="phone_1" name="phone_1" class="form-control" value="{{ session('phone_1') }}" disabled="disabled">
                                                    
                                                    </div><!-- /.form-group -->
                                                    <div class="form-group @if($errors->has('phone_2')) error @endif">
                                                        <label for="">@lang('profile.phone2')</label>
                                                        <input type="text" id="phone_1" name="phone_1" class="form-control" value="{{ session('phone_2')}}" disabled="disabled">
                                                    </div><!-- /.form-group -->
                                                    <div class="form-group">
                                                        <label for=""><a href="{{ url('userprofile') }}">@lang('profile.update_profile_link')</a></label>
                                                    </div><!-- /.form-group -->
                                                </div><!-- /.col-* -->

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="">@lang('profile.gender')</label>
                                                        <input type="text" id="phone_1" name="phone_1" class="form-control" value="{{ session('gender')}}" disabled="disabled">
                                                    </div><!-- /.form-group -->

                                                    <div class="form-group">
                                                            <label for="">@lang('general.building_permit_reserved_admin')</label>
                                                            <textarea name="comment" class="form-control" rows="10"  disabled="disabled">{{ old('comment', $building->comments) }}</textarea>
                                                    </div><!-- /.form-group -->
                                                </div><!-- /.col-* -->
                                            </div><!-- /.row -->
                                            <!-- <div class="center"> -->
                                                <!-- <div class="form-group-btn"> -->
                                                    <!-- <button type="submit" class="btn btn-primary">@lang('profile.update_profile')</button> -->
                                                <!-- </div>/.form-group-btn -->
                                            <!-- </div>/.center -->

                                        </div><!-- /.col-* -->
                                    </div><!-- /.row -->
                                </fieldset>
                            </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-18 col-lg-18">
                                <article>
                                <a href="{{ url('/permit-request') }}" class="btn btn-primary center-block">@lang('general.building_permit_apply_now')</a>
                                </article>                             
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div><!-- /.main-inner -->
        </div><!-- /.main -->
    </div><!-- /.main-wrapper -->

    @include("webapp.includes.call-to-action-footer")

@endsection


