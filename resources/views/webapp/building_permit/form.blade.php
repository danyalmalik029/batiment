
@extends("webapp.layouts.default")

@section('content')


    <div class="content-title">
        <div class="content-title-inner">
            <div class="container">
                <h1>{{ (isset($content['title']))?$content['title']:"ETB Batiment: " . trans("general.menu_building_permit") }}</h1>
            </div><!-- /.container -->
        </div><!-- /.content-title-inner -->
    </div>

    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">
                <div class="content">
                    <div class="container maincontent">
                        <div class="col-xs-12">

                            @if(isset($editing))
                                <form method="POST" id="regForm" action="{{ route('build_permit_save') }}" name="form">
                                    <input type="hidden" id="amount" name="amount" value="{{ old('amount', $build_amount) }}" >
                                    @else
                                        <form method="POST" id="regForm" action="{{ route('build_permit_store') }}" name="form">
                                            <input type="hidden" id="amount" name="amount" value="{{ old('amount', $build_amount) }}" >
                                            @endif

                                            {{ csrf_field() }}


                                            <?php
                                            //  Montant total du permit de Construire
                                            $total =  \Config::get('settings.build_amount');
                                            ?>

                                            <input type="hidden" name="totalPayment" value="{{ $total }}">
                                            <input type="hidden" name="payment_information" id="paymentinfo" value="Frais du permis de Construire">



                                            @if ($errors->has('recipice'))
                                                <span class="has-error"  style="font-weight: bold; color:red;">
                                        {{ $errors->first('recipice') }}
                                    </span>
                                            @endif

                                            @if ($errors->has('ground_plane'))
                                                <span class="has-error"  style="font-weight: bold; color:red;">
                                        {{ $errors->first('ground_plane') }}
                                    </span>
                                            @endif

                                            @if ($errors->has('plan_facade'))
                                                <span class="has-error"  style="font-weight: bold; color:red;">
                                        {{ $errors->first('plan_facade') }}
                                    </span>
                                            @endif

                                            @if ($errors->has('photo_ground1'))
                                                <span class="has-error"  style="font-weight: bold; color:red;">
                                        {{ $errors->first('photo_ground1') }}
                                    </span>
                                            @endif

                                            @if ($errors->has('photo_ground2'))
                                                <span class="has-error"  style="font-weight: bold; color:red;">
                                        {{ $errors->first('photo_ground2') }}
                                    </span>
                                            @endif

                                            @if ($errors->has('situation_plan'))
                                                <span class="has-error"  style="font-weight: bold; color:red;">
                                        {{ $errors->first('situation_plan') }}
                                    </span>
                                            @endif

                                            <div class="tab"><h2>@lang('general.permit_submit_step1')</h2><br />

                                                <div class="row">
                                                    <input type="hidden" id="id" name="id" value="{{ old('id', $building->id) }}" >
                                                    <div class="table-responsive" id="list_de_trucs">
                                                        <table class="tableaupm">
                                                            <tr>
                                                                <td class="td_color"><img class="imagetb" src="{{ asset('assets/webapp/img/maison_contemporaine.jpg') }}" /></td>
                                                                <td><img class="imagetb" src="{{ asset('assets/webapp/img/maison_traditionnelle.jpeg') }}" /></td>
                                                                <td class="td_color"><img class="imagetb" src="{{ asset('assets/webapp/img/maison_en_bois.jpg') }}" /></td>
                                                                <td><img class="imagetb" src="{{ asset('assets/webapp/img/maison_ecologique.jpg') }}" /></td>
                                                                <td class="td_color"><img class="imagetb" src="{{ asset('assets/webapp/img/maison_plain_pieds.jpg') }}" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_color"><p>@lang('general.permit_submit_house_cont')  <br /> @lang('general.permit_submit_house_cont_text')   </p></td>
                                                                <td><p>@lang('general.permit_submit_house_trad')  <br /> @lang('general.permit_submit_house_trad_text') </p></td>
                                                                <td class="td_color"><p>@lang('general.permit_submit_house_bois')  <br /> @lang('general.permit_submit_house_bois_text') </p></td>
                                                                <td><p>@lang('general.permit_submit_house_eco') <br /> @lang('general.permit_submit_house_eco_text')  </p></td>
                                                                <td class="td_color"><p>@lang('general.permit_submit_house_plai') <br /> @lang('general.permit_submit_house_plai_text') </p></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_color"> <p> <label for="house_const"> @lang('general.permit_submit_house_cont') </label> <br /><input type="radio" name="house" id="house_const" value="Maison contemporaine" <?php if($building->construction_type=="Maison contemporaine") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''" checked></p></td>
                                                                <td> <p> <label for="house_trad">@lang('general.permit_submit_house_trad') </label><br /> <input type="radio" name="house" id="house_trad" value="Maison traditionnelle" <?php if($building->construction_type=="Maison traditionnelle") {?> checked <?php }  ?> class="form-control" oninput="this.className = ''"></p></td>
                                                                <td class="td_color"> <p> <label for="house_bois">@lang('general.permit_submit_house_bois') </label><br /> <input type="radio" name="house" id="house_bois" value="Maison ossature bois" class="form-control" <?php if($building->construction_type=="Maison ossature bois") {?> checked <?php }  ?> oninput="this.className = ''"></p></td>
                                                                <td> <p> <label for="house_eco">@lang('general.permit_submit_house_eco') </label> <br /><input type="radio" name="house" id="house_eco"  value="Maison écologique" class="form-control" <?php if($building->construction_type=="Maison écologique") {?> checked <?php }  ?> oninput="this.className = ''"></p></td>
                                                                <td class="td_color"> <p> <label for="house_plai">@lang('general.permit_submit_house_plai') </label> <br /><input type="radio" name="house" id="house_plai" value="Maison de plain-pieds" class="form-control" <?php if($building->construction_type=="Maison de plain-pieds") {?> checked <?php }  ?> oninput="this.className = ''"></p></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                            @if(isset($files['situation_plan']))
                                                <?php
                                                $thumbSrc_situation_plan = (is_file($files['situation_plan']))?
                                                    asset($files['situation_plan']):
                                                    asset('');
                                                $name_situation_plan = explode('/', $thumbSrc_situation_plan);
                                                ?>
                                            @endif

                                            @if(isset($files['ground_plane']))
                                                <?php
                                                $thumbSrc_ground_plane = (is_file($files['ground_plane']))?
                                                    asset($files['ground_plane']):
                                                    asset('');
                                                $name_ground_plane = explode('/', $thumbSrc_ground_plane);
                                                ?>
                                            @endif

                                            @if(isset($files['plan_facade']))
                                                <?php
                                                $thumbSrc_plan_facade = (is_file($files['plan_facade']))?
                                                    asset($files['plan_facade']):
                                                    asset('');
                                                $name_plan_facade = explode('/', $thumbSrc_plan_facade);
                                                ?>
                                            @endif

                                            @if(isset($files['photo_ground1']))
                                                <?php
                                                $thumbSrc_photo_ground1 = (is_file($files['photo_ground1']))?
                                                    asset($files['photo_ground1']):
                                                    asset('');
                                                $name_photo_ground1 = explode('/', $thumbSrc_photo_ground1);
                                                ?>
                                            @endif

                                            @if(isset($files['photo_ground2']))
                                                <?php
                                                $thumbSrc_photo_ground2 = (is_file($files['photo_ground2']))?
                                                    asset($files['photo_ground2']):
                                                    asset('');
                                                $name_photo_ground2 = explode('/', $thumbSrc_photo_ground2);
                                                ?>
                                            @endif


                                            <div class="tab"><h2>@lang('general.permit_submit_step2')</h2>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <p><label for="">@lang('general.permit_submit_situation_plan')</label>  @if(isset($files['situation_plan']))<a href="#"  >{{  $files['situation_plan']    }}</a>  <input type="hidden" name="situation_plan1"  @if(isset($files['situation_plan'])) value="{{ old('situation_plan', $files['situation_plan']) }} " @endif class="form-control">@endif<input type="file" name="situation_plan"  class="form-control"></p>
                                                                @if ($errors->has('situation_plan'))
                                                                    <span class="has-error"  style="font-weight: bold; color:red;">
                                                                                            {{ $errors->first('situation_plan') }}
                                                                                        </span>
                                                                @endif
                                                                <p><label for="">@lang('general.permit_submit_ground_plan')</label> @if(isset($files['ground_plane']))<a href="#"  >{{  $files['ground_plane']    }}</a> <input type="hidden"  name="ground_plane11"  @if(isset($files['ground_plane'])) value="{{ old('ground_plane', $files['ground_plane']) }} " @endif class="form-control">@endif<input type="file" name="ground_plane"  class="form-control" ></p>
                                                                @if ($errors->has('ground_plane'))
                                                                    <span class="has-error"  style="font-weight: bold; color:red;">
                                                                                            {{ $errors->first('ground_plane') }}
                                                                                        </span>
                                                                @endif
                                                                <p><label for="">@lang('general.permit_submit_plan_facade')</label> @if(isset($files['plan_facade']))<a href="#"  >{{  $files['plan_facade']    }}</a> <input type="hidden"  name="plan_facade1"  @if(isset($files['plan_facade'])) value="{{ old('plan_facade', $files['plan_facade']) }}" @endif    class="form-control">@endif<input type="file" name="plan_facade"   class="form-control"  ></p>
                                                                @if ($errors->has('plan_facade'))
                                                                    <span class="has-error"  style="font-weight: bold; color:red;">
                                                                                            {{ $errors->first('plan_facade') }}
                                                                                        </span>
                                                                @endif
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><label for="">@lang('general.permit_submit_photo_ground1')</label> @if(isset($files['photo_ground1']))<a href="#"  >{{  $files['photo_ground1']    }}</a> <input type="hidden"  name="photo_ground11"  @if(isset($files['photo_ground1'])) value="{{ old('photo_ground1', $files['photo_ground1']) }}" @endif class="form-control">@endif<input type="file" name="photo_ground1"   class="form-control"  ></p>
                                                                @if ($errors->has('photo_ground1'))
                                                                    <span class="has-error"  style="font-weight: bold; color:red;">
                                                                                            {{ $errors->first('photo_ground1') }}
                                                                                        </span>
                                                                @endif
                                                                <p><label for="">@lang('general.permit_submit_photo_ground2')</label> @if(isset($files['photo_ground2']))<a href="#"  >{{  $files['photo_ground2']    }}</a> <input type="hidden"  name="photo_ground21"  @if(isset($files['photo_ground2'])) value="{{ old('photo_ground2', $files['photo_ground2']) }}" @endif class="form-control">@endif<input type="file" name="photo_ground2"  class="form-control"  ></p>
                                                                @if ($errors->has('photo_ground2'))
                                                                    <span class="has-error"  style="font-weight: bold; color:red;">
                                                                                            {{ $errors->first('photo_ground2') }}
                                                                                        </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab"><h2>@lang('general.permit_submit_step3')</h2>
                                                <h5>@lang('general.permit_submit_step3_text')</h5>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <p><label for="">@lang('general.permit_submit_ground') </label> <input type="textarea" rows="8" name="ground" value="{{ old('ground', $building->ground) }}" id="ground" class="form-control"  oninput="this.className = ''"></p>
                                                                <p><label for="">@lang('general.permit_submit_building') </label> <input type="text" name="building" id="building" value="{{ old('building', $building->building) }}" class="form-control"  oninput="this.className = ''"></p>
                                                                <p><label for="">@lang('general.permit_submit_access') </label> <input type="text" name="access" id="access"  value="{{ old('access', $building->access) }}" class="form-control"  oninput="this.className = ''"></p>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <p><label for="">@lang('general.permit_submit_aspect') </label> <input type="textarea" name="aspect" id="aspect" rows="8"  class="form-control" value="{{ old('aspect', $building->aspect) }}" oninput="this.className = ''"></p>
                                                                <p><label for="">@lang('general.permit_submit_petitioner_name') </label> <input type="text" name="petitionary_name" id="petitionary_name"  class="form-control" value="{{ old('petitionary_name', $building->petitionary_name) }}" oninput="this.className = ''"></p>
                                                                <p><label for="">@lang('general.permit_submit_lieu_construction') </label> <input type="textarea" rows="8" name="place_construction" id="place_construction" value="{{ old('place_construction', $building->place_construction) }}" class="form-control"  oninput="this.className = ''"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h5>@lang('general.permit_submit_1part') </h5>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <p><input type="radio" name="characteristics_place"  value="Zone Urbaine" <?php if($building->characteristics_place=="Zone Urbaine") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''" checked> <label for="">@lang('general.permit_submit_urban')</label></p>
                                                                <p><input type="radio" name="characteristics_place" value="Zone Rurale" <?php if($building->characteristics_place=="Zone Rurale") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''"> <label for="">@lang('general.permit_submit_rural')</label></p>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <p><input type="radio"  name="characteristics_place" value="Terrain Isolé" <?php if($building->characteristics_place=="Terrain Isolé") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''"> <label for="">@lang('general.permit_submit_isolated')</label></p>
                                                                <p><input type="radio"name="characteristics_place"  value="Lotissement" <?php if($building->characteristics_place=="Lotissement") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''"> <label for="">@lang('general.permit_submit_allotment')</label></p>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <p><input type="radio" rows="8" name="characteristics_place" value="Centre ville"<?php if($building->characteristics_place=="Centre ville") {?> checked <?php }  ?>  class="form-control"  oninput="this.className = ''"> <label for="">@lang('general.permit_submit_center')</label></p>
                                                                <p><input type="radio" name="characteristics_place" value="littoral" <?php if($building->characteristics_place=="littoral") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''"> <label for="">@lang('general.permit_submit_littoral')</label></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <h5>@lang('general.permit_submit_surrounding')</h5>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <p><input type="radio" name="surrounding_landscape" value="Habitat Densifié" <?php if($building->surrounding_landscape=="Habitat Densifié") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''" checked> <label for="">@lang('general.permit_submit_densified')</label></p>
                                                                <p><input type="radio" name="surrounding_landscape" value="Habitat clairsemé" <?php if($building->surrounding_landscape=="Habitat clairsemé") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''"> <label for="">@lang('general.permit_submit_sparse')</label></p>
                                                                <p><input type="radio"  name="surrounding_landscape" value="Arbres Nombreux" <?php if($building->surrounding_landscape=="Arbres Nombreux") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''"> <label for="">@lang('general.permit_submit_trees')</label></p>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <p><input type="radio" name="surrounding_landscape"  value="Terrain sec" <?php if($building->surrounding_landscape=="Terrain sec") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''"> <label for="">@lang('general.permit_submit_dry')</label></p>
                                                                <p><input type="radio" name="surrounding_landscape" value="Terrain tres vert" <?php if($building->surrounding_landscape=="Terrain tres vert") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''"> <label for="">@lang('general.permit_submit_green')</label></p>
                                                                <p><input type="radio" name="surrounding_landscape" value="Autres voies" <?php if($building->surrounding_landscape=="Autres voies") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''"> <label for="">@lang('general.permit_submit_other')</label></p>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <p><input type="radio" rows="8" value="Bordure RN" name="surrounding_landscape" <?php if($building->surrounding_landscape=="Bordure RN") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''"> <label for="">@lang('general.permit_submit_RN')</label></p>
                                                                <p><input type="radio" value="Bordure RD" name="surrounding_landscape" <?php if($building->surrounding_landscape=="Bordure RD") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''"> <label for="">@lang('general.permit_submit_RD')</label></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h5>@lang('general.permit_submit_part2')</h5>
                                                <h6>@lang('general.permit_submit_part2_1')</h6>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <p><input type="radio"  name="concerned_land" value="Plat" <?php if($building->concerned_land=="Plat") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''" checked> <label for="">@lang('general.permit_submit_dish')</label></p>
                                                                <p><input type="radio" name="concerned_land" value="En pente douce" <?php if($building->concerned_land=="En pente douce") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''"> <label for="">@lang('general.permit_submit_fresh')</label></p>
                                                                <p><input type="radio" name="concerned_land" value="En pente forte" <?php if($building->concerned_land=="En pente forte") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''"> <label for="">@lang('general.permit_submit_strong')</label></p>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <p><input type="radio"  name="concerned_land" value="Végétation faible" <?php if($building->concerned_land=="Végétation faible") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''"> <label for="">@lang('general.permit_submit_low')</label></p>
                                                                <p><input type="radio" name="concerned_land" value="Végétation forte" <?php if($building->concerned_land=="Végétation forte") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''"> <label for="">@lang('general.permit_submit_strong_veget')</label></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <h6>@lang('general.permit_submit_part2_2')</h6>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <p><input type="radio"  name="neighboring_constructions" value="Immeuble" <?php if($building->neighboring_constructions=="Immeuble") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''" checked> <label for="">@lang('general.permit_submit_estate')</label></p>
                                                                <p><input type="radio" name="neighboring_constructions" value="Maisons RDC" <?php if($building->neighboring_constructions=="Maisons RDC") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''"> <label for="">@lang('general.permit_submit_houseRDC')</label></p>
                                                                <p><input type="radio"  name="neighboring_constructions" value="Maison sur sol" <?php if($building->neighboring_constructions=="Maison sur sol") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''"> <label for="">@lang('general.permit_submit_houseland')</label></p>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <p><input type="radio"  name="neighboring_constructions" value="Tous 2 pans"  <?php if($building->neighboring_constructions=="Tous 2 pans") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''"> <label for="">@lang('general.permit_submit_2pans')</label></p>
                                                                <p><input type="radio"  name="neighboring_constructions" value="Tous 4 pans" <?php if($building->neighboring_constructions=="Tous 4 pans") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''"> <label for="">@lang('general.permit_submit_4pans')</label></p>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <p><input type="radio" rows="8" name="neighboring_constructions" value="Fortes pente" <?php if($building->neighboring_constructions=="Fortes pente") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''"> <label for="">@lang('general.permit_submit_strong_slope')</label></p>
                                                                <p><input type="radio"  name="neighboring_constructions" value="Faible pente" <?php if($building->neighboring_constructions=="Faible pente") {?> checked <?php }  ?> class="form-control"  oninput="this.className = ''"> <label for="">@lang('general.permit_submit_low_slope')</label></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            @if(isset($files['recipice']))
                                                <?php
                                                $thumbSrc_recipice = (is_file($files['recipice']))?
                                                    asset($files['recipice']):
                                                    asset('');
                                                $name_recipice = explode('/', $thumbSrc_recipice);
                                                ?>
                                            @endif

                                            <div class="tab"><h2>@lang('general.permit_submit_step4')</h2>
                                                <p>@lang('general.permit_submit_step4_link') <a href="https://www.service-public.fr/particuliers/vosdroits/F1986">https://www.service-public.fr/particuliers/vosdroits/F1986</a><br/></p>
                                                <p><label for="">@lang('general.permit_submit_step4_text')</label> @if(isset($files['recipice']))<a href="#" target="_blank">{{ $files['recipice'] }}</a> <input type="hidden" name="recipice" @if(isset($files['recipice'])) value="{{ old('recipice', $files['recipice']) }} " @endif class="form-control">@endif <input type="file" name="recipice" id="recipice" class="form-control"  oninput="this.className = ''"></p>

                                                @if ($errors->has('recipice'))
                                                    <span class="has-error"  style="font-weight: bold; color:red;">
                                            {{ $errors->first('recipice') }}
                                        </span>
                                                @endif
                                            </div>

                                            <!-- bouton de paiement paypal -->
                                            <div class="tab"><h2>@lang('general.permit_submit_step5')</h2>
                                                <div class="row justify-content-center">
                                                        <center>  <h2>582.45 $</h2>  </center>
                                                    <div id="paypal-button" ></div>
                                                </div>
                                            </div>



                                            <input type="hidden" name="amount" value="{{ $total }}">
                                            <!-- End bouton paypal -->
                                            <div style="overflow:auto;">
                                                <div style="float:right;">
                                                    <button type="button" id="prevBtn" class="btn btn-primary" onclick="nextPrev(-1)">@lang('general.permit_submit_bt_previous')</button>
                                                    <button type="button" id="nextBtn"  class="btn btn-primary"onclick="nextPrev(1)">@lang('general.permit_submit_bt_next')</button>
                                                </div>
                                            </div>

                                            <!-- Circles which indicates the steps of the form: -->
                                            <div style="text-align:center;margin-top:40px;">
                                                <span class="step"></span>
                                                <span class="step"></span>
                                                <span class="step"></span>
                                                <span class="step"></span>
                                                <span class="step"></span>
                                            </div>
                                        </form>
                        </div>

                    </div>
                </div>
            </div><!-- /.main-inner -->
        </div><!-- /.main -->
    </div><!-- /.main-wrapper -->

    <!-- Deposit from -->
<?php
/*
    <form method="POST" id="depositCompleteForm" action="{{ route('checkout') }}" name="form">
        {{ csrf_field() }}
        <input type="hidden" name="totalPayment" value="{{ $total }}">
        <input type="hidden" name="payment_information" id="paymentinfo" value="Frais de dossier du permit de construire">
    </form>
*/
?>


    <script src="https://www.paypalobjects.com/api/checkout.js"></script>
    <script>

        paypal.Button.render({

            env: '{{ env('PAYPAL_ENV','production') }}', // 'production' Or 'sandbox'

            // PayPal Client IDs - replace with your own
            // Create a PayPal app: https://developer.paypal.com/developer/applications/create
            locale: 'fr_FR',

            style: {
                size: 'responsive',
                color: 'gold',
                shape: 'pill',
                label: 'checkout'
            },

            client: {
                sandbox:    '{{ env('PAYPAL_SANDBOX_ID','') }}'
            },

            // Show the buyer a 'Pay Now' button in the checkout flow
            commit: true,

            // payment() is called when the button is clicked
            payment: function(data, actions) {

                // Make a call to the REST api to create the payment
                return actions.payment.create({
                    payment: {
                        transactions: [
                            {
                                amount: { total: '{{ $total }}', currency: 'USD' }
                            }
                        ]
                    }
                });
            },

            // onAuthorize() is called when the buyer approves the payment
            onAuthorize: function(data, actions) {

                // Make a call to the REST api to execute the payment
                return actions.payment.execute().then(function() {
                    var paymentinfo = document.getElementById('paymentinfo');
                    paymentinfo.value = JSON.stringify(payment);

                    document.getElementById('regForm').submit();
                });
            }

        }, '#paypal-button');


        // faire un clique sur le bouton active lorsque la page s'ouvre
        $(document).ready(function(){
            document.getElementById('monclick').click();
        });


    </script>


    @include("webapp.includes.call-to-action-footer")

@endsection



@section('script')
<script type="text/javascript" src="{{ asset('assets/webapp/js/buildingPermit.js') }}"></script>
@endsection
