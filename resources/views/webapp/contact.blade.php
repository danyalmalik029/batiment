@extends("webapp.layouts.default")


@section('content')

    <div class="content-title">
        <div class="content-title-inner">
            <div class="container">
                <h1>@lang('contact.page_title')</h1>
            </div><!-- /.container -->
        </div><!-- /.content-title-inner -->
    </div>

    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">

                <div class="content">
                    <div class="container maincontent">

                        @if (session('message'))
                            <div class="alert alert-success">
                                {{ session('message') }}
                            </div>
                        @endif


                        <div class="row">
                            <div class="clear clear-15"></div>
                            <div class="col-md-8 col-lg-9">
                                <div class="page-subheader page-subheader-small">
                                    <h3>@lang('contact.form')</h3>
                                </div>

                                <div class="row">
                                    <form method="post" action="{{ url('/contact') }}">
                                        {{ csrf_field() }}
                                        <div class="col-sm-6">
                                            <div class="form-group @if($errors->has('your_name')) error @endif">
                                                <label>@lang('contact.your_name')</label>
                                                <input type="text" name="your_name" required class="form-control" value="{{ old('your_name')}}">
                                                @if ($errors->has('your_name'))
                                                    <span class="has-error">
                                                        {{ $errors->first('your_name') }}
                                                    </span>
                                                @endif
                                            </div><!-- /.form-group -->
                                        </div><!-- /.col-* -->

                                        <div class="col-sm-6">
                                            <div class="form-group @if($errors->has('your_email')) error @endif">
                                                <label>@lang('contact.your_email')</label>
                                                <input type="mail" name="your_email" required class="form-control" value="{{ old('your_email')}}">
                                                @if ($errors->has('your_email'))
                                                    <span class="has-error">
                                                        {{ $errors->first('your_email') }}
                                                    </span>
                                                @endif
                                            </div><!-- /.form-group -->
                                        </div><!-- /.col-* -->
                                        <div class="col-sm-6">
                                            <div class="form-group @if($errors->has('phone')) error @endif">
                                                <label>@lang('contact.phone')</label>
                                                <input type="text" name="phone" required class="form-control" value="{{ old('phone')}}">
                                                @if ($errors->has('phone'))
                                                    <span class="has-error">
                                                        {{ $errors->first('phone') }}
                                                    </span>
                                                @endif
                                            </div><!-- /.form-group -->
                                        </div><!-- /.col-* -->

                                        <div class="col-sm-6">
                                            <div class="form-group @if($errors->has('city')) error @endif">
                                                <label>@lang('contact.city')</label>
                                                <input type="text" name="city" required class="form-control" value="{{ old('city')}}">
                                                @if ($errors->has('city'))
                                                    <span class="has-error">
                                                        {{ $errors->first('city') }}
                                                    </span>
                                                @endif
                                            </div><!-- /.form-group -->
                                        </div><!-- /.col-* -->

                                        <div class="col-sm-6">
                                            <div class="form-group @if($errors->has('subject')) error @endif">
                                                <label>@lang('contact.subject')</label>
                                                <input type="text" name="subject" required class="form-control" value="{{ old('subject')}}">
                                                @if ($errors->has('subject'))
                                                    <span class="has-error">
                                                        {{ $errors->first('subject') }}
                                                    </span>
                                                @endif
                                            </div><!-- /.form-group -->
                                        </div><!-- /.col-* -->

                                        <div class="col-sm-12">
                                            <div class="form-group @if($errors->has('message')) error @endif">
                                                <label>@lang('contact.message')</label>
                                                <textarea class="form-control" name="mymessage" required rows="6" placeholder="@lang('contact.message_placeholder')"> {{ old('mymessage')}}</textarea>
                                                @if ($errors->has('mymessage'))
                                                    <span class="has-error">
                                                        {{ $errors->first('mymessage') }}
                                                    </span>
                                                @endif
                                            </div><!-- /.form-control -->
                                        </div><!-- /.col-* -->

                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-primary pull-right">@lang('contact.submit')</button>
                                        </div><!-- /.col-* -->
                                    </form>
                                </div><!-- /.row -->
                            </div><!-- /.col-* -->

                            <div class="col-md-4 col-lg-3">
                                <div class="sidebar">
                                    <div class="widget">
                                        <h2 class="widgettitle">@lang('contact.information')</h2>

                                        <table class="contact">
                                            <tbody>

                                            <tr>
                                                <th>@lang('contact.email'):</th>
                                                <td><a href="mailto:&#105;&#110;&#102;&#111;&#064;&#101;&#116;&#098;&#045;&#098;&#097;&#116;&#105;&#109;&#101;&#110;&#116;&#046;&#099;&#111;&#109;">&#105;&#110;&#102;&#111;&#064;&#101;&#116;&#098;&#045;&#098;&#097;&#116;&#105;&#109;&#101;&#110;&#116;&#046;&#099;&#111;&#109;</a></td>
                                            </tr>

                                            <tr>
                                                <th>@lang('contact.skyoe'):</th>
                                                <td>
                                                    <a href="skype:etb-batiment.com?chat">ETB batiment</a>
                                                </td>
                                            </tr>

                                            <tr>
                                                <th colspan="2">@lang('contact.follow_us')</th>
                                            </tr>

                                            <tr>
                                                <td colspan="2">
                                                    <div class="social-side">
                                                        <div class="social">
                                                            <a href="https://www.facebook.com/etbbatiment" target="_blank"><i class="fa fa-facebook"></i></a>
                                                            <a href="https://twitter.com/etbbatiment" target="_blank"><i class="fa fa-twitter"></i></a>
                                                            <a href="https://www.linkedin.com/company/etbbatiment/" target="_blank"><i class="fa fa-linkedin"></i></a>
                                                            <!-- <a href="{{ url('/contact') }}"><i class="fa fa-envelope"></i></a> -->
                                                            <!-- <a href="#"><i class="fa fa-instagram"></i></a> -->
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </div><!-- /.widget -->
                                </div><!-- /.sidebar -->
                            </div><!-- /.col-* -->
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </div>

            </div><!-- /.main-inner -->
        </div><!-- /.main -->
    </div><!-- /.main-wrapper -->

@endsection