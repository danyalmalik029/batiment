@extends("webapp.layouts.default")

@section('content')


    <div class="content-title">
        <div class="content-title-inner">
            <div class="container">
                <h1>{{ (isset($content['title']))?$content['title']:"ETB Batiment, " . trans("general.general_generalnotfoundtitle") }}</h1>
            </div><!-- /.container -->
        </div><!-- /.content-title-inner -->
    </div>

    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">
                <div class="content">
                    <div class="container maincontent">

                        <div class="row">
                            <div class="col-xs-12 col-md-8 col-lg-9">
                                <article>
                                    @if(isset($content['body']))

                                        @if($content['status'] === 'ACTIVE' or (isset($user) and $user->role_id==1) or (isset($user) and $user->role_id==3))

                                        {!! $content['body'] !!}
                                        @if($content['slug'] === 'permis-de-construire-pour-une-maison-individuelle')
                                        @include('webapp.building_permit.make_request');
                                        
                                        @endif
                                    

                                        @else
                                            <div class="row">
                                                <div class="warning">
                                                    <h2>@lang('general.general_undermaintenance')!</h2>
                                                    <hr />
                                                    <a href="{{ url('/') }}" class="btn btn-secondary"><i class="fa fa-long-arrow-left"></i> @lang('general.general_backhome')</a>
                                                </div><!-- /.warning -->
                                            </div>
                                        @endif

                                    @else
                                </article>
                                <div class="row">
                                    <div class="warning">
                                        <h1>404</h1>
                                        <hr />
                                        <a href="{{ url('/') }}" class="btn btn-secondary"><i class="fa fa-long-arrow-left"></i> Return Home</a>
                                    </div><!-- /.warning -->
                                </div>
                                @endif
                            </div>
                            <div class="col-xs-12 col-md-4 col-lg-3">

                            </div>
                        </div>

                    </div>
                </div>
            </div><!-- /.main-inner -->
        </div><!-- /.main -->
    </div><!-- /.main-wrapper -->

    @include("webapp.includes.call-to-action-footer")

@endsection