
@foreach($plans as $plan)
    <div class="col-md-4">
        <div class="listing-box">
            <div class="listing-box-image" style="

            @if(count($plan->images) != 0)
                    background-image: url('{{ asset("storage/propertiesImages/" . $plan->images[0]->image_name) }}');
            @else
                    background-image: url('{{ asset("assets/img/tmp/tmp-5.jpg") }}')
            @endif
                    ">
                          <span class="listing-box-image-links">
                            @if(Auth::check() && $plan->user_id == Auth::id())
                                  <a href="{{ url('/edit-property/'.$plan->id) }}" style="@if($plan->statut == '0')background: #d9534f !important;@endif"><i class="fa fa-pencil-square-o"></i> <span>@lang('properties.edit_property')</span></a>
                              @endif

                              <a href="{{ url('/view-details-property/'.$plan->id) }}" target="_blank" style="@if($plan->statut == '0')background: #d9534f !important;@endif"><i class="fa fa-search"></i> <span>@lang('properties.view_detail')</span></a>


                              @if(Auth::check() && $plan->user_id == Auth::id())
                                  @if($plan->statut == '0')
                                      <a href="{{ url('/delete-property/'.$plan->id) }}" style="background: #d9534f !important;" class="enableProperty" value="{{$plan->id}}" name="{{$plan->title}}"><i class="fa fa-ban"></i> <span>@lang('properties.enable_property')</span></a>
                                      <a href="{{ url('/delete-property/'.$plan->id) }}" style="background: #d9534f !important;" class="deleteProperty" value="{{$plan->id}}" name="{{$plan->title}}"><i class="fa fa-ban"></i> <span>@lang('properties.delete_property')</span></a>
                                  @else
                                      <a href style="@if($plan->statut == '0')background: #d9534f !important;@endif" class="disableProperty" value="{{$plan->id}}" name="{{$plan->title}}"><i class="fa fa-ban"></i> <span>@lang('properties.disable_property')</span></a>
                                  @endif
                              @endif
                          </span>
            </div><!-- /.listing-box-image -->

            <div class="listing-box-title" style="@if($plan->statut == '0') background-color: #d9534f !important; @endif">
                <h2><a href="{{ url('/edit-property/'.$plan->id) }}">{{$plan->title}}</a></h2>
                <h3>{{$plan->price}} &#128;</h3>
            </div><!-- /.listing-box-title -->
            <div class="listing-box-content">
                <dl>
                    <dt>@lang('properties.category')</dt><dd>@lang('general.ads_category_' . \App\Models\Categories::find($plan->catid)->category_slug)</dd>
                    <dt>@lang('properties.type')</dt><dd>@lang('properties.type_' . $plan->ad_dtype)</dd>
                    <dt>@lang('properties.area')</dt><dd>{{$plan->area}} (m²)</dd>
                </dl>
            </div><!-- /.listing-box-cotntent -->
            <div class="listing-boxes-colored">
                <dl>
                    <dt>@lang('properties.choose')</dt><dd><input type="radio"  onclick="checkPlan({{$plan->price}})" data-price="{{$plan->price}}" name="plan" id="{{$plan->title}}" value="{{$plan->id}}"   oninput="this.className = ''" ></dd>

                </dl>
            </div>
        </div><!-- /.listing-box -->
    </div><!-- /.col-* -->
@endforeach
{!! $plans->render() !!}
