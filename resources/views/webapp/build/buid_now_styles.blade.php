<div id="invoice" class="col-md-offset-2 col-md-8">

    <div class="invoice overflow-auto">
        <div style="min-width: 600px">
            <div  id="down_pay_label" >
            <label>@lang('build.down_payment_label') <input type="number" value="0" name="down_payment" id="down_payment" /></label>
            </div>
            <main>

                <table border="0" cellspacing="0" cellpadding="0">
                    <thead>
                    <tr>
                        <th colspan="3" style="text-align: center!important"><h2>@lang('build.summary_table_heading')</h2></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="no">01</td>
                        <td class="text-left"><h3>@lang('build.summary_amount_of_land')</h3>
                        </td>
                        <td class="total"><b><span id="amount_land"></span>&#128;</b></td>
                    </tr>
                    <tr>
                        <td class="no">02</td>
                        <td class="text-left"><h3>@lang('build.summary_amount_of_plan')</h3></td>

                        <td class="total"><b><span id="amount_plan"></span>&#128;</b></td>
                    </tr>
                    <tr>
                        <td class="no">03</td>
                        <td class="text-left"><h3>@lang('build.summary_notary_fees')</h3></td>

                        <td class="total"><b><span id="notary"></span>&#128;</b></td>
                    </tr>
                    <tr>
                        <td class="no">04</td>
                        <td class="text-left"><h3>@lang('build.summary_bank_charge')</h3></td>

                        <td class="total"><b><span id="bank_charges"></span>&#128;</b></td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="2">@lang('build.summary_sub_total')</td>
                        <td><b><span id="f_sum"></span>&#128;</b></td>
                    </tr>
                    <tr>
                        <td colspan="2">@lang('build.summary_grand_total')</td>
                        <td><b><span id="t_sum"></span>&#128;</b></td>
                    </tr>
                    </tfoot>
                </table>

            </main>
        </div>
        <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
        <div></div>
    </div>
</div>
<style>
    .invoice {
        position: relative;
        background-color: #FFF;
        padding: 15px 15px 0 15px;
    }

    .invoice header {
        padding: 10px 0;
        margin-bottom: 20px;
        border-bottom: 1px solid #3989c6
    }

    .invoice .company-details {
        text-align: right
    }

    .invoice .company-details .name {
        margin-top: 0;
        margin-bottom: 0
    }

    .invoice .contacts {
        margin-bottom: 20px
    }

    .invoice .invoice-to {
        text-align: left
    }

    .invoice .invoice-to .to {
        margin-top: 0;
        margin-bottom: 0
    }

    .invoice .invoice-details {
        text-align: right
    }

    .invoice .invoice-details .invoice-id {
        margin-top: 0;
        color: #3989c6
    }

    .invoice main .thanks {
        margin-top: -100px;
        font-size: 2em;
        margin-bottom: 50px
    }

    .invoice main .notices {
        padding-left: 6px;
        border-left: 6px solid #3989c6
    }

    .invoice main .notices .notice {
        font-size: 1.2em
    }

    .invoice table {
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 20px
    }

    .invoice table td, .invoice table th {
        padding: 15px;
        background: #eee;
        border-bottom: 1px solid #fff
    }

    .invoice table th {
        white-space: nowrap;
        font-weight: 400;
        font-size: 16px
    }

    .invoice table td h3 {
        margin: 0;
        font-weight: 400;
        text-align: left !important;
        color: #39B54A;
        font-size: 1.2em
    }

    .invoice table .qty, .invoice table .total, .invoice table .unit {
        text-align: right;
        font-size: 1.2em
    }

    .invoice table .no {
        color: #fff;
        font-size: 1.6em;
        text-align: right !important;
        background: #39B54A;
    }

    .invoice table .unit {
        background: #ddd
    }

    .invoice table .total {
        background: #39B54A;
        color: #fff;
        width: 1% !important;
    }

    .invoice table tbody tr:last-child td {
        border: none
    }

    .invoice table tfoot td {
        background: 0 0;
        border-bottom: none;
        white-space: nowrap;
        text-align: right !important;
        padding: 10px 20px;
        font-size: 1.2em;
        border-top: 1px solid #aaa
    }

    .invoice table tfoot tr:first-child td {
        border-top: none
    }

    .invoice table tfoot tr:last-child td {
        color: #39B54A;
        font-size: 1.4em;
        text-align: right;
        border-top: 1px solid #39B54A;
    }

    .invoice table tfoot tr td:first-child {
        border: none
    }

    .invoice footer {
        width: 100%;
        text-align: center;
        color: #777;
        border-top: 1px solid #aaa;
        padding: 8px 0
    }

    @media print {
        .invoice {
            font-size: 11px !important;
            overflow: hidden !important
        }

        .invoice footer {
            position: absolute;
            bottom: 10px;
            page-break-after: always
        }

        .invoice > div:last-child {
            page-break-before: always
        }
    }
</style>