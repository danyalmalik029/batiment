@extends("webapp.layouts.default")

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">

@endsection

@section('content')
{{-- Modal Success --}}
<div class="modal fade" id="modal-success" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">@lang('build.title_success')</h5>
      </div>
      <div class="modal-body">
        <p> @lang('build.body_success')</p>
      </div>
      <div class="modal-footer">
        <button type="button" id="close" class="btn btn-secondary" >@lang('build.close')</button>
      </div>
    </div>
  </div>
</div>

{{-- Alert Modal Direct --}}

<div class="modal fade" id="modal-alert1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-sm-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">@lang('build.title_success')</h5>
      </div>
      <div class="modal-body">
        <p> @lang('build.applybuildingpermit')</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('build.close')</button>
        <button type="button" id="submitmodal" class="btn btn-primary">@lang('build.proceedmodal')</button>
      </div>
    </div>
  </div>
</div>
{{-- Alert Modal Direct Funding --}}

<div class="modal fade" id="modal-alert2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-sm-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">@lang('build.title_success')</h5>
      </div>
      <div class="modal-body">
        <p> @lang('build.funding')</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" id="closemodal">@lang('build.closemodal')</button>
        <button type="button" id="submitmodal2" class="btn btn-primary">@lang('build.proceedmodal')</button>
      </div>
    </div>
  </div>
</div>


<div class="main-wrapper">
 <div class="main">
   <div class="main-inner">

     <div class="content-title">
      <div class="content-title-inner">
       <div class="container">		
        <h1>{{$title}}</h1>
      </div><!-- /.container -->
    </div><!-- /.content-title-inner -->
  </div><!-- /.content-title -->

  <br>
  <div class="content">
    <div class="container maincontent">
      <div class="col-xs-12">

        <form id="regForm" method="POST" action="{{url('/build-form/add')}}" enctype="multipart/form-data">
        {{csrf_field()}}
          {{-- Tab 1 --}}
          <div class="tab"><h2>@lang('build.typepropriete')</h2><br />

            <div class="row">
              <div class="table-responsive" id="list_de_trucs">
                <table class="tableaupm">
                  <tr>
                    <td class="td_color"><img class="imagetb" src="{{ asset('assets/webapp/img/maison_contemporaine.jpg') }}" /></td>
                    <td><img class="imagetb" src="{{ asset('assets/webapp/img/maison_traditionnelle.jpeg') }}" /></td>
                    <td class="td_color"><img class="imagetb" src="{{ asset('assets/webapp/img/maison_en_bois.jpg') }}" /></td>
                    <td><img class="imagetb" src="{{ asset('assets/webapp/img/maison_ecologique.jpg') }}" /></td>
                    <td class="td_color"><img class="imagetb" src="{{ asset('assets/webapp/img/maison_plain_pieds.jpg') }}" /></td>
                  </tr>
                  <tr>
                    <td class="td_color"><p>@lang('general.permit_submit_house_cont')  <br /> @lang('general.permit_submit_house_cont_text')   </p></td>
                    <td><p>@lang('general.permit_submit_house_trad')  <br /> @lang('general.permit_submit_house_trad_text') </p></td>
                    <td class="td_color"><p>@lang('general.permit_submit_house_bois')  <br /> @lang('general.permit_submit_house_bois_text') </p></td>
                    <td><p>@lang('general.permit_submit_house_eco') <br /> @lang('general.permit_submit_house_eco_text')  </p></td>
                    <td class="td_color"><p>@lang('general.permit_submit_house_plai') <br /> @lang('general.permit_submit_house_plai_text') </p></td>
                  </tr>
                  <tr>
                    <td class="td_color"> <p> <label for="house_const"> @lang('general.permit_submit_house_cont') </label> <br /><input type="radio" name="house" id="house_const" value="Maison contemporaine" class="form-control"  oninput="this.className = ''" ></p></td>
                    <td> <p> <label for="house_trad">@lang('general.permit_submit_house_trad') </label><br /> <input type="radio" name="house" id="house_trad" value="Maison traditionnelle"  class="form-control" oninput="this.className = ''"></p></td>
                    <td class="td_color"> <p> <label for="house_bois">@lang('general.permit_submit_house_bois') </label><br /> <input type="radio" name="house" id="house_bois" value="Maison ossature bois" class="form-control"  oninput="this.className = ''"></p></td>
                    <td> <p> <label for="house_eco">@lang('general.permit_submit_house_eco') </label> <br /><input type="radio" name="house" id="house_eco"  value="Maison écologique" class="form-control"  oninput="this.className = ''"></p></td>
                    <td class="td_color"> <p> <label for="house_plai">@lang('general.permit_submit_house_plai') </label> <br /><input type="radio" name="house" id="house_plai" value="Maison de plain-pieds" class="form-control"  oninput="this.className = ''"></p></td>
                  </tr>
                </table>
              </div>
            </div>
          </div>

          {{-- Tab 2  --}}
          <div class="tab"><h2>@lang('build.typeland')</h2><br />
          <div class="row"> 
            <div class="form-group">
            <h2>@lang('build.askland')</h2>
            <label><input type="radio" class="form-control" name="askland" value="yes">@lang('build.oui')</label>
            <br>
            <label><input type="radio" class="form-control" name="askland" value="no">@lang('build.non')</label>

            </div>
            <div class="col-sm-6" id="land_upload" style="display:none;">
              <h2>@lang('build.license')</h2>
              <div class="form-group">
                <input type="file" id="land_permission" class="form-control" name="land_permission">

              </div>
            </div>
          </div>


            <div class="row" id="landselection" style="display:none;">
                @include('webapp.build.land_search')
            <h2>@lang('build.typeland_option')</h2>
              <div class="table-responsive" id="list_de_trucs">
                  <table class="tableaupm" style="width: 100%">
                      <tr>
                          <td id="listings-items">
                            @include('webapp.build.ajax_lands')
                          </td>
                      </tr>
                  </table>
              </div>
            </div>

          </div>

          {{-- Tab 3 --}}
          <div class="tab"><h2>@lang('build.typeplan')</h2><br />
          <div class="row">
            <div class="form-group">
            <h2>@lang('build.askplan')</h2>
            <label><input type="radio" class="form-control" name="askplan" value="yes">@lang('build.oui')</label>
            <br>
            <label><input type="radio" class="form-control" name="askplan" value="no">@lang('build.non')</label>

            </div>
          </div>
          <div class="row" id="planquestion2" style="display:none;">

            <h2>@lang('build.askuploadplan')</h2>
            <div class="col-sm-3">
            
            <div class="form-group">
            <input type="file" id="plan_permission" class="form-control" name="plan_permission" required>

            </div>
            </div>
          </div>
          <div class="row" id="planquestion" style="display:none;">
            <h2>@lang('build.askarea')</h2>
            <h6>@lang('build.askarea_note')</h6>
          <div class="col-sm-3">
            
            <div class="form-group">
            <input type="number" id="area" class="form-control" value="0" min="0" name="area">

            </div>
            </div>
          </div>

            <div class="row" id="planselection" style="display:none;">
                @include('webapp.build.plan')
          <br>
              <div class="table-responsive" id="list_de_trucs">
            <h2>@lang('build.typeplan_option')</h2>
                <table class="tableaupm" style="width: 100%">
                  <tr >
                      <td id="listings-items-plan">
                        @include('webapp.build.ajax_plan')
                      </td>
                  </tr>
               </table>
             </div>
           </div>
         </div>
            {{-- Tab 4 --}}
            <div class="tab"><h2>@lang('build.summary_project_title')</h2><br />
                <br>
                <div class="form-group" >
                    <h2>Do you have a down payment for your project?</h2>
                    <label><input type="radio" class="form-control" value="yes" id="summary1" name="summary">@lang('build.oui')</label>
                    <br>
                    <label><input type="radio" class="form-control"  value="no" id="summary2" name="summary">@lang('build.non')</label>
                </div>
                <div class="row">
                <div class=" " id="calculation" style="display: none;">
                    <input type="hidden" id="income" class="form-control" value="1" min="1" name="income" required>

                    {{--<h1 style="text-align: center">Summary</h1>--}}
                    {{--<p>Base on your selection here is the summary of your project</p>--}}
                    @include('webapp.build.buid_now_styles')
                    {{--<ul>--}}
                        {{--<li>Amount of the land selected: AMOUNT_LAND =  <b><span id="amount_land"></span>&#128;</b></li>--}}
                        {{--<li>Amount of the plan selected: AMOUNT_PLAN =  <b><span id="amount_plan"></span>&#128;</b></li>--}}
                        {{--<li>Notary fees : NATARY_FEE (NF)= <b><span id="notary"></span>&#128;</b></li>--}}
                        {{--<li>Bank charges : BANK_CHARGES (BC) = <b><span id="bank_charges"></span>&#128;</b></li>--}}
                        {{--<li>FIRST_SUM = <b><span id="f_sum"></span>&#128;</b></li>--}}
                        {{--<li>TOTAL CHARGE = <b><span id="t_sum"></span>&#128;</b></li>--}}
                    {{--</ul>--}}

                </div>
                </div>

            </div>
            {{-- Tab 5 --}}
         <div class="tab"><h2>@lang('build.typebudget')</h2><br />

          <div class="row selectable" >
            {{--<select class="image-picker masonry show-html" name="budget">--}}
              {{--@if ( Config::get('app.locale') == 'fr')--}}
              {{--<option data-img-src="{{asset('assets/webapp/img/button/opt1.png')}}" value="1">Option 1</option>--}}
              {{--<option data-img-src="{{asset('assets/webapp/img/button/opt2.png')}}" value="2">Option 2</option>--}}
              {{--<option data-img-src="{{asset('assets/webapp/img/button/opt3.png')}}"  value="3">Option 3</option>--}}
              {{--<option data-img-src="{{asset('assets/webapp/img/button/opt4.png')}}"  value="4">Option 4</option>--}}
              {{--<option data-img-src="{{asset('assets/webapp/img/button/opt5.png')}}"  value="5">Option 5</option>--}}
              {{--<option data-img-src="{{asset('assets/webapp/img/button/opt6.png')}}"  value="6">Option 6</option>--}}
              {{--@else--}}
              {{--<option data-img-src="{{asset('assets/webapp/img/button/opt1en.png')}}" value="1">Option 1</option>--}}
              {{--<option data-img-src="{{asset('assets/webapp/img/button/opt2en.png')}}" value="2">Option 2</option>--}}
              {{--<option data-img-src="{{asset('assets/webapp/img/button/opt3en.png')}}"  value="3">Option 3</option>--}}
              {{--<option data-img-src="{{asset('assets/webapp/img/button/opt4en.png')}}"  value="4">Option 4</option>--}}
              {{--<option data-img-src="{{asset('assets/webapp/img/button/opt5en.png')}}"  value="5">Option 5</option>--}}
              {{--<option data-img-src="{{asset('assets/webapp/img/button/opt6en.png')}}"  value="6">Option 6</option>--}}
              {{--@endif--}}
            {{--</select>--}}

                  <div class="col-md-3 ui-widget-content " id="contract1" style="padding: 30px;text-align: center;height: 290px!important;">
                      <img class="imagetb" src="{{asset('assets/img/contract1.jpg')}}">
                      <span><b>@lang('build.contract1')</b></span></div>
                  <div class="col-md-3 ui-widget-content " id="contract2" style="padding: 49px;text-align: center;height: 290px!important;">
                      <img class="imagetb" src="{{asset('assets/img/contract2.jpg')}}">
                      <b>@lang('build.contract2')</b></div>
                  <div class="col-md-3 ui-widget-content " id="contract3" style="padding: 50px;text-align: center;height: 290px!important;">
                      <img class="imagetb" src="{{asset('assets/img/contract3.jpg')}}">
                      <b>@lang('build.contract3')</b></div>
                    <input type="hidden" name="contract_text" id="contract_text">
          </div>
          <br>
          <div class="form-group" id="test1">
            {{--<h2>@lang('build.buildingpermit')</h2>--}}
            {{--<label><input type="radio" class="form-control" value="yes" id="permit1" name="permit">@lang('build.oui')</label>--}}
            {{--<br>--}}
            {{--<label><input type="radio" class="form-control"  value="no" id="permit2" name="permit">@lang('build.non')</label>--}}
             {{--<a href="google.com" style="display:none;"></a> --}}
            {{--<h6 id="ask_upload" style="display:none;">@lang('build.ask_upload')</h6>--}}
            {{--<input type="file" class="form-control" id="permit_file" name="permit_file" style="display:none;">--}}
          </div>

            <div class="form-group" id="test2" style="display:none;">
            <h2>@lang('build.funding')</h2>
            <h6><i>@lang('build.notefunding')</i></h6>
            <label><input type="radio" class="form-control" name="fund" value="yes">@lang('build.oui')</label>
            <br>
            <label><input type="radio" class="form-control" name="fund" value="no">@lang('build.non')</label>

          </div>
            <div class="form-group" id="test3" style="display:none;">
            <h2>@lang('build.submit_income')</h2>
            {{-- <h6><i>@lang('build.notefunding')</i></h6> --}}
          <div class="col-sm-3">  
            <div class="input-group">
            </div>
            </div>
          </div>
        </div>
            {{-- Tab 6 --}}
            <div class="tab">
                <div class="form-group">
                    <h2>@lang('build.financing_team')</h2>
                    <label><input type="radio" class="form-control" id="income1" name="askincome" value="yes">@lang('build.oui')</label>
                    <br>
                    <label><input type="radio" class="form-control" id="income2" name="askincome" value="no">@lang('build.non')</label>

                </div>
            </div>
            <div class="tab">
                <div class="form-group">
                    <h2>@lang('build.construction_project_message')</h2>
                    <div class="checkbox">
                        <label>
                            <div class="ez-checkbox ez-checked"><input type="checkbox" name="argeement" class="ez-hide"></div><b> @lang('build.checkbox_agreement_message')</b></label>
                    </div>
                </div>
            </div>
        <br>
        <div style="overflow:auto;">
          <div style="float:right;">
            <button type="button" id="prevBtn" class="btn btn-primary" onclick="nextPrev(-1)">@lang('general.permit_submit_bt_previous')</button>
            <button type="button" id="nextBtn"  class="btn btn-primary"onclick="nextPrev(1)">@lang('general.permit_submit_bt_next')</button>
          </div>
        </div>

        <!-- Circles which indicates the steps of the form: -->
        <div style="text-align:center;margin-top:40px;">
          <span class="step"></span>
          <span class="step"></span>
          <span class="step"></span>
          <span class="step"></span>
          <span class="step"></span>
          <span class="step"></span>
          <span class="step"></span>


        </div>
      </form>
    </div>

  </div>
</div><!-- /.content -->
</div><!-- /.main-inner -->
</div><!-- /.main -->
</div><!-- /.main-wrapper -->

@include("webapp.includes.call-to-action-footer")

@endsection
<style>
    .box-style{
        margin-top: 5%;
        padding-top: 20px;
        padding-bottom: 20px;
        border: 2px rgba(109, 109, 109, 0.72) solid;
        border-radius: 10px;
    }
    .selectable .ui-selecting { background: #43e555; }
    .selectable .ui-selected { background: #39B54A; color: white; }
    .selectable { list-style-type: none; margin: 0; padding: 0; width: 100%; }
    .selectable div { margin-left: 30px; font-size: 1.8em; min-height: 210px; }
</style>
@section('script')
  <script>
      $(window).on('hashchange', function() {
          if (window.location.hash) {
              var page = window.location.hash.replace('#', '');
              if (page == Number.NaN || page <= 0) {
                  return false;
              }else{
                  getData(page);
              }
          }
      });
      // $('.getLand').change(function (){
      //
      // });


      $(document).ready(function()
      {
          $( function() {
              $( "#selectable" ).selectable();
          } );

          function calculations()
          {
              var land_price = parseFloat(localStorage.getItem('land_price'));
              var plan_price = parseFloat(localStorage.getItem('plan_price'));
              var notary = ((10*localStorage.getItem('land_price'))/100);
              var sum = land_price + plan_price;
              var bank_charges = (15*sum)/1000;
              var f_sum = land_price+plan_price+notary+bank_charges;
              var t_sum = f_sum-($('#down_payment').val());
              $('#amount_land').text(land_price);
              $('#amount_plan').text(plan_price);
              $('#notary').text(notary);
              $('#bank_charges').text(bank_charges);
              $('#f_sum').text(f_sum);
              $('#t_sum').text(t_sum);
              $('#income').val(t_sum);
          }
          $('#down_payment').keyup(function() {
                calculations();
              // alert(this.value);
          });

          $(document).on('click', '#listings-items .pagination a',function(event)
          {
              $('li').removeClass('active');
              $(this).parent('li').addClass('active');
              event.preventDefault();
              var myurl = $(this).attr('href');
              var page=$(this).attr('href').split('page=')[1];
              getData(page);

          });
          $(document).on('click', '#listings-items-plan .pagination a',function(event)
          {
              $('li').removeClass('active');
              $(this).parent('li').addClass('active');
              event.preventDefault();
              var myurl = $(this).attr('href');
              var page=$(this).attr('href').split('page=')[1];

              $.ajax(
                  {
                      url: '?page=' + page,
                      data: {plan:true},
                      // data:{price_to:price_to,price_from:price_from,category:category,type:type,area:area,lat:lat,long:long},
                      type: "get",
                      datatype: "html",
                  })
                  .done(function(data)
                  {
                      $("#listings-items-plan").empty().html(data);
                      location.hash = 1;
                  })
                  .fail(function(jqXHR, ajaxOptions, thrownError)
                  {
                      alert('No response from server');
                  });

          });

          $('#plan_search').click(function (event) {
              var price_from    =   $('#plan_price_from').val();
              var price_to      =   $('#plan_price_to').val();
              var area          =   $('#plan_area').val();
              $.ajax(
                  {
                      url: '?page=' + 1,
                      data:{price_to:price_to,price_from:price_from,area:area,plan:true},
                      type: "get",
                      datatype: "html",
                  })
                  .done(function(data)
                  {
                      $("#listings-items-plan").empty().html(data);
                      location.hash = 1;
                  })
                  .fail(function(jqXHR, ajaxOptions, thrownError)
                  {
                      alert('No response from server');
                  });
          });

          $('#search_now').click(function (event) {
              var price_from    =   $('#price_from').val();
              var price_to      =   $('#price_to').val();
              var category      =   $('#category').val();
              var type          =   $('#type').val();
              var area          =   $('#area').val();
              var lat           =   $('#address_lat').val();
              var long          =   $('#address_lng').val();

              // alert('lat:'+lat+', long:'+long);
              // return;
              $.ajax(
                  {
                      url: '?page=' + 1,
                      data:{price_to:price_to,price_from:price_from,category:category,type:type,area:area,lat:lat,long:long,land:true},
                      type: "get",
                      datatype: "html",
                  })
                  .done(function(data)
                  {
                      $("#listings-items").empty().html(data);
                      location.hash = 1;
                  })
                  .fail(function(jqXHR, ajaxOptions, thrownError)
                  {
                      alert('No response from server');
                  });
          });
      });
      $(document).ready(function() {
          $("#contract1").click(function(e) {
              e.stopPropagation();
              $("#contract1").css('border','4px solid #39B54A');
              $("#contract_text").val("CONTRACT OF WORKS BY BODY OF STATE");
              $("#contract2").css('border','2px solid #dddddd');
              $("#contract3").css('border','2px solid #dddddd');
              $('#nextBtn').prop('disabled',false);
          });
          $("#contract2").click(function(e) {
              e.stopPropagation();
              $("#contract2").css('border','4px solid #39B54A');
              $("#contract_text").val("CCMI CONTRACT");
              $("#contract1").css('border','2px solid #dddddd');
              $("#contract3").css('border','2px solid #dddddd');
              $('#nextBtn').prop('disabled',false);
          });
          $("#contract3").click(function(e) {
              e.stopPropagation();
              $("#contract3").css('border','4px solid #39B54A');
              $("#contract_text").val("GENERAL CONTRACT");
              $("#contract1").css('border','2px solid #dddddd');
              $("#contract2").css('border','2px solid #dddddd');
              $('#nextBtn').prop('disabled',false);
          });



      });
      function getData(page){
          var price_from    =   $('#price_from').val();
          var price_to      =   $('#price_to').val();
          var category      =   $('#category').val();
          var type          =   $('#type').val();
          var area          =   $('#area').val();
          var lat           =   $('#address_lat').val();
          var long          =   $('#address_lng').val();

          $.ajax(
              {
                  url: '?page=' + page,
                  data:{price_to:price_to,price_from:price_from,category:category,type:type,area:area,lat:lat,long:long,land:true},
                  type: "get",
                  datatype: "html",
              })
              .done(function(data)
              {
                  $("#listings-items").empty().html(data);
                  location.hash = page;
              })
              .fail(function(jqXHR, ajaxOptions, thrownError)
              {
                  alert('No response from server');
              });
      }
  </script>
<script type="text/javascript" src="{{ asset('assets/webapp/js/build.js') }}"></script>
<script type="text/javascript">
  $("select").imagepicker();
</script>
<script type="text/javascript">
var counter = 0;
//this script for on click in radio button when choosing house
    $("input[name='house']").click(function(){
      $('#nextBtn').prop('disabled',false);

  });


$("input[name='argeement']").click(function(){
    // var abc = $("input[name='argeement']:checked").val();
    if($(this).prop("checked") == true)
    {
        $('#nextBtn').prop('disabled',false);
    }

  });


//this script for on click in radio button when choosing land
  $("input[name='askland']").click(function(){
    var abc = $("input[name='askland']:checked").val();

    if(abc== 'no'){   
      $('#landselection').show('slow');
      $('#land_upload').hide('slow');
      $("input[name='land']").prop('required',true);
      $("input[name='land_permission']").prop('required',false);
       $('#nextBtn').prop('disabled',true);
    }
    else {
      $('#landselection').hide('slow');
      $('#land_upload').show('slow');
      $("input[name='land']").prop('required',false);
      $("input[name='land_permission']").prop('required',true);
      $("input[name='land']").prop('checked',false);
       // $('#nextBtn').prop('disabled',false);
    }
  });

  $("input[name='land_permission']").change(function(){
      if($('#land_permission').get(0).files.length === 0){
       $('#nextBtn').prop('disabled',true);
      }
      else{
       $('#nextBtn').prop('disabled',false);
      }
  });
  //this script for on click in radio button when choosing land

  function checkLand(amount){
      var land  =  amount;
      localStorage.setItem('land_price',land);
    $('#nextBtn').prop('disabled',false);
  };

  //this script for on click in radio button when choosing plan
  $("input[name='askplan']").click(function(){
    var abc = $("input[name='askplan']:checked").val();
      $('#planquestion').show('slow');


    if(abc== 'no'){   
      $('#planselection').show('slow');
      $("input[name='plan']").prop('required',true);
       $('#nextBtn').prop('disabled',true);
       $('#planquestion2').hide('slow');
       $('#plan_permission').prop('required',false);
    }
    else {
      $('#planquestion2').show('slow');
      $('#plan_permission').prop('required',true);
      $('#planselection').hide('slow');
      $("input[name='plan']").prop('required',false);
      $("input[name='plan']").prop('checked',false);


    }
  });
  //this script for on click in radio button when choosing plan
  function checkPlan(amount){
      localStorage.setItem('plan_price',amount);
      var land_price = parseFloat(localStorage.getItem('land_price'));
      var plan_price = parseFloat(localStorage.getItem('plan_price'));
      var notary = ((10*localStorage.getItem('land_price'))/100);
      var sum = land_price+plan_price;
      var bank_charges = (15*sum)/1000;
      var f_sum = land_price+plan_price+notary+bank_charges;
      var t_sum = f_sum-($('#down_payment').val());
      $('#amount_land').text(land_price);
      $('#amount_plan').text(plan_price);
      $('#notary').text(notary);
      $('#bank_charges').text(bank_charges);
      $('#f_sum').text(f_sum);
      $('#t_sum').text(t_sum);
      $('#income').val(t_sum);
    $('#nextBtn').prop('disabled',false);
  }
  $("input[name='plan_permission']").change(function(){
    if ($('#plan_permission').get(0).files.length === 0) {

    $('#nextBtn').prop('disabled',true);
}
else {
    $('#nextBtn').prop('disabled',false);

}
  });
  var number = document.getElementById('area');

// Listen for input event on numInput.
number.onkeydown = function(e) {
    if(!((e.keyCode > 95 && e.keyCode < 106)
      || (e.keyCode > 47 && e.keyCode < 58) 
      || e.keyCode == 8)) {
        return false;
    }
}
  var number2 = document.getElementById('income');

// Listen for input event on numInput.
number2.onkeydown = function(e) {
    // $('#nextBtn').prop('disabled',false);
        $('#nextBtn').prop('disabled',false);
    if(!((e.keyCode > 95 && e.keyCode < 106)
      || (e.keyCode > 47 && e.keyCode < 58) 
      || e.keyCode == 8)) {
        $('#nextBtn').prop('disabled',false);
        return false;
    }

}
number2.onkeyup = function(e){
setTimeout(func, 1000);
function func() {
   if(number2.value == 0) {
    return number2.value=1;
   }
}
}

$("input[name='summary']").click(function(){
    var abc = $("input[name='summary']:checked").val();
    if(abc=='yes')
    {
        $('#nextBtn').prop('disabled',false);
        $('#calculation').show('slow');
        $('#down_pay_label').show('slow');
        $('#down_payment').val(0);
        calculations();

    }
    else{
        $('#calculation').show('slow');
        $('#down_pay_label').hide('slow');
        $('#down_payment').val(0);
        var land_price = parseFloat(localStorage.getItem('land_price'));
        var plan_price = parseFloat(localStorage.getItem('plan_price'));
        var notary = ((10*localStorage.getItem('land_price'))/100);
        var sum = land_price + plan_price;
        var bank_charges = (15*sum)/1000;
        var f_sum = land_price+plan_price+notary+bank_charges;
        var t_sum = f_sum-0;
        $('#amount_land').text(land_price);
        $('#amount_plan').text(plan_price);
        $('#notary').text(notary);
        $('#bank_charges').text(bank_charges);
        $('#f_sum').text(f_sum);
        $('#t_sum').text(t_sum);
        $('#income').val(t_sum);
        $('#nextBtn').prop('disabled',false);
    }
});

//this script for on click in radio button when choosing building permit
  $("input[name='permit']").click(function(){
    var abc = $("input[name='permit']:checked").val();

    if(abc== 'no'){   
      $('#permit_file').hide('slow');
      $('#ask_upload').hide('slow');
      $('#permit_file').prop('required',false);
      $("#modal-alert1").modal('show'); 

    }
    else {
      $('#permit_file').show('slow');
      $('#ask_upload').show('slow');
      $('#permit_file').prop('required',true);
     }
    }); 

//this script for on click submit proceed button on modal alert
  $("#submitmodal").click(function(){

    // $("input[name='permit']").val('yes').prop("checked",true);
    // $("input[name='permit']").prop("checked",false);
    $("#test1").hide();
    $("#test1").show('slow');

    // $("input[name='permit']").prop("checked",true).prop('value','yes');
    // $("#permit2").prop("checked",false);
    // $("#permit1").prop("checked",true);
    $('#modal-alert1').modal('hide');
    var win = window.open('{{url('make-a-request')}}', '_blank');
    if (win) {
      //Browser has allowed it to be opened
      win.focus();
    } else {
        //Browser has blocked it
        alert('Please allow popups for this website');
      }
  });

//this script for on click submit proceed button on modal alert2
  $("#submitmodal2").click(function(){
    // $("input[name='askincome']").prop("checked",false);
    $('#modal-alert2').modal('hide');
    $("#nextBtn").prop('disabled',false);
    // $("input[name='askincome']").prop("checked",true);
    var win = window.open('{{url('/bank-financial')}}', '_blank');
    if (win) {
      //Browser has allowed it to be opened
      win.focus();
    } else {
        //Browser has blocked it
        alert('Please allow popups for this website');
      }
  });
//this script for on click in radio button when choosing personal income
  $("input[name='askincome']").click(function(){
    var abc = $("input[name='askincome']:checked").val();
      $("input[name='permit']").prop('checked',false);
    if(abc== 'no'){   
      // $('#modal-alert2').modal('show');
      // $('#test3').hide('slow');
      $('#nextBtn').prop('disabled',false);
      // $("input[name='income']").val(1);
    }
    else {
        $('#nextBtn').prop('disabled',false);
        var win = window.open('{{url('/bank-financial')}}', '_blank');
        if (win) {
            //Browser has allowed it to be opened
            win.focus();
        } else {
            //Browser has blocked it
            alert('Please allow popups for this website');
        }

    }
  });
//this script for on click in radio button when choosing apply funding
  $("#closemodal").click(function(){   
      window.open('{{url('')}}', '_self');
    
  });
  //this script for closing modal

  $("#close").click(function(){
    window.open('{{url('/view-build-now')}}', '_self');
  });


  //this script for ajax post when submit button clicked
  $("#nextBtn").click(function(){
    counter++ ;
    if(counter==7 ){
    var form = new FormData($("#regForm")[0]);
    $.ajax({
      /* the route pointing to the post function */
      url: '/build-form/add',
      type: 'POST',
      /* send the csrf-token and the input to the controller */
      headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
      // data : { "house" : $("input[name='house']").val() ,"askland" : $("input[name='askland']:checked").val(), "askplan" : $("input[name='askplan']:checked").val(),"permit" : $("input[name='permit']:checked").val(), "land" : $("input[name='land']").val(), "land_permission" : $("input[name='land_permission']").val(), "permit_file" : $("input[name='permit_file']").val(), "budget" : $("input[name='budget']").val(), "area" : $("input[name='area']").val(), "income" : $("input[name='income']").val(), "askincome" : $("input[name='askincome']:checked").val() },
      data : form,
      dataType: 'JSON',
      processData: false,
      contentType: false,
      /* remind that 'data' is the response of the AjaxController */
      success: function (data) { 
        $("#modal-success").modal('show');
      }
    }); 
    
    }
  });
  $("#prevBtn").click(function(){
    counter--;
  });


</script>
@endsection

