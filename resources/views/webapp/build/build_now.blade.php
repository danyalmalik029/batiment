@extends("webapp.layouts.default")

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">

@endsection

@section('content')

    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">

                        <div class="box-style col-md-offset-3 col-md-6 ">
                            <h1 style="text-align: center">{{$title}}</h1>
                            <p>{{$text_1}}</p>
                            <p>{{$text_2}}</p>
                            <p>{{$text_3}}</p>
                            <a class="aligncenter btn btn-primary" href="{{route('view-build-now')}}"><b>{{$button_text}} !</b></a>
                        </div>


            </div>
        </div>
    </div>
    <style>
        .box-style{
            margin-top: 5%;
            padding-top: 20px;
            padding-bottom: 20px;
            border: 2px rgba(140, 140, 140, 0.46) solid;
            border-radius: 10px;
        }
    </style>
    @include("webapp.includes.call-to-action-footer")

@endsection

@section('script')


@endsection