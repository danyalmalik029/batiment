<div class="col-xs-12" style="border: 2px rgba(0,0,0,0.49) solid;border-radius: 5px">
    <div class="map-filter-horizontal">
        <div class="map-filter filter" style="margin-bottom: 20px;">
            <div class="row">

                <div class="form-group col-md-4">
                    <label>@lang('properties.area') (m²)</label>

                    <input type="text" class="form-control" name="plan_area" placeholder="Ex : xx.xx" id="plan_area">
                </div><!-- /.form-group -->

                <div class="col-md-4">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label> @lang('general.general_price_from')</label>
                            <input type="text" class="form-control" id="plan_price_from"  name="plan_minPrice" value="0" >
                        </div><!-- /.form-group -->

                        <div class="form-group col-md-6">
                            <label> @lang('general.general_price_to')</label>
                            <input type="text" class="form-control" id="plan_price_to"  name="plan_maxPrice" value="" >
                        </div><!-- /.form-group -->
                    </div><!-- /.row -->
                </div><!-- /.col-* -->

            {{----}}
                <div class="col-md-4">
                    <div class="form-group-btn form-group-btn-placeholder-gap">
                        <button type="button" id="plan_search" class="btn btn-primary btn-block"> @lang('general.general_search_now')</button>
                    </div><!-- /.form-group -->
                </div><!-- /.col-* -->
            </div>
            {{--</div><!-- /.row -->--}}
        </div><!-- /.filter -->
    </div><!-- /.filter-wrap-->
</div>