@extends("webapp.layouts.default")

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">

@endsection

@section('content')

<div class="main-wrapper">
   <div class="main">
       <div class="main-inner">

           <div class="content-title">
              <div class="content-title-inner">
                 <div class="container">		
                    <h1>{{$title}}</h1>
                </div><!-- /.container -->
            </div><!-- /.content-title-inner -->
        </div><!-- /.content-title -->

        <br>
        <div class="content">
           <div class="container">
            <div class="row">
                <div class="col-sm-12">
                @if ( Config::get('app.locale') == 'fr')
                    <img src="{{asset('assets/img/etb-fr-build.jpg')}}" class="img-fluid" >
                @else
                    <img src="{{asset('assets/img/etb-en-build.jpg')}}" class="img-fluid" >
                @endif
                </div> 
            </div>


            <div style="overflow:auto;">
                <div style="float:right;">
                    <a href="{{ url('/build-form/') }}" class="btn btn-primary">@lang('general.general_proceed')</a>
                </div>
            </div>

        </div><!-- /.container -->
    </div><!-- /.content -->
</div><!-- /.main-inner -->
</div><!-- /.main -->
</div><!-- /.main-wrapper -->

@include("webapp.includes.call-to-action-footer")

@endsection

@section('script')


@endsection

