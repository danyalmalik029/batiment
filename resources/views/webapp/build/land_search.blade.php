<div class="col-xs-12" style="border: 2px rgba(0,0,0,0.49) solid;border-radius: 5px">
    <div class="map-filter-horizontal">
        <div class="map-filter filter" style="margin-bottom: 20px;">
            <div class="row">
                <div class="form-group col-md-3">
                    <label>@lang('general.general_property_categorie')</label>

                    <select class="form-control" name="category" id="category" >
                        @foreach($categories as $cat)
                            <option value="{{ $cat->category_slug }}">@lang('general.ads_category_' . $cat->category_slug)</option>
                        @endforeach
                    </select>
                </div><!-- /.form-group -->

                <div class="form-group col-md-3">
                    <label>@lang('general.general_contract')</label>

                    <select class="form-control" name="type" id="type">
                        <option value="rent" > @lang('general.general_rent')</option>
                        <option value="buy" > @lang('general.general_buy')</option>
                        <option value="sell" > @lang('general.general_sell')</option>
                    </select>
                </div><!-- /.form-group -->
                <div class="form-group col-md-3">
                    <label>@lang('properties.area') (m²)</label>

                    <input type="text" class="form-control" name="area" placeholder="Ex : xx.xx" id="area">
                </div><!-- /.form-group -->

                <div class="col-md-3">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label> @lang('general.general_price_from')</label>
                            <input type="text" class="form-control" id="price_from"  name="minPrice" value="0" >
                        </div><!-- /.form-group -->

                        <div class="form-group col-md-6">
                            <label> @lang('general.general_price_to')</label>
                            <input type="text" class="form-control" id="price_to"  name="maxPrice" value="5000" >
                        </div><!-- /.form-group -->
                    </div><!-- /.row -->
                </div><!-- /.col-* -->
            </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>@lang('properties.address_autocomplete_label')</label>

                            <input type="text" class="form-control" id="autocomplete" onFocus="geolocate()" placeholder="@lang('properties.address_autocomplete_placeholder')">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>@lang('properties.address_street_number')</label>

                            <input readonly="true" type="text" class="form-control" id="street_number" name="address_street_number" required style="<?php if($errors->first('address_street_number')){?> border: 1px solid red !important; <?php } ?>"  >

                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>@lang('properties.address_steet_name')</label>

                            <input readonly="true" type="text" class="form-control" id="route" name="address_steet_name" required style="<?php if($errors->first('address_steet_name')){?> border: 1px solid red !important; <?php } ?>"  >


                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>@lang('properties.address_apt')</label>

                            <input type="text" class="form-control" id="address_apt" name="address_apt" style="<?php if($errors->first('address_apt')){?> border: 1px solid red !important; <?php } ?>"  >


                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>@lang('properties.address_city')</label>

                            <input readonly="true" type="text" class="form-control" id="locality" name="address_city" required style="<?php if($errors->first('address_city')){?> border: 1px solid red !important; <?php } ?>" >

                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>@lang('properties.address_region')</label>

                            <input readonly="true" type="text" class="form-control" id="administrative_area_level_1" name="address_region" required style="<?php if($errors->first('address_region')){?> border: 1px solid red !important; <?php } ?>" >


                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>@lang('properties.address_zip_code')</label>

                            <input readonly="true" type="text" class="form-control" id="postal_code" name="address_zip_code" required style="<?php if($errors->first('address_zip_code')){?> border: 1px solid red !important; <?php } ?>" >


                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>@lang('properties.address_country')</label>

                            <input readonly="true" type="text" class="form-control" id="country" name="address_country" required style="<?php if($errors->first('address_country')){?> border: 1px solid red !important; <?php } ?>">


                        </div>
                    </div>

                    <input type="hidden" name="address_lat" id="address_lat" />
                    <input type="hidden" name="address_lng" id="address_lng"/>
                </div>
                <div class="col-md-2">
                    <div class="form-group-btn form-group-btn-placeholder-gap">
                        <button type="button" id="search_now" class="btn btn-primary btn-block"> @lang('general.general_search_now')</button>
                    </div><!-- /.form-group -->
                </div><!-- /.col-* -->
            {{--</div><!-- /.row -->--}}
        </div><!-- /.filter -->
    </div><!-- /.filter-wrap-->
</div>