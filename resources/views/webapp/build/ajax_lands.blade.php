
            @foreach($lands as $land)
                <div class="col-md-4">
                    <div class="listing-box">
                        <div class="listing-box-image" style="

                        @if(count($land->images) != 0)
                                background-image: url('{{ asset("storage/propertiesImages/" . $land->images[0]->image_name) }}')
                        @else
                                background-image: url('{{ asset("assets/img/tmp/tmp-5.jpg") }}')
                        @endif
                                ">
                          <span class="listing-box-image-links">
                            @if(Auth::check() && $land->user_id == Auth::id())
                                  <a href="{{ url('/edit-property/'.$land->id) }}" style="@if($land->statut == '0')background: #d9534f !important;@endif"><i class="fa fa-pencil-square-o"></i> <span>@lang('properties.edit_property')</span></a>
                              @endif

                              <a href="{{ url('/view-details-property/'.$land->id) }}" target="_blank" style="@if($land->statut == '0')background: #d9534f !important;@endif"><i class="fa fa-search"></i> <span>@lang('properties.view_detail')</span></a>



                              @if(Auth::check() && $land->user_id == Auth::id())
                                  @if($land->statut == '0')
                                      <a href="{{ url('/delete-property/'.$land->id) }}" style="background: #d9534f !important;" class="enableProperty" value="{{$land->id}}" name="{{$land->title}}"><i class="fa fa-ban"></i> <span>@lang('properties.enable_property')</span></a>
                                      <a href="{{ url('/delete-property/'.$land->id) }}" style="background: #d9534f !important;" class="deleteProperty" value="{{$land->id}}" name="{{$land->title}}"><i class="fa fa-ban"></i> <span>@lang('properties.delete_property')</span></a>
                                  @else
                                      <a href style="@if($land->statut == '0')background: #d9534f !important;@endif" class="disableProperty" value="{{$land->id}}" name="{{$land->title}}"><i class="fa fa-ban"></i> <span>@lang('properties.disable_property')</span></a>
                                  @endif
                              @endif
                          </span>
                        </div><!-- /.listing-box-image -->

                        <div class="listing-box-title" style="@if($land->statut == '0') background-color: #d9534f !important; @endif">
                            <h2><a href="{{ url('/edit-property/'.$land->id) }}">{{$land->title}}</a></h2>
                            <h3>{{$land->price}} &#128;</h3>
                        </div><!-- /.listing-box-title -->
                        <div class="listing-box-content">
                            <dl>
                                <dt>@lang('properties.category')</dt><dd>@lang('general.ads_category_' . \App\Models\Categories::find($land->catid)->category_slug)</dd>
                                <dt>@lang('properties.type')</dt><dd>@lang('properties.type_' . $land->ad_dtype)</dd>
                                <dt>@lang('properties.area')</dt><dd>{{$land->area}} (m²)</dd>
                            </dl>
                        </div><!-- /.listing-box-cotntent -->
                        <div class="listing-boxes-colored">
                            <dl>
                                <dt>@lang('properties.choose')</dt><dd><input type="radio" class="getLand" onclick="checkLand({{$land->price}})" data-price="{{$land->price}}" name="land" id="{{$land->title}}" value="{{$land->id}}"   oninput="this.className = ''" ></dd>

                            </dl>
                        </div>
                    </div><!-- /.listing-box -->
                </div><!-- /.col-* -->
            @endforeach
            {!! $lands->render() !!}
