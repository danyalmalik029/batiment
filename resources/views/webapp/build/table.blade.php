{{--@extends("webapp.layouts.default")--}}

{{--@section('css')--}}
{{--<meta name="csrf-token" content="{{ csrf_token() }}">--}}
    {{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css" rel="stylesheet" type="text/css"> --}}
    {{--<link href="{{ asset('assets/admin/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">--}}
    {{--<link href="{{ asset('assets/admin/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">--}}
{{--@endsection--}}

{{--@section('content')--}}

{{--<div class="main-wrapper">--}}
   {{--<div class="main">--}}
       {{--<div class="main-inner">--}}

           {{--<div class="content-title">--}}
              {{--<div class="content-title-inner">--}}
                 {{--<div class="container">		--}}
                    {{--<h1>{{$title}}</h1>--}}
                {{--</div><!-- /.container -->--}}
            {{--</div><!-- /.content-title-inner -->--}}
        {{--</div><!-- /.content-title -->--}}

        {{--<br>--}}
        {{--<div class="content">--}}
           {{--<div class="container">--}}

              {{----}}
                {{--<table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">--}}
                  {{--<thead>--}}
                    {{--<tr>--}}
                      {{--<th>No</th>--}}
                      {{--<th>Username</th>--}}
                      {{--<th>Construction Type</th>--}}
                      {{--<th>Land</th>--}}
                      {{--<th>Plan</th>--}}
                      {{--<th>Area</th>--}}
                      {{--<th>Budget</th>--}}
                      {{--<th>Building Permit</th>--}}
                      {{--<th>Building Income</th>--}}
                      {{--<th>Date Submitted</th>--}}
                    {{--</tr>--}}
                  {{--</thead>--}}
                  {{--<tbody>--}}
                  {{--@foreach ($projects as $key=>$project)--}}
                    {{-- expr --}}
                    {{--<tr>--}}
                      {{--<td>{{$key+1}}</td>--}}
                      {{--<td>{{$project->user->name}}</td>--}}
                      {{--<td>{{$project->construction_type}}</td>--}}
                      {{--<td>--}}
                        {{--@if($project->land_id <> 0)--}}
                        {{--<a href="{{url('view-details-property/'.$project->land_id)}}">{{$project->land->title}}</a>--}}
                        {{--@else--}}
                        {{--<a href="{{url('downloadfile/'.$project->land_permission)}}">Download Land Permission</a>--}}
                        {{--@endif--}}
                      {{--</td>--}}
                      {{--<td>--}}
                        {{--@if($project->plan_id <> 0)--}}
                        {{--<a href="{{url('view-details-plan/'.$project->plan_id)}}">{{$project->plan->title}}</a>--}}
                        {{--@else--}}
                        {{--<a href="{{url('downloadfile/'.$project->plan_permission)}}">Download Project Plan</a>--}}
                        {{--@endif--}}
                      {{--</td>--}}
                      {{--<td>--}}
                        {{--@if($project->area == 0)--}}
                        {{--Don't Know The Area Size--}}
                        {{--@else--}}
                        {{--{{$project->area}} m<sup>2</sup>--}}
                        {{--@endif--}}
                      {{--</td>--}}
                      {{--<td>--}}
                        {{--@if($project->budget == 1)--}}
                        {{--Don't know budget yet--}}
                        {{--@elseif($project->budget == 2)--}}
                        {{--Less Than 100 000 €--}}
                        {{--@elseif($project->budget == 3)--}}
                        {{--101 000 € - 150 000 €--}}
                        {{--@elseif($project->budget == 4)--}}
                        {{--151 000 € - 200 000 €--}}
                        {{--@elseif($project->budget == 5)--}}
                        {{--201 000 € - 300 000 €--}}
                        {{--@else--}}
                        {{--More Than 300 000 €--}}
                        {{--@endif--}}

                      {{--</td>--}}
                      {{--<td>--}}
                        {{--@if($project->building_permit == 0)--}}
                        {{--Using ETB Help to get permission--}}
                        {{--@else--}}
                        {{--<a href="{{url('downloadfile/'.$project->building_permit)}}">Download Building Permission</a>--}}
                        {{--@endif--}}
                      {{--</td>--}}
                      {{--<td>--}}
                        {{--@if($project->income == 0)--}}
                        {{--Using ETB Help to get money--}}
                        {{--@else--}}
                        {{--{{$project->income}}--}}
                        {{--@endif--}}
                      {{--</td>--}}
                      {{--<td>{{$project->created_at->format('d, F Y')}}</td>--}}
                    {{--</tr>--}}
                  {{--@endforeach--}}
                  {{--</tbody>--}}
                {{--</table>--}}
          {{----}}


{{--             <div style="overflow:auto;">--}}
                {{--<div style="float:right;">--}}
                    {{--<a href="{{ url('/build-form/') }}" class="btn btn-primary">@lang('general.general_proceed')</a>--}}
                {{--</div>--}}
            {{--</div> --}}

        {{--</div><!-- /.container -->--}}
    {{--</div><!-- /.content -->--}}
{{--</div><!-- /.main-inner -->--}}
{{--</div><!-- /.main -->--}}
{{--</div><!-- /.main-wrapper -->--}}

{{--@include("webapp.includes.call-to-action-footer")--}}

{{--@endsection--}}

{{--@section('script')--}}

{{--<script type="text/javascript" src="{{ asset('assets/admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>--}}
{{--<script type="text/javascript" src="{{ asset('assets/admin/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>--}}
{{--<script type="text/javascript" src="{{ asset('assets/admin/plugins/datatables/dataTables.responsive.min.js') }}"></script>--}}
{{--<script type="text/javascript" src="{{ asset('assets/admin/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>--}}
{{--<script type="text/javascript">--}}
  {{--$(document).ready(function() {--}}
    {{--$('#example').DataTable({--}}
      {{--language: {--}}
        {{--paginate: {--}}
      {{--next: '&#8594;', // or '→'--}}
      {{--previous: '&#8592;' // or '←' --}}
    {{--}--}}
  {{--}--}}
    {{--});--}}
{{--} );--}}
{{--</script>--}}
{{--@endsection--}}

