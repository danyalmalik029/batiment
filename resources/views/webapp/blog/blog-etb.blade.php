@extends("webapp.layouts.default")

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">

@endsection


@section('content')

    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">
                <div class="content-title">
                    <div class="content-title-inner">
                        <div class="container">
                            <h1>{{ $pageTitle }}</h1>
                        </div><!-- /.container -->
                    </div><!-- /.content-title-inner -->
                </div>

                <div class="content">
                    <div class="container">
                        <div class="posts posts-grid">
                            <div class="row">
                                @foreach($articles as $article)
                                    <div class="col-lg-4">
                                        <div class="post">
                                            <div class="post-image">

                                                <a href="/{{ $slug }}/{{ $article->slug }}" class="post-image-link" style="background-image: url('{{ asset("storage/" . $article->image ) }}');"></a>

                                                <div class="post-author">
								<span class="post-author-image">
                                    <?php

                                    $cover_image_name = $article->userProfil;

                                    $imgSrc = (is_file('storage/' . $cover_image_name))?
                                        asset('storage/' . $cover_image_name):
                                        asset('assets/img/no-avatar-available.jpg');

                                    ?>
									<a href="/{{ $slug }}/{{ $article->slug }}" style="background-image: url('{{ $imgSrc  }}');"></a>
								</span><!-- /.post-author-image -->

                                                    <a href="/{{ $slug }}/{{ $article->slug }}">
                                                        <span class="post-author-name">{{ $article->userName }}</span>
                                                    </a>
                                                </div><!-- /.post-author -->
                                            </div><!-- /.post-image -->

                                            <div class="post-title">
                                                <h2><a href="/{{ $slug }}/{{ $article->slug }}">{{ $article->title }}</a></h2>
                                            </div><!-- /.post-title -->

                                            <div class="post-meta">
                                                <?php   /*
                                                <div class="post-meta-item">
                                                    Posted by <a href="/{{ $slug }}/{{ $article->slug }}">{{ $article->userName }}</a>
                                                </div><!-- /.post-meta-item -->
                                                */    ?>
                                                <div class="post-meta-item">
                                                    {{ substr($article->created_at, 0, 10) }}
                                                </div><!-- /.post-meta-item -->


                                            </div><!-- /.post-meta -->

                                            <div class="post-content">
                                                <p>

                                                {{ $article->excerpt }}

                                                </p>
                                            </div><!-- /.post-content -->

                                            <div class="post-read-more">
                                                <a href="/{{ $slug }}/{{ $article->slug }}">Read More <i class="fa fa-chevron-right"></i></a>
                                            </div><!-- /.post-read-more -->
                                        </div><!-- /.post-->
                                    </div><!-- /.col-* -->
                                @endforeach
                            </div><!-- /.row -->
                        </div><!-- /.posts-->

                        <div class="pagination-wrapper">

                        {!! $articles->links('vendor.pagination.bootstrap-4') !!}

                        <!-- /.pagination -->

                        </div><!-- /.pagination-wrapper -->

                    </div><!-- /.container -->

                </div><!-- /.content -->
            </div>
        </div>
    </div>
    @include("webapp.includes.call-to-action-footer")

@endsection







