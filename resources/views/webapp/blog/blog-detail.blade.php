@extends("webapp.layouts.default")


@section('meta')
    @if(isset($details['slug']))

        <!-- Twitter -->
        <meta name="twitter:card" content="Summary">
        <meta name="twitter:site" content="{{ url('/blog/' . $details['slug']) }}">
        <meta name="twitter:title" content="{{ $details['title'] }}">
        <meta name="twitter:description" content="{{ substr( strip_tags($details['body']) ,0 , 300) }}">
        <meta name="twitter:creator" content="ETB Batiment">
        <meta name="twitter:image:src" content="{{ asset("storage/" . $details['image']) }}">
        <meta name="twitter:player" content="">

        <!-- Open Graph General (Facebook & Pinterest) -->
        <meta property="og:url" content="{{ url('/blog/' . $details['slug']) }}">
        <meta property="og:title" content="{{ $details['title'] }}">
        <meta property="og:description" content="{{ substr( strip_tags($details['body']) ,0 , 300) }}">
        <meta property="og:site_name" content="ETB Batiment">
        <meta property="og:image" content="{{ asset("storage/" . $details['image']) }}">
        <meta property="fb:admins" content="">
        <meta property="fb:app_id" content="">
        <meta property="og:type" content="article">
        <meta property="og:locale" content="fr_FR">
        <meta property="og:audio" content="">
        <meta property="og:video" content="">
    @endif
@endsection


@section('content')


    <div class="content-title">
        <div class="content-title-inner">
            <div class="container">
                <h1>{{ (isset($details['title']))?$details['title']:"ETB Batiment " }}</h1>
            </div><!-- /.container -->
        </div><!-- /.content-title-inner -->
    </div>

    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">
                <div class="content">
                    <div class="container maincontent">

                        <div class="row">
                            <div class="col-xs-12 col-md-8 col-lg-9">

                                @if(count($details))
                                    @if($details['status'] === 'PUBLISHED' or (isset($user) and $user->role_id==1) or (isset($user) and $user->role_id==3))

                                        <article>
                                            <div class="article-image">
                                                <img src="{{ asset("storage/" . $details['image']) }}" class="img-full" style="width: 100%; margin-bottom: 30px;" /></div>
                                            {!! $details['body'] !!}
                                        </article>

                                        <h2>@lang('general.general_share_on'):</h2>

                                        <div class="share-buttons">
                                            <a href="http://www.facebook.com/sharer/sharer.php?u={{ url('/blog/' . $details['slug']) }}" target="_blank" class="social-popup">
                                                <i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i>
                                            </a>
                                            <a href="http://twitter.com/share?url={{ url('/blog/' . $details['slug']) }}" target="_blank" class="social-popup">
                                                <i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i>
                                            </a>
                                            <a href="https://plus.google.com/share?url={{ url('/blog/' . $details['slug']) }}" target="_blank" class="social-popup">
                                                <i class="fa fa-google-plus-square fa-2x" aria-hidden="true"></i>
                                            </a>
                                        </div>

                                    <hr />

                                        <div class="facebook-comments">
                                            <div class="fb-comments" data-href="{{ url('/blog/' . $details['slug']) }}" data-width="100%" data-numposts="5"></div>
                                        </div>

                                    @else

                                        <div class="row">
                                            <div class="warning">
                                                <h2>@lang('general.general_undermaintenance')!</h2>
                                                <hr />
                                                <a href="{{ url('/') }}" class="btn btn-secondary"><i class="fa fa-long-arrow-left"></i> @lang('general.general_backhome')</a>
                                            </div><!-- /.warning -->
                                        </div>

                                    @endif
                                @else
                                <div class="row">
                                    <div class="warning">
                                        <h1>404</h1>
                                        <hr />
                                        <a href="{{ url('/') }}" class="btn btn-secondary"><i class="fa fa-long-arrow-left"></i> Return Home</a>
                                    </div><!-- /.warning -->
                                </div>
                                @endif
                            </div>
                            <div class="col-xs-12 col-md-4 col-lg-3">

                            </div>
                        </div>

                    </div>
                </div>
            </div><!-- /.main-inner -->
        </div><!-- /.main -->
    </div><!-- /.main-wrapper -->

    @include("webapp.includes.call-to-action-footer")

@endsection