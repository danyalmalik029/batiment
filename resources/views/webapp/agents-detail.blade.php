@extends("webapp.layouts.default")

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">

@endsection


@section('content')

    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">

                <div class="content-title">
                    <div class="content-title-inner">
                        <div class="container">
                            <h1>{{ $pageTitle }}</h1>
                        </div><!-- /.container -->
                    </div><!-- /.content-title-inner -->
                </div><!-- /.content-title -->


                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="listing-detail col-md-8 col-lg-9">
                                <div class="listing-user">
                                    <div class="listing-user-image">
                                        <a href="#" style="background-image: url('{{ asset("storage/" . $agents['avatar']) }}');"></a>
                                    </div><!-- /.listing-user-image -->

                                    <div class="listing-user-title">
                                        <h2>{{ $agents['name'] }}</h2>
                                        <h3>Senior Real Estate Manager</h3>
                                    </div><!-- /.listing-user-title -->

                                    <div class="listing-user-social">
                                        <ul>
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                            <li><a href="#"><i class="fa fa-foursquare"></i></a></li>
                                        </ul><!-- /.listing-row-social -->
                                    </div><!-- /.listing-user-social -->
                                </div><!-- /.listing-user -->

                                <div class="row">
                                    <div class="col-lg-5">
                                        <div class="overview">
                                            <h2>Information</h2>

                                            <ul>
                                                <li><strong>Name</strong><span>{{ $agents['name'] }}</span></li>
                                                <li><strong>E-mail</strong><span>{{ $agents['email'] }}</span></li>

                                                <!--
                                                <li><strong>Phone</strong><span> </span></li>
                                                <li><strong>Website</strong><span> </span></li>
                                                <li><strong>Real Estate</strong><span> </span></li>
                                                -->

                                            </ul>
                                        </div><!-- /.overview -->
                                    </div><!-- col-* -->

                                    <div class="col-lg-7">
                                        <!--
                                        <h2>About Agent</h2>

                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vel sem ut magna posuere tincidunt non ut magna. Proin ac massa at arcu semper consectetur ut eget est. Praesent dignissim consectetur mollis. Mauris tristique interdum luctus. Fusce sed arcu pretium, elementum lorem sed, pellentesque ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vel sem ut magna posuere tincidunt non ut magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vel sem ut magna posuere tincidunt non ut magna.
                                        </p>
                                        -->
                                    </div><!-- /.col-* -->
                                </div><!-- /.row -->

                                <h2>Contact Agent</h2>

                                <p>
                                    In dictum fringilla sem vitae condimentum. Maecenas venenatis odio et eros venenatis, vel bibendum mauris elementum. Maecenas id lobortis tortor. Nam condimentum pulvinar odio eget tincidunt. Curabitur in mattis enim.
                                </p>

                                <div class="listing-contact-form">
                                    <div class="row">
                                        <form method="post" action="?">
                                            <div class="col-sm-5">
                                                <div class="form-group">
                                                    <label>Name</label>
                                                    <input type="text" class="form-control">
                                                </div><!-- /.form-group -->

                                                <div class="form-group">
                                                    <label>E-mail</label>
                                                    <input type="text" class="form-control">
                                                </div><!-- /.form-group -->

                                                <div class="form-group">
                                                    <label>Subject</label>
                                                    <input type="text" class="form-control">
                                                </div><!-- /.form-group -->
                                            </div><!-- /.col-* -->

                                            <div class="col-sm-7">
                                                <div class="form-group">
                                                    <label>Message</label>
                                                    <textarea class="form-control" rows="10" placeholder="Please keep your message as simple as possible."></textarea>
                                                </div><!-- /.form-group -->
                                            </div><!-- /.col-* -->

                                            <div class="col-sm-12">
                                                <div class="form-group-btn">
                                                    <button class="btn btn-primary pull-right">Send Message</button>
                                                </div><!-- /.form-group-btn -->
                                            </div><!-- /.col-* -->
                                        </form>
                                    </div><!-- /.row -->
                                </div><!-- /.listing-contact-form -->

                                <h2>Agent Assigned Properties</h2>

                                <div class="listing-box-wrapper">
                                    <div class="row">

                                        <div class="col-md-6 col-lg-4">
                                            <div class="listing-box">
                                                <div class="listing-box-image" style="background-image: url('assets/img/tmp/tmp-5.jpg')">
                                                    <div class="listing-box-image-label">Featured</div><!-- /.listing-box-image-label -->

                                                    <span class="listing-box-image-links">
                                                        <a href="properties.html"><i class="fa fa-heart"></i> <span>Add to favorites</span></a>
			                                            <a href="properties-detail-standard.html"><i class="fa fa-search"></i> <span>View detail</span></a>
                                                        <a href="properties-compare.html"><i class="fa fa-balance-scale"></i> <span>Compare property</span></a>
		                                            </span>
                                                </div><!-- /.listing-box-image -->

                                                <div class="listing-box-title">
                                                    <h2><a href="properties-detail-standard.html">Bright Island Route</a></h2>
                                                    <h3>$ 40.000</h3>
                                                </div><!-- /.listing-box-title -->

                                                <div class="listing-box-content">
                                                    <dl>
                                                        <dt>Type</dt><dd>House</dd>
                                                        <dt>Location</dt><dd>New York</dd>
                                                        <dt>Area</dt><dd>180 sqft</dd>
                                                    </dl>
                                                </div><!-- /.listing-box-cotntent -->
                                            </div><!-- /.listing-box -->
                                        </div><!-- /.col-* -->

                                        <div class="col-md-6 col-lg-4">
                                            <div class="listing-box">
                                                <div class="listing-box-image" style="background-image: url('assets/img/tmp/tmp-4.jpg')">
                                                    <div class="listing-box-image-label">Featured</div><!-- /.listing-box-image-label -->

                                                    <span class="listing-box-image-links">
                                                        <a href="properties.html"><i class="fa fa-heart"></i> <span>Add to favorites</span></a>
			                                            <a href="properties-detail-standard.html"><i class="fa fa-search"></i> <span>View detail</span></a>
			                                            <a href="properties-compare.html"><i class="fa fa-balance-scale"></i> <span>Compare property</span></a>
		                                            </span>
                                                </div><!-- /.listing-box-image -->

                                                <div class="listing-box-title">
                                                    <h2><a href="properties-detail-standard.html">Misty Valley</a></h2>
                                                    <h3>$ 27.000</h3>
                                                </div><!-- /.listing-box-title -->

                                                <div class="listing-box-content">
                                                    <dl>
                                                        <dt>Type</dt><dd>House</dd>
                                                        <dt>Location</dt><dd>New York</dd>
                                                        <dt>Area</dt><dd>180 sqft</dd>
                                                    </dl>
                                                </div><!-- /.listing-box-cotntent -->
                                            </div><!-- /.listing-box -->
                                        </div><!-- /.col-* -->

                                        <div class="col-md-6 col-lg-4">
                                            <div class="listing-box">
                                                <div class="listing-box-image" style="background-image: url('assets/img/tmp/tmp-11.jpg')">
                                                    <div class="listing-box-image-label">Featured</div><!-- /.listing-box-image-label -->

                                                    <span class="listing-box-image-links">
                                                        <a href="properties.html"><i class="fa fa-heart"></i> <span>Add to favorites</span></a>
			                                            <a href="properties-detail-standard.html"><i class="fa fa-search"></i> <span>View detail</span></a>
			                                            <a href="properties-compare.html"><i class="fa fa-balance-scale"></i> <span>Compare property</span></a>
		                                            </span>
                                                </div><!-- /.listing-box-image -->

                                                <div class="listing-box-title">
                                                    <h2><a href="properties-detail-standard.html">Foggy Anchor Campus</a></h2>
                                                    <h3>$ 840.000</h3>
                                                </div><!-- /.listing-box-title -->

                                                <div class="listing-box-content">
                                                    <dl>
                                                        <dt>Type</dt><dd>House</dd>
                                                        <dt>Location</dt><dd>New York</dd>
                                                        <dt>Area</dt><dd>180 sqft</dd>
                                                    </dl>
                                                </div><!-- /.listing-box-cotntent -->
                                            </div><!-- /.listing-box -->
                                        </div><!-- /.col-* -->

                                        <div class="col-md-6 col-lg-4">
                                            <div class="listing-box">
                                                <div class="listing-box-image" style="background-image: url('assets/img/tmp/tmp-10.jpg')">
                                                    <div class="listing-box-image-label">Featured</div><!-- /.listing-box-image-label -->

                                                    <span class="listing-box-image-links">
			                                            <a href="properties.html"><i class="fa fa-heart"></i> <span>Add to favorites</span></a>
			                                            <a href="properties-detail-standard.html"><i class="fa fa-search"></i> <span>View detail</span></a>
                                                        <a href="properties-compare.html"><i class="fa fa-balance-scale"></i> <span>Compare property</span></a>
		                                            </span>
                                                </div><!-- /.listing-box-image -->

                                                <div class="listing-box-title">
                                                    <h2><a href="properties-detail-standard.html">Golden Fawn Thicket</a></h2>
                                                    <h3>$ 321.000</h3>
                                                </div><!-- /.listing-box-title -->

                                                <div class="listing-box-content">
                                                    <dl>
                                                        <dt>Type</dt><dd>House</dd>
                                                        <dt>Location</dt><dd>New York</dd>
                                                        <dt>Area</dt><dd>180 sqft</dd>
                                                    </dl>
                                                </div><!-- /.listing-box-cotntent -->
                                            </div><!-- /.listing-box -->
                                        </div><!-- /.col-* -->

                                        <div class="col-md-6 col-lg-4">
                                            <div class="listing-box">
                                                <div class="listing-box-image" style="background-image: url('assets/img/tmp/tmp-3.jpg')">
                                                    <div class="listing-box-image-label">Featured</div><!-- /.listing-box-image-label -->

                                                    <span class="listing-box-image-links">
			                                            <a href="properties.html"><i class="fa fa-heart"></i> <span>Add to favorites</span></a>
			                                            <a href="properties-detail-standard.html"><i class="fa fa-search"></i> <span>View detail</span></a>
			                                            <a href="properties-compare.html"><i class="fa fa-balance-scale"></i> <span>Compare property</span></a>
                                                    </span>
                                                </div><!-- /.listing-box-image -->

                                                <div class="listing-box-title">
                                                    <h2><a href="properties-detail-standard.html">Little Brook Court</a></h2>
                                                    <h3>$ 800 / per week</h3>
                                                </div><!-- /.listing-box-title -->

                                                <div class="listing-box-content">
                                                    <dl>
                                                        <dt>Type</dt><dd>House</dd>
                                                        <dt>Location</dt><dd>New York</dd>
                                                        <dt>Area</dt><dd>180 sqft</dd>
                                                    </dl>
                                                </div><!-- /.listing-box-cotntent -->
                                            </div><!-- /.listing-box -->
                                        </div><!-- /.col-* -->

                                        <div class="col-md-6 col-lg-4">
                                            <div class="listing-box">
                                                <div class="listing-box-image" style="background-image: url('assets/img/tmp/tmp-2.jpg')">
                                                    <div class="listing-box-image-label">Featured</div><!-- /.listing-box-image-label -->

                                                    <span class="listing-box-image-links">
                                                        <a href="properties.html"><i class="fa fa-heart"></i> <span>Add to favorites</span></a>
			                                            <a href="properties-detail-standard.html"><i class="fa fa-search"></i> <span>View detail</span></a>
			                                            <a href="properties-compare.html"><i class="fa fa-balance-scale"></i> <span>Compare property</span></a>
		                                            </span>
                                                </div><!-- /.listing-box-image -->

                                                <div class="listing-box-title">
                                                    <h2><a href="properties-detail-standard.html">Quiet Parade</a></h2>
                                                    <h3>$ 256.000</h3>
                                                </div><!-- /.listing-box-title -->

                                                <div class="listing-box-content">
                                                    <dl>
                                                        <dt>Type</dt><dd>House</dd>
                                                        <dt>Location</dt><dd>New York</dd>
                                                        <dt>Area</dt><dd>180 sqft</dd>
                                                    </dl>
                                                </div><!-- /.listing-box-cotntent -->
                                            </div><!-- /.listing-box -->
                                        </div><!-- /.col-* -->

                                    </div><!-- /.row -->
                                </div><!-- /.listing-box-wrapper -->

                                <div class="pagination-wrapper">
                                    <ul class="pagination">
                                        <li class="page-item disabled">
                                            <a class="page-link" href="#" aria-label="Previous">
                                                <span aria-hidden="true">&laquo;</span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                        </li>

                                        <li class="page-item">
                                            <a class="page-link" href="#">1</a>
                                        </li>

                                        <li class="page-item"><a class="page-link" href="#">2</a></li>

                                        <li class="page-item active"><a class="page-link" href="#">3 <span class="sr-only">(current)</span></a></li>

                                        <li class="page-item"><a class="page-link" href="#">4</a></li>

                                        <li class="page-item"><a class="page-link" href="#">5</a></li>

                                        <li class="page-item">
                                            <a class="page-link" href="#" aria-label="Next">
                                                <span aria-hidden="true">&raquo;</span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </li>
                                    </ul><!-- /.pagination -->
                                </div><!-- /.pagination-wrapper -->
                            </div><!-- /.col-* -->

                            <div class="col-md-4 col-lg-3">
                                <div class="widget">
                                    <h3 class="widgettitle">Recent Properties</h3>


                                    <div class="listing-small">
                                        <div class="listing-small-inner">
                                            <div class="listing-small-image">
                                                <a href="#" style="background-image: url('assets/img/tmp/tmp-5.jpg');">
                                                </a>
                                            </div><!-- /.listing-small-image -->

                                            <div class="listing-small-content">
                                                <h3><a href="#">Bright Island Route</a></h3>
                                                <h4>$ 40.000</h4>
                                            </div><!-- /.listing-small-content -->
                                        </div><!-- /.listing-small-inner -->
                                    </div><!-- /.listing-small -->

                                    <div class="listing-small">
                                        <div class="listing-small-inner">
                                            <div class="listing-small-image">
                                                <a href="#" style="background-image: url('assets/img/tmp/tmp-4.jpg');">
                                                </a>
                                            </div><!-- /.listing-small-image -->

                                            <div class="listing-small-content">
                                                <h3><a href="#">Misty Valley</a></h3>
                                                <h4>$ 27.000</h4>
                                            </div><!-- /.listing-small-content -->
                                        </div><!-- /.listing-small-inner -->
                                    </div><!-- /.listing-small -->

                                    <div class="listing-small">
                                        <div class="listing-small-inner">
                                            <div class="listing-small-image">
                                                <a href="#" style="background-image: url('assets/img/tmp/tmp-11.jpg');">
                                                </a>
                                            </div><!-- /.listing-small-image -->

                                            <div class="listing-small-content">
                                                <h3><a href="#">Foggy Anchor Campus</a></h3>
                                                <h4>$ 840.000</h4>
                                            </div><!-- /.listing-small-content -->
                                        </div><!-- /.listing-small-inner -->
                                    </div><!-- /.listing-small -->

                                </div><!-- /.widget -->

                                <div class="widget">
                                    <ul class="nav nav-pills nav-stacked nav-style-secondary">
                                        <li class="nav-item"><a href="#" class="nav-link"><i class="fa fa-download"></i> Download Footprints</a></li>
                                        <li class="nav-item"><a href="#" class="nav-link"><i class="fa fa-file-text"></i> PDF Version</a></li>
                                        <li class="nav-item"><a href="#" class="nav-link"><i class="fa fa-file"></i> Legal Document</a></li>
                                    </ul><!-- /.menu -->
                                </div><!-- /.widget -->

                                <div class="widget">
                                    <h2 class="widgettitle">Contact Information</h2>

                                    <table class="contact">
                                        <tbody>
                                        <tr>
                                            <th>Address:</th>
                                            <td>2300 Main Ave.<br>Lost Angeles, CA 23123<br>United States<br></td>
                                        </tr>

                                        <tr>
                                            <th>Phone:</th>
                                            <td>+0-123-456-789</td>
                                        </tr>

                                        <tr>
                                            <th>E-mail:</th>
                                            <td><a href="mailto:info@example.com">info@example.com</a></td>
                                        </tr>

                                        <tr>
                                            <th>Skype:</th>
                                            <td>your.company</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div><!-- /.widget -->
                            </div><!-- /.col-* -->
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </div><!-- /.content -->
            </div><!-- /.main-inner -->
        </div><!-- /.main -->
    </div><!-- /.main-wrapper -->


    @include("webapp.includes.call-to-action-footer")

@endsection