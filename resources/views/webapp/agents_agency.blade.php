@extends("webapp.layouts.default")

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">

@endsection


@section('content')

    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">
                <div class="content-title">
                    <div class="content-title-inner">
                        <div class="container">
                            <h1>{{ $pageTitle }}</h1>
                        </div><!-- /.container -->
                    </div><!-- /.content-title-inner -->
                </div>

                <div class="content">
                    <div class="container">
                        <div class="posts posts-grid">
                            <div class="row">

                                @foreach($agents as $agency)

                                            <div class="col-md-6 col-lg-4">
                                                <div class="listing-box post">
                                                    <?php

                                                    $cover_image_name = $agency->avatar;

                                                    $imgSrc = (is_file('storage/' . $cover_image_name))?
                                                        asset('storage/' . $cover_image_name):
                                                        asset('assets/img/no-avatar-available.jpg');

                                                    ?>
                                                    <div class="listing-box-image"  style="background-image: url('{{ $imgSrc }}');">
                                                        <a href="#" class="listing-box-image-link">
                                                        </a>
                                                    </div><!-- /.listing-box-image -->

                                                    <div class="listing-box-title">
                                                        <h2><a href="#">{{ $agency->name }}</a></h2>
                                                    </div><!-- /.listing-box-title -->

                                                    <div class="listing-box-content">
                                                        <p>
                                                            <!-- <strong>143</strong> properties in catalogue<br> -->
                                                            <a href="#"><i class="fa fa-at"></i> {{ $agency->email  }}</a><br>
                                                            <!-- <a href="#"><i class="fa fa-phone"></i> </a> -->
                                                        </p>

                                                        <ul class="listing-row-actions">
                                                            <li><a href="{{ url('/view-all-properties/' . $agency->id) }}"><i class="fa fa-building"></i> View Properties</a></li>
                                                        </ul><!-- /.listing-row-actions -->

                                                    </div><!-- /.listing-box-cotntent -->
                                                </div><!-- /.listing-box -->
                                            </div>




                                @endforeach
                            </div><!-- /.row -->
                        </div><!-- /.posts-->

                        <!--
                        <div class="pagination-wrapper">

                            { ! ! $agents->links('vendor.pagination.bootstrap-4') ! ! }

                        </div><!-- /.pagination-wrapper -->

                    </div><!-- /.container -->

                </div><!-- /.content -->
            </div>
        </div>
    </div>
    @include("webapp.includes.call-to-action-footer")

@endsection
