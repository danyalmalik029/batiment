@extends("webapp.layouts.default")

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">

@endsection


@section('content')

    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">
                <div class="content-title">
                    <div class="content-title-inner">
                        <div class="container">
                            <h1>{{ $pageTitle }}</h1>
                        </div><!-- /.container -->
                    </div><!-- /.content-title-inner -->
                </div>

                <div class="content">
                    <div class="container">
                        <div class="posts posts-grid">
                            <div class="row">
                                @foreach($agents as $agency)

                                    @if(count($agency['agents']))
                                    <div class="col-md-12">
                                        <h3 class="agency-separator-title">{{ $agency['agency']['name'] }}</h3>
                                    </div>

                                    @foreach($agency['agents'] as $agent)

                                    <div class="col-md-6 col-lg-4">
                                        <div class="listing-box post">
                                            <?php

                                            $cover_image_name = $agent['avatar'];

                                            $imgSrc = (is_file('storage/' . $cover_image_name))?
                                                asset('storage/' . $cover_image_name):
                                                asset('assets/img/no-avatar-available.jpg');

                                            ?>
                                            <div class="listing-box-image"  style="background-image: url('{{ $imgSrc }}');">
                                                <a href="#" class="listing-box-image-link">
                                                </a>
                                            </div><!-- /.listing-box-image -->

                                            <div class="listing-box-title">
                                                <h2><a href="#">{{ $agent['name'] }}</a></h2>
                                            </div><!-- /.listing-box-title -->

                                            <div class="listing-box-content">
                                                <p>
                                                    <!-- <strong>143</strong> properties in catalogue<br> -->
                                                    <a href="#"><i class="fa fa-at"></i> {{ $agent['email'] }}</a><br>
                                                    <!-- <a href="#"><i class="fa fa-phone"></i> </a> -->
                                                </p>

                                                <ul class="listing-row-actions">
                                                    <li><a href="{{ url('/view-all-properties/' . $agent['id']) }}"><i class="fa fa-building"></i> @lang('profile.view_agent_properties')</a></li>
                                                </ul><!-- /.listing-row-actions -->

                                                <!--
                                                <ul class="listing-box-social">
                                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                                </ul><!-- /.listing-row-social -->
                                            </div><!-- /.listing-box-cotntent -->
                                        </div><!-- /.listing-box -->
                                    </div>

                                    @endforeach

                                    @endif

                                @endforeach
                            </div><!-- /.row -->
                        </div><!-- /.posts-->

                        <!--
                        <div class="pagination-wrapper">

                            { ! ! $agents->links('vendor.pagination.bootstrap-4') ! ! }

                        </div><!-- /.pagination-wrapper -->

                    </div><!-- /.container -->

                </div><!-- /.content -->
            </div>
        </div>
    </div>
    @include("webapp.includes.call-to-action-footer")

@endsection







