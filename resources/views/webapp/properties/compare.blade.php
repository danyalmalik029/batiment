@extends("webapp.layouts.default")

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">

@endsection

@section('content')

    <div class="main-wrapper">
	    <div class="main">
	        <div class="main-inner">
	        		        
				<div class="content-title">
					<div class="content-title-inner">
						<div class="container">		
							<h1>@lang('properties.compare_title')</h1>
						</div><!-- /.container -->
					</div><!-- /.content-title-inner -->
				</div><!-- /.content-title -->
				<br>

				<div class="content">
	                <div class="container">
						<div class="listing-compare-wrapper">
							<div class="row">

                                @if(!$compare_1 && !$compare_2 && !$compare_3)
                                <div class="listing-compare-description-wrapper col-xs-12" >
                                    You must add at least one property to compare...
                                </div>

                                @endif

                                @if($compare_1 || $compare_2 || $compare_3 )
								<div class="listing-compare-description-wrapper col-sm-3" >
									<div class="listing-compare-description">
										<ul>		
											<li style="height: 47px;">@lang('properties.price')</li>
										  	<li style="height: 47px;">@lang('properties.bedrooms')</li>
										  	<li style="height: 47px;">@lang('properties.bathrooms')</li>
										  	<li style="height: 47px;">@lang('properties.area')</li>
										  	<li style="height: 47px;">@lang('properties.legaldocument')</li>
										  	<li style="height: 47px;">@lang('properties.conditioning')</li>
										  	<li style="height: 47px;">@lang('properties.cleaning')</li>
										  	<li style="height: 47px;">@lang('properties.dishwasher')</li>
										  	<li style="height: 47px;">@lang('properties.grill')</li>
										  	<li style="height: 47px;">@lang('properties.internet')</li>
										  	<li style="height: 47px;">@lang('properties.microwave')</li>
										  	<li style="height: 47px;">@lang('properties.pool')</li>
										  	<li style="height: 47px;">@lang('properties.terrace')</li>
										  	<li style="height: 47px;">@lang('properties.balcony')</li>
										  	<li style="height: 47px;">@lang('properties.cofeepot')</li>
										  	<li style="height: 47px;">@lang('properties.dvd')</li>
										  	<li style="height: 47px;">@lang('properties.hairdryer')</li>
										  	<li style="height: 47px;">@lang('properties.iron')</li>
										  	<li style="height: 47px;">@lang('properties.oven')</li>
										  	<li style="height: 47px;">@lang('properties.radio')</li>
										  	<li style="height: 47px;">@lang('properties.toaster')</li>
										  	<li style="height: 47px;">@lang('properties.bedding')</li>
										  	<li style="height: 47px;">@lang('properties.computer')</li>
										  	<li style="height: 47px;">@lang('properties.fan')</li>
										  	<li style="height: 47px;">@lang('properties.heating')</li>
										  	<li style="height: 47px;">@lang('properties.juicer')</li>
										  	<li style="height: 47px;">@lang('properties.parking')</li>
										  	<li style="height: 47px;">@lang('properties.roofterrace')</li>
										  	<li style="height: 47px;">@lang('properties.towelwes')</li>
										  	<li style="height: 47px;">@lang('properties.cabletv')</li>
										  	<li style="height: 47px;">@lang('properties.cot')</li>
										  	<li style="height: 47px;">@lang('properties.fridge')</li>
										  	<li style="height: 47px;">@lang('properties.hifi')</li>
										  	<li style="height: 47px;">@lang('properties.lift')</li>
										  	<li style="height: 47px;">@lang('properties.parquet')</li>
										  	<li style="height: 47px;">@lang('properties.smoking')</li>
										  	<li style="height: 47px;">@lang('properties.useofpool')</li>				
										</ul>
									</div><!-- /.listing-compare-description -->	
								</div><!-- /.col-sm-3 -->
                                @endif

                                @if($compare_1)

								<div class="listing-compare-col-wrapper col-sm-3">
									<div class="listing-compare-col">
										<div class="listing-compare-image">
											<a href="{{ url('/view-details-property/'.$compare_1->id) }}" class="listing-compare-image-link" style="
												@if(count($compare_1->images) != 0)
														background-image: url(storage/propertiesImages/{{$compare_1->images[0]->image_name}})
													@else
														background-image: url('assets/img/tmp/tmp-5.jpg')
													@endif
											">
											</a>
										</div><!-- /.listing-compare-image -->

										<div class="listing-compare-title">
                                            <div class="form-control">{{$compare_1->title}}</div>
										</div><!-- /.listing-compare-title -->

										<ul class="listing-compare-list">
											<li style="height: 47px;">{{$compare_1->price}}</li>
											<li style="height: 47px;">{{$compare_1->bedrooms}}</li>
											<li style="height: 47px;">{{$compare_1->bathrooms}}</li>
											<li style="height: 47px;">{{$compare_1->area}} m²</li>
											<li class="yes" style="height: 47px;">{{$compare_1->legaldocument}}</li>
											<li class="@if($compare_1->conditioning) yes @else no @endif" style="height: 47px;">
												@if($compare_1->conditioning)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
											</li>
											<li class="@if($compare_1->cleaning) yes @else no @endif" style="height: 47px;">
												@if($compare_1->cleaning)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_1->dishwasher) yes @else no @endif" style="height: 47px;">
												@if($compare_1->dishwasher)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_1->grill) yes @else no @endif" style="height: 47px;">
												@if($compare_1->grill)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_1->internet) yes @else no @endif" style="height: 47px;">
												@if($compare_1->internet)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_1->microwave) yes @else no @endif" style="height: 47px;">
												@if($compare_1->microwave)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_1->pool) yes @else no @endif" style="height: 47px;">
												@if($compare_1->pool)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_1->terrace) yes @else no @endif" style="height: 47px;">
												@if($compare_1->terrace)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_1->balcony) yes @else no @endif" style="height: 47px;">
												@if($compare_1->balcony)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_1->cofeepot) yes @else no @endif" style="height: 47px;">
												@if($compare_1->cofeepot)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_1->dvd) yes @else no @endif" style="height: 47px;">
												@if($compare_1->dvd)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_1->hairdryer) yes @else no @endif" style="height: 47px;">
												@if($compare_1->hairdryer)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif</li>
											<li class="@if($compare_1->iron) yes @else no @endif" style="height: 47px;">
												@if($compare_1->iron)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_1->oven) yes @else no @endif" style="height: 47px;">
												@if($compare_1->oven)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_1->radio) yes @else no @endif" style="height: 47px;">
												@if($compare_1->radio)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_1->toaster) yes @else no @endif" style="height: 47px;">
												@if($compare_1->toaster)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_1->bedding) yes @else no @endif" style="height: 47px;">
												@if($compare_1->bedding)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_1->computer) yes @else no @endif" style="height: 47px;">
												@if($compare_1->computer)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_1->fan) yes @else no @endif" style="height: 47px;">
												@if($compare_1->fan)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_1->heating) yes @else no @endif" style="height: 47px;">
												@if($compare_1->heating)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_1->juicer) yes @else no @endif" style="height: 47px;">
												@if($compare_1->juicer)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_1->parking) yes @else no @endif" style="height: 47px;">
												@if($compare_1->parking)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_1->roofterrace) yes @else no @endif" style="height: 47px;">
												@if($compare_1->roofterrace)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_1->towelwes) yes @else no @endif" style="height: 47px;">
												@if($compare_1->towelwes)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_1->cabletv) yes @else no @endif" style="height: 47px;">
												@if($compare_1->cabletv)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_1->cot) yes @else no @endif" style="height: 47px;">
												@if($compare_1->cot)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_1->fridge) yes @else no @endif" style="height: 47px;">
												@if($compare_1->fridge)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_1->hifi) yes @else no @endif" style="height: 47px;">
												@if($compare_1->hifi)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_1->lift) yes @else no @endif" style="height: 47px;">
												@if($compare_1->lift)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_1->parquet) yes @else no @endif" style="height: 47px;">
												@if($compare_1->parquet)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_1->smoking) yes @else no @endif" style="height: 47px;">
												@if($compare_1->smoking)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_1->useofpool) yes @else no @endif" style="height: 47px;">
												@if($compare_1->useofpool)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>

                                            <li>
                                                {{ Form::open() }}
                                                {{ Form::token() }}
                                                <input type="hidden" name="action" value="remove" />
                                                <input type="hidden" name="id" value="{{ $compare_1->id }}" />

                                                <button type="submit" class="btn btn-warning btn-sm"><i class="fa fa-trash"></i>Remove from compare</button>

                                                {{ Form::close() }}
                                            </li>


										</ul><!-- /.listing-compare-list -->
									</div><!-- /.listing-compare-col -->
								</div><!-- /.listing-compare-col-wrapper -->

                                @endif

                                @if($compare_2)

								<div class="listing-compare-col-wrapper col-sm-3"> 
									<div class="listing-compare-col">
										<div class="listing-compare-image">
											<a href="{{ url('/view-details-property/'.$compare_2->id) }}" class="listing-compare-image-link" style="
												@if(count($compare_2->images) != 0)
														background-image: url(storage/propertiesImages/{{$compare_2->images[0]->image_name}})
													@else
														background-image: url('assets/img/tmp/tmp-5.jpg')
													@endif
											">
											</a>
										</div><!-- /.listing-compare-image -->

										<div class="listing-compare-title">
                                            <div class="form-control">{{$compare_2->title}}</div>
										</div><!-- /.listing-compare-title -->

										<ul class="listing-compare-list">
											<li style="height: 47px;">{{$compare_2->price}}</li>
											<li style="height: 47px;">{{$compare_2->bedrooms}}</li>
											<li style="height: 47px;">{{$compare_2->bathrooms}}</li>
											<li style="height: 47px;">{{$compare_2->area}} m²</li>
											<li class="yes" style="height: 47px;">{{$compare_2->legaldocument}}</li>
											<li class="@if($compare_2->conditioning) yes @else no @endif" style="height: 47px;">
												@if($compare_2->conditioning)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
											</li>
											<li class="@if($compare_2->cleaning) yes @else no @endif" style="height: 47px;">
												@if($compare_2->cleaning)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_2->dishwasher) yes @else no @endif" style="height: 47px;">
												@if($compare_2->dishwasher)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_2->grill) yes @else no @endif" style="height: 47px;">
												@if($compare_2->grill)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_2->internet) yes @else no @endif" style="height: 47px;">
												@if($compare_2->internet)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_2->microwave) yes @else no @endif" style="height: 47px;">
												@if($compare_2->microwave)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_2->pool) yes @else no @endif" style="height: 47px;">
												@if($compare_2->pool)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_2->terrace) yes @else no @endif" style="height: 47px;">
												@if($compare_2->terrace)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_2->balcony) yes @else no @endif" style="height: 47px;">
												@if($compare_2->balcony)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_2->cofeepot) yes @else no @endif" style="height: 47px;">
												@if($compare_2->cofeepot)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_2->dvd) yes @else no @endif" style="height: 47px;">
												@if($compare_2->dvd)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_2->hairdryer) yes @else no @endif" style="height: 47px;">
												@if($compare_2->hairdryer)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif</li>
											<li class="@if($compare_2->iron) yes @else no @endif" style="height: 47px;">
												@if($compare_2->iron)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_2->oven) yes @else no @endif" style="height: 47px;">
												@if($compare_2->oven)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_2->radio) yes @else no @endif" style="height: 47px;">
												@if($compare_2->radio)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_2->toaster) yes @else no @endif" style="height: 47px;">
												@if($compare_2->toaster)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_2->bedding) yes @else no @endif" style="height: 47px;">
												@if($compare_2->bedding)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_2->computer) yes @else no @endif" style="height: 47px;">
												@if($compare_2->computer)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_2->fan) yes @else no @endif" style="height: 47px;">
												@if($compare_2->fan)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_2->heating) yes @else no @endif" style="height: 47px;">
												@if($compare_2->heating)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_2->juicer) yes @else no @endif" style="height: 47px;">
												@if($compare_2->juicer)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_2->parking) yes @else no @endif" style="height: 47px;">
												@if($compare_2->parking)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_2->roofterrace) yes @else no @endif" style="height: 47px;">
												@if($compare_2->roofterrace)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_2->towelwes) yes @else no @endif" style="height: 47px;">
												@if($compare_2->towelwes)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_2->cabletv) yes @else no @endif" style="height: 47px;">
												@if($compare_2->cabletv)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_2->cot) yes @else no @endif" style="height: 47px;">
												@if($compare_2->cot)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_2->fridge) yes @else no @endif" style="height: 47px;">
												@if($compare_2->fridge)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_2->hifi) yes @else no @endif" style="height: 47px;">
												@if($compare_2->hifi)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_2->lift) yes @else no @endif" style="height: 47px;">
												@if($compare_2->lift)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_2->parquet) yes @else no @endif" style="height: 47px;">
												@if($compare_2->parquet)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_2->smoking) yes @else no @endif" style="height: 47px;">
												@if($compare_2->smoking)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_2->useofpool) yes @else no @endif" style="height: 47px;">
												@if($compare_2->useofpool)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>

                                            <li>
                                                {{ Form::open() }}
                                                {{ Form::token() }}
                                                <input type="hidden" name="action" value="remove" />
                                                <input type="hidden" name="id" value="{{ $compare_2->id }}" />

                                                <button type="submit" class="btn btn-warning btn-sm"><i class="fa fa-trash"></i>Remove from compare</button>

                                                {{ Form::close() }}
                                            </li>


										</ul><!-- /.listing-compare-list -->
									</div><!-- /.listing-compare-col -->
								</div><!-- /.listing-compare-col-wrapper -->

                                @endif

                                @if($compare_3)

								<div class="listing-compare-col-wrapper col-sm-3"> 
									<div class="listing-compare-col">
										<div class="listing-compare-image">
											<a href="{{ url('/view-details-property/'.$compare_3->id) }}" class="listing-compare-image-link" style="
												@if(count($compare_3->images) != 0)
														background-image: url(storage/propertiesImages/{{$compare_3->images[0]->image_name}})
													@else
														background-image: url('assets/img/tmp/tmp-5.jpg')
													@endif
											">
											</a>
										</div><!-- /.listing-compare-image -->

										<div class="listing-compare-title">
											<div class="form-control">{{$compare_3->title}}</div>
										</div><!-- /.listing-compare-title -->

										<ul class="listing-compare-list">
											<li style="height: 47px;">{{$compare_3->price}}</li>
											<li style="height: 47px;">{{$compare_3->bedrooms}}</li>
											<li style="height: 47px;">{{$compare_3->bathrooms}}</li>
											<li style="height: 47px;">{{$compare_3->area}} m²</li>
											<li class="yes" style="height: 47px;">{{$compare_3->legaldocument}}</li>
											<li class="@if($compare_3->conditioning) yes @else no @endif" style="height: 47px;">
												@if($compare_3->conditioning)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
											</li>
											<li class="@if($compare_3->cleaning) yes @else no @endif" style="height: 47px;">
												@if($compare_3->cleaning)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_3->dishwasher) yes @else no @endif" style="height: 47px;">
												@if($compare_3->dishwasher)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_3->grill) yes @else no @endif" style="height: 47px;">
												@if($compare_3->grill)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_3->internet) yes @else no @endif" style="height: 47px;">
												@if($compare_3->internet)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_3->microwave) yes @else no @endif" style="height: 47px;">
												@if($compare_3->microwave)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_3->pool) yes @else no @endif" style="height: 47px;">
												@if($compare_3->pool)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_3->terrace) yes @else no @endif" style="height: 47px;">
												@if($compare_3->terrace)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_3->balcony) yes @else no @endif" style="height: 47px;">
												@if($compare_3->balcony)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_3->cofeepot) yes @else no @endif" style="height: 47px;">
												@if($compare_3->cofeepot)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_3->dvd) yes @else no @endif" style="height: 47px;">
												@if($compare_3->dvd)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_3->hairdryer) yes @else no @endif" style="height: 47px;">
												@if($compare_3->hairdryer)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif</li>
											<li class="@if($compare_3->iron) yes @else no @endif" style="height: 47px;">
												@if($compare_3->iron)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_3->oven) yes @else no @endif" style="height: 47px;">
												@if($compare_3->oven)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_3->radio) yes @else no @endif" style="height: 47px;">
												@if($compare_3->radio)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_3->toaster) yes @else no @endif" style="height: 47px;">
												@if($compare_3->toaster)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_3->bedding) yes @else no @endif" style="height: 47px;">
												@if($compare_3->bedding)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_3->computer) yes @else no @endif" style="height: 47px;">
												@if($compare_3->computer)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_3->fan) yes @else no @endif" style="height: 47px;">
												@if($compare_3->fan)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_3->heating) yes @else no @endif" style="height: 47px;">
												@if($compare_3->heating)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_3->juicer) yes @else no @endif" style="height: 47px;">
												@if($compare_3->juicer)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_3->parking) yes @else no @endif" style="height: 47px;">
												@if($compare_3->parking)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_3->roofterrace) yes @else no @endif" style="height: 47px;">
												@if($compare_3->roofterrace)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_3->towelwes) yes @else no @endif" style="height: 47px;">
												@if($compare_3->towelwes)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_3->cabletv) yes @else no @endif" style="height: 47px;">
												@if($compare_3->cabletv)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_3->cot) yes @else no @endif" style="height: 47px;">
												@if($compare_3->cot)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_3->fridge) yes @else no @endif" style="height: 47px;">
												@if($compare_3->fridge)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_3->hifi) yes @else no @endif" style="height: 47px;">
												@if($compare_3->hifi)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_3->lift) yes @else no @endif" style="height: 47px;">
												@if($compare_3->lift)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_3->parquet) yes @else no @endif" style="height: 47px;">
												@if($compare_3->parquet)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_3->smoking) yes @else no @endif" style="height: 47px;">
												@if($compare_3->smoking)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>
											<li class="@if($compare_3->useofpool) yes @else no @endif" style="height: 47px;">
												@if($compare_3->useofpool)
													<i class="fa fa-check"></i>
												@else
													<i class="fa fa-times"></i>
												@endif
												</li>

                                            <li>
                                                {{ Form::open() }}
                                                {{ Form::token() }}
                                                <input type="hidden" name="action" value="remove" />
                                                <input type="hidden" name="id" value="{{ $compare_3->id }}" />

                                                <button type="submit" class="btn btn-warning btn-sm"><i class="fa fa-trash"></i>Remove from compare</button>

                                                {{ Form::close() }}
                                            </li>


										</ul><!-- /.listing-compare-list -->
									</div><!-- /.listing-compare-col -->
								</div><!-- /.listing-compare-col-wrapper -->

                                @endif
								
							</div><!-- /.row -->
						</div><!-- /.listing-compare-wrapper -->
					</div><!-- /.container -->

	            </div><!-- /.content -->
	        </div><!-- /.main-inner -->
	    </div><!-- /.main -->
    </div><!-- /.main-wrapper -->

@endsection

@section('script')

	<script type="text/javascript">

		var token = $('meta[name="csrf-token"]').attr('content');

		$("#prop1").change(function(){
			var param = "id=" + $("#prop1").val();
			console.log(token);
            $.ajax({
                type: 'POST',
                cache:false,
                headers:{'X-XSRF-TOKEN':token},
                url: 'getDetails',
                data: param,
                dataType: 'json',
                success: function(data){
                  	var option = "";
             		console.log(data);
                  	$.each(data, function(index, value) {
                    $("#listQuaterRetour").append('<option class="listToRemoveR" value="'+ value.id +'">'+    value.nom +'</option>');
                  });
               //console.log(option);
               }
            });
        });
	</script>

@endsection
