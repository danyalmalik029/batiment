@extends("webapp.layouts.default")

<?php

	$imgSrc = '';

	foreach( $list as $img ){
		$imgSrc = asset('storage/propertiesImages/' . $img);
		break;
	}

?>

@section('meta')
	@if(isset($ads->id))

		<!-- Twitter -->
		<meta name="twitter:card" content="Summary">
		<meta name="twitter:site" content="{{ url('/view-details-property/'.$ads->id) }}">
		<meta name="twitter:title" content="{{ $ads->title }}">
		<meta name="twitter:description" content="{{ substr( strip_tags($ads->description) ,0 , 300) }}">
		<meta name="twitter:creator" content="{{ $ads->user->name }}">
		<meta name="twitter:image:src" content="{{ $imgSrc }}">
		<meta name="twitter:player" content="">

		<!-- Open Graph General (Facebook & Pinterest) -->
		<meta property="og:url" content="{{ url('/view-details-property/'.$ads->id) }}">
		<meta property="og:title" content="{{ $ads->title }}">
		<meta property="og:description" content="{{ substr( strip_tags($ads->description) ,0 , 300) }}">
		<meta property="og:site_name" content="ETB Batiment">
		<meta property="og:image" content="{{ $imgSrc }}">
		<meta property="fb:admins" content="">
		<meta property="fb:app_id" content="">
		<meta property="og:type" content="article">
		<meta property="og:locale" content="fr_FR">
		<meta property="og:audio" content="">
		<meta property="og:video" content="">
	@endif
@endsection

@section('content')

    <div class="main-wrapper">
	    <div class="main">
	        <div class="main-inner">
	        		        
				<div class="content-title">
					<div class="content-title-inner">
						<div class="container">		
							<h1>{{$ads->title}}</h1>
						</div><!-- /.container -->
					</div><!-- /.content-title-inner -->
				</div><!-- /.content-title -->
				
	            <div class="content">
	                <div class="container">
						<div class="row">
					        <div class="col-sm-12">
					        <br><br><br>
					            <div class="listing-detail-attributes">
								    <div class="listing-detail-attribute">
								        <div class="key">@lang('properties.properties_added_by')</div>
								        <div class="value">
                                            <?php

                                            $cover_image_name = $ads->user->avatar;

                                            $imgSrc = (is_file('storage/' . $cover_image_name))?
                                                asset('storage/' . $cover_image_name):
                                                asset('assets/img/no-avatar-available.jpg');

                                            ?>
								            <div class="value-image">
								                <a href="{{ url('/view-all-properties/' . $ads->user->id) }}" style="background-image: url('{{ $imgSrc }}');">
								                	
								                </a>
								            </div><!-- /.value-image -->

								            {{$ads->user->name}}
								        </div>
								    </div><!-- /.listing-detail-attribute -->   

								    <div class="listing-detail-attribute">
								        <div class="key">@lang('properties.category')</div>
								        <div class="value">@lang('general.ads_category_' . $ads->categories->category_slug)</div>
								    </div><!-- /.listing-detail-attribute -->

								    <div class="listing-detail-attribute">
								        <div class="key">@lang('properties.type')</div>
								        <div class="value">@lang('properties.' .$ads->ad_dtype ) </div>
								    </div><!-- /.listing-detail-attribute -->

								    <div class="listing-detail-attribute">
								        <div class="key">@lang('properties.price')</div>
								        <div class="value">{{$ads->price}} &#128;</div>
								    </div><!-- /.listing-detail-attribute -->
								</div><!-- /.listing-detail-attributes -->            
        					</div><!-- /.col-sm-12 -->

							<div class="col-lg-12">
								@if (session('message'))
									<div class="alert alert-success">
										{{ session('message') }}
									</div>
								@endif
							</div>
        					
        					<div class="listing-detail col-md-8  col-lg-9">
        						@if(count($errors->all()))

						            @include('partials.error',['type'=>'error','message'=>'Sorry, some errors occured. Please scroll to have details.'])

						        @endif 

						        @if($sucess != "null")

						            @include('partials.sucess',['type'=>'error','message'=>$sucess])

						        @endif

								<div id="lightgallery">
									@foreach( $list as $img )
									<a href="{{ url('../storage/propertiesImages/' . $img) }}">
										<img src="{{ url('../storage/propertiesImages/' . $img) }}" class="img-fluid" />
										<span class="zoom"><i class="fa fa-search-plus fa-3x" aria-hidden="true"></i></span>
									</a>
									@endforeach
								</div>

								<div class="row">            
								    <div class="col-lg-5">
								        <div class="overview">
								            <h2>@lang('properties.property_attributes')</h2>
								            <ul>
								                <li><strong>@lang('properties.price')</strong><span>{{$ads->price}} &#128;</span></li>
								                <li><strong>@lang('properties.status')</strong><span>@lang('properties.' .$ads->ad_dtype )</span></li>
								                <li><strong>@lang('properties.bedrooms')</strong><span>{{$ads->bedrooms}}</span></li>
								                <li><strong>@lang('properties.bathrooms')</strong><span>{{$ads->bathrooms}}</span></li>
								                <li><strong>@lang('properties.area')</strong><span>{{$ads->area}} m²</span></li>
								            </ul>
								        </div><!-- /.overview -->
								    </div><!-- /.col-* -->

								    <div class="col-lg-7">
								        <h2>@lang('properties.properties_description')</h2>
								        <p>
								            {{$ads->description}}
								        </p>

								    </div><!-- /.col-* -->
								</div><!-- /.row -->

                                <h2>@lang('properties.property_address_header')</h2>

                                <p>
                                    {{ $ads->address_suppl }}
                                </p>

            					<h2>@lang('properties.amenities')</h2>

								<ul class="amenities">
								    <li class="<?php if($ads->conditioning){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.conditioning')</li>
								    <li class="<?php if($ads->cleaning){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.cleaning')</li>
								    <li class="<?php if($ads->dishwasher){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.dishwasher')</li>
								    <li class="<?php if($ads->grill){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.grill')</li>
								    <li class="<?php if($ads->internet){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.internet')</li>
								    <li class="<?php if($ads->microwave){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.microwave')</li>
								    <li class="<?php if($ads->pool){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.pool')</li>
								    <li class="<?php if($ads->terrace){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.terrace')</li>
								    <li class="<?php if($ads->balcony){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.balcony')</li>
								    <li class="<?php if($ads->cofeepot){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.cofeepot')</li>
								    <li class="<?php if($ads->dvd){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.dvd')</li>
								    <li class="<?php if($ads->hairdryer){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.hairdryer')</li>
								    <li class="<?php if($ads->iron){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.iron')</li>
								    <li class="<?php if($ads->oven){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.oven')</li>
								    <li class="<?php if($ads->radio){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.radio')</li>
								    <li class="<?php if($ads->toaster){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.toaster')</li>
								    <li class="<?php if($ads->bedding){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.bedding')</li>
								    <li class="<?php if($ads->computer){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.computer')</li>
								    <li class="<?php if($ads->fan){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.fan')</li>
								    <li class="<?php if($ads->heating){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.heating')</li>
								    <li class="<?php if($ads->juicer){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.juicer')</li>
								    <li class="<?php if($ads->parking){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.parking')</li>
								    <li class="<?php if($ads->roofterrace){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.roofterrace')</li>
								    <li class="<?php if($ads->towelwes){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.towelwes')</li>
								    <li class="<?php if($ads->cabletv){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.cabletv')</li>
								    <li class="<?php if($ads->cot){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.cot')</li>
								    <li class="<?php if($ads->fridge){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.fridge')</li>
								    <li class="<?php if($ads->hifi){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.hifi')</li>
								    <li class="<?php if($ads->lift){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.lift')</li>
								    <li class="<?php if($ads->parquet){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.parquet')</li>
								    <li class="<?php if($ads->smoking){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.smoking')</li>
								    <li class="<?php if($ads->useofpool){ ?> yes <?php }else{ ?> no <?php } ?>">@lang('properties.useofpool')</li>
								</ul>

								<h2>@lang('general.general_share_on'):</h2>

								<div class="share-buttons">
									<a href="http://www.facebook.com/sharer/sharer.php?u={{ url('/view-details-property/'.$ads->id) }}" target="_blank" class="social-popup">
										<i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i>
									</a>
									<a href="http://twitter.com/share?url={{ url('/view-details-property/'.$ads->id) }}" target="_blank" class="social-popup">
										<i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i>
									</a>
									<a href="https://plus.google.com/share?url={{ url('/view-details-property/'.$ads->id) }}" target="_blank" class="social-popup">
										<i class="fa fa-google-plus-square fa-2x" aria-hidden="true"></i>
									</a>
								</div>

								

								@if(Auth::Check() && ($ads->user->id != Auth::id()))
									<h2>@lang('properties.messages_title')</h2>
									{{Form::open(['url'=>'/sendMessage','method'=>'POST','role'=>'form', 'files' => true]) }}
				            			{{ Form::token() }}
										<div class="comment-create">
											<form method="post" action="?">
												<input type="hidden" name="ads" value="{{$ads->id}}">
												<div class="row">
			
													<div class="col-sm-12">
														<div class="form-group">
															<label>@lang('properties.subject')</label>
															<input type="text" class="form-control" name="subject" style="<?php if($errors->first('subject')){?> border: 1px solid red !important; <?php } ?>"  value="{{old('subject')}}" required>

															<?php if($errors->first('subject')){?>
												 <span style="color: red !important; font-size: 10px;">$errors->first('subject')</span>
											<?php } ?>
														</div><!-- /.form-group -->				
													</div><!-- /.col-* -->
												</div><!-- /.row -->

												<div class="form-group">
													<label>@lang('properties.message')</label>
													<textarea class="form-control" rows="5" name="message" style="<?php if($errors->first('message')){?> border: 1px solid red !important; <?php } ?>"  value="{{old('message')}}" required></textarea>

													<?php if($errors->first('message')){?>
												 <span style="color: red !important; font-size: 10px;">$errors->first('message')</span>
											<?php } ?>
												</div><!-- /.form-group -->

												<div class="form-group-btn">
													<button class="btn btn-primary pull-right">@lang('properties.send_message')</button>
												</div><!-- /.form-group-btn -->
											</form>
										</div><!-- /.comment-create -->
									{{ Form::close() }}
								@endif
								
        					</div><!-- /.col-* -->

					        <div class="col-md-4 col-lg-3">
					        	@if(!empty($userList))
					        	<div class="widget">
									<ul class="menu nav nav-stacked">
										<li class="nav-item">
											<a href="{{url('/list-messages/' . $ads->id)}}" class="nav-link"><i class="fa fa-chevron-right"></i> @lang('properties.properties_messages')</a>
										</li>
									</ul><!-- /.nav -->
								</div><!-- /.widget -->
								@endif
								@if(Auth::Check() && ($ads->user->id == Auth::id()))
					            <div class="widget">
									<div class="side-card">
										<ul class="nav nav-stacked nav-style-primary">
											<li class="nav-item">
												<a href="{{ url('/edit-property/'.$ads->id) }}" class="nav-link">
													<i class="fa fa-edit"></i> @lang('properties.edit_property')
												</a>
											</li><!-- /.nav-item -->
										</ul><!-- /.nav-style-primary -->
									</div>
								</div><!-- /.widget -->
									@endif
					            <div class="widget">
									<table class="contact">
										<h2 class="widgettitle">@lang('properties.advertizer')</h2>
										<tbody>
										<tr>
											<th>@lang('properties.advertizer_name'):</th>
											<td>{{ $owner->name }}</td>
										</tr>

										<tr>
											<th>@lang('properties.address'):</th>
											<td>{{ $owner->address }}<br>{{ $owner->city }}, {{ $owner->region }}<br>{{ $owner->country }}</td>
										</tr>

										<tr>
											<th>@lang('properties.Phone'):</th>
											<td>{{ $owner->phone_1 }}</td>
										</tr>

										<tr>
											<th>@lang('properties.email'):</th>
											<td><a href="mailto:{{ $owner->email }}">{{ $owner->email }}</a></td>
										</tr>

										<tr>
											<th>@lang('properties.member_since'):</th>
											<td>{{ substr($owner->updated_at, 0, 10) }}</td>
										</tr>

										</tbody>
									</table>
								</div><!-- /.widget -->

								@if(Auth::Check() && ($ads->user->id != Auth::id()) && false)
						            <div class="widget widget-background-white">
									    <h4>@lang('properties.sendessage')</h4>

									   {{Form::open(['url'=>'/sendMessage','method'=>'POST','role'=>'form', 'files' => true]) }}
				            				{{ Form::token() }}
				            				<input type="hidden" name="ads" value="{{$ads->id}}">
									        
									        <div class="form-group">
									            <label>@lang('properties.subject')</label>
									            <input type="text" class="form-control" name="subject"  style="<?php if($errors->first('subject')){?> border: 1px solid red !important; <?php } ?>"  value="{{old('subject')}}" required>

									            <?php if($errors->first('subject')){?>
												 <span style="color: red !important; font-size: 10px;">$errors->first('subject')</span>
											<?php } ?>
									        </div><!-- /.form-group -->                

									        <div class="form-group">
									            <label>@lang('properties.message')</label>
									            <textarea class="form-control" rows="4" name="message" style="<?php if($errors->first('message')){?> border: 1px solid red !important; <?php } ?>" required></textarea>

									            <?php if($errors->first('message')){?>
												 <span style="color: red !important; font-size: 10px;">$errors->first('message')</span>
											<?php } ?>
									        </div><!-- /.form-group -->                                

									        <div class="form-group-btn">
									            <button type="submit" class="btn btn-primary btn-block">@lang('properties.send_message')</button>
									        </div><!-- /.form-group-btn -->
									    {{ Form::close() }}
									</div><!-- /.widget -->
								@endif

                                @include('webapp.includes.compare-list-widget')
					                      
					        </div><!-- /.col-* -->
    					</div><!-- /.row -->
					</div><!-- /.container -->
	            </div><!-- /.content -->
	        </div><!-- /.main-inner -->
	    </div><!-- /.main -->
    </div><!-- /.main-wrapper -->

@endsection

@section('script')
	<script type="text/javascript">
        $(document).ready(function() {
            $("#lightgallery").lightGallery();
        });
	</script>
@endsection
