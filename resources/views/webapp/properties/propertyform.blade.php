@extends("webapp.layouts.default")

@section('content')
	
	<div class="main-wrapper">
	    <div class="main">
	        <div class="main-inner">   		        
					<div class="content-title">
						<div class="content-title-inner">
							<div class="container">		
								<h1>@lang('properties.submit_property')</h1>
							</div><!-- /.container -->
						</div><!-- /.content-title-inner -->
					</div><!-- /.content-title -->
				<div class="container maincontent">
					<div class="row">
                        @if($editing)
                        {{Form::open(['url'=>'/update-property','method'=>'POST','role'=>'form', 'files' => true, 'id' => 'propertyForm']) }}
                        @else
                        {{Form::open(['url'=>'/save-property','method'=>'POST','role'=>'form', 'multipart','files' => true, 'id' => 'propertyForm']) }}
                        @endif
                        {{ Form::token() }}
                        <input type="hidden" name="property" value="{{$ads->id}}">

						<div class="col-md-8 col-lg-9">
				            <div class="content">
				            	@if(count($errors->all()))

					              @include('partials.error',['type'=>'error','message'=>'Sorry, some errors occured. Please scroll to have details.'])

					            @endif 

					            @if($sucess != "null")

					              @include('partials.sucess',['type'=>'error','message'=>$sucess])

					            @endif
				            	
									<div class="page-header page-header-small">
										<h3>@lang('properties.basic_information')</h3>
									</div><!-- /.page-header -->

									<div class="form-group">
											<label>@lang('properties.property_title') *</label>

											<input type="text" class="form-control" name="title" required style="<?php if($errors->first('title')){?> border: 1px solid red !important; <?php } ?>"  value="{{old('title', $ads->title)}}">

											<?php if($errors->first('title')){?>
												 <span style="color: red !important; font-size: 10px;">$errors->first('title')</span>
											<?php } ?>
									</div><!-- /.form-group -->

									<div class="form-group">
											<label>@lang('properties.category') *</label>

											<select class="form-control" name="categorie" required>
                                                    <option value="">@lang('properties.choose_an_option')</option>
												@foreach(\App\Models\Categories::all() as $c)
											    	<option value="{{$c->id}}" <?php if ($c->id == old('categorie', $ads->catid)){ ?> selected <?php } ?>>@lang('general.ads_category_' . $c->category_slug)</option>
											    @endforeach
											</select>
										</div>
									<div class="form-group">
										<label>@lang('properties.description')</label>

										<textarea class="form-control" rows="6" name="description" required style="<?php if($errors->first('description')){?> border: 1px solid red !important; <?php } ?>" ">{{old('description' , $ads->description)}}</textarea>

										<?php if($errors->first('description')){?>
												 <span style="color: red !important; font-size: 10px;">$errors->first('title')</span>
										<?php } ?>
									</div><!-- /.form-group -->


									<div class="page-header page-header-small">
										<h3>@lang('properties.property_information')</h3>
									</div><!-- /.page-header -->

									<div class="row">
										<div class="col-sm-4">
										    <div class="form-group">
										        <label>@lang('properties.property_type')</label>

									            <select class="form-control" name="ad_dtype">
									                <option value="rent" <?php if ("rent" == old('ad_dtype' , $ads->ad_dtype)){ ?> selected <?php } ?>>@lang('properties.Rent')</option>
									                <option value="buy" <?php if ("buy" == old('ad_dtype' , $ads->ad_dtype)){ ?> selected <?php } ?>>@lang('properties.Buy')</option>
									                <option value="sell" <?php if ("sell" == old('ad_dtype' , $ads->ad_dtype)){ ?> selected <?php } ?>>@lang('properties.Sell')</option>
									                <!-- <option value="community" <?php if ("community" == old('ad_dtype' , $ads->ad_dtype)){ ?> selected <?php } ?>>@lang('properties.Community')</option> -->
									            </select>
										    </div><!-- /.form-group -->
									    </div><!-- /.col-* -->

										<div class="col-sm-4">
										    <div class="form-group">
										        <label>@lang('properties.price') (&#128;)</label>

										        <input type="text" class="form-control" name="price" required style="<?php if($errors->first('price')){?> border: 1px solid red !important; <?php } ?>"  value="{{old('price' , $ads->price)}}">

										        <?php if($errors->first('price')){?>
													 <span style="color: red !important; font-size: 10px;">{{$errors->first('price')}}</span>
												<?php } ?>
										    </div><!-- /.form-group -->
									    </div><!-- /.col-* -->

									    <div class="col-sm-4">
										    <div class="form-group">
										        <label>@lang('properties.bedrooms')</label>
                                                    {{ Form::select('bedrooms', $roomsOptions , old('bedrooms', $ads->bedrooms) ,['class' => 'form-control']) }}
									            <?php if($errors->first('bedrooms')){?>
													 <span style="color: red !important; font-size: 10px;">{{$errors->first('bedrooms')}}</span>
												<?php } ?>
										    </div><!-- /.form-group -->
									    </div><!-- /.col-* -->
									</div><!-- /.row -->

									<div class="row">
										<div class="col-sm-4">
										    <div class="form-group">
										        <label>@lang('properties.bathrooms')</label>

                                                {{ Form::select('bathrooms', $roomsOptions, old('bedrooms', $ads->bathrooms), ['class' => 'form-control']) }}

									            <?php if($errors->first('bathrooms')){?>
													 <span style="color: red !important; font-size: 10px;">{{$errors->first('bathrooms')}}</span>
												<?php } ?>
										    </div><!-- /.form-group -->
									    </div><!-- /.col-* -->

										<div class="col-sm-4">
										    <div class="form-group">
										        <label>@lang('properties.area') (m²)</label>

										        <input type="text" class="form-control" name="area" placeholder="Ex : xx.xx" style="<?php if($errors->first('area')){?> border: 1px solid red !important; <?php } ?>"  value="{{old('area' , $ads->area)}}">

										        <?php if($errors->first('area')){?>
													 <span style="color: red !important; font-size: 10px;">{{$errors->first('area')}}</span>
												<?php } ?>
										    </div><!-- /.form-group -->
									    </div><!-- /.col-* -->

									    
									</div><!-- /.row -->

                                    <div class="page-header page-header-small">
                                        <h3>@lang('properties.property_address_header')</h3>
                                    </div><!-- /.page-header -->

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>@lang('properties.address_autocomplete_label')</label>

                                                <input type="text" class="form-control" id="autocomplete" onFocus="geolocate()" placeholder="@lang('properties.address_autocomplete_placeholder')">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>@lang('properties.address_street_number')</label>

                                                <input readonly="true" type="text" class="form-control" id="street_number" name="address_street_number" required style="<?php if($errors->first('address_street_number')){?> border: 1px solid red !important; <?php } ?>"  value="{{old('address_street_number' , $ads->address_street_number)}}">

                                                <?php if($errors->first('address_street_number')){?>
                                                    <span style="color: red !important; font-size: 10px;">{{$errors->first('address_street_number')}}</span>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>@lang('properties.address_steet_name')</label>

                                                <input readonly="true" type="text" class="form-control" id="route" name="address_steet_name" required style="<?php if($errors->first('address_steet_name')){?> border: 1px solid red !important; <?php } ?>"  value="{{old('address_steet_name' , $ads->address_steet_name)}}">

                                                <?php if($errors->first('address_steet_name')){?>
                                                    <span style="color: red !important; font-size: 10px;">{{$errors->first('address_steet_name')}}</span>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>@lang('properties.address_apt')</label>

                                                <input type="text" class="form-control" id="address_apt" name="address_apt" style="<?php if($errors->first('address_apt')){?> border: 1px solid red !important; <?php } ?>"  value="{{old('address_apt' , $ads->address_apt)}}">

                                                <?php if($errors->first('address_apt')){?>
                                                    <span style="color: red !important; font-size: 10px;">{{$errors->first('address_apt')}}</span>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>@lang('properties.address_city')</label>

                                                <input readonly="true" type="text" class="form-control" id="locality" name="address_city" required style="<?php if($errors->first('address_city')){?> border: 1px solid red !important; <?php } ?>"  value="{{old('address_city' , $ads->address_city)}}">

                                                <?php if($errors->first('address_city')){?>
                                                    <span style="color: red !important; font-size: 10px;">{{$errors->first('address_city')}}</span>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>@lang('properties.address_region')</label>

                                                <input readonly="true" type="text" class="form-control" id="administrative_area_level_1" name="address_region" required style="<?php if($errors->first('address_region')){?> border: 1px solid red !important; <?php } ?>"  value="{{old('address_region' , $ads->address_region)}}">

                                                <?php if($errors->first('address_region')){?>
                                                    <span style="color: red !important; font-size: 10px;">{{$errors->first('address_region')}}</span>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>@lang('properties.address_zip_code')</label>

                                                <input readonly="true" type="text" class="form-control" id="postal_code" name="address_zip_code" required style="<?php if($errors->first('address_zip_code')){?> border: 1px solid red !important; <?php } ?>"  value="{{old('address_zip_code' , $ads->address_zip_code)}}">

                                                <?php if($errors->first('address_zip_code')){?>
                                                    <span style="color: red !important; font-size: 10px;">{{$errors->first('address_zip_code')}}</span>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>@lang('properties.address_country')</label>

                                                <input readonly="true" type="text" class="form-control" id="country" name="address_country" required style="<?php if($errors->first('address_country')){?> border: 1px solid red !important; <?php } ?>"  value="{{old('address_country' , $ads->address_country)}}">

                                                <?php if($errors->first('address_country')){?>
                                                    <span style="color: red !important; font-size: 10px;">{{$errors->first('address_country')}}</span>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>@lang('properties.address_suppl')</label>
                                                <input type="text" name="address_suppl" class="form-control" id="address_suppl"  value="{{old('address_suppl' , $ads->address_suppl)}}">
                                            </div>
                                        </div>
                                        <input type="hidden" name="address_lat" id="address_lat" value="{{old('address_lat' , $ads->address_lat)}}" />
                                        <input type="hidden" name="address_lng" id="address_lng" value="{{old('address_lng' , $ads->address_lng)}}" />
                                    </div>

									<div class="page-header page-header-small">
										<h3>@lang('properties.additional_information')</h3>
									</div><!-- /.page-header -->

									<div class="checkbox-list">
										<div class="checkbox">
											<label><input type="checkbox" name="conditioning" <?php if(old('conditioning', $ads->conditioning)){ ?> checked <?php }?> > @lang('properties.conditioning')</label>
										</div><!-- /.checkbox-->

										<div class="checkbox">
											<label><input type="checkbox" name="cleaning" <?php if(old('cleaning', $ads->cleaning)){ ?> checked <?php }?>> @lang('properties.cleaning')</label>
										</div><!-- /.checkbox-->

										<div class="checkbox">
											<label><input type="checkbox" name="dishwasher" <?php if(old('dishwasher', $ads->dishwasher)){ ?> checked <?php }?>> @lang('properties.dishwasher')</label>
										</div><!-- /.checkbox-->

										<div class="checkbox">
											<label><input type="checkbox" name="grill" <?php if(old('grill', $ads->grill)){ ?> checked <?php }?>> @lang('properties.grill')</label>
										</div><!-- /.checkbox-->

										<div class="checkbox">
											<label><input type="checkbox" name="internet" <?php if(old('internet', $ads->internet)){ ?> checked <?php }?>> @lang('properties.internet')</label>
										</div><!-- /.checkbox-->

										<div class="checkbox">
											<label><input type="checkbox" name="microwave" <?php if(old('microwave', $ads->microwave)){ ?> checked <?php }?>> @lang('properties.microwave')</label>
										</div><!-- /.checkbox-->

										<div class="checkbox">
											<label><input type="checkbox" name="pool" <?php if(old('pool', $ads->pool)){ ?> checked <?php }?>> @lang('properties.pool')</label>
										</div><!-- /.checkbox-->

										<div class="checkbox">
											<label><input type="checkbox" name="terrace" <?php if(old('terrace', $ads->terrace)){ ?> checked <?php }?>> @lang('properties.terrace')</label>
										</div><!-- /.checkbox-->

										<div class="checkbox">
											<label><input type="checkbox" name="balcony" <?php if(old('balcony', $ads->balcony)){ ?> checked <?php }?>> @lang('properties.balcony')</label>
										</div><!-- /.checkbox-->

										<div class="checkbox">
											<label><input type="checkbox" name="cofeepot" <?php if(old('cofeepot', $ads->cofeepot)){ ?> checked <?php }?>> @lang('properties.cofeepot')</label>
										</div><!-- /.checkbox-->

										<div class="checkbox">
											<label><input type="checkbox" name="dvd" <?php if(old('dvd', $ads->dvd)){ ?> checked <?php }?>> @lang('properties.dvd')</label>
										</div><!-- /.checkbox-->

										<div class="checkbox">
											<label><input type="checkbox" name="hairdryer" <?php if(old('hairdryer', $ads->hairdryer)){ ?> checked <?php }?>> @lang('properties.hairdryer')</label>
										</div><!-- /.checkbox-->

										<div class="checkbox">
											<label><input type="checkbox" name="iron" <?php if(old('iron', $ads->iron)){ ?> checked <?php }?>> @lang('properties.iron')</label>
										</div><!-- /.checkbox-->

										<div class="checkbox">
											<label><input type="checkbox" name="oven" <?php if(old('oven', $ads->oven)){ ?> checked <?php }?>> @lang('properties.oven')</label>
										</div><!-- /.checkbox-->

										<div class="checkbox">
											<label><input type="checkbox" name="radio" <?php if(old('radio', $ads->radio)){ ?> checked <?php }?>> @lang('properties.radio')</label>
										</div><!-- /.checkbox-->

										<div class="checkbox">
											<label><input type="checkbox" name="toaster" <?php if(old('toaster', $ads->toaster)){ ?> checked <?php }?>> @lang('properties.toaster')</label>
										</div><!-- /.checkbox-->

										<div class="checkbox">
											<label><input type="checkbox" name="bedding" <?php if(old('bedding', $ads->bedding)){ ?> checked <?php }?>> @lang('properties.bedding')</label>
										</div><!-- /.checkbox-->

										<div class="checkbox">
											<label><input type="checkbox" name="computer" <?php if(old('computer', $ads->computer)){ ?> checked <?php }?>> @lang('properties.computer')</label>
										</div><!-- /.checkbox-->	

										<div class="checkbox">
											<label><input type="checkbox" name="fan" <?php if(old('fan', $ads->fan)){ ?> checked <?php }?>> @lang('properties.fan')</label>
										</div><!-- /.checkbox-->

										<div class="checkbox">
											<label><input type="checkbox" name="heating" <?php if(old('heating', $ads->heating)){ ?> checked <?php }?>> @lang('properties.heating')</label>
										</div><!-- /.checkbox-->

										<div class="checkbox">
											<label><input type="checkbox" name="juicer" <?php if(old('juicer', $ads->juicer)){ ?> checked <?php }?>> @lang('properties.juicer')</label>
										</div><!-- /.checkbox-->

										<div class="checkbox">
											<label><input type="checkbox" name="parking" <?php if(old('parking', $ads->parking)){ ?> checked <?php }?>>  @lang('properties.parking')</label>
										</div><!-- /.checkbox-->

										<div class="checkbox">
											<label><input type="checkbox" name="roofterrace" <?php if(old('roofterrace', $ads->roofterrace)){ ?> checked <?php }?>> @lang('properties.roofterrace')</label>
										</div><!-- /.checkbox-->

										<div class="checkbox">
											<label><input type="checkbox" name="towelwes" <?php if(old('towelwes', $ads->towelwes)){ ?> checked <?php }?>> @lang('properties.towelwes')</label>
										</div><!-- /.checkbox-->

										<div class="checkbox">
											<label><input type="checkbox" name="cabletv" <?php if(old('cabletv', $ads->cabletv)){ ?> checked <?php }?>> @lang('properties.cabletv')</label>
										</div><!-- /.checkbox-->

										<div class="checkbox">
											<label><input type="checkbox" name="cot" <?php if(old('cot', $ads->cot)){ ?> checked <?php }?>> @lang('properties.cot')</label>
										</div><!-- /.checkbox-->

										<div class="checkbox">
											<label><input type="checkbox" name="fridge" <?php if(old('fridge', $ads->fridge)){ ?> checked <?php }?>> @lang('properties.fridge')</label>
										</div><!-- /.checkbox-->

										<div class="checkbox">
											<label><input type="checkbox" name="hifi" <?php if(old('hifi', $ads->hifi)){ ?> checked <?php }?>> @lang('properties.hifi')</label>
										</div><!-- /.checkbox-->

										<div class="checkbox">
											<label><input type="checkbox" name="lift" <?php if(old('lift', $ads->lift)){ ?> checked <?php }?>> @lang('properties.lift')</label>
										</div><!-- /.checkbox-->

										<div class="checkbox">
											<label><input type="checkbox" name="parquet" <?php if(old('parquet', $ads->parquet)){ ?> checked <?php }?>> @lang('properties.parquet')</label>
										</div><!-- /.checkbox-->

										<div class="checkbox">
											<label><input type="checkbox" name="smoking" <?php if(old('smoking', $ads->smoking)){ ?> checked <?php }?>> @lang('properties.smoking')</label>
										</div><!-- /.checkbox-->

										<div class="checkbox">
											<label><input type="checkbox" name="useofpool" <?php if(old('useofpool', $ads->useofpool)){ ?> checked <?php }?>> @lang('properties.useofpool')</label>
										</div><!-- /.checkbox-->
									</div><!-- /.checkbox-list -->

									
									<br>
									<div class="left">
                                        <a href="{{ url('/view-all-properties') }}" class="btn btn-info" >@lang('properties.cancel_propertyform')</a>
                                        @if(!$editing)
                                            <button class="btn btn-warning" type="reset" id="resetBtn">@lang('properties.reset_propertyform')</button>
                                            <button class="btn btn-primary" type="submit">@lang('properties.submit_property')</button>
                                        @else
                                            <button><a class="btn btn-primary" style="margin: -10px !important;">@lang('properties.save_property')</a></button>
                                        @endif
									</div><!-- /.center -->
								
				            </div><!-- /.content -->
			            </div><!-- /.col-* -->
			            <br>
			            <div class="col-md-4 col-lg-3">
			            	<div class="sidebar">

			            		<div class="widget">
									<h2 class="widgettitle">@lang('properties.upload_image')</h2>

                                    @if(count($adsImages) < 10)

									<ul style="background: none !important;" class="menu nav nav-stacked" id="files">

										<li class="nav-item" style="margin: 5px;">
											<div class="input-group">
												<span id="addUploader">
													<span class="input-group-addon" style="background: white; padding: 10px;  border: 2px solid #45b75d !important; cursor: pointer;">
														<i class="fa fa-plus-circle" aria-hidden="true" style="color: #44a950 !important;">
															
														</i>@lang('properties.add_image')
													</span>
												</span>
												<input type="hidden" name="fileNumber" id="fileNumber" value="1" >	
											</div>	
										</li>

										<li class="nav-item">
											<input type="file" name="f1" accept="image/*" class="form-control" @if(!$editing) required @endif>
										</li>
										
									</ul><!-- /.nav -->

                                    @endif

                                    @if(isset($adsImages))
                                    <div class="row" id="adsThumbnails">
                                        @foreach($adsImages as $image)
                                            <div class="col-xs-12 col-md-6 thumbnail">
                                                <div class="image">
                                                    <a href="#" class="deleteimage" data-image-id="{{ $image['image_id'] }}" ><i class="fa fa-times-circle-o fa-2x" aria-hidden="true"></i></a>
                                                    <img src="../storage/propertiesImages/{{ $image['image_name'] }}" />
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    @endif
								</div><!-- /.widget -->
			            	</div><!-- /.sidebar -->
			            </div><!-- /.col-* -->
                        {{ Form::close() }}
		            </div><!-- /.row -->
                    <div class="row">
                        <div id="propertyFormSaver">
                            <h1>Saving property...</h1>
                            <img src="{{ url('assets/img/loading.gif') }}" />
                        </div>
                    </div>
	            </div><!-- /.container -->
	        </div><!-- /.main-inner -->
	    </div><!-- /.main -->
    </div><!-- /.main-wrapper -->
    {{ Form::open(['method'=>'POST','role'=>'form', 'id' => 'deleteImageForm']) }}
    {{ Form::token() }}

    <input type="hidden" name="action" value="deleteimage">
    <input type="hidden" name="property" value="{{$ads->id}}">
    <input type="hidden" name="image_id" id="current_image_id" value="">

    {{ Form::close() }}

@include("webapp.includes.call-to-action-footer")

@endsection

@section('script')
<?php $maxuploads = (count($adsImages))?(10 - count($adsImages)):10; ?>
	
	<script type="text/javascript">
		$('#addUploader').click(function(){
			var fileNumber = $( "#fileNumber" ).val();

			if(fileNumber < {{ $maxuploads }}){
				fileNumber++;
				$( "#files" ).append( "<li class=\"nav-item\"> <input type=\"file\" name=f" + fileNumber +" accept=\"image/*\" class=\"form-control\"></li>" );
				$( "#fileNumber" ).val(fileNumber);
			}else{
				$('#addUploader').css('display', 'none');
			}

		});

		$("#propertyForm").submit(function () {

            $("#propertyForm").scrollTop(300);

            $(this).css("display","none");

            $("#propertyFormSaver").css("display","block");
        });
		
		$(".deleteimage").click(function () {

		    var currentImageLink = $(this);

            swal({
                    title: "@lang('properties.are_you_sure')",
                    text: "@lang('properties.will_be_deleted')",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#39B54A",
                    confirmButtonText: "@lang('properties.yes_delete_it')",
                    closeOnConfirm: true
                },
                function(){
                    var adsId = currentImageLink.data('image-id');
                    // Submit the delete image form
                    $("#current_image_id").val(adsId);

                    $('#deleteImageForm').submit();
                });
        });

        $('#resetBtn').click(function(){
            $(".ez-checkbox").removeClass("ez-checked");
        });
	</script>
@endsection