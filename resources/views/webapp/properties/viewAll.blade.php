@extends("webapp.layouts.default")

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">

@endsection

@section('content')

	<div class="main-wrapper">
	    <div class="main">
	        <div class="main-inner">
	        		        
					<div class="content-title">
						<div class="content-title-inner">
							<div class="container">		
								<h1>{{$title}}</h1>
							</div><!-- /.container -->
						</div><!-- /.content-title-inner -->
					</div><!-- /.content-title -->
				
				<br>
	            <div class="content">
	                <div class="container">
						
						<div class="row">
                            @if(!isset($isSearch))
                            <div class="col-xs-12">
                                {{Form::open(['url'=>'/search-property','method'=>'POST','role'=>'form']) }}
                                {{ Form::token() }}
                                <div class="map-filter-horizontal">
                                    <div class="map-filter filter" style="margin-bottom: 20px;">
                                        <div class="row">
                                            <div class="form-group col-md-2">
                                                <label>@lang('general.general_property_categorie')</label>

                                                <select class="form-control" name="category" >
                                                    @foreach($categories as $cat)
                                                        <option value="{{ $cat->category_slug }}">@lang('general.ads_category_' . $cat->category_slug)</option>
                                                    @endforeach
                                                </select>
                                            </div><!-- /.form-group -->

                                            <div class="form-group col-md-2">
                                                <label>@lang('general.general_contract')</label>

                                                <select class="form-control" name="type" >
                                                    <option value="rent" > @lang('general.general_rent')</option>
                                                    <option value="buy" > @lang('general.general_buy')</option>
                                                    <option value="sell" > @lang('general.general_sell')</option>
                                                </select>
                                            </div><!-- /.form-group -->

                                            <div class="col-md-4">
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label> @lang('general.general_price_from')</label>
                                                        <input type="text" class="form-control"  name="minPrice" value="0" required>
                                                    </div><!-- /.form-group -->

                                                    <div class="form-group col-md-6">
                                                        <label> @lang('general.general_price_to')</label>
                                                        <input type="text" class="form-control"  name="maxPrice" value="5000" required>
                                                    </div><!-- /.form-group -->
                                                </div><!-- /.row -->
                                            </div><!-- /.col-* -->

                                            <div class="col-md-2">
                                                <div class="form-group-btn form-group-btn-placeholder-gap">
                                                    <button type="submit" class="btn btn-primary btn-block"> @lang('general.general_search_now')</button>
                                                </div><!-- /.form-group -->
                                            </div><!-- /.col-* -->
                                        </div><!-- /.row -->
                                    </div><!-- /.filter -->
                                </div><!-- /.filter-wrapper -->
                                {{ Form::close() }}
                            </div>
                            @endif
							<div class="col-md-8 col-lg-9">
								<div class="row">
                                    <div class="col-xs-12">
                                    @if($sucess != "null")

                                            @include('partials.alert',['type'=>'error','message'=>$sucess])

                                        @endif
                                    @if($error != "null")

                                            @include('partials.error',['type'=>'error','message'=>$error])

                                        @endif
                                    </div>

									@foreach($properties as $property)		
										<div class="col-md-6 col-lg-4">
											<div class="listing-box">
												<div class="listing-box-image" style="

													@if(count($property->images) != 0)
														background-image: url('{{ asset("storage/propertiesImages/" . $property->images[0]->image_name) }}')
													@else
														background-image: url('{{ asset("assets/img/tmp/tmp-5.jpg") }}')
													@endif
													">
													<div class="listing-box-image-label" style="@if($property->statut == '0')background: #d9534f !important;@endif">
														@if($property->statut == '1')
															@lang('properties.active_property')
														@else
															@lang('properties.disabled_property')
														@endif
													</div><!-- /.listing-box-image-label -->
													
													<span class="listing-box-image-links">
														@if(Auth::check() && $property->user_id == Auth::id())
															<a href="{{ url('/edit-property/'.$property->id) }}" style="@if($property->statut == '0')background: #d9534f !important;@endif"><i class="fa fa-pencil-square-o"></i> <span>@lang('properties.edit_property')</span></a>
														@endif

														<a href="{{ url('/view-details-property/'.$property->id) }}" style="@if($property->statut == '0')background: #d9534f !important;@endif"><i class="fa fa-search"></i> <span>@lang('properties.view_detail')</span></a>

														@if($property->statut == '1')
															<a href="{{ url('/compare-property/'.$property->id) }}" style="@if($property->statut == '0')background: #d9534f !important;@endif" class="compareProperty" value="{{$property->id}}" name="{{$property->title}}"><i class="fa fa-balance-scale"></i> <span>@lang('properties.compare_list')</span></a>
														@endif

														@if(Auth::check() && $property->user_id == Auth::id())
															@if($property->statut == '0')
																<a href="{{ url('/delete-property/'.$property->id) }}" style="background: #d9534f !important;" class="enableProperty" value="{{$property->id}}" name="{{$property->title}}"><i class="fa fa-ban"></i> <span>@lang('properties.enable_property')</span></a>
																<a href="{{ url('/delete-property/'.$property->id) }}" style="background: #d9534f !important;" class="deleteProperty" value="{{$property->id}}" name="{{$property->title}}"><i class="fa fa-ban"></i> <span>@lang('properties.delete_property')</span></a>
															@else
																<a href style="@if($property->statut == '0')background: #d9534f !important;@endif" class="disableProperty" value="{{$property->id}}" name="{{$property->title}}"><i class="fa fa-ban"></i> <span>@lang('properties.disable_property')</span></a>
															@endif
														@endif 
													</span>		
												</div><!-- /.listing-box-image -->

												<div class="listing-box-title"
													style="@if($property->statut == '0') background-color: #d9534f !important; @endif"
												>
													<h2><a href="{{ url('/edit-property/'.$property->id) }}">{{$property->title}}</a></h2>
													<h3>{{$property->price}} &#128;</h3>
												</div><!-- /.listing-box-title -->

												<div class="listing-box-content">
													<dl>
														<dt>@lang('properties.category')</dt><dd>@lang('general.ads_category_' . \App\Models\Categories::find($property->catid)->category_slug)</dd>
														<dt>@lang('properties.type')</dt><dd>@lang('properties.type_' . $property->ad_dtype)</dd>
														<dt>@lang('properties.area')</dt><dd>{{$property->area}} (m²)</dd>
														
													</dl>
												</div><!-- /.listing-box-cotntent -->
											</div><!-- /.listing-box -->			
										</div><!-- /.col-* -->
									@endforeach
									
									
								</div><!-- /.row -->		

								{{ $properties->links() }}	
							</div><!-- /.col-sm-* -->
							
							<div class="col-md-4 col-lg-3"> 			
								@if(Auth::check() && (isset($userProfile->address)))
								<div class="widget">
									<h2 class="widgettitle">Contact Information</h2>

									<table class="contact">
										<tbody>
											<tr>
												<th>@lang('properties.address'):</th>
												<td>{{$userProfile->address}}<br>{{$userProfile->city}}, {{$userProfile->region}}<br>{{$userProfile->country}}</td>
											</tr>

											<tr>
												<th>@lang('properties.Phone'):</th>
												<td>{{$userProfile->phone_1}}</td>
											</tr>

											<tr>
												<th>@lang('properties.email'):</th>
												<td><a href="mailto:info@example.com">{{Auth::User()->email}}</a></td>
											</tr>

											
										</tbody>
									</table>	
								</div><!-- /.widget -->	
								@endif	

                                @include('webapp.includes.compare-list-widget')

							</div><!-- /.widget -->
						</div><!-- /.row -->
					</div><!-- /.container -->
	            </div><!-- /.content -->
	        </div><!-- /.main-inner -->
	    </div><!-- /.main -->
    </div><!-- /.main-wrapper -->

@include("webapp.includes.call-to-action-footer")

@endsection

@section('script')

<script type="text/javascript">

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $(".disableProperty").click(function(){

        var id = $(this).attr("value");
        var name = $(this).attr("name");

        swal({
                title: "Are you sure?",
                text: "The property : " + name + " will be disable. It will not be visible for the other users.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#39B54A",
                confirmButtonText: "Yes, disable it!",
                closeOnConfirm: false
            },
            function(){
                $.ajax({
                    type: 'POST',
                    cache:false,
                    headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
                    url: 'api/classified/put',
                    data: "id=" + id,
                    dataType: 'json',

                    success : function(data, statut){
                        //console.log(data.statut);
                        if(data.statut == "done")
                            swal({
                                    title: "Disabled!",
                                    text: "Your property : " + name + " has been disabled.",
                                    type: "success",
                                    showCancelButton: false,
                                    confirmButtonColor: "#39B54A",
                                    confirmButtonText: "OK",
                                    closeOnConfirm: false
                                },
                                function(){
                                    location.reload();
                                });
                        //swal("Disabled!", "Your property : " + $(this).attr("name") + " has been disabled.", "success");
                    },

                    error : function(resultat, statut, erreur){
                        swal("Warning!", "Sorry. An error occured while disabling : " + name + ". Please retry later", "error");
                    }
                });


            });

        return false;
    });

    $(".deleteProperty").click(function(){

        var id = $(this).attr("value");
        var name = $(this).attr("name");

        swal({
                title: "Are you sure?",
                text: "The property : " + name + " will be delete.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#39B54A",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function(){
                $.ajax({
                    type: 'POST',
                    cache:false,
                    headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
                    url: 'api/classified/delete',
                    data: "id=" + id,
                    dataType: 'json',

                    success : function(data, statut){
                        //console.log(data.statut);
                        if(data.statut == "done")
                            swal({
                                    title: "Deleted!",
                                    text: "Your property : " + name + " has been deleted.",
                                    type: "success",
                                    showCancelButton: false,
                                    confirmButtonColor: "#39B54A",
                                    confirmButtonText: "OK",
                                    closeOnConfirm: false
                                },
                                function(){
                                    location.reload();
                                });
                        //swal("Disabled!", "Your property : " + $(this).attr("name") + " has been disabled.", "success");
                    },

                    error : function(resultat, statut, erreur){
                        swal("Warning!", "Sorry. An error occured while deleting : " + name + ". Please retry later", "error");
                    }
                });


            });

        return false;
    });

    $(".enableProperty").click(function(){

        var id = $(this).attr("value");
        var name = $(this).attr("name");

        swal({
                title: "Are you sure?",
                text: "The property : " + name + " will be visible for all users.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#39B54A",
                confirmButtonText: "Yes, enable it!",
                closeOnConfirm: false
            },
            function(){
                $.ajax({
                    type: 'POST',
                    cache:false,
                    headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
                    url: 'api/classified/enable',
                    data: "id=" + id,
                    dataType: 'json',

                    success : function(data, statut){
                        //console.log(data.statut);
                        if(data.statut == "done")
                            swal({
                                    title: "Enabled!",
                                    text: "Your property : " + name + " has been enabled.",
                                    type: "success",
                                    showCancelButton: false,
                                    confirmButtonColor: "#39B54A",
                                    confirmButtonText: "OK",
                                    closeOnConfirm: false
                                },
                                function(){
                                    location.reload();
                                });
                        //swal("Disabled!", "Your property : " + $(this).attr("name") + " has been disabled.", "success");
                    },

                    error : function(resultat, statut, erreur){
                        swal("Warning!", "Sorry. An error occured while enabling : " + name + ". Please retry later", "error");
                    }
                });


            });

        return false;
    });

    $(".compareProperty").click(function(event){

        event.preventDefault();

        var id = $(this).attr("value");
        var name = $(this).attr("name");
        console.log(id);

        swal({
                title: "@lang('general.general_compare_add')",
                text: "@lang('general.general_compare_add_pre') '" + name + "' @lang('general.general_compare_add_suf')",
                type: "info",
                showCancelButton: true,
                cancelButtonText: "@lang('general.general_compare_add_cancel')",
                confirmButtonText: "@lang('general.general_compare_add_yes_add')",
                closeOnConfirm: false
            },

            function(){

                $.ajax({
                    url: 'api/classified/addCompareProperty',
                    type: 'POST',
                    data: {_token: CSRF_TOKEN, id: id},
                    dataType: 'JSON',
                    success: function (data) {

                        console.log(data);

                        location.reload();

                        //swal("Added!", "The property.", "success");
                    },

                    error : function(resultat, statut, erreur){
                        swal("Warning!", "Sorry. An error occured while enabling : " + name + ". Please retry later", "error");
                    }
                });
            });

    });
</script>

@endsection

