@extends("webapp.layouts.default")

@section('content')

<div class="main-wrapper">
	    <div class="main">
	        <div class="main-inner">

				<div class="content-title">
					<div class="content-title-inner">
						<div class="container">
							<h1>@lang('properties.discussion_for') : <b>{{ $ads->title }}</b></h1>
						</div><!-- /.container -->
					</div><!-- /.content-title-inner -->
				</div><!-- /.content-title -->
				<br>
	            <div class="content">
	                <div class="container">
	                	<div class="row">
	                		<div class="listing-detail col-md-12 col-lg-12">
	                			<ul class="comments">
									@foreach($messages as $sms)
										<li>
											<div class="comment">
												<div class="comment-content">
													<div class="comment-meta">
														<div class="comment-meta-author">
															@lang('properties.posted_by') <strong>{{ $sms->senderName }}.  <a href="mailto:{{ $sms->senderEmail }}" target="_top">{{ $sms->senderEmail }}</a></strong>
														</div><!-- /.comment-meta-author -->

														<div class="comment-meta-date">
															<span>{{ $sms->created_at }}</span>
														</div>
													</div><!-- /.comment -->

													<div class="comment-body">
														<div class="comment-rating">
															{{$sms->subject}}
														</div><!-- /.comment-rating -->

														{{ $sms->message_body }}
													</div><!-- /.comment-body -->
												</div><!-- /.comment-content -->
											</div><!-- /.comment -->
										</li>
									@endforeach
								</ul>
	                		</div>
							<div class="col-md-12 col-lg-12">
								@if($messages_type == 'plan')
								<a href="{{ url('view-details-plan/' . $ads->id) }}">« Retour à l'annonce</a>
								@else
								<a href="{{ url('view-details-property/' . $ads->id) }}">« Retour à l'annonce</a>
								@endif
							</div>
	                	</div>
	                </div>
	            </div>
	        </div>
	    </div>
</div>

@endsection