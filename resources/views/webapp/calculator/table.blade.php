@extends("webapp.layouts.default")

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css" rel="stylesheet" type="text/css"> --}}
    <link href="{{ asset('assets/admin/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/admin/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('content')

<div class="main-wrapper">
   <div class="main">
       <div class="main-inner">

           <div class="content-title">
              <div class="content-title-inner">
                 <div class="container">		
                    <h1>{{$title}}</h1>
                </div><!-- /.container -->
            </div><!-- /.content-title-inner -->
        </div><!-- /.content-title -->

        <br>
        <div class="content">
           <div class="container">


                <table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Email</th>
                      <th>Phone</th>
                      <th>Result</th>
                      <th>Date Submitted</th>
                      <th class="center">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach ($calculator as $key=>$cal)
                    {{-- expr --}}
                    <tr>
                      <td>{{$key+1}}</td>
                      <td>{{$cal->email}}</td>
                      <td>{{$cal->phone}}</td>
                      <td>
                         <a href="{{url('downloadfile/'.$cal->pdf)}}">Download Result</a> 
                      </td>
                      <td>{{$cal->created_at->format('d, F Y')}}</td>
                      <td class="center"><form action="{{url('/calculator/delete/'.$cal->id)}}" method="POST">{{csrf_field()}}<button class="btn btn-danger"><i class="fa fa-trash"></i></button></form></td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
          
                <br>
                <br>

                <div style="overflow:auto;">
                  <div style="float:right;">
                    <form method="POST" action="{{url('/calculator/destroy')}}">  
                    {{csrf_field()}}
                      <button class="btn btn-danger">Delete All Junk Data</button>
                    </form>
                  </div>
                </div>


        </div><!-- /.container -->
    </div><!-- /.content -->
</div><!-- /.main-inner -->
</div><!-- /.main -->
</div><!-- /.main-wrapper -->

@include("webapp.includes.call-to-action-footer")

@endsection

@section('script')

<script type="text/javascript" src="{{ asset('assets/admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable({
      language: {
        paginate: {
      next: '&#8594;', // or '→'
      previous: '&#8592;' // or '←' 
    }
  }
    });
} );
</script>
@endsection

