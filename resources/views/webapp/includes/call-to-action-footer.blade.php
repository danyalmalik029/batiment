<div class="container">
    <div class="call-to-action">
        <div class="call-to-action-inner">
            <div class="call-to-action-title">
                <div class="logo-shape"></div><!-- /.logo-shape -->
                <h1>@lang('general.general_cta_title')</h1>
                <h2>@lang('general.general_cta_info')</h2>
            </div><!-- /.call-to-action-title -->

            <a href="{{ url('/contact/') }}" class="btn btn-primary">@lang('general.general_cta_contact_us')</a>
        </div><!-- /.call-to-action-inner -->
    </div><!-- /.call-to-action -->
</div>