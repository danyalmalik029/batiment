<div class="form-group m-b-0 text-xs-center">
    <div class="full-width col-sm-12">
        <a href="/login/facebook">
            <button type="button" class="btn btn-facebook waves-effect waves-light m-t-20">
                <i class="fa fa-facebook m-r-5"></i> Facebook
            </button>
        </a>
        <!-- <a href="/login/twitter">
            <button type="button" class="btn btn-twitter waves-effect waves-light m-t-20">
                <i class="fa fa-twitter m-r-5"></i> Twitter
            </button>
        </a>-->
        <a href="/login/google">
            <button type="button" class="btn btn-googleplus waves-effect waves-light m-t-20">
                <i class="fa fa-google-plus m-r-5"></i> Google+
            </button>
        </a>
    </div>
</div>