<div style="@if(session('compare'))display : block;@else display : none;@endif" id="compareHeader">
    <h5 class="widgettitle">@lang('properties.compare_items')</h5>
</div>
<div id="compareList">
    @if(session('compare') > 0)

    @for($i=1; $i <= session('compare'); $i++)

    <?php $com = "compare_".$i; $id = session($com); ?>

    @if($id)

    <div class="listing-small">
        <div class="listing-small-inner">
            
            <!--
            <div class="listing-small-image">
                <a style="background-image: url('assets/img/tmp/agent-1.jpg');"></a>
            </div><!-- /.listing-small-image -->

            <div class="listing-small-content">
                <h3> {{\App\Models\Ads::find($id)->title}}</h3>
                <h4>{{\App\Models\Ads::find($id)->ad_dtype}}</h4>
            </div><!-- /.listing-small-content -->
        </div><!-- /.listing-small-inner -->
    </div><!-- /.listing-small -->

    @endif

    @endfor

    <div>
        <a href="{{ url('/compare-properties')  }}" class="btn btn-info btn-sm btn-block">@lang('properties.compare')</a>
    </div>
    @endif
</div>