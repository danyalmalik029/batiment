<div class="footer-wrapper">
    <div class="container">
        <div class="footer-inner">
            <div class="footer-top">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <h2>@lang('general.general_etb_batiment')</h2>

                        <p>
                            @lang('general.general_etbfooterinfo')
                        </p>

                    </div>

                    <!--
                    <div class="col-xs-12 col-md-4">
                        <h2>@lang('general.general_subscribe_to_newsletter')</h2>

                        <form method="post" action="?">
                            <div class="input-group">
                                <input type="email" class="form-control" placeholder="@lang('general.general_your_email_address')">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" type="submit">@lang('general.general_subscribe')</button>
                                </span>
                            </div>
                        </form>
                        <p>
                            * @lang('general.general_nospam').
                        </p>
                    </div>
                    -->

                    <div class="col-xs-12 col-md-6">
                        <h2>@lang('general.general_follow_us')</h2>
                        <div class="social">
                            <a href="https://www.facebook.com/etbbatiment" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="https://twitter.com/etbbatiment" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="https://www.linkedin.com/company/etbbatiment/" target="_blank"><i class="fa fa-linkedin"></i></a>
                            <a href="{{ url('/contact') }}"><i class="fa fa-envelope"></i></a>
                            <!-- <a href="#"><i class="fa fa-instagram"></i></a> -->
                        </div>
                    </div>
                </div>
            </div>

            <div class="footer-bottom">
                <div class="footer-left">
                    &copy; <?php echo date("Y") ?>, @lang('general.general_copyright').
                </div><!-- /.footer-left -->

                <div class="footer-right">
                    <ul class="nav nav-pills">
                        <li class="nav-item"><a href="{{ url('/post/'.trans('general.post_alias_terms_and_conditions').'/') }}" class="nav-link">@lang('general.menu_terms_and_conditions')</a></li>
                        <li class="nav-item"><a href="{{ url('/contact') }}" class="nav-link">Contact</a></li>
                    </ul>
                </div><!-- /.footer-right -->
            </div><!-- /.footer-bottom -->
        </div><!-- /.footer-inner -->
    </div><!-- /.container -->
</div><!-- /.footer-wrapper -->
</div><!-- /.page-wrapper -->
<div class="header-sticky"></div>

<script type="text/javascript" src="{{ asset('assets/webapp/js/jquery.js') }}"></script>
<script type="text/javascript" src="{{asset('dist/jquery.inputmask.bundle.min.js')}}"></script>

@if(!Request::is('estimate-calculation'))
<script type="text/javascript" src="{{ asset('assets/webapp/js/jquery.ezmark.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/webapp/js/villareal.js?43543fc34vt3r4b76gJJHJK767tg') }}"></script>
@endif
<script type="text/javascript" src="{{ asset('assets/webapp/js/masonry.pkgd.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/webapp/js/image-picker.min.js') }}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="{{ asset('assets/webapp/js/tether.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/webapp/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/webapp/js/gmap3.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/webapp/js/leaflet.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/webapp/js/leaflet.markercluster.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/webapp/libraries/owl-carousel/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/webapp/libraries/chartist/chartist.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/webapp/js/scrollPosStyler.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/webapp/js/bootstrap-datepicker.js?43543fc34vt3r4b76gJJHJK767tg') }}"></script>

<script type="text/javascript" src="{{ asset('assets/webapp/lightgallery/dist/js/lightgallery-all.min.js?43543fc34vt3r4b76gJJHJK767tg') }}"></script>

<script type="text/javascript" src="{{ asset('sweetalert-master/dist/sweetalert.min.js') }}"></script>



@yield('script')

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAgvcJbdpJT1ceiDay_79_4fccXiM5VDH8&libraries=places&callback=initAutocomplete" async defer></script>

<script>
    function markRead(id) {
        $.get('{{route('mark_noti_read')}}' + '/' + id, function (data) {
            //success data
//            alert('1');
            if(data=='success')
            {
                console.log($('.url').attr('data-id'));
                window.location =   ($('.url').attr('data-id'));

            }
            console.log(data);
        })
    }
</script>
</body>
</html>
