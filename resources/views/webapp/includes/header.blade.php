
<?php $page = (isset($page)) ? $page : ""; ?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

    <?php if(isset($pagetitle)): ?>

    <title>{{ $pagetitle }}</title>

    <?php else: ?>

    <title>ETB Batiment</title>

    <?php endif;  ?>

    <meta name="keywords" content="ETB Batiment, "/>
    <meta name="description" content="ETB Batiment, trouvez tout ce qu'il y a de bon pour votre logement à un seul endroit"/>
    <meta name="subject" content="Site web immobilier">
    <meta name="copyright"content="ETB Batiment">
    <meta name="language" content="fr_FR">
    <meta name="robots" content="index,follow" />
    <meta name="revised" content="Sunday, April 21th, 2016, 3:30 AM" />
    <meta name="abstract" content="">
    <meta name="topic" content="">
    <meta name="summary" content="">
    <meta name="Classification" content="News">

    <?php if(isset($article['article']['userfullname'])): ?>

    <meta name="author" content="{{ $article['article']['userfullname'] }}" >

    <?php else: ?>
    <meta name="author" content="ETB Batiment" >

    <?php endif;  ?>

    <meta name="designer" content="Hilaire Douanla, hdouanla.ei@gmail.com">
    <meta name="copyright" content="2017, ETB Batiment">
    <meta name="reply-to" content="info@etb-batiment.com">
    <meta name="owner" content="">
    <meta name="url" content="http://etb-batiment.com">
    <meta name="identifier-URL" content="http://etb-batiment.com">
    <meta name="directory" content="submission">
    <meta name="category" content="ads">
    <meta name="coverage" content="Worldwide">
    <meta name="distribution" content="Global">
    <meta name="rating" content="General">
    <meta name="revisit-after" content="7 days">
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">

    <script src="https://js.pusher.com/4.3/pusher.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,300,700&subset=latin,latin-ext" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/webapp/fonts/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/webapp/libraries/owl-carousel/owl.carousel.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/webapp/libraries/chartist/chartist.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/webapp/css/leaflet.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/webapp/css/image-picker.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/webapp/css/leaflet.markercluster.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/webapp/css/leaflet.markercluster.default.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/webapp/css/styles.css?43543fc34vt3r4b76gJJHJK767tg') }}" rel="stylesheet" type="text/css" id="css-primary">
    <link href="{{ asset('assets/webapp/css/datepicker.css') }}" rel="stylesheet" type="text/css" id="css-primary">
    <link href="{{ asset('assets/webapp/lightgallery/dist/css/lightgallery.min.css') }}" rel="stylesheet" type="text/css" id="css-primary">
    <link href="{{ asset('sweetalert-master/dist/sweetalert.css') }}" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    @yield('meta')

    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/webapp/favicon.png') }}">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- App Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('assets/webapp/img/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('assets/webapp/img/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('assets/webapp/img/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('assets/webapp/img/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('assets/webapp/img/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('assets/webapp/img/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('assets/webapp/img/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('assets/webapp/img/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/webapp/img/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{asset('assets/webapp/img/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('assets/webapp/img/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('assets/webapp/img/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/webapp/img/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('assets/webapp/img/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{asset('assets/webapp/img/ms-icon-144x144.png')}}">
    <meta name="theme-color" content="#ffffff">

    @yield('css')
    <script>
        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;

        var pusher = new Pusher('991e3127ef123cef076b', {
            cluster: 'ap2',
            encrypted: true,
            forceTLS: true
        });

        var channel = pusher.subscribe('message-{{\Illuminate\Support\Facades\Auth::id()}}');
        channel.bind('Event', function(data) {
            // setTimeout(function () {
            {{--// create the notification--}}
            {{--var notification = new NotificationFx({--}}
            {{--message: '<a href="{{route('admin.file_discussion')}}/'+data.file_id+'"><h4>New notification received</h4><p>'+data.message+'</p></a>',--}}
            {{--layout: 'growl',--}}
            {{--effect: 'slide',--}}
            {{--type: 'notice', // notice, warning or error--}}
            {{--});--}}

            {{--// show the notification--}}
            {{--notification.show();--}}

            {{--}, 1200);--}}
                $('#badge').text(data.count);
            if($('#menu li').size()>0)
            {
                $('#empty').remove();
            }
            $('#menu').prepend('<li onclick="markRead(\''+data.notify_id+'\')">' +
                '<a style="padding:0px 5px;" class="url"  data-id="{{url('/list-messages')}}/'+data.message.ads_id+'"><b>'+data.user.name+'</b> commented on property</a>'+
                '</li>');
            if($('#menu li').size() > 4)
            {
                $('#menu li:last').remove();
            }
        });

    </script>

</head>

<body class="">

<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.11&appId=411637895902424';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>


<div class="page-wrapper">

    <div class="header-wrapper">
        <div class="header">
            <div class="header-inner">
                <div class="container">
                    <div class="header-topbar">
                        <div class="header-topbar-left">
                            <ul class="nav nav-pills nav-topbar">
                                @if (Auth::guest())
                                    <li class="nav-item"><a href="{{ url('/login') }}" class="nav-link">@lang('general.general_login')</a></li>
                                    <li class="nav-item"><a href="{{ url('/register') }}" class="nav-link">@lang('general.general_register')</a></li>
                                @else
                                    <li class="nav-item"><a href="{{ url('/userprofile') }}" class="nav-link"><i class="fa fa-user" aria-hidden="true"></i> @lang('general.general_profile')</a></li>
                                    <li class="nav-item" id="notification-item">
                                        <a  data-toggle="dropdown"  aria-expanded="false" class="nav-link"><span><i class="fa fa-bell" aria-hidden="true"></i></span> Notification
                                            <span class="badge" id="badge">{{count(\Illuminate\Support\Facades\Auth::user()->unreadNotifications)}}</span></a>
                                        <ul class="dropdown-menu" id="menu" role="menu">
                                            @forelse(\Illuminate\Support\Facades\Auth::user()->unreadNotifications as $notification)
                                            <li onclick="markRead('{{$notification->id}}')">
                                                <a  style="padding:0px 5px;" class="url" data-id="{{url('/list-messages')}}/{{$notification->data["message"]["ads_id"]}}" href="#"><b>{{$notification->data["user"]["name"]}}</b> commented on property</a>
                                            </li>
                                                @empty
                                                <li>
                                                    <a id="empty">No Unread Messages</a>
                                                </li>
                                                @endforelse
                                        </ul>
                                    </li>
                                    <li class="nav-item"><a href="{{ url('/logout') }}" class="nav-link">@lang('general.general_logout')</a></li>
                                @endif

                                <li class="nav-item"><a href="{{ url('/contact') }}" class="nav-link">@lang('general.general_contact')</a></li>
                            </ul><!-- /.nav -->
                        </div><!-- /.header-topbar-left -->

                        <div class="header-topbar-right nav-topbar nav-topbar-social">
                            <ul class="nav nav-pills">
                                <li class="nav-item"><a href="https://www.facebook.com/etbbatiment" class="nav-link" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                <li class="nav-item"><a href="https://twitter.com/etbbatiment" class="nav-link" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                <li class="nav-item"><a href="https://www.linkedin.com/company/etbbatiment/" class="nav-link" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                <li class="nav-item"><a href="{{ url('/contact') }}" class="nav-link"><i class="fa fa-envelope-o"></i></a></li>
                            </ul><!-- /.nav -->
                        </div><!-- /.header-topbar-right -->
                    </div><!-- /.header-topbar -->

                    <div class="header-top">
                        <div class="header-top-inner">
                            <a class="header-logo" href="{{ url('/') }}">
                                <img src="{{ asset('assets/webapp/img/logo_etb.png') }}" />
                            </a><!-- /.header-logo -->

                            <div id="header-buttons">
                                <div class="row">
                                    <div class="col-md-4"><a href="{{ url('/bank-financial') }}" class="link-item action-button">@lang('general.general_action_buy')</a></div>
                                    <div class="col-md-4"><a href="{{ route('beforeStart') }}" class="link-item action-button"> @lang('general.general_action_build') </a></div>
                                    <!--
                                    <div class="col-md-4"><a href="{{ url('/my-properties') }}" class="link-item action-button">@lang('general.general_action_manage')</a></div>
                                    -->
                                    <div class="col-md-4"><a href="{{ url('/estimate-calculation') }}" class="link-item action-button">@lang('general.general_action_devis')</a></div>
                                    <div class="col-md-4"><a href="{{ url('/view-rent-properties') }}" class="link-item action-button">@lang('general.general_action_rent')</a></div>
                                    <div class="col-md-4"><a href="{{ url('/view-sale-properties') }}" class="link-item action-button">@lang('general.general_action_sell')</a></div>
                                    <div class="col-md-4"><a href="{{ url('/submit-property') }}" class="link-item action-button">@lang('general.general_action_submit')</a></div>
                                </div>
                            </div>

                            <button class="navbar-toggler pull-xs-right hidden-md-up" type="button" data-toggle="collapse" data-target=".nav-primary-wrapper">
                                <span></span>
                                <span></span>
                                <span></span>
                            </button>
                            {{--Archionline accompanies thousands of individuals in their housing project and is now making available its works costing tool. With some information about your home, the online calculator tells you the price of your individual home, be it a modern home, a wood house or a traditional home. The calculator will also offer to receive free by email the detailed price batch by lot.--}}
                        </div><!-- /.header-top-inner -->
                    </div><!-- /.header-top -->

                    <div class="header-bottom">
                        <div class="header-bottom-inner">

                            <div class="nav-primary-wrapper collapse navbar-toggleable-sm">
                                <ul class="nav nav-pills nav-primary">
                                    <li class="nav-item">
                                        <a href="{{ url('/') }}" class="nav-link <?php if($page == 'home') echo "active"; ?>">
                                            @lang('general.menu_home')
                                        </a>
                                    </li>

                                    <li class="nav-item nav-item-parent">
                                        <a href="#" class="nav-link <?php if(($page == 'submit-properties') || ($page == 'view-properties') || ($page == 'my-properties')) echo "active"; ?>">
                                            {{trans('general.menu_properties')}}
                                        </a>

                                        <ul class="sub-menu">
                                            <li><a href="{{ url('/view-all-properties') }}" style="<?php if($page == 'view-properties') echo "background-color: #39B54A !important; color: white !important;"; ?>">
                                            @lang('general.menu_view_properties')</a></li>
                                            <li><a href="{{ url('/my-properties') }}" style="<?php if($page == 'my-properties') echo "background-color: #39B54A !important; color: white !important;"; ?>">@lang('general.menu_my_properties')</a></li>
                                            <li ><a href="{{ url('/submit-property') }}" style="<?php if($page == 'submit-properties') echo "background-color: #39B54A !important; color: white !important;"; ?>">@lang('general.menu_submit_property')</a></li>
                                            <li><a href="{{ url('/compare-properties')  }}">@lang('general.menu_compare_properties')</a></li>
                                            <li><a href="{{ url('/post/'.trans('general.post_alias_help_properties').'/') }}">@lang('general.menu_help_properties')</a></li>
                                        </ul>
                                    </li>

                                    <li class="nav-item nav-item-parent">
                                        <a href="#" class="nav-link <?php if(($page == 'submit-plan') || ($page == 'upload-image-form') || ($page == 'plans-zip')|| ($page == 'my-plans')) echo "active"; ?>">
                                            {{trans('general.menu_plan')}}
                                        </a>

                                        <ul class="sub-menu">
                                            <li><a href="{{ url('/view-all-plans') }}" style="<?php if($page == 'view-all-plans') echo "background-color: #39B54A !important; color: white !important;"; ?>">
                                                    @lang('general.menu_view_plans')</a></li>
                                            <li><a href="{{ url('/my-plans') }}" style="<?php if($page == 'my-plans') echo "background-color: #39B54A !important; color: white !important;"; ?>">@lang('general.menu_my_plans')</a></li>
                                            <li><a href="{{ url('/create-plan') }}" style="<?php if($page == 'submit-plan') echo "background-color: #39B54A !important; color: white !important;"; ?>">
                                            @lang('general.menu_post_plan')</a></li>
                                            <li><a href="{{ url('/post/'.trans('general.post_alias_help_plans').'/') }}">@lang('general.menu_help_plans')</a></li>
                                        </ul>
                                    </li>

                                    <li class="nav-item nav-item-parent	">
                                        <a href="#" class="nav-link ">
                                            @lang('general.menu_agents')
                                        </a>

                                        <ul class="sub-menu">
                                            <li><a href="{{ url('/agents') }}">@lang('general.menu_all_agents')</a></li>
                                            <li><a href="{{ url('/post/'.trans('general.post_alias_bocome_agent').'/') }}">@lang('general.menu_become_agent')</a></li>
                                            <li><a href="{{ url('/agencies') }}">@lang('general.menu_agencies')</a></li>
                                            <li><a href="{{ url('/post/'.trans('general.post_alias_become_agency').'/') }}">@lang('general.menu_submit_agency')</a></li>
                                        </ul>
                                    </li>

                                    <li class="nav-item nav-item-parent">
                                        <a href="#" class="nav-link ">
                                            @lang('general.menu_community')
                                        </a>

                                        <ul class="sub-menu">

                                            <li><a href="{{ url('/blog') }}">@lang('general.menu_blog')</a></li>
                                            <li><a href="{{ url('/guide') }}">@lang('general.menu_construction_guide')</a></li>

                                        </ul>
                                    </li>
                                    <!--
                                    <li class="nav-item nav-item-parent">
                                        <a href="#" class="nav-link ">
                                            @lang('general.menu_tools')
                                        </a>

                                        <ul class="sub-menu">

                                            <li><a href="{{ url('/') }}">@lang('general.menu_3d_plans')</a></li>
                                            <li><a href="{{ url('/') }}">@lang('general.menu_interior_design')</a></li>
                                            <li><a href="{{ url('/') }}">@lang('general.menu_exterior_design')</a></li>
                                            <li><a href="{{ url('/post/'.trans('general.post_alias_apps').'/') }}">@lang('general.menu_apps')</a></li>

                                        </ul>
                                    </li>
                                    -->

                                    <li class="nav-item nav-item-parent">
                                        <a href="#" class="nav-link ">
                                            @lang('general.menu_building_permit')
                                        </a>

                                        <ul class="sub-menu">
                                            <li><a href="{{ url('/post/'.trans('general.post_alias_licence_guide').'/') }}">@lang('general.menu_building_licence_guide')</a></li>
                                            <!-- <li><a href="{{ url('/') }}">@lang('general.menu_building_my_file')</a></li> -->
                                            <!-- <li><a href="{{ url('/') }}">@lang('general.menu_building_town_planning')</a></li> -->
                                            <li><a href="{{ url('/post/'.trans('general.post_alias_avoid_a_refusal').'/') }}">@lang('general.menu_building_avoid_refusal')</a></li>
                                            <!-- <li><a href="{{ url('/') }}">@lang('general.menu_building_conformity')</a></li> -->
                                            <!-- <li><a href="{{ url('/') }}">@lang('general.menu_building_decision')</a></li> -->

                                            <li><a href="{{ url('make-a-request') }}">@lang('general.menu_building_individual_house')</a></li>

                                        </ul>
                                    </li>

                                    <li class="nav-item nav-item-parent">
                                        <a href="#" class="nav-link ">
                                            @lang('general.menu_town_halls')
                                        </a>

                                        <ul class="sub-menu">
                                            <li><a href="#">@lang('general.menu_renovation')</a></li>
                                            <li><a href="#">@lang('general.menu_garage')</a></li>
                                            <li><a href="#">@lang('general.menu_isolation')</a></li>
                                            <li><a href="#">@lang('general.menu_gros_oeuvre')</a></li>
                                            <li><a href="#">@lang('general.menu_toiture')</a></li>
                                            <li><a href="#">@lang('general.menu_extension')</a></li>
                                            <li><a href="#">@lang('general.menu_piscine')</a></li>
                                        </ul>
                                    </li>


                                    <li class="nav-item nav-item-parent">
                                        <a href="{{ url('/') }}" class="nav-link ">
                                            @lang('general.menu_about_us')
                                        </a>

                                        <ul class="sub-menu">
                                            <li><a href="{{ url('/post/'.trans('general.post_alias_what_we_do').'/') }}">@lang('general.menu_what_we_do')</a></li>
                                            <li><a href="{{ url('/post/'.trans('general.post_alias_our_objectives').'/') }}">@lang('general.menu_our_objectives')</a></li>
                                            <li><a href="{{ url('/post/'.trans('general.post_alias_our_core_values').'/') }}">@lang('general.menu_our_core_values')</a></li>

                                            <li><a href="{{ url('/post/'.trans('general.post_alias_frequently_asked_questions').'/') }}">@lang('general.menu_faq')</a></li>
                                            <li><a href="{{ url('/contact/') }}">@lang('general.menu_contact')</a></li>

                                            <li><a href="{{ url('/post/'.trans('general.post_alias_terms_and_conditions').'/') }}">@lang('general.menu_terms_and_conditions')</a></li>
                                        </ul>
                                    </li>
                                    @if(!Auth::guest() && Auth::user()->role_id == 1)
                                    {{--<li class="nav-item nav-item-parent">--}}
                                    {{--<a  class="nav-link ">--}}
                                            {{--Data--}}
                                        {{--</a>--}}

                                        {{--<ul class="sub-menu">--}}
                                            {{--<li><a href="{{url('/build-table')}}">Build</a></li>--}}
                                            {{--<li><a href="{{url('/calculator-table')}}">Devis</a></li>--}}
                                        {{--</ul>--}}
                                    {{--</li>--}}
                                    @endif

                                </ul>
                            </div><!-- /.nav-primary -->


                            <ul class="nav nav-pills nav-languages">
                                <li class="nav-item"><a href="{{ url('language/fr') }}" class="nav-link @if(Session::get('language') === 'fr') active @endif">FR</a></li>
                                <li class="nav-item"><a href="{{ url('language/en') }}" class="nav-link @if(Session::get('language') === 'en') active @endif">EN</a></li>
                                <!-- <li class="nav-item"><a href="{{ url('language/es') }}" class="nav-link @if(Session::get('language') === 'es') active @endif">ES</a></li> -->
                            </ul><!-- /.currencies -->
                        </div><!-- /.header-bottom-inner -->
                    </div><!-- /.header-bottom -->
                </div><!-- /.container -->
            </div><!-- /.header-inner -->
        </div><!-- /.header -->
    </div><!-- /.header-wrapper-->

<style>
    .badge{
        background-color: #ed1e79e6;
        border-radius: 15px;
        position: absolute;
        left: 195px;
        top: 0px;
        padding: 0px 5px 3px 5px;
        font-size: 10px;
        height: 20px;
    }
    .nav-item.open>.nav-link{
        background-color: #47c657!important;
    }
    #menu{
        position: absolute!important;
        top: 18%!important;
        left: 13.5%!important;
        padding-left: 5px!important;
    }
    #menu li a{
        font-size: 12px;
    }
    #menu li a:hover{
        text-decoration: none;
        /*font-weight: bold;*/
        cursor:pointer;
    }
    #notification-item a{
        cursor: pointer;
    }
</style>