@extends("webapp.layouts.default")

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">

@endsection


@section('content')

    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">
                <div class="content-title">
                    <div class="content-title-inner">
                        <div class="container">
                            <h1>{{ $pageTitle }}</h1>
                        </div><!-- /.container -->
                    </div><!-- /.content-title-inner -->
                </div>

                <div class="content">
                    <div class="container">
                        <div class="posts posts-grid">
                            <div class="row">

                                @foreach($agencies as $agency)
                                <div class="listing-row-simple-image __web-inspector-hide-shortcut__">
                                    <div class="listing-row post">
                                        <div class="listing-row-inner">

                                            <?php

                                            $cover_image_name = $agency->avatar;

                                            $imgSrc = (is_file('storage/' . $cover_image_name))?
                                                asset('storage/' . $cover_image_name):
                                                asset('assets/img/no-agency-logo-available.png');

                                            ?>

                                            <div class="listing-row-image" style="background-image: url('{{ $imgSrc }}');">
                                            </div>

                                            <div class="listing-row-content">
                                                <h3>
                                                    <a href="#">{{ $agency->name }}</a> <span>/ City</span>
                                                </h3>

                                                <!--
                                                <div class="listing-row-subtitle">
                                                    20 properties in list
                                                </div><!-- /.listing-row-subtitle -->


                                                <ul class="listing-row-actions">
                                                    <li><a href="{{ url('/view-all-properties/' . $agency->id) }}"><i class="fa fa-building"></i> @lang('profile.view_agency_properties')</a></li>
                                                    <li><a href="{{ url('/agents/' . $agency->id) }}"><i class="fa fa-user"></i> @lang('profile.view_agency_agents')</a></li>
                                                </ul><!-- /.listing-row-actions -->
                                            </div><!-- /.listing-row-content -->
                                        </div><!-- /.listing-row-inner -->
                                    </div><!-- /.listing-row -->
                                </div>
                                @endforeach

                            </div><!-- /.row -->
                        </div><!-- /.posts-->

                        <!--
                        <div class="pagination-wrapper">

                            { ! ! $agents->links('vendor.pagination.bootstrap-4') ! ! }

                        </div><!-- /.pagination-wrapper -->

                    </div><!-- /.container -->

                </div><!-- /.content -->
            </div>
        </div>
    </div>
    @include("webapp.includes.call-to-action-footer")

@endsection







