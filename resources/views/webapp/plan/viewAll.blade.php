@extends("webapp.layouts.default")

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">

@endsection

@section('content')

	<div class="main-wrapper">
	    <div class="main">
	        <div class="main-inner">
	        		        
					<div class="content-title">
						<div class="content-title-inner">
							<div class="container">		
								<h1>{{$title}}</h1>
							</div><!-- /.container -->
						</div><!-- /.content-title-inner -->
					</div><!-- /.content-title -->
				
				<br>
	            <div class="content">
	                <div class="container">
						
						<div class="row">		
							<div class="col-md-8 col-lg-9">
								<div class="row">
									@if($sucess != "null")

								        @include('partials.alert',['type'=>'error','message'=>$sucess])

								    @endif
								    @if($error != "null")

						              @include('partials.error',['type'=>'error','message'=>$error])

						            @endif

									@foreach($plans as $plan)


                                        <?php

                                            $files = $plan->files;

                                            foreach ($files as $file) {
                                                if($file->path == '/planImages/') {
                                                    $image = $file;
                                                    break;
                                                }
                                            }
                                        ?>

										<div class="col-md-6 col-lg-4">
											<div class="listing-box">
												<div class="listing-box-image" style="

													@if(isset($image))
														background-image: url(storage/planImages/{{ $image->nom }})
													@else
														background-image: url('assets/img/tmp/tmp-5.jpg')
													@endif
													">
													<div class="listing-box-image-label" style="@if($plan->statut == '0')background: #d9534f !important;@endif">
														@if($plan->statut == '1')
															@lang('plan.active_plan')
														@else
															@lang('plan.disabled_plan')
														@endif
													</div><!-- /.listing-box-image-label -->
													
													<span class="listing-box-image-links">
														@if(Auth::check() && $plan->user_id == Auth::id())
															<a href="{{ url('/edit-plan/'.$plan->id) }}" style="@if($plan->statut == '0')background: #d9534f !important;@endif"><i class="fa fa-pencil-square-o"></i> <span>@lang('plan.edit_plan')</span></a>
														@endif

														<a href="{{ url('/view-details-plan/'.$plan->id) }}" style="@if($plan->statut == '0')background: #d9534f !important;@endif"><i class="fa fa-search"></i> <span>@lang('plan.view_detail')</span></a>

														<!-- @if($plan->statut == '1')
															<a href="{{ url('/compare-plan/'.$plan->id) }}" style="@if($plan->statut == '0')background: #d9534f !important;@endif" class="compareProperty" value="{{$plan->id}}" name="{{$plan->title}}"><i class="fa fa-balance-scale"></i> <span>@lang('plan.compare_list')</span></a>
														@endif -->

														@if(Auth::check() && $plan->user_id == Auth::id())
															@if($plan->statut == '0')
																<a href="{{ url('/delete-plan/'.$plan->id) }}" style="background: #d9534f !important;" class="enableProperty" value="{{$plan->id}}" name="{{$plan->title}}"><i class="fa fa-ban"></i> <span>@lang('plan.enable_plan')</span></a>
																<a href="{{ url('/delete-plan/'.$plan->id) }}" style="background: #d9534f !important;" class="deleteProperty" value="{{$plan->id}}" name="{{$plan->title}}"><i class="fa fa-ban"></i> <span>@lang('plan.delete_plan')</span></a>
															@else
																<a href style="@if($plan->statut == '0')background: #d9534f !important;@endif" class="disableProperty" value="{{$plan->id}}" name="{{$plan->title}}"><i class="fa fa-ban"></i> <span>@lang('plan.disable_plan')</span></a>
															@endif
														@endif 
													</span>		
												</div><!-- /.listing-box-image -->

												<div class="listing-box-title" style="@if($plan->statut == '0') background-color: #d9534f !important; @endif">
													<h2><a href="{{ url('/view-details-plan/'.$plan->id) }}">{{$plan->title}}</a></h2>
													<h3>{{$plan->price}} </h3>
												</div><!-- /.listing-box-title -->

												<div class="listing-box-content">
													<dl>
														<dt>@lang('plan.title')</dt><dd>{{$plan->title}}</dd>
													</dl>
												</div><!-- /.listing-box-cotntent -->
											</div><!-- /.listing-box -->			
										</div><!-- /.col-* -->
									@endforeach
									
									
								</div><!-- /.row -->		

								{{ $plans->links() }}	
							</div><!-- /.col-sm-* -->
							
							<div class="col-md-4 col-lg-3"> 			
								@if(Auth::check() && (isset($userProfile->address)))
								<div class="widget">
									<h2 class="widgettitle">Contact Information</h2>

									<table class="contact">
										<tbody>
											<tr>
												<th>@lang('properties.address'):</th>
												<td>{{$userProfile->address}}<br>{{$userProfile->city}}, {{$userProfile->region}}<br>{{$userProfile->country}}</td>
											</tr>

											<tr>
												<th>@lang('properties.Phone'):</th>
												<td>{{$userProfile->phone_1}}</td>
											</tr>

											<tr>
												<th>@lang('properties.email'):</th>
												<td><a href="mailto:info@example.com">{{Auth::User()->email}}</a></td>
											</tr>

											
										</tbody>
									</table>	
								</div><!-- /.widget -->	
								@endif	

                                <!-- @include('webapp.includes.compare-list-widget') -->

							</div><!-- /.widget -->
						</div><!-- /.row -->
					</div><!-- /.container -->
	            </div><!-- /.content -->
	        </div><!-- /.main-inner -->
	    </div><!-- /.main -->
    </div><!-- /.main-wrapper -->

@include("webapp.includes.call-to-action-footer")

@endsection

@section('script')

<script type="text/javascript">

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $(".disableProperty").click(function(){

        var id = $(this).attr("value");
        var name = $(this).attr("name");

        swal({
                title: "Are you sure?",
                text: "The plan : " + name + " will be disable. It will not be visible for the other users.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#39B54A",
                confirmButtonText: "Yes, disable it!",
                closeOnConfirm: false
            },
            function(){
                $.ajax({
                    type: 'POST',
                    cache:false,
                    headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
                    url: 'api/plan/put',
                    data: "id=" + id,
                    dataType: 'json',

                    success : function(data, statut){
                        //console.log(data.statut);
                        if(data.statut == "done")
                            swal({
                                    title: "Disabled!",
                                    text: "Your plan : " + name + " has been disabled.",
                                    type: "success",
                                    showCancelButton: false,
                                    confirmButtonColor: "#39B54A",
                                    confirmButtonText: "OK",
                                    closeOnConfirm: false
                                },
                                function(){
                                    location.reload();
                                });
                        //swal("Disabled!", "Your property : " + $(this).attr("name") + " has been disabled.", "success");
                    },

                    error : function(resultat, statut, erreur){
                        swal("Warning!", "Sorry. An error occured while disabling : " + name + ". Please retry later", "error");
                    }
                });


            });

        return false;
    });

    $(".deleteProperty").click(function(){

        var id = $(this).attr("value");
        var name = $(this).attr("name");

        swal({
                title: "Are you sure?",
                text: "The plan : " + name + " will be delete.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#39B54A",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function(){
                $.ajax({
                    type: 'POST',
                    cache:false,
                    headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
                    url: 'api/plan/delete',
                    data: "id=" + id,
                    dataType: 'json',

                    success : function(data, statut){
                        //console.log(data.statut);
                        if(data.statut == "done")
                            swal({
                                    title: "Deleted!",
                                    text: "Your plan : " + name + " has been deleted.",
                                    type: "success",
                                    showCancelButton: false,
                                    confirmButtonColor: "#39B54A",
                                    confirmButtonText: "OK",
                                    closeOnConfirm: false
                                },
                                function(){
                                    location.reload();
                                });
                        //swal("Disabled!", "Your property : " + $(this).attr("name") + " has been disabled.", "success");
                    },

                    error : function(resultat, statut, erreur){
                        swal("Warning!", "Sorry. An error occured while deleting : " + name + ". Please retry later", "error");
                    }
                });


            });

        return false;
    });

    $(".enableProperty").click(function(){

        var id = $(this).attr("value");
        var name = $(this).attr("name");

        swal({
                title: "Are you sure?",
                text: "The plan : " + name + " will be visible for all users.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#39B54A",
                confirmButtonText: "Yes, enable it!",
                closeOnConfirm: false
            },
            function(){
                $.ajax({
                    type: 'POST',
                    cache:false,
                    headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
                    url: 'api/plan/enable',
                    data: "id=" + id,
                    dataType: 'json',

                    success : function(data, statut){
                        //console.log(data.statut);
                        if(data.statut == "done")
                            swal({
                                    title: "Enabled!",
                                    text: "Your plan : " + name + " has been enabled.",
                                    type: "success",
                                    showCancelButton: false,
                                    confirmButtonColor: "#39B54A",
                                    confirmButtonText: "OK",
                                    closeOnConfirm: false
                                },
                                function(){
                                    location.reload();
                                });
                        //swal("Disabled!", "Your property : " + $(this).attr("name") + " has been disabled.", "success");
                    },

                    error : function(resultat, statut, erreur){
                        swal("Warning!", "Sorry. An error occured while enabling : " + name + ". Please retry later", "error");
                    }
                });


            });

        return false;
    });

    // $(".compareProperty").click(function(event){

    //     event.preventDefault();

    //     var id = $(this).attr("value");
    //     var name = $(this).attr("name");
    //     console.log(id);

    //     swal({
    //             title: "Add to compare",
    //             text: "Add '" + name + "' to comparison grid?",
    //             type: "info",
    //             showCancelButton: true,
    //             confirmButtonText: "Yes, add!",
    //             closeOnConfirm: false
    //         },

    //         function(){

    //             $.ajax({
    //                 url: 'api/plan/addCompareProperty',
    //                 type: 'POST',
    //                 data: {_token: CSRF_TOKEN, id: id},
    //                 dataType: 'JSON',
    //                 success: function (data) {

    //                     console.log(data);

    //                     location.reload();

    //                     //swal("Added!", "The property.", "success");
    //                 },

    //                 error : function(resultat, statut, erreur){
    //                     swal("Warning!", "Sorry. An error occured while enabling : " + name + ". Please retry later", "error");
    //                 }
    //             });
    //         });

    // });
</script>

@endsection

