@extends("webapp.layouts.default")

<?php

	$imgSrc = '';

	foreach( $list as $img ){
	    if(is_file('storage/planImages/' . $img)) {
            $imgSrc = asset('storage/planImages/' . $img);
            break;
		}
	}

?>

@section('meta')
	@if(isset($plan->id))

		<!-- Twitter -->
		<meta name="twitter:card" content="Summary">
		<meta name="twitter:site" content="{{ url('/view-details-plan/'.$plan->id) }}">
		<meta name="twitter:title" content="{{ $plan->title }}">
		<meta name="twitter:description" content="{{ substr( strip_tags($plan->description) ,0 , 300) }}">
		<meta name="twitter:creator" content="{{ $plan->user->name }}">
		<meta name="twitter:image:src" content="{{ $imgSrc }}">
		<meta name="twitter:player" content="">

		<!-- Open Graph General (Facebook & Pinterest) -->
		<meta property="og:url" content="{{ url('/view-details-plan/'.$plan->id) }}">
		<meta property="og:title" content="{{ $plan->title }}">
		<meta property="og:description" content="{{ substr( strip_tags($plan->description) ,0 , 300) }}">
		<meta property="og:site_name" content="ETB Batiment">
		<meta property="og:image" content="{{ $imgSrc }}">
		<meta property="fb:admins" content="">
		<meta property="fb:app_id" content="">
		<meta property="og:type" content="article">
		<meta property="og:locale" content="fr_FR">
		<meta property="og:audio" content="">
		<meta property="og:video" content="">
	@endif
@endsection

@section('content')

    <div class="main-wrapper">
	    <div class="main">
	        <div class="main-inner">
	        		        
				<div class="content-title">
					<div class="content-title-inner">
						<div class="container">		
							<h1>{{$plan->title}}</h1>
						</div><!-- /.container -->
					</div><!-- /.content-title-inner -->
				</div><!-- /.content-title -->
				
	            <div class="content">
	                <div class="container">
					    <div class="row">
					        <div class="col-sm-12">
					        <br><br><br>
					            <div class="listing-detail-attributes">
								    <div class="listing-detail-attribute">
								        <div class="key">@lang('plan.plan_added_by')</div>
								        <div class="value">
								            <div class="value-image">
								                <a href="#" style="background-image: url('assets/img/tmp/agent-1.jpg');">
								                	
								                </a>
								            </div><!-- /.value-image -->

								            {{$plan->user->name}}
								        </div>
								    </div><!-- /.listing-detail-attribute -->   

								</div><!-- /.listing-detail-attributes -->            
        					</div><!-- /.col-sm-12 -->
        					
        					<div class="listing-detail col-md-8 col-lg-9">
        						@if(count($errors->all()))

						            @include('partials.error',['type'=>'error','message'=>'Sorry, some errors occured. Please scroll to have details.'])

						        @endif 

						        @if(session('sucess'))

						            @include('partials.sucess',['type'=>'error','message'=> session('sucess') ])

						        @endif

								<div id="lightgallery">
									@foreach( $list as $img )
										@if(strpos($img, 'plansource') === false)
										<a href="{{ url('../storage/planImages/' . $img) }}">
											<img src="{{ url('../storage/planImages/' . $img) }}" class="img-fluid" />
											<span class="zoom"><i class="fa fa-search-plus fa-3x" aria-hidden="true"></i></span>
										</a>
										@endif
									@endforeach
								</div>

								<div class="row">            
								    
								    <div class="col-lg-7">
								        <h2>@lang('plan.plan_description')</h2>
								        <p>
								            {{$plan->description}}
								        </p>

								    </div><!-- /.col-* -->
								</div><!-- /.row -->


								<h2>@lang('general.general_share_on'):</h2>

								<div class="share-buttons">
									<a href="http://www.facebook.com/sharer/sharer.php?u={{ url('/view-details-plan/'.$plan->id) }}" target="_blank" class="social-popup">
										<i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i>
									</a>
									<a href="http://twitter.com/share?url={{ url('/view-details-plan/'.$plan->id) }}" target="_blank" class="social-popup">
										<i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i>
									</a>
									<a href="https://plus.google.com/share?url={{ url('/view-details-plan/'.$plan->id) }}" target="_blank" class="social-popup">
										<i class="fa fa-google-plus-square fa-2x" aria-hidden="true"></i>
									</a>
								</div>


								@if(Auth::Check() && ($plan->user->id != Auth::id()))
									<h2>Write To The Owner</h2>
									{{Form::open(['url'=>'/sendPlanMessage','method'=>'POST','role'=>'form', 'files' => true]) }}
				            			{{ Form::token() }}
										<div class="comment-create">
											<form method="post" action="?">
												<input type="hidden" name="plan" value="{{$plan->id}}">
												<div class="row">
			
													<div class="col-sm-12">
														<div class="form-group">
															<label>@lang('plan.subject')</label>
															<input type="text" class="form-control" name="subject" style="<?php if($errors->first('subject')){?> border: 1px solid red !important; <?php } ?>"  value="{{old('subject')}}" required>

															<?php if($errors->first('subject')){?>
												 				<span style="color: red !important; font-size: 10px;">$errors->first('subject')</span>
															<?php } ?>
														</div><!-- /.form-group -->				
													</div><!-- /.col-* -->
												</div><!-- /.row -->

												<div class="form-group">
													<label>@lang('plan.message')</label>
													<textarea class="form-control" rows="5" name="message" style="<?php if($errors->first('message')){?> border: 1px solid red !important; <?php } ?>"  value="{{old('message')}}" required></textarea>

													<?php if($errors->first('message')){?>
														 <span style="color: red !important; font-size: 10px;">$errors->first('message')</span>
													<?php } ?>
												</div><!-- /.form-group -->

												<div class="form-group-btn">
													<button class="btn btn-primary pull-right">@lang('plan.send_message')</button>
												</div><!-- /.form-group-btn -->
											</form>
										</div><!-- /.comment-create -->
									{{ Form::close() }}
								@endif
        					</div><!-- /.col-* -->

					        <div class="col-md-4 col-lg-3">
								@if(!empty($userList))
									<div class="widget">
										<ul class="menu nav nav-stacked">
											<li class="nav-item">
												<a href="{{url('/list-plan-messages/' . $plan->id)}}" class="nav-link"><i class="fa fa-chevron-right"></i> @lang('properties.properties_messages')</a>
											</li>
										</ul><!-- /.nav -->
									</div><!-- /.widget -->
								@endif
								<div class="widget">
									<table class="contact">
										<h2 class="widgettitle">@lang('properties.advertizer')</h2>
										<tbody>
										<tr>
											<th>@lang('properties.advertizer_name'):</th>
											<td>{{ $owner->name }}</td>
										</tr>

										<tr>
											<th>@lang('properties.address'):</th>
											<td>{{ $owner->address }}<br>{{ $owner->city }}, {{ $owner->region }}<br>{{ $owner->country }}</td>
										</tr>

										<tr>
											<th>@lang('properties.Phone'):</th>
											<td>{{ $owner->phone_1 }}</td>
										</tr>

										<tr>
											<th>@lang('properties.email'):</th>
											<td><a href="mailto:{{ $owner->email }}">{{ $owner->email }}</a></td>
										</tr>

										<tr>
											<th>@lang('properties.member_since'):</th>
											<td>{{ substr($owner->updated_at, 0, 10) }}</td>
										</tr>

										</tbody>
									</table>
								</div><!-- /.widget -->
					            <div class="widget">
								    <ul class="nav nav-stacked nav-style-primary">
								    	@if(Auth::Check() && ($plan->user->id == Auth::id()))
									        <li class="nav-item">
									            <a href="{{ url('/edit-plan/'.$plan->id) }}" class="nav-link">
									                <i class="fa fa-edit"></i> @lang('plan.edit_plan')
									            </a>
									        </li><!-- /.nav-item -->
								        @endif

								    </ul><!-- /.nav-style-primary -->
								</div><!-- /.widget -->

								@if(Auth::Check() && ($plan->user->id != Auth::id()) && false)
						            <div class="widget widget-background-white">
									    <h4>@lang('plan.sendessage')</h4>

									   {{Form::open(['url'=>'/sendPlanMessage','method'=>'POST','role'=>'form', 'files' => true]) }}
				            				{{ Form::token() }}
				            				<input type="hidden" name="plan" value="{{ $plan->id }}">
									        
									        <div class="form-group">
									            <label>@lang('plan.subject')</label>
									            <input type="text" class="form-control" name="subject"  style="<?php if($errors->first('subject')){?> border: 1px solid red !important; <?php } ?>"  value="{{old('subject')}}" required>

									            <?php if($errors->first('subject')){?>
													<span style="color: red !important; font-size: 10px;">$errors->first('subject')</span>
												<?php } ?>
									        </div><!-- /.form-group -->                

									        <div class="form-group">
									            <label>@lang('plan.message')</label>
									            <textarea class="form-control" rows="4" name="message" style="<?php if($errors->first('message')){?> border: 1px solid red !important; <?php } ?>" required></textarea>

									            <?php if($errors->first('message')){?>
													<span style="color: red !important; font-size: 10px;">$errors->first('message')</span>
												<?php } ?>
									        </div><!-- /.form-group -->                                

									        <div class="form-group-btn">
									            <button type="submit" class="btn btn-primary btn-block">@lang('properties.send_message')</button>
									        </div><!-- /.form-group-btn -->
									    {{ Form::close() }}
									</div><!-- /.widget -->
								@endif

                                @include('webapp.includes.compare-list-widget')
					                      
					        </div><!-- /.col-* -->
    					</div><!-- /.row -->
					</div><!-- /.container -->
	            </div><!-- /.content -->
	        </div><!-- /.main-inner -->
	    </div><!-- /.main -->
    </div><!-- /.main-wrapper -->

@endsection

@section('script')
	<script type="text/javascript">
        $(document).ready(function() {
            $("#lightgallery").lightGallery();
        });
	</script>
@endsection

