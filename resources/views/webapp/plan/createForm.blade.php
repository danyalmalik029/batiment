@extends("webapp.layouts.default")

@section('content')
	
	<div class="main-wrapper">
	    <div class="main">
	        <div class="main-inner">   		        
					<div class="content-title">
						<div class="content-title-inner">
							<div class="container">		
								<h1>@lang('plan.submit_plan')</h1>
							</div><!-- /.container -->
						</div><!-- /.content-title-inner -->
					</div><!-- /.content-title -->
				<div class="container maincontent">
					<div class="row">
                        @if($editing)
                        	{{Form::open(['url'=>'/update-plan','method'=>'POST','role'=>'form', 'files' => true, 'id' => 'planForm']) }}
                        @else
                        	{{Form::open(['url'=>'/save-plan','method'=>'POST','role'=>'form', 'files' => true, 'id' => 'planForm']) }}
                        @endif
                        {{ Form::token() }}
                        <input type="hidden" name="plan" value="{{$plan->id}}">

						<div class="col-md-8 col-lg-9">
				            <div class="content">
				            	@if(count($errors->all()))

					              @include('partials.error',['type'=>'error','message'=>'Sorry, some errors occured. Please scroll to have details.'])

					            @endif 

					            @if($sucess != "null")

					              @include('partials.sucess',['type'=>'error','message'=>$sucess])

					            @endif
				            	
									<div class="page-header page-header-small">
										<h2>@lang('plan.basic_information')</h2>
									</div><!-- /.page-header -->

									<div class="form-group">
											<label>@lang('plan.plan_title') *</label>

											<input type="text" class="form-control" name="title" required style="<?php if($errors->first('title')){?> border: 1px solid red !important; <?php } ?>"  value="{{old('title', $plan->title)}}">

											<?php if($errors->first('title')){?>
												 <span style="color: red !important; font-size: 10px;">$errors->first('title')</span>
											<?php } ?>
									</div><!-- /.form-group -->

									<div class="form-group">
										<label>@lang('plan.description')</label>

										<textarea class="form-control" rows="6" name="description" required style="<?php if($errors->first('description')){?> border: 1px solid red !important; <?php } ?>" ">{{old('description' , $plan->description)}}</textarea>

										<?php if($errors->first('description')){?>
												 <span style="color: red !important; font-size: 10px;">$errors->first('title')</span>
										<?php } ?>
									</div><!-- /.form-group -->

									<br>
									<div class="left">
                                        @if(!$editing)
										<button class="btn btn-warning" type="reset" id="resetBtn">@lang('plan.reset_propertyform')</button>
                                        <button class="btn btn-primary" type="submit">@lang('plan.submit_plan')</button>
                                        @else
										<button><a class="btn btn-primary" style="margin: -10px !important;">@lang('plan.save_plan')</a></button>
                                        @endif
									</div><!-- /.center -->

									@if($editing)
									<div class="page-header page-header-small">
										<h2>Sources</h2>
									</div><!-- /.page-header -->
									<div class="col-xs-12">
										<ul>
											@foreach($planImages as $file)
												@if(strpos($file['nom'], 'plansource') !== false)
													<li style="width: 100%; padding: 10px; list-style:none; margin:0; border-bottom: 1px dotted #0b4a3d;">
														<a href="{{ url('planfile/' . $file['nom']) }}" target="_blank">{{ $file['nom'] }}</a>
														<a href="#" class="deleteimage" data-image-id="{{ $file['id'] }}" style="float: right; margin-right: 20px;"><i class="fa fa-times-circle-o fa-2x" aria-hidden="true"></i></a>
													</li>
												@endif
											@endforeach
										</ul>
									</div>
									<a href="{{ url('/upload-picture-to-plan/' . $plan->id) }}" class="btn btn-primary">@lang('plan.add_source')</a>
									@endif
				            </div><!-- /.content -->
			            </div><!-- /.col-* -->
			            <br>
			            <div class="col-md-4 col-lg-3">
			            	<div class="sidebar">
			            		<div class="widget">
									<h2 class="widgettitle">@lang('plan.upload_file')</h2>

                                    @if(count($planImages) < 10)

										<ul style="background: none !important;" class="menu nav nav-stacked" id="files">

											<li class="nav-item" style="margin: 5px;">
												<div class="input-group">
													<span id="addUploader">
														<span class="input-group-addon" style="background: white; padding: 10px;  border: 2px solid #45b75d !important; cursor: pointer;">
															<i class="fa fa-plus-circle" aria-hidden="true" style="color: #44a950 !important;">
																
															</i>@lang('plan.add_image')
														</span>
													</span>
													<input type="hidden" name="fileNumber" id="fileNumber" value="1" >	
												</div>	
											</li>

											<li class="nav-item">
												<input type="file" name="f1" accept=".cad, .CAD, image/*" class="form-control" @if(!$editing) required @endif>
											</li>
											
										</ul><!-- /.nav -->

                                    @endif

                                    @if(isset($planImages))
	                                    <div class="row" id="adsThumbnails">
											<div class="col-xs-12">
												<h4><br>Images</h4>
											</div>
	                                        @foreach($planImages as $image)
												@if(strpos($image['nom'], 'plansource') === false)
	                                            <div class="col-xs-12 col-md-6 thumbnail">
	                                                <div class="image">
	                                                    <a href="#" class="deleteimage" data-image-id="{{ $image['id'] }}" ><i class="fa fa-times-circle-o fa-2x" aria-hidden="true"></i></a>
	                                                    <img src="../storage/{{ $image['path'] }}{{ $image['nom'] }}" />
	                                                </div>
	                                            </div>
												@endif
	                                        @endforeach
	                                    </div>
                                    @endif
								</div><!-- /.widget -->
			            	</div><!-- /.sidebar -->
			            </div><!-- /.col-* -->
                        {{ Form::close() }}
		            </div><!-- /.row -->
                    <div class="row">
                        <div id="planFormSaver" style="display: none;">
                            <h1>Saving plan...</h1>
                            <img src="{{ url('assets/img/loading.gif') }}" />
                        </div>
                    </div>
	            </div><!-- /.container -->
	        </div><!-- /.main-inner -->
	    </div><!-- /.main -->
    </div><!-- /.main-wrapper -->
    {{ Form::open(['method'=>'POST','role'=>'form', 'id' => 'deleteImageForm']) }}
    {{ Form::token() }}

	    <input type="hidden" name="action" value="deleteimage">
	    <input type="hidden" name="property" value="{{$plan->id}}">
	    <input type="hidden" name="image_id" id="current_image_id" value="">

    {{ Form::close() }}

@include("webapp.includes.call-to-action-footer")

@endsection

@section('script')
<?php $maxuploads = (count($planImages))?(10 - count($planImages)):10; ?>
	
	<script type="text/javascript">
		$('#addUploader').click(function(){
			var fileNumber = $( "#fileNumber" ).val();

			if(fileNumber < {{ $maxuploads }}){
				fileNumber++;
				$( "#files" ).append( "<li class=\"nav-item\"> <input type=\"file\" name=f" + fileNumber +" accept=\".cad, .CAD, image/*\" class=\"form-control\"></li>" );
				$( "#fileNumber" ).val(fileNumber);
			}else{
				$('#addUploader').css('display', 'none');
			}

		});

		$("#planForm").submit(function () {

            $("#planForm").scrollTop(300);

            $(this).css("display","none");

            $("#planFormSaver").css("display","block");
        });
		
		$(".deleteimage").click(function () {

		    var currentImageLink = $(this);

            swal({
                    title: "@lang('properties.are_you_sure')",
                    text: "@lang('properties.will_be_deleted')",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#39B54A",
                    confirmButtonText: "@lang('properties.yes_delete_it')",
                    closeOnConfirm: true
                },
                function(){
                    var planId = currentImageLink.data('image-id');

                    // Submit the delete image form
                    $("#current_image_id").val(planId);

                    $('#deleteImageForm').submit();
                });
        });

        $('#resetBtn').click(function(){
            $(".ez-checkbox").removeClass("ez-checked");
        });
	</script>
@endsection