@extends("webapp.layouts.default")

@section('content')
	
	<div class="main-wrapper">
	    <div class="main">
	        <div class="main-inner">   		        
					<div class="content-title">
						<div class="content-title-inner">
							<div class="container">		
								<h1>@lang('plan.upload_images')</h1>
							</div><!-- /.container -->
						</div><!-- /.content-title-inner -->
					</div><!-- /.content-title -->
				<div class="container maincontent">
					<div class="row">
                        
                        {{Form::open(['url'=>'/upload-zip-file','method'=>'POST','role'=>'form', 'files' => true, 'id' => 'planForm']) }}

                        {{ Form::token() }}
	                        

							<div class="col-md-6 col-lg-7">
					            <div class="content">
					            	@if(count($errors->all()))

						              @include('partials.error',['type'=>'error','message'=>'Sorry, some errors occured. Please scroll to have details.'])

						            @endif 

						            @if($sucess != "null")

						              @include('partials.sucess',['type'=>'error','message'=>$sucess])

						            @endif
					            	
										<div class="page-header page-header-small">
											<h3>@lang('plan.upload_plan_text')</h3>
										</div><!-- /.page-header -->

										<div class="form-group">

											<select name="plan" required style = "padding: 10px 80px 10px 80px;border: 2px solid #45b75d !important;">

												@foreach($plans as $plan)
											  		<option value="{{$plan->id}}">{{$plan->title}}</option>
											  	@endforeach

											</select>

										</div><!-- /.form-group -->

										<div class="form-group">
											

		                                    
										</div><!-- /.form-group -->

										<br>
										<div class="left">
	                                        
											<button><a class="btn btn-primary" style="margin: -10px !important;">@lang('plan.save_images')</a></button>
	                                        
										</div><!-- /.center -->

									
					            </div><!-- /.content -->
				            </div><!-- /.col-* -->
				            <br>
				            <div class="col-md-6 col-lg-5">
				            	<div class="sidebar">

				            		<div class="page-header page-header-small" style="padding-top: 10px !important;">
										<h3>@lang('plan.upload_zip')</h3>
									</div><!-- /.page-header -->

				            		<div class="widget">
											<ul style="background: none !important;" class="menu nav nav-stacked" id="files">

												

												<li class="nav-item">
													<input type="file" name="f1" accept=".zip, .rar" class="form-control" required >
													<input type="hidden" name="fileNumber" id="fileNumber" value="1" >
												</li>
													
											</ul><!-- /.nav -->
									</div><!-- /.widget -->
				            	</div><!-- /.sidebar -->
				            </div><!-- /.col-* -->
                        {{ Form::close() }}
		            </div><!-- /.row -->
                    <div class="row">
                        <div id="planFormSaver" style="display: none;">
                            <h1>Uploading images...</h1>
                            <img src="{{ url('assets/img/loading.gif') }}" />
                        </div>
                    </div>
	            </div><!-- /.container -->
	        </div><!-- /.main-inner -->
	    </div><!-- /.main -->
    </div><!-- /.main-wrapper -->
    <!-- {{ Form::open(['method'=>'POST','role'=>'form', 'id' => 'deleteImageForm']) }}
    {{ Form::token() }}

	    <input type="hidden" name="action" value="deleteimage">
	    <input type="hidden" name="property" value="{{$plan->id}}">
	    <input type="hidden" name="image_id" id="current_image_id" value="">

    {{ Form::close() }} -->

@include("webapp.includes.call-to-action-footer")

@endsection

@section('script')
	
	<script type="text/javascript">
		// $('#addUploader').click(function(){
		// 	var fileNumber = $( "#fileNumber" ).val();

		// 	fileNumber++;
		// 	$( "#files" ).append( "<li class=\"nav-item\"> <input type=\"file\" name=f" + fileNumber +" accept=\".cad, .CAD, image/*\" class=\"form-control\"></li>" );
		// 	$( "#fileNumber" ).val(fileNumber);
			

		// });

		$("#planForm").submit(function () {

            $("#planForm").scrollTop(300);

            $(this).css("display","none");

            $("#planFormSaver").css("display","block");
        });
		
		
	</script>
@endsection