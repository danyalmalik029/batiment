@extends("webapp.layouts.default")

@section('content')
	
	<div class="main-wrapper">
	    <div class="main">
	        <div class="main-inner">   		        
					<div class="content-title">
						<div class="content-title-inner">
							<div class="container">		
								<h1>@lang('plan.upload_images')</h1>
							</div><!-- /.container -->
						</div><!-- /.content-title-inner -->
					</div><!-- /.content-title -->
				<div class="container maincontent">
					<div class="row">
                        
                        {{Form::open(['url'=>'/save-upload-picture-to-plan','method'=>'POST','role'=>'form', 'files' => true, 'id' => 'planForm']) }}

                        {{ Form::token() }}
	                        

							<div class="col-xs-12">
								<div class="page-header page-header-small" style="padding-top: 10px !important;">
									<h3>@lang('plan.upload_files'): {{ $plan->title }}</h3>
								</div><!-- /.page-header -->

								<div class="content">
					            	@if(count($errors->all()))

						              @include('partials.error',['type'=>'error','message'=>'Sorry, some errors occured. Please scroll to have details.'])

						            @endif 

						            @if($sucess != "null")

						              @include('partials.sucess',['type'=>'error','message'=>$sucess])

						            @endif

										<input type="hidden" name="plan" value="{{ $plan->id }}">
									
					            </div><!-- /.content -->
				            </div><!-- /.col-* -->
				            <br>
				            <div class="col-xs-12">
				            	<div class="sidebar">
				            		<div class="widget">
											<ul style="background: none !important;" class="menu nav nav-stacked" id="files">

												<li class="nav-item" style="margin: 0 5px 5px;">
													<div class="input-group">
														<span id="addUploader">
															<span class="input-group-addon" style="background: white; padding: 10px;  border: 2px solid #45b75d !important; cursor: pointer;">
																<i class="fa fa-plus-circle" aria-hidden="true" style="color: #44a950 !important;">

																</i>@lang('plan.add_image')
															</span>
														</span>
														<input type="hidden" name="fileNumber" id="fileNumber" value="1" >
													</div>
												</li>

												<li class="nav-item">
													<input type="file" name="f1" accept=".cad, .CAD, .PDF, image/*" class="form-control" required >
												</li>

											</ul><!-- /.nav -->
									</div><!-- /.widget -->
				            	</div><!-- /.sidebar -->
				            </div><!-- /.col-* -->
						<div class="col-xs-12">
							<div class="left">

								<button><a class="btn btn-primary" style="margin: -10px !important;">@lang('plan.save_files')</a></button>

							</div><!-- /.center -->
						</div>
                        {{ Form::close() }}
		            </div><!-- /.row -->
                    <div class="row">
                        <div id="planFormSaver" style="display: none;">
                            <h1>Uploading/Chargement...</h1>
                            <img src="{{ url('assets/img/loading.gif') }}" />
                        </div>
                    </div>
	            </div><!-- /.container -->
	        </div><!-- /.main-inner -->
	    </div><!-- /.main -->
    </div><!-- /.main-wrapper -->

@include("webapp.includes.call-to-action-footer")

@endsection

@section('script')
	
	<script type="text/javascript">
		$('#addUploader').click(function(){
			var fileNumber = $( "#fileNumber" ).val();

			fileNumber++;
			$( "#files" ).append( "<li class=\"nav-item\"> <input type=\"file\" name=f" + fileNumber +" accept=\".cad, .CAD, .PDF, image/*\" class=\"form-control\"></li>" );
			$( "#fileNumber" ).val(fileNumber);
			

		});

		$("#planForm").submit(function () {

            $("#planForm").scrollTop(300);

            $(this).css("display","none");

            $("#planFormSaver").css("display","block");
        });
		
		
	</script>
@endsection