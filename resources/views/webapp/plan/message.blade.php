@extends("webapp.layouts.default")

@section('content')

<div class="main-wrapper">
	    <div class="main">
	        <div class="main-inner">
	        		        
				<div class="content-title">
					<div class="content-title-inner">
						<div class="container">		
							<h1>@lang('properties.discussion_with') : <b>{{$sender->name}}</b></h1>
						</div><!-- /.container -->
					</div><!-- /.content-title-inner -->
				</div><!-- /.content-title -->
				<br>
	            <div class="content">
	                <div class="container">
	                	<div class="row">
	                		<div class="listing-detail col-md-12 col-lg-12">
	                			<ul class="comments">
	                				<li>
	                					<a href="#reply">
		                					<div class="comment" style="    text-align: center; background: #39b54a; padding: 10px; border-radius: 5px;">
												<span href="#" style="    color: white;">@lang('plan.reply')</span>
											</div>
										</a>
	                				</li>
									@foreach($messages as $sms)

										@if($sms->sender_id == Auth::id())
											<li>
												<div class="comment">			
													<div class="comment-author">
														<a href="#" style="background-image: url('assets/img/tmp/agent-1.jpg');"></a>
													</div><!-- /.comment-author -->

													<div class="comment-content">			
														<div class="comment-meta">
															<div class="comment-meta-author">
																@lang('plan.posted_by') <a href="#">{{$sender->name}}</a>
															</div><!-- /.comment-meta-author -->

															<div class="comment-meta-date">
																<span>{{$sms->created_at}}</span>
															</div>
														</div><!-- /.comment -->

														<div class="comment-body">
															<div class="comment-rating">
																{{$sms->subject}}
															</div><!-- /.comment-rating -->

															{{$sms->message_body}}
														</div><!-- /.comment-body -->
													</div><!-- /.comment-content -->
												</div><!-- /.comment -->		
											</li>
										@else
											<li>
												<ul>
													<li>
														<div class="comment">			
															<div class="comment-author">
																<a href="#" style="background-image: url('assets/img/tmp/agent-1.jpg');"></a>
															</div><!-- /.comment-author -->

															<div class="comment-content">			
																<div class="comment-meta">
																	<div class="comment-meta-author">
																		@lang('plan.posted_by') <a href="#">{{Auth::User()->name}}</a>
																	</div><!-- /.comment-meta-author -->

																	<div class="comment-meta-date">
																		<span>{{$sms->created_at}}</span>
																	</div>
																</div><!-- /.comment -->

																<div class="comment-body">
																	<div class="comment-rating">
																		{{$sms->subject}}
																	</div><!-- /.comment-rating -->

																	{{$sms->message_body}}
																</div><!-- /.comment-body -->
															</div><!-- /.comment-content -->
														</div><!-- /.comment -->		
													</li>
												</ul>
											</li>
										@endif

									@endforeach
								</ul>
	                		</div>
	                	</div>
	                	<div class="row">
	                		<div class="widget widget-background-white" id="reply">
								<h4>@lang('plan.send_message')</h4>

								{{Form::open(['url'=>'/sendPlanMessage','method'=>'POST','role'=>'form', 'files' => true]) }}
				            		{{ Form::token() }}
				            		<input type="hidden" name="ads" value="{{$ads->id}}">
									        
									<div class="form-group">
									    <label>@lang('plan.subject')</label>
									    <input type="text" class="form-control" name="subject"  style="<?php if($errors->first('subject')){?> border: 1px solid red !important; <?php } ?>"  value="{{old('subject')}}" required>

									    <?php if($errors->first('subject')){?>
											<span style="color: red !important; font-size: 10px;">$errors->first('subject')</span>
										<?php } ?>
									</div><!-- /.form-group -->                

									<div class="form-group">
									    <label>@lang('plan.message')</label>
									    <textarea class="form-control" rows="4" name="message" style="<?php if($errors->first('message')){?> border: 1px solid red !important; <?php } ?>" required></textarea>

									        <?php if($errors->first('message')){?>
											<span style="color: red !important; font-size: 10px;">$errors->first('message')</span>
											<?php } ?>
									</div><!-- /.form-group -->                                

									<div class="form-group-btn">
									    <button type="submit" class="btn btn-primary btn-block">@lang('plan.send_message')</button>
									</div><!-- /.form-group-btn -->
								{{ Form::close() }}
							</div><!-- /.widget -->
	                	</div>
	                </div>
	            </div>
	        </div>
	    </div>
</div>

@endsection