@extends("webapp.layouts.default")


@section('content')

<div class="content-title">
    <div class="content-title-inner">
        <div class="container">
            <h1>503: ETB Batiment,  @lang('general.general_be_back')</h1>
        </div><!-- /.container -->
    </div><!-- /.content-title-inner -->
</div>

<div class="main-wrapper">
    <div class="main">
        <div class="main-inner">
            <div class="content">
                <div class="container maincontent">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="warning">
                                    <h1>404</h1>
                                    <hr />
                                    <a href="{{ url('/') }}" class="btn btn-secondary"><i class="fa fa-long-arrow-left"></i> Return Home</a>
                                </div><!-- /.warning -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.main-inner -->
    </div><!-- /.main -->
</div><!-- /.main-wrapper -->

@include("webapp.includes.call-to-action-footer")

@endsection