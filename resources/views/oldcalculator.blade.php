@extends("webapp.layouts.default")


@section('content')
{{-- Start modal --}}
<div class="modal fade" id="modal-email" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">@lang('calculator.success')</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form method="POST">
       {{ csrf_field() }}
       <input type="hidden" id="id" name="id">
            <div class="form-group">
                <h4>@lang('calculator.email')</h4>
                    <label>@lang('calculator.input_email')</label>
                    &nbsp; &nbsp;
                    <input type="email"  class="form-control"  name="email" required>
            </div><!-- /.form-group -->

            <div class="form-group">
                    <label>@lang('calculator.benefit')*</label>
                    &nbsp; &nbsp;
                    <label><input type="radio" class="form-control" name="benefit" required value="yes">@lang('calculator.oui')</label>
                    &nbsp; &nbsp;
                    <label><input type="radio" class="form-control" name="benefit" required value="no">@lang('calculator.non')</label>
                </div><!-- /.form-group -->
            <div id="phone-modal" style="display:none;" class="form-group">
                <h4>@lang('calculator.telephone')</h4>
                    &nbsp; &nbsp;
                    <input type="number"  class="form-control" value="0" min="0" name="phone">
                </div><!-- /.form-group -->
{{--         <div>
       <input name="submit" type="button" class="btn btn-primary" value="@lang('calculator.receive')">
       <input name="submit" type="button" class="btn btn-primary" value="@lang('calculator.cancel')">
        </div>     --}}
       </form>
      </div>
      <div class="modal-footer">
        <button type="button" id="cancel" class="btn btn-secondary">@lang('calculator.cancel')</button>
        <button type="button" id="receive" class="btn btn-primary">@lang('calculator.receive')</button>
    </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-email-success" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">@lang('calculator.notification')</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p> @lang('calculator.notification_message')</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('calculator.close')</button>
      </div>
    </div>
  </div>
</div>

{{--  end of modal --}}

{{--  content start --}}
<div class="content-title">
        <div class="content-title-inner">
            <div class="container">
                <h1>@lang('contact.page_title')</h1>
            </div><!-- /.container -->
        </div><!-- /.content-title-inner -->
    </div>

    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">

                <div class="content">
                    <div class="container maincontent">

                        @if (session('message'))
                            <div class="alert alert-success">
                                {{ session('message') }}
                            </div>
                        @endif


                        <div class="row">
                            <div class="clear clear-15"></div>
                            <div class="col-md-8 col-lg-9">
                                <div class="page-subheader page-subheader-small">
                                    <h3>@lang('calculator.title')</h3>
                                </div>

                                <form method="post" id="myForm">
                                        {{ csrf_field() }}
{{--                                         <div class="col-sm-6">
                                          <div class="form-group">
                                            <fieldset>
                                                <label class="col-sm-4 control-label" style="text-align: left;">Quel style de maison ? *</label>
                                                <div class="inline radio-fancy">
                                                    <label for="style-modern" class="radio-inline">
                                                        <input type="radio" id="style-modern" name="style" value="1" />
                                                        Moderne
                                                    </label>
                                                    <label for="style-tradi" class="radio-inline">
                                                        <input type="radio" id="style-tradi" name="style" value="2" />
                                                        Traditionnelle
                                                    </label>
                                                </div>
                                            </fieldset>
                                        </div>
                                        </div><!-- /.col-* --> --}}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group @if($errors->has('your_email')) error @endif">
                                            <label>@lang('calculator.ask_style')*</label>
                                            &nbsp; &nbsp;
                                            <label><input type="radio" class="form-control" name="style" value="1">@lang('calculator.style_1')</label>
                                            &nbsp; &nbsp;
                                            <label><input type="radio" class="form-control" name="style" value="2">@lang('calculator.style_2')</label>
                                        </div><!-- /.form-group -->
                                    </div><!-- /.col-* -->
                                </div><!-- /.row -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group @if($errors->has('your_email')) error @endif">
                                            <label>@lang('calculator.ask_floor')*</label>
                                            &nbsp; &nbsp;  &nbsp; &nbsp;  &nbsp; &nbsp;  
                                            <label><input type="radio" class="form-control" name="floor" value="yes">@lang('calculator.oui')</label>
                                            &nbsp; &nbsp;  &nbsp; &nbsp;  &nbsp; &nbsp; 
                                            <label><input type="radio" class="form-control" name="floor" value="no">@lang('calculator.non')</label>
                                        </div><!-- /.form-group -->
                                    </div><!-- /.col-* -->
                                </div><!-- /.row -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group @if($errors->has('your_email')) error @endif">
                                            <label>@lang('calculator.ask_roof')*</label>
                                            &nbsp; &nbsp;
                                            <label><input type="radio" class="form-control" name="roof" value="1">@lang('calculator.roof_1')</label>
                                            &nbsp; &nbsp;
                                            <label><input type="radio" class="form-control" name="roof" value="2">@lang('calculator.roof_2')</label>
                                        </div><!-- /.form-group -->
                                    </div><!-- /.col-* -->
                                </div><!-- /.row -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group @if($errors->has('your_email')) error @endif">
                                            <label>@lang('calculator.ask_room')*</label>
                                            &nbsp; &nbsp;
                                            <input type="number" id="room" class="form-control" value="0" min="0" name="room" required>
                                        </div><!-- /.form-group -->
                                    </div><!-- /.col-* -->
                                    <div class="col-md-3">
                                        <div class="form-group @if($errors->has('your_email')) error @endif">
                                            <label>@lang('calculator.ask_bathrooms')</label>
                                            &nbsp; &nbsp;
                                            <input type="number" class="form-control" value="0" min="0" name="bathroom" required>
                                        </div><!-- /.form-group -->
                                    </div><!-- /.col-* -->
                                    <div class="col-md-3">
                                        <div class="form-group @if($errors->has('your_email')) error @endif">
                                            <label>@lang('calculator.ask_toilets')</label>
                                            &nbsp; &nbsp;
                                            <input type="number" class="form-control" value="0" min="0" name="toilet" required>
                                        </div><!-- /.form-group -->
                                    </div><!-- /.col-* -->

                                </div><!-- /.row -->

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group @if($errors->has('your_email')) error @endif">
                                            <label>@lang('calculator.ask_livingroom')*</label>
                                            &nbsp; &nbsp;
                                            <input type="number"  class="form-control" value="0" min="0" name="livingroom" required>
                                        </div><!-- /.form-group -->
                                    </div><!-- /.col-* -->
                                    <div class="col-md-3">
                                        <div class="form-group @if($errors->has('your_email')) error @endif">
                                            <label>@lang('calculator.ask_garage')</label>
                                            &nbsp; &nbsp;
                                            <input type="number"  class="form-control" value="0" min="0" name="garage" required>
                                        </div><!-- /.form-group -->
                                    </div><!-- /.col-* -->
                                    <div class="col-md-3">
                                        <div class="form-group @if($errors->has('your_email')) error @endif">
                                            <label>@lang('calculator.ask_terrace')</label>
                                            &nbsp; &nbsp;
                                            <input type="number"  class="form-control" value="0" min="0" name="terrace" required>
                                        </div><!-- /.form-group -->
                                    </div><!-- /.col-* -->
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                    <div class="form-group">
                                        <label>@lang('calculator.ask_department')</label>
                                        <select class="form-control" name="department">
                                        <option value="2">01 - Ain </option>
                                        <option value="3">02 - Aisne </option>
                                        <option value="4">03 - Allier </option>
                                        <option value="5">04 - Alpes de Hautes-Provence </option>
                                        <option value="6">05 - Hautes-Alpes </option>
                                        <option value="7">06 - Alpes-Maritimes </option>
                                        <option value="8">07 - Ardèche </option>
                                        <option value="9">08 - Ardennes </option>
                                        <option value="10">09 - Ariège </option>
                                        <option value="11">10 - Aube </option>
                                        <option value="12">11 - Aude </option>
                                        <option value="13">12 - Aveyron </option>
                                        <option value="14">13 - Bouches-du-Rhône </option>
                                        <option value="15">14 - Calvados </option>
                                        <option value="16">15 - Cantal </option>
                                        <option value="17">16 - Charente </option>
                                        <option value="18">17 - Charente-Maritime </option>
                                        <option value="19">18 - Cher </option>
                                        <option value="20">19 - Corrèze </option>
                                        <option value="21">2A - Corse-du-Sud </option>
                                        <option value="22">2B - Haute-Corse </option>
                                        <option value="23">21 - Côte-d'Or </option>
                                        <option value="24">22 - Côtes d'Armor </option>
                                        <option value="25">23 - Creuse </option>
                                        <option value="26">24 - Dordogne </option>
                                        <option value="27">25 - Doubs </option>
                                        <option value="28">26 - Drôme </option>
                                        <option value="29">27 - Eure </option>
                                        <option value="30">28 - Eure-et-Loir </option>
                                        <option value="31">29 - Finistère </option>
                                        <option value="32">30 - Gard </option>
                                        <option value="33">31 - Haute-Garonne </option>
                                        <option value="34">32 - Gers </option>
                                        <option value="35">33 - Gironde </option>
                                        <option value="36">34 - Hérault </option>
                                        <option value="37">35 - Ille-et-Vilaine </option>
                                        <option value="38">36 - Indre </option>
                                        <option value="39">37 - Indre-et-Loire </option>
                                        <option value="40">38 - Isère </option>
                                        <option value="41">39 - Jura </option>
                                        <option value="42">40 - Landes </option>
                                        <option value="43">41 - Loir-et-Cher </option>
                                        <option value="44">42 - Loire </option>
                                        <option value="45">43 - Haute-Loire </option>
                                        <option value="46">44 - Loire-Atlantique </option>
                                        <option value="47">45 - Loiret </option>
                                        <option value="48">46 - Lot </option>
                                        <option value="49">47 - Lot-et-Garonne </option>
                                        <option value="50">48 - Lozère </option>
                                        <option value="51">49 - Maine-et-Loire </option>
                                        <option value="52">50 - Manche </option>
                                        <option value="53">51 - Marne </option>
                                        <option value="54">52 - Haute-Marne </option>
                                        <option value="55">53 - Mayenne </option>
                                        <option value="56">54 - Meurthe-et-Moselle </option>
                                        <option value="57">55 - Meuse </option>
                                        <option value="58">56 - Morbihan </option>
                                        <option value="59">57 - Moselle </option>
                                        <option value="60">58 - Nièvre </option>
                                        <option value="61">59 - Nord </option>
                                        <option value="62">60 - Oise </option>
                                        <option value="63">61 - Orne </option>
                                        <option value="64">62 - Pas-de-Calais </option>
                                        <option value="65">63 - Puy-de-Dôme </option>
                                        <option value="66">64 - Pyrénées-Atlantiques </option>
                                        <option value="67">65 - Hautes-Pyrénées </option>
                                        <option value="68">66 - Pyrénées-Orientales </option>
                                        <option value="69">67 - Bas-Rhin </option>
                                        <option value="70">68 - Haut-Rhin </option>
                                        <option value="71">69 - Rhône </option>
                                        <option value="72">70 - Haute-Saône </option>
                                        <option value="73">71 - Saône-et-Loire </option>
                                        <option value="74">72 - Sarthe </option>
                                        <option value="75">73 - Savoie </option>
                                        <option value="76">74 - Haute-Savoie </option>
                                        <option value="77">75 - Paris </option>
                                        <option value="78">76 - Seine-Maritime </option>
                                        <option value="79">77 - Seine-et-Marne </option>
                                        <option value="80">78 - Yvelines </option>
                                        <option value="81">79 - Deux-Sèvres </option>
                                        <option value="82">80 - Somme </option>
                                        <option value="83">81 - Tarn </option>
                                        <option value="84">82 - Tarn-et-Garonne </option>
                                        <option value="85">83 - Var </option>
                                        <option value="86">84 - Vaucluse </option>
                                        <option value="87">85 - Vendée </option>
                                        <option value="88">86 - Vienne </option>
                                        <option value="89">87 - Haute-Vienne </option>
                                        <option value="90">88 - Vosges </option>
                                        <option value="91">89 - Yonne </option>
                                        <option value="92">90 - Territoire-de-Belfort </option>
                                        <option value="93">91 - Essonne </option>
                                        <option value="94">92 - Hauts-de-Seine </option>
                                        <option value="95">93 - Seine-Saint-Denis </option>
                                        <option value="96">94 - Val-de-Marne </option>
                                        <option value="97">95 - Val-d'Oise </option>
                                        <option value="98">971 - Guadeloupe</option>
                                        <option value="99">972 - Martinique</option>
                                        <option value="100">973 - Guyane</option>
                                        <option value="101">974 - La Réunion</option>   
                                        </select>
                                    </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    
                                <div class="col-sm-12">
                                    <button type="button" id="submit" class="btn btn-primary">@lang('calculator.calculate')</button>
                                </div><!-- /.col-* -->
                                </div>
                            </form>
                        </div><!-- /.col-* -->

                            <div class="col-md-4 col-lg-3">
                                <div class="sidebar">
                                    <div class="widget">
                                        <h2 class="widgettitle">@lang('contact.information')</h2>

                                        <p align="justify">Déterminer le budget de votre construction de maison de rêve est souvent
                                        	compliqué. Beaucoup de paramètres entrent en jeu et il est difficile de trouver une
                                        	fourchette de prix pour votre projet.</p>
                                        	<img src="{{asset('/assets/webapp/img/calculatrice.jpg')}}" class="img-fluid" alt="Calculatrice" />
                                        	<p align="justify"><br>Archionline accompagne des milliers de
                                        		particuliers dans leur projet d’habitat et met aujourd’hui à disposition son outil de
                                        		chiffrage travaux. Avec quelques informations sur votre maison, la calculatrice en
                                        		ligne vous indique le prix de votre maison individuelle, que ce soit une maison
                                        		moderne, une maison bois ou une maison traditionnelle. La calculette vous
                                        		proposera également de recevoir gratuitement par email le prix détaillé lot par lot.</p>
                                    </div><!-- /.widget -->
                                </div><!-- /.sidebar -->
                            </div><!-- /.col-* -->
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </div>

            </div><!-- /.main-inner -->
        </div><!-- /.main -->
    </div><!-- /.main-wrapper -->

@include("webapp.includes.call-to-action-footer")


@endsection

@section('script')
<script src="https://unpkg.com/tippy.js@2.5.2/dist/tippy.all.min.js"></script>
<script type="text/javascript">
//this script for on click in radio button when choosing land
  $("input[name='benefit']").click(function(){
    var abc = $("input[name='benefit']:checked").val();

    if(abc== 'yes'){   
      $('#phone-modal').show('slow');
      $("input[name='phone']").prop('required',true);
    }
    else {
      $('#phone-modal').hide('slow');
      $("input[name='phone']").prop('required',false);
      $("input[name='phone']").val(0);
    }
  });
    $(document).ready(function(){
            $("#submit").click(function(){
                $.ajax({
                    /* the route pointing to the post function */
                    url: '/estimate-calculation/add',
                    type: 'POST',
                    /* send the csrf-token and the input to the controller */
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    data : { "room" : $("#room").val() ,"floor" : $("input[name='floor']:checked").val() },
                    dataType: 'JSON',
                    /* remind that 'data' is the response of the AjaxController */
                    success: function (data) { 
                        $("#modal-email").modal('show');
                        $("#id").val(data.id);
                    }
                }); 
            });
      
      
            $("#receive").click(function(){
            var id = $('#id').val();
            var url = '/estimate-calculation/receive/'+id;
            console.log(url);
                $.ajax({
                    /* the route pointing to the post function */
                    url: url,
                    type: 'POST',
                    /* send the csrf-token and the input to the controller */
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    data : { "phone" : $("input[name='phone']").val() ,"email" : $("input[name='email']").val() },
                    dataType: 'JSON',
                    /* remind that 'data' is the response of the AjaxController */
                    success: function (data) { 
                        $("#modal-email").modal('hide');
                        $("#modal-email-success").modal('show');
                    }
                }); 
            });
   

            $("#cancel").click(function(){
            var id = $('#id').val();
            var url = '/estimate-calculation/cancel/'+id;
                $.ajax({
                    /* the route pointing to the post function */
                    url: url,
                    type: 'POST',
                    /* send the csrf-token and the input to the controller */
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    /* remind that 'data' is the response of the AjaxController */
                    success: function () { 
                        window.open('{{url('')}}', '_self');
                    }
                }); 
            });
              }); 
     
    //     $('.radio-fancy').each(function () {

    //     var group = $('<div class="input-group"><div class="btn-group"><input type="hidden" name=""/></div></div>');
    //     var groupHtml = '';

    //     $(this).find('input').each(function () {
    //         var text = $(this).parent().text().trim();
    //         var name = $(this).attr('name');
    //         var title = ($(this).attr('title')) ? $(this).attr('title') : "";

    //         groupHtml = '<a class="btn btn-primary btn-sm not-active" data-toggle="' + name + '" data-title="' + $(this).val() + '" title="' + title + '">' + text + '</a>';
    //         $(group).find('.btn-group').append(groupHtml);

    //         $(group).find('input[type=hidden]').attr('name', name);
    //     });

    //     $(this).html(group);
    // });

    // $('.radio-fancy').find('a.btn').each(function () {
    //     tippy($(this)[0], {
    //         arrow: true,
    //         arrowType: 'round',
    //         size: 'large',
    //         placement: 'bottom'
    //     });
    // });

    // $('.btn-group a').on('click', function () {

    //     var sel = $(this).data('title');
    //     var tog = $(this).data('toggle');
    //     $('input[type=hidden][name="' + tog + '"]').val(sel);

    //     $('a[data-toggle="' + tog + '"]').not('[data-title="' + sel + '"]').removeClass('active').addClass('not-active');
    //     $('a[data-toggle="' + tog + '"][data-title="' + sel + '"]').removeClass('not-active').addClass('active');
    // });

</script>
@endsection

