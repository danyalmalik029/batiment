@extends("webapp.layouts.default")


@section('content')
    <div class="main-wrapper">
        <div class="main">
            <div class="main-inner">
                <div class="content">
                    {{Form::open(['url'=>'/search-property','method'=>'POST','role'=>'form']) }}
                        {{ Form::token() }}
                        <div class="map-google-wrapper pull-top">
                            <div id="map-google"></div>
                            <div class="map-filter-wrapper map-filter-horizontal">
                                <div class="map-filter filter">
                                    <div class="row">
                                        <div class="form-group col-md-2">
                                            <label>@lang('general.general_property_categorie')</label>

                                            <select class="form-control" name="category" >
                                                @foreach($categories as $cat)
                                                    <option value="{{ $cat->category_slug }}">@lang('general.ads_category_' . $cat->category_slug)</option>
                                                @endforeach
                                            </select>
                                        </div><!-- /.form-group -->

                                        <div class="form-group col-md-2">
                                            <label>@lang('general.general_contract')</label>

                                            <select class="form-control" name="type" >
                                                <option value="rent" > @lang('general.general_rent')</option>
                                                <option value="buy" > @lang('general.general_buy')</option>
                                                <option value="sell" > @lang('general.general_sell')</option>
                                            </select>
                                        </div><!-- /.form-group -->

                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label> @lang('general.general_price_from')</label>
                                                    <input type="text" class="form-control"  name="minPrice" value="0" required>
                                                </div><!-- /.form-group -->

                                                <div class="form-group col-md-6">
                                                    <label> @lang('general.general_price_to')</label>
                                                    <input type="text" class="form-control"  name="maxPrice" value="5000" required>
                                                </div><!-- /.form-group -->
                                            </div><!-- /.row -->
                                        </div><!-- /.col-* -->

                                        <div class="col-md-2">
                                            <div class="form-group-btn form-group-btn-placeholder-gap">
                                                <button type="submit" class="btn btn-primary btn-block"> @lang('general.general_search_now')</button>
                                            </div><!-- /.form-group -->
                                        </div><!-- /.col-* -->
                                    </div><!-- /.row -->
                                </div><!-- /.filter -->
                            </div><!-- /.filter-wrapper -->
                        </div><!-- /.map-google-wrapper -->
                    {{ Form::close() }}
                    <div class="information-bar">
                        <div class="container">
                            <i class="fa fa-star"></i> @lang('general.general_tagline')</a>.
                        </div><!-- /.container -->
                    </div><!-- /.information-bar -->

                    <div class="container">
                        <div class="page-header">
                            <h1>@lang('general.general_home_slogan')</h1>
                            <!--
                            <ul>
                                <li><a href="properties.html">Recent</a></li>
                                <li><a href="properties.html">Featured</a></li>
                                <li><a href="properties.html">Reduced</a></li>
                                <li><a href="properties.html">Sale</a></li>
                            </ul>
                            -->
                        </div><!-- /.page-header -->

                        <div class="row">
                            <div class="col-md-6">

                                <div class="listing-box listing-box-simple">
                                    <div class="listing-box-image" style="background-image: url('../storage/propertiesImages/{{ (isset($properties[0]['images'][0]))?$properties[0]['images'][0]['image_name']:asset("assets/img/tmp/tmp-5.jpg") }}')">
                                        <div class="listing-box-image-title">
                                            <h2><a href="{{ url('/view-details-property/'.$properties[0]['id']) }}">{{ $properties[0]['title'] }}</a></h2>
                                            <h3>&#128; {{ $properties[0]['price'] }}</h3>
                                        </div><!-- /.listing-box-image-title -->

                                        <span class="listing-box-image-links">
                                            <a href="{{ url('/view-details-property/'.$properties[0]['id']) }}"><i class="fa fa-search"></i> <span>@lang('general.general_view_detail')</span></a>
                                        </span>
                                    </div><!-- /.listing-box-image -->
                                </div><!-- /.listing-box -->
                            </div><!-- /.col-* -->

                            <div class="col-md-6">
                                <div class="row">

                                    @for($i = 1; $i < 3; $i++)

                                    <div class="col-xs-12 col-sm-6">
                                        <div class="listing-box">
                                            <div class="listing-box-image" style="background-image: url('../storage/propertiesImages/{{ $properties[$i]['images'][0]['image_name'] }}')">
                                                <!-- <div class="listing-box-image-label">Featured</div><!-- /.listing-box-image-label -->

                                                <span class="listing-box-image-links">
                                                    <a href="{{ url('/view-details-property/'.$properties[$i]['id']) }}"><i class="fa fa-search"></i> <span>@lang('general.general_view_detail')</span></a>
                                                </span>

                                            </div><!-- /.listing-box-image -->

                                            <div class="listing-box-title">
                                                <h2><a href="{{ url('/view-details-property/'.$properties[$i]['id']) }}">{{ $properties[$i]['title'] }}</a></h2>
                                                <h3>&#128; {{ $properties[$i]['price'] }}</h3>
                                            </div><!-- /.listing-box-title -->

                                            <div class="listing-box-content">
                                                <dl>
                                                    <dt>@lang('general.general_type')</dt><dd>{{ $properties[$i]['category_name'] }}</dd>
                                                    <dt>@lang('general.general_location')</dt><dd>{{ $properties[$i]['address_city'] }}</dd>
                                                    <dt>@lang('general.general_area')</dt><dd>{{ $properties[$i]['area'] }} m<sup>2</sup></dd>
                                                </dl>
                                            </div><!-- /.listing-box-cotntent -->
                                        </div><!-- /.listing-box -->
                                    </div><!-- /.col-* -->

                                    @endfor

                                </div><!-- /.row -->
                            </div><!-- /.col-* -->
                        </div><!-- /.row -->

                        <div class="row">
                            <div class="col-md-12">
                                <div class="listing-box-wrapper">
                                    <div class="row">

                                        @for($i = 3; $i < 7; $i++)



                                        <div class="col-xs-12 col-sm-3">
                                            <div class="listing-box">
                                                <div class="listing-box-image" style="background-image: url('../storage/propertiesImages/{{ $properties[$i]['images'][0]['image_name'] }}')">
                                                    <!-- <div class="listing-box-image-label">Featured</div><!-- /.listing-box-image-label -->

                                                    <span class="listing-box-image-links">
                                                    <a href="{{ url('/view-details-property/'.$properties[$i]['id']) }}"><i class="fa fa-search"></i> <span>@lang('general.general_view_detail')</span></a>
                                                </span>

                                                </div><!-- /.listing-box-image -->

                                                <div class="listing-box-title">
                                                    <h2><a href="{{ url('/view-details-property/'.$properties[$i]['id']) }}">{{ $properties[$i]['title'] }}</a></h2>
                                                    <h3>&#128; {{ $properties[$i]['price'] }}</h3>
                                                </div><!-- /.listing-box-title -->

                                                <div class="listing-box-content">
                                                    <dl>
                                                        <dt>@lang('general.general_type')</dt><dd>@lang('properties.type_' . $properties[$i]['ad_dtype'])</dd>
                                                        <dt>@lang('general.general_location')</dt><dd>{{ $properties[$i]['address_city'] }}</dd>
                                                        <dt>@lang('general.general_area')</dt><dd>{{ $properties[$i]['area'] }} m<sup>2</sup></dd>
                                                    </dl>
                                                </div><!-- /.listing-box-cotntent -->
                                            </div><!-- /.listing-box -->
                                        </div><!-- /.col-* -->

                                        @endfor

                                    </div><!-- /.row -->
                                </div><!-- /.listing-box-wrapper -->
                            </div><!-- /.col-sm-* -->
                        </div><!-- /.row -->


                        <div class="push-top">
                            <div class="background-white fullwidth">
                                <div class="boxes">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="box">
                                                <div class="box-content">

                                                </div><!-- /.box-content -->
                                            </div><!-- /.box -->
                                        </div><!-- /.col-* -->
                                    </div><!-- /.row -->
                                </div><!-- /.boxes -->
                            </div>

                        </div>

                        <div class="push-bottom">
                            <div class="page-header">
                                <h1>@lang('general.general_latest_plans')</h1>
                            </div><!-- /.page-header -->

                            <div class="categories">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <div class="row">

                                            @foreach($plans as $key=>$plan)
                                                @if($key < 2)

                                                <?php

                                                    $files = $plan['images'];

                                                    foreach ($files as $file) {
                                                        if($file['path'] == '/planImages/') {
                                                            $image = $file;
                                                            break;
                                                        }
                                                    }
                                                ?>

                                            <div class="col-sm-6">
                                                <div class="category" style="background-image: url('../storage/planImages/{{ $image['nom'] }}');">
                                                    <a href="{{ url('/view-details-plan/' . $plan['id']) }}" class="category-link">
                                                        <span class="category-content">
                                                            <span class="category-title">{{ $plan['title'] }}</span>
                                                            <span class="btn btn-primary">@lang('general.general_read_more_plan')</span>
                                                        </span>
                                                    </a>
                                                </div><!-- /.category -->
                                            </div><!-- /.col-* -->
                                                @endif
                                            @endforeach

                                        </div><!-- /.row -->

                                        @foreach($plans as $key=>$plan)
                                            @if($key === 2)
                                            <?php

                                                $files = $plan['images'];

                                                foreach ($files as $file) {
                                                    if($file['path'] == '/planImages/') {
                                                        $image = $file;
                                                        break;
                                                    }
                                                }
                                            ?>
                                        <div class="category" style="background-image: url('../storage/planImages/{{ $image['nom'] }}');">
                                            <a href="{{ url('/view-details-plan/' . $plan['id']) }}" class="category-link">
                                                <span class="category-content">
                                                    <span class="category-title">{{ $plan['title'] }}</span>
                                                    <span class="btn btn-primary">@lang('general.general_read_more_plan')</span>
                                                </span><!-- /.category-content -->
                                            </a>
                                        </div><!-- /.category -->
                                            @endif
                                        @endforeach
                                    </div><!-- /.col-* -->

                                    <div class="col-lg-4">

                                        @foreach($plans as $key=>$plan)
                                            @if($key === 3)
                                            <?php

                                                $files = $plan['images'];

                                                foreach ($files as $file) {
                                                    if($file['path'] == '/planImages/') {
                                                        $image = $file;
                                                        break;
                                                    }
                                                }
                                            ?>
                                        <div class="category category-vertical" style="background-image: url('../storage/planImages/{{ $image['nom'] }}');">
                                            <a href="{{ url('/view-details-plan/' . $plan['id']) }}" class="category-link">
                                                <span class="category-content">
                                                    <span class="category-title">{{ $plan['title'] }}</span>
                                                    <span class="btn btn-primary">@lang('general.general_read_more_plan')</span>
                                                </span><!-- /.category-content -->
                                            </a>
                                        </div><!-- /.category -->
                                            @endif
                                        @endforeach

                                    </div><!-- /.col-* -->
                                </div><!-- /.row -->
                            </div><!-- /.categories -->
                        </div>
                    </div><!-- /.container -->

                    <div class="image-wrapper">
                        <div class="container">
                            <div class="row">
                                <div class="image-description-wrapper col-md-12 col-lg-7">
                                    <div class="image-description">
                                        <h2>@lang('general.general_home_rent_sologan_header')</h2>

                                        <p>
                                            @lang('general.general_home_rent_sologan')
                                        </p>

                                        <a href="{{ url('/view-rent-properties') }}" class="btn btn-primary">@lang('general.general_home_rent_sologan_button')</a>
                                    </div><!-- /.image-description -->
                                </div><!-- /.col-* -->
                            </div><!-- /.row -->
                        </div><!-- /.container -->

                        <div class="image-holder col-lg-5 col-lg-offset-7 hidden-md-down" style="background-image: url('{{ asset("assets/webapp/img/tmp/tmp-" . rand(1,13) . ".jpg") }}')">
                        </div><!-- /.col-* -->
                    </div><!-- /.image-wrapper -->

                    <div class="container">
                        <div class="page-header">
                            <h2>@lang('general.general_featured_properties')</h2>
                        </div>

                        <div class="listing-box-wrapper listing-box-background">
                            <div class="listing-carousel-wrapper">
                                <div class="listing-carousel">

                                    @foreach($features as $feature)
                                        <div class="listing-box">
                                            <div class="listing-box-image" style="background-image: url('../storage/propertiesImages/{{ $feature->image_name}}')">
                                                <div class="listing-box-image-label">@lang('general.general_featured')</div><!-- /.listing-box-image-label -->
                                                <span class="listing-box-image-links">
                                                    <!--<a href="properties.html"><i class="fa fa-heart"></i> <span>Add to favorites</span></a>-->
                                                    <a href="{{ url('/view-details-property/'.$feature->id)}}"><i class="fa fa-search"></i> <span>@lang('general.general_view_detail')</span></a>
                                                    <!--<a href="properties-compare.html"><i class="fa fa-balance-scale"></i> <span>Compare property</span></a>
                                                    <!--<a href="properties-detail-standard.html"><i class="fa fa-search"></i> <span>View detail</span></a>-->
                                                </span>
                                            </div><!-- /.listing-box-image -->
                                            <div class="listing-box-title">{{$feature->title}}</a></h2>
                                                <h3>{{'$'.' '.$feature->price}}</h3>
                                            </div><!-- /.listing-box-title -->
                                            <div class="listing-box-content">
                                                <dl>
                                                    <dt>@lang('general.general_type')</dt><dd>@lang('properties.type_' . $feature->ad_dtype)</dd>
                                                    <dt>@lang('general.general_location')</dt><dd>{{ $feature->address_city }}</dd>
                                                    <dt>@lang('general.general_area')</dt><dd>{{ $feature->area }} m<sup>2</sup></dd>
                                                </dl>
                                            </div><!-- /.listing-box-cotntent -->
                                        </div><!-- /.listing-box -->
                                    @endforeach;

                                </div>
                            </div>
                        </div>
                    </div><!-- /.container -->
                </div><!-- /.content -->
            </div><!-- /.main-inner -->
        </div><!-- /.main -->
    </div><!-- /.main-wrapper -->

@include("webapp.includes.call-to-action-footer")

@endsection