<?php

//app/Helpers/ContentHook.php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class ContentHook {
    /**
     * @param int $user_id User-id
     *
     * @return string
     */
    public static function get_content($slug) {

        $page = DB::table('pages')->where('slug', $slug)->first();

        return (isset($page->body) ? $page->body : 'Not found');

    }
}