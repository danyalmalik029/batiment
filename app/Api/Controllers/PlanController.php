<?php
/**
 * Created by IntelliJ IDEA.
 * User: hdouanla
 * Date: 2017-06-06
 * Time: 1:31 AM
 */

namespace App\Api\Controllers;
use Illuminate\Http\Request;
use App\Models\Plan;
use App\Models\Images;
use Response;

class PlanController extends ApiBaseController
{
    // Get an Ad matching a specific Id
    public function getDetails(Request $request, $id) {
        $adQuery = Ads::where('id',$id)->first();

        if($adQuery === null) {
            return [];
        }

        // Ad found
        return $adQuery->toArray();
    }

    public function disableProperty(Request $request) {
    	//dd($request->id);
        $planQuery = Plan::find($request->id);

        $planQuery->statut = '0';
        $planQuery->update();

        return Response::json(["statut" => "done"]);
        
    }

    public function deleteProperty(Request $request) {
    	//dd($request->id);
        $planQuery = Plan::find($request->id);

        $planQuery->statut = '2';
        $planQuery->update();

        return Response::json(["statut" => "done"]);
        
    }

    public function enableProperty(Request $request) {
    	//dd($request->id);
        $planQuery = Plan::find($request->id);

        $planQuery->statut = '1';
        $planQuery->update();

        return Response::json(["statut" => "done"]);
        
    }

    public function compareLength(Request $request) {
    	
        $count =  is_null($request->session()->get('compare')) ? 0 : $request->session()->get('compare');

        //dd($count);

        return Response::json(["statut" => $count]);
        
    }

    public function addCompareProperty(Request $request) {
    	//var_dump($request->session()->all());
        $count =  is_null($request->session()->get('compare_plan')) ? 0 : $request->session()->get('compare_plan');

        if($count < 3){
        	$count++;
        	$request->session()->put('compare_plan'.$count, $request->id);
        	$request->session()->put('compare_plan', $count);

        }else{
        	//dd("ici");
        	$request->session()->put('compare_plan_3', $request->session()->get('compare_plan_2'));
        	$request->session()->put('compare_plan_2', $request->session()->get('compare_plan_1'));
        	$request->session()->put('compare_plan_1', $request->id);
        }
        //var_dump($request->session()->all());

        return Response::json(["statut" => $count]);
        
    }

    public function remove(Request $request) {
    	//var_dump($request->session()->all());
        $count =  is_null($request->session()->get('compare_plan')) ? 0 : $request->session()->get('compare_plan');
        $length = $count;

        for ($i=1; $i <= $count; $i++) { 
        	if( $request->session()->get('compare_plan_'.$i) == $request->id ){
        		$request->session()->forget('compare_plan_'.$i);
        		$length--;
				$request->session()->put('compare_plan', $length);
        	}
        }

        return Response::json(["statut" => "done"]);
        
    }

    public function mapProperties() {

        $properties = Ads::where('statut', '1')
            ->orderBy('id', 'DESC')
            ->limit(\Config::get('settings.listLenght'))
            ->join('ads_categories','ads_categories.id','=','ads.catid')
            ->select('ads.*', 'ads_categories.category_name')
            ->get()
            ->toArray();

        $i = 1;

        foreach ($properties as $key=>$row) {

            $image = "";

            // Find images related to this property
            $imagesQuery = Images::where('ads_id',$properties[$key]['id'])
                ->join('user_image','user_image.id','=','ads_images.image_id')
                ->select('ads_images.*','user_image.image_name as image_name')
                ->get();

            if($imagesQuery !== null) {
                $imagesArray = $imagesQuery->toArray();
                $image = "../storage/propertiesImages/" . $imagesArray[0]['image_name'];
            }

            $properties[$key] = [
                "adId" => $properties[$key]["id"],
                "id" => "marker-" . $i,
                "center"  =>  [
                    $properties[$key]['address_lat'],
                    $properties[$key]['address_lng']
                ],
                "icon" => "<i class='fa fa-building'></i>",
                "title" => $properties[$key]["title"],
                "price" => "&#128; " . $properties[$key]["price"],
                "image" => $image
            ];

            $i++;
        }

        return Response::json(["data" => $properties]);
    }

}