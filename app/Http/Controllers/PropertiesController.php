<?php

namespace App\Http\Controllers;

use App\Events\ReplyLandEvent;
use App\Notifications\RepliedToLand;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\PropertiesRequest;

use App\Models\Ads;
use App\Models\Categories;
use App\Models\Images;
use Validator;
use Auth;
use Carbon\Carbon;
use Plupload;
use File;
use Image;

use Illuminate\Support\Facades\Storage;

use App\Models\UserImage;
use App\Models\SubmissionMessage;
use App\Models\Users;
use App\Models\UserProfile;


class PropertiesController extends Controller
{
	/**
	 * Create a new authentication controller instance.
	 *
	 * @return void
	 */
	public function __construct() 
	{
	    parent::__construct();
		$this->middleware('auth', [
		    'except' => [
		        'viewAllProperties',
                'details',
                'search',
                'compareProperties',
                'viewRentProperties',
                'viewSaleProperties',
                'viewBuildProperties',
                'viewUserProperties']]);
	}
    /**
     * Create properties form
     *
     * @return \Illuminate\Http\Response
     */
    public function propertyform(Request $request, $id = 0)
    {

        $data = [
            'page' => 'submit-properties',
            'sucess' => 'null',
            'editing' => false,
            'adsImages' => []
        ];

        $adsId = intval($id);

        // If asked for image deletion, then proceed it
        if($request->action) {
            if($request->action === 'deleteimage' ) {
                Images::where('ads_id',$adsId)
                    ->where('image_id', $request->image_id)
                    ->delete();
            }
        }

        if($adsId) {
            $ads = Ads::where('id',$adsId)->first();
            if($ads !== null) {
                $data['editing'] = true;
                $data['ads'] = $ads;
                $data['categorie'] = Categories::find($ads->catid);

                // Find images related to this property
                $imagesQuery = Images::where('ads_id',$adsId)
                    ->join('user_image','user_image.id','=','ads_images.image_id')
                    ->select('ads_images.*','user_image.image_name as image_name')
                    ->get();

                $adsImages = ($imagesQuery !== null)?$imagesQuery->toArray():[];

                $data['adsImages'] = $adsImages;


            } elseif(($ads === null) && ($adsId > 0)) {
                // Requestig for an unexisting property, bad ID
                // Initialize with empty values
                return redirect('view-all-properties');
            }
        } else {
            $data['ads'] = new Ads();
        }



        $data['roomsOptions'] = [
          0 => 0,
          1 => 1,
          2 => 2,
          3 => 3,
          4 => 4,
          5 => 5,
          6 => 6,
          7 => 7,
          8 => 8,
          9 => 9
        ];

        $data['bathroomsOptions'] = [
          0 => 0,
          1 => 1,
          2 => 2,
          3 => 3,
          4 => 4,
          5 => 5,
          6 => 6,
          7 => 7,
          8 => 8,
          9 => 9
        ];

        return view('webapp.properties.propertyform')->with($data);
    }

     /**
     * Save properties form
     *
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
    	//dd($request->cleaning);
    	$regex = "/^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/";

    	$validator = Validator::make($request->all(), [
    		'title' => 'required',
			'description' => 'required',
			'ad_dtype' => 'required',
			'price' => 'required|integer',
			'bedrooms' => 'required|integer',
			'bathrooms' => 'required|integer',
			'area' =>  array('required','regex:'.$regex)
        ]);

        if ($validator->fails()) {

        	//dd(count($validator->errors()->all()));
           return redirect()->back()
                        ->withErrors($validator->errors())
                        ->withInput();
        }

        $property = new Ads();

        $property->catid =  $request->categorie;
        $property->title =  $request->title;
        $property->slug =  str_slug($request->title, '-');
        $property->description =  $request->description;
        //$property->Adscol =  $request->Adscol;
        $property->user_id =  Auth::id();
        $property->ad_dtype =  $request->ad_dtype;
        $property->action =  "offering";
        $property->price =  $request->price;
        $property->bedrooms =  $request->bedrooms;
        $property->bathrooms =  $request->bathrooms;
        $property->area =  $request->area;
        $property->address_street_number =  $request->address_street_number;
        $property->address_steet_name =  $request->address_steet_name;
        $property->address_city =  $request->address_city;
        $property->address_region =  $request->address_region;
        $property->address_zip_code =  $request->address_zip_code;
        $property->address_country =  $request->address_country;
        $property->address_apt =  $request->address_apt;
        $property->address_lat =  $request->address_lat;
        $property->address_lng =  $request->address_lng;
        $property->address_suppl =  $request->address_suppl;
        //$property->legaldocument =  $request->legaldocument;
        $property->conditioning =   ($request->conditioning == "on") ? true : false;
        $property->cleaning =   ($request->cleaning == "on") ? true : false;
        $property->dishwasher =   ($request->dishwasher == "on") ? true : false;
        $property->grill =   ($request->grill == "on") ? true : false;
        $property->internet =   ($request->internet == "on") ? true : false;
        $property->microwave =   ($request->microwave == "on") ? true : false;
        $property->pool =   ($request->pool == "on") ? true : false;
        $property->terrace =   ($request->terrace == "on") ? true : false;
        $property->balcony =   ($request->balcony == "on") ? true : false;
        $property->cofeepot =   ($request->cofeepot == "on") ? true : false;
        $property->dvd =   ($request->dvd == "on") ? true : false;
        $property->hairdryer =   ($request->hairdryer == "on") ? true : false;
        $property->iron =   ($request->iron == "on") ? true : false;
        $property->oven =   ($request->oven == "on") ? true : false;
        $property->radio =   ($request->radio == "on") ? true : false;
        $property->toaster =   ($request->toaster == "on") ? true : false;
        $property->bedding =   ($request->bedding == "on") ? true : false;
        $property->computer =   ($request->computer == "on") ? true : false;
        $property->fan =   ($request->fan == "on") ? true : false;
        $property->heating =   ($request->heating == "on") ? true : false;
        $property->juicer =   ($request->juicer == "on") ? true : false;
        $property->parking =   ($request->parking == "on") ? true : false;
        $property->roofterrace =   ($request->roofterrace == "on") ? true : false;
        $property->towelwes =   ($request->towelwes == "on") ? true : false;
        $property->cabletv =   ($request->cabletv == "on") ? true : false;
        $property->cot =   ($request->cot == "on") ? true : false;
        $property->fridge =   ($request->fridge == "on") ? true : false;
        $property->hifi =   ($request->hifi == "on") ? true : false;
        $property->lift =   ($request->lift == "on") ? true : false;
        $property->parquet =   ($request->parquet == "on") ? true : false;
        $property->smoking =   ($request->smoking == "on") ? true : false;
        $property->useofpool =   ($request->useofpool == "on") ? true : false;
        $property->updated_at = Carbon::now();
        $property->created_at = Carbon::now();
        $property->statut = "1";
        $property->save();

        $this->__saveAdImages($request, $property->id);

        return redirect("my-properties");
    }

    /**
     * Create properties form
     *
     * @return \Illuminate\Http\Response
     */
    public function viewAllProperties()
    {
    	$properties = Ads::where('statut', '1')
            ->orderBy('id', 'DESC')
            ->paginate(\Config::get('settings.listLenght'));

        $userProfile = null;
        if(Auth::check()){
            $userProfile = UserProfile::where('iduser', Auth::id())->first();
        }

    	//dd(Auth::User()->ads);

        return view('webapp.properties.viewAll')->with([
        	'page' => 'view-properties',
        	'sucess' => 'null',
            'error' => 'null',
        	'properties' => $properties,
            'userProfile' => $userProfile,
            'categories' => Categories::All(),
            'title' => trans('properties.view_all_properties')
        ]);
    }

    /**
     * Create properties form
     *
     * @return \Illuminate\Http\Response
     */
    public function viewRentProperties()
    {
    	$properties = Ads::where('statut', '1')
            ->where('ad_dtype', 'rent')
            ->orderBy('id', 'DESC')
            ->paginate(\Config::get('settings.listLenght'));

        $user = null;
        if(Auth::check()){
            $user = UserProfile::where('iduser', Auth::id())->first();
        }

    	//dd(Auth::User()->ads);

        return view('webapp.properties.viewAll')->with([
        	'page' => 'view-properties',
        	'sucess' => 'null',
            'error' => 'null',
        	'properties' => $properties,
            'userProfile' => $user,
            'categories' => Categories::All(),
            'title' => trans('properties.view_rent_properties')
        ]);
    }

    /**
     * Create properties form
     *
     * @return \Illuminate\Http\Response
     */
    public function viewSaleProperties()
    {
    	$properties = Ads::where('statut', '1')
            ->where('ad_dtype', 'sell')
            ->orderBy('id', 'DESC')
            ->paginate(\Config::get('settings.listLenght'));

        $user = null;
        if(Auth::check()){
            $user = UserProfile::where('iduser', Auth::id())->first();
        }

    	//dd(Auth::User()->ads);

        return view('webapp.properties.viewAll')->with([
        	'page' => 'view-properties',
        	'sucess' => 'null',
            'error' => 'null',
        	'properties' => $properties,
            'userProfile' => $user,
            'categories' => Categories::All(),
            'title' => trans('properties.view_sale_properties')
        ]);
    }

    /**
     * Create properties form
     *
     * @return \Illuminate\Http\Response
     */
    public function viewBuildProperties()
    {
    	$properties = Ads::where('ads.statut', '1')
            ->join('ads_categories','ads_categories.id', '=', 'ads.catid')
            ->where('ads_categories.category_slug','build')
            ->orderBy('ads.id', 'DESC')
            ->select('ads.*', 'ads_categories.category_slug')
            ->paginate(\Config::get('settings.listLenght'));

        $user = null;
        if(Auth::check()){
            $user = UserProfile::where('iduser', Auth::id())->first();
        }

    	//dd(Auth::User()->ads);

        return view('webapp.properties.viewAll')->with([
        	'page' => 'view-properties',
        	'sucess' => 'null',
            'error' => 'null',
        	'properties' => $properties,
            'userProfile' => $user,
            'categories' => Categories::All(),
            'title' => trans('properties.view_build_properties')
        ]);
    }


    /**
     * Create properties form
     *
     * @return \Illuminate\Http\Response
     */
    public function myProperties()
    {
    	//dd(Auth::id());
    	$properties = Ads::where('user_id', Auth::id())->where('statut', '<>', '2')
            ->orderBy('id', 'DESC')
            ->paginate(\Config::get('settings.listLength'));

    	//dd($properties);
        return view('webapp.properties.viewAll')->with([
        	'page' => 'my-properties',
        	'sucess' => 'null',
            'error' => 'null',
        	'properties' => $properties,
            'categories' => Categories::All(),
            'title' => trans('profile.my_properties')
        ]);
    }


    /**
     * Create properties form
     *
     * @return \Illuminate\Http\Response
     */
    public function viewUserProperties($id)
    {
    	//dd(Auth::id());
    	$properties = Ads::where('user_id', $id)->where('statut', '<>', '2')
            ->orderBy('id', 'DESC')
            ->paginate(\Config::get('settings.listLength'));

    	//dd($properties);
        return view('webapp.properties.viewAll')->with([
        	'page' => 'my-properties',
        	'sucess' => 'null',
            'error' => 'null',
        	'properties' => $properties,
            'categories' => Categories::All(),
            'title' => @trans('profile.user_properties')
        ]);
    }

    /**
     * Create properties form
     *
     * @return \Illuminate\Http\Response
     */
    public function details($id)
    {
    	$ads = Ads::find($id);
    	$categorie = Categories::find($ads->catid);

    	$var = Images::where('ads_id', $ads->id)->get();
    	$list = array();
    	$i = 1;
    	foreach ($var as $key ) {
    		$list[$i] = UserImage::find($key->image_id)->image_name;
    		$i++;
    	}
    	
    	$userList = array();
    	$messages = array();
    	
    	if(Auth::check())
    		$messages = SubmissionMessage::where('ads_id', $ads->id)->where('receiver_id', Auth::id())->get();
    	else
    		$messages = array();

    	$i = 1;
    	foreach ($messages as $key) {
    		$user = Users::find($key->sender_id);
    		if(!in_array($user, $userList)){
    			$userList[$i] = $user;
    			$i++;
    		}
    	}

    	$data = [
            'page' => 'view-properties',
            'sucess' => 'null',
            'ads' => $ads,
            'categorie' => $categorie,
            'list' => $list,
            'owner' => User::where('id', $ads->user_id)->first(),
            'pagetitle' => "ETB Batiment, " . $ads->title,
            'userList' => $userList
        ];

    	//dd($userList);
        return view('webapp.properties.details')->with($data);
    }

    /**
     * Create properties form
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$ads = Ads::find($id);
    	$categorie = Categories::find($ads->catid);

        return view('webapp.properties.edit')->with([
        	'page' => 'view-properties',
        	'sucess' => 'null',
        	'ads' => $ads,
        	'categorie' => $categorie
        ]);
    }

    /**
     * Create properties form
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

    	$regex = "/^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/";

    	$validator = Validator::make($request->all(), [
    		'title' => 'required',
			'description' => 'required',
			'ad_dtype' => 'required',
			'price' => 'required|integer',
			'bedrooms' => 'required|integer',
			'bathrooms' => 'required|integer',
			'area' =>  array('required','regex:'.$regex)

        ]);

        if ($validator->fails()) {

        	//dd(count($validator->errors()->all()));
           return redirect()->back()
                        ->withErrors($validator->errors())
                        ->withInput();
        }

        //dd($request->all());
        $property = Ads::find($request->property);

        $property->catid =  $request->categorie;
        $property->title =  $request->title;
        $property->slug =  str_slug($request->title, '-');
        $property->description =  $request->description;
        //$property->Adscol =  $request->Adscol;
        $property->ad_dtype =  $request->ad_dtype;
        $property->action =  "offering";
        $property->price =  $request->price;
        $property->bedrooms =  $request->bedrooms;
        $property->bathrooms =  $request->bathrooms;
        $property->area =  $request->area;
        $property->address_street_number =  $request->address_street_number;
        $property->address_steet_name =  $request->address_steet_name;
        $property->address_city =  $request->address_city;
        $property->address_region =  $request->address_region;
        $property->address_zip_code =  $request->address_zip_code;
        $property->address_country =  $request->address_country;
        $property->address_apt =  $request->address_apt;
        $property->address_lat =  $request->address_lat;
        $property->address_lng =  $request->address_lng;
        $property->address_suppl =  $request->address_suppl;
        //$property->legaldocument =  $request->legaldocument;
        $property->conditioning =   ($request->conditioning == "on") ? true : false;
        $property->cleaning =   ($request->cleaning == "on") ? true : false;
        $property->dishwasher =   ($request->dishwasher == "on") ? true : false;
        $property->grill =   ($request->grill == "on") ? true : false;
        $property->internet =   ($request->internet == "on") ? true : false;
        $property->microwave =   ($request->microwave == "on") ? true : false;
        $property->pool =   ($request->pool == "on") ? true : false;
        $property->terrace =   ($request->terrace == "on") ? true : false;
        $property->balcony =   ($request->balcony == "on") ? true : false;
        $property->cofeepot =   ($request->cofeepot == "on") ? true : false;
        $property->dvd =   ($request->dvd == "on") ? true : false;
        $property->hairdryer =   ($request->hairdryer == "on") ? true : false;
        $property->iron =   ($request->iron == "on") ? true : false;
        $property->oven =   ($request->oven == "on") ? true : false;
        $property->radio =   ($request->radio == "on") ? true : false;
        $property->toaster =   ($request->toaster == "on") ? true : false;
        $property->bedding =   ($request->bedding == "on") ? true : false;
        $property->computer =   ($request->computer == "on") ? true : false;
        $property->fan =   ($request->fan == "on") ? true : false;
        $property->heating =   ($request->heating == "on") ? true : false;
        $property->juicer =   ($request->juicer == "on") ? true : false;
        $property->parking =   ($request->parking == "on") ? true : false;
        $property->roofterrace =   ($request->roofterrace == "on") ? true : false;
        $property->towelwes =   ($request->towelwes == "on") ? true : false;
        $property->cabletv =   ($request->cabletv == "on") ? true : false;
        $property->cot =   ($request->cot == "on") ? true : false;
        $property->fridge =   ($request->fridge == "on") ? true : false;
        $property->hifi =   ($request->hifi == "on") ? true : false;
        $property->lift =   ($request->lift == "on") ? true : false;
        $property->parquet =   ($request->parquet == "on") ? true : false;
        $property->smoking =   ($request->smoking == "on") ? true : false;
        $property->useofpool =   ($request->useofpool == "on") ? true : false;
        $property->updated_at = Carbon::now();
        $property->update();

        $this->__saveAdImages($request, $property->id);

        return redirect("my-properties");
    }

    /**
     * Create properties form
     *
     * @return \Illuminate\Http\Response
     */
    public function sendMessage(Request $request)
    {

    	$validator = Validator::make($request->all(), [
			'subject' => 'required',
			'message' => 'required'
        ]);

        if ($validator->fails()) {

        	//dd(count($validator->errors()->all()));
           return redirect()->back()
                        ->withErrors($validator->errors())
                        ->withInput();
        }

        //dd($request->all());
        $property = Ads::find($request->ads);

        if(Auth::id() != $property->user_id){
	        $message = new SubmissionMessage ();
	        $message->sender_id = Auth::id();
	        $message->subject = $request->subject;
			$message->message_body = $request->message;
			$message->receiver_id = $property->user_id;
			$message->ads_id = $property->id; 
			$property->updated_at = Carbon::now();
        	$property->created_at = Carbon::now();
		}else{
			$message = new SubmissionMessage ();
	        $message->sender_id = $property->user_id;
	        $message->subject = $request->subject;
			$message->message_body = $request->message;
			$message->receiver_id = Auth::id();
			$message->ads_id = $property->id;
			$property->updated_at = Carbon::now();
        	$property->created_at = Carbon::now();
		} 

		$message->save();
        $reciever=User::where('id',$message->receiver_id)->first();
        $reciever->notify(new RepliedToLand($message));
        $notify_id=$reciever->notifications()->orderBy('created_at', 'desc')->first();
        $count=count($reciever->unreadNotifications);
        $user=Auth::user();
        event(new ReplyLandEvent($message,$user,$reciever,$count,$notify_id->id));
		return redirect('/view-details-property/' . $property->id)->with('message', trans('properties.message_sent'));
    }


    /**
     * Create properties form
     *
     * @return \Illuminate\Http\Response
     */
    public function listMessages($adsId)
    {
    	$ads = Ads::where('id', $adsId)
            ->where('user_id', Auth::id())
            ->first();

    	if($ads === null)
    	    return redirect('view-all-properties');

    	$messages = SubmissionMessage::where('ads_id', $adsId)
            ->join('users', 'users.id', 'submission_message.sender_id')
            ->select(
                'submission_message.*',
                'users.name as senderName',
                'users.email as senderEmail'
            )
            ->orderBy('id', 'des')->get();

        return view('webapp.properties.messages')->with([
        	'page' => 'view-properties',
        	'sucess' => 'null',
        	'ads' => $ads,
            'messages_type' => 'ads',
        	'messages' => $messages
        ]);
    }


    /**
     * Search properties form
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        //dd($request->cleaning);
        

        $cat = Categories::where('category_slug', $request->category)->first();
        $properties = Ads::where('catid', $cat->id)
                        ->where('ad_dtype', $request->type)
                        ->whereBetween('price', [$request->minPrice, $request->maxPrice])
                        ->paginate(\Config::get('settings.listLength'));

        //UserImage::find(Ads::find(51)->imagesLinked[0]->image_id)
        //dd(Ads::find(51)->images[0]->image_name);
        return view('webapp.properties.viewAll')->with([
            'page' => 'view-properties',
            'sucess' => trans('properties.search_results_found'),
            'error' => 'null',
            'properties' => $properties,
            'categories' => Categories::All(),
            'isSearch' => true,
            'title' => trans('properties.search_title')
        ]);
    }


    /**
     * Create properties form
     *
     * @return \Illuminate\Http\Response
     */
    public function compareProperties(Request $request)
    {

        // If requested to remove from compare, then proceed
        if($request->action === 'remove') {

            $count =  is_null($request->session()->get('compare')) ? 0 : $request->session()->get('compare');
            $length = $count;

            for ($i=1; $i <= 4; $i++) {
                if( $request->session()->get('compare_'.$i) == $request->id ){
                    $request->session()->forget('compare_'.$i);
                    $length--;
                    $request->session()->put('compare', $length);
                }
            }
        }

        $compare_1 = (session('compare_1'))?Ads::find(session('compare_1')):null;
        $compare_2 = (session('compare_2'))?Ads::find(session('compare_2')):null;
        $compare_3 = (session('compare_3'))?Ads::find(session('compare_3')):null;

        return view('webapp.properties.compare')->with([
            'page' => 'compare-properties',
            'sucess' => 'null',
            'compare_1' => $compare_1,
            'compare_2' => $compare_2,
            'compare_3' => $compare_3
        ]);

    }

    private function __saveAdImages($request , $ads_id) {
        
        // preprage title information aswell
        $titleSlug = str_slug($request->title, '-').'-'.str_random(10);

        for ($i=1; $i <= $request->fileNumber; $i++) {
            $var = "f".$i;

            if($request->hasFile($var)){

                $file = $request->file($var);
                $extension = $file->getClientOriginalExtension();

                //dd($request->title);
                $name = $titleSlug.'-'.$i;

                // Upload the image
                Image::make($file)
                    ->resize(900, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })
                    ->insert('assets/webapp/img/logo_etb_watermark.png', 'bottom-right', 10, 10)
                    ->save('storage/propertiesImages/'.$name.'.'.$extension);

                // Generate the thumbnail for the newly uploaded image
                Image::make('storage/propertiesImages/'.$name.'.'.$extension)
                    ->resize(255, 160)
                    ->save('storage/propertiesImages/'.$name.'-thumb.'.$extension);

                $imageModel = new UserImage();
                $imageModel->ad_id = "0";
                $imageModel->image_name = $name.'.'.$extension;
                //$imageModel->id = (UserImage::orderBy('id', 'desc')->first()) ? (UserImage::orderBy('id', 'desc')->first()->id + 1) : 1;
                $imageModel->save();

                //dd($imageModel);
                $imageLink = new Images();
                $imageLink->ads_id = $ads_id;
                $imageLink->image_id = $imageModel->id;
                $imageLink->ordering = $i;
                $imageLink->save();
            }
        }
    }
    
}
