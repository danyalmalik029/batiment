<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use App\Mail\CalculatorEmail;
use App\Models\Calculator;
use PDF;
class CalculatorController extends Controller
{
    public function index(){
        return view('calculator');
    }
    //function to store result calculation to db and save the pdf into storage
    public function store(Request $request)
    {
        $total=0;
        $total=floatval($request->livingroom*1408.57)+$total;
        $total=floatval($request->terrace*1102.34)+$total;
        $total=floatval($request->garage*1250)+$total;
        if($request->floor == "yes"){
            $total += 25000;
        }
        if($request->room > 0){
            $room=$request->room*11000;
            $total +=$room;
        }

        $room = $request->room;
        $floor = $request->floor;
        $pdfname = uniqid() .".pdf";
        $pdf = PDF::loadView('pdf.calculatorpdf',compact('total','room','floor'));
        $path = '../storage/files/'.$pdfname;

        $pdf->save($path);
        $calculator = new Calculator ;
        $calculator->cost = $total;
        $calculator->pdf = $pdfname;
        $calculator->room = $room;
        $calculator->floor = $floor;
        $calculator->save();
        $id = $calculator->id;
        $response = array(
            'id' => $id,
            );


        return response()->json($response);
    }
    //function to send email with attachment, calling calculatoremail mail-trait
    public function sendMail(Request $request, $id){
        $calculator = Calculator::find($id);
        $calculator->email = $request->email;
        if(isset($request->phone)){
        $calculator->phone = $request->phone;
        }
        $calculator->status = 1;
        $calculator->save();

        $data = [
            'cost' => $calculator->cost,
            'pdf' => $calculator->pdf
        ];
        Mail::to($request->email, 'Info ETB')
            ->send(new CalculatorEmail($data));
        $response = array(
            'message' => 'success'
            );
        return response()->json($response);

    }

    public function cancel($id){
        $calculator = Calculator::find($id);
        $path = '../storage/files/'.$calculator->pdf;
        unlink($path);
        $calculator->delete();

        return redirect('/');
    }
    public function retrieve(){
        $calculator = Calculator::all();
        return view('webapp.calculator.table')->with([
            'calculator' => $calculator,
            'title' => trans('build.build_title')
            ]);
    }
    public function delete($id){
        $calculator = Calculator::find($id);
        $path = '../storage/files/'.$calculator->pdf;
        unlink($path);
        $calculator->delete();
        return redirect()->back();
    }

    public function destroy(){
        $calculator = Calculator::where('status',0)->get();
        foreach($calculator as $c){
            $path = '../storage/files/'.$c->pdf;
            unlink($path);
        }
        Calculator::where('status',0)->delete();
        return redirect()->back();
    }
}
