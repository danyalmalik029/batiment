<?php

namespace App\Http\Controllers\Auth;

use App;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Translation\Translator;
use Socialite;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/userprofile/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);

        $this->middleware(function ($request, $next) {

            if(session()->has( 'language' )) {
                App::setLocale(session()->get( 'language' ));
            } else {
                $locale = $this->__get_client_language();
                App::setLocale($locale);
                session()->put('language', $locale);
            }

            return $next($request);
        });
    }

    // Get the browser's language
    private function __get_client_language($default = 'fr')
    {

        $availableLanguages = array('fr', 'en');

        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            $langs = explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);

            foreach ($langs as $value) {
                $choice = substr($value, 0, 2);
                if (in_array($choice, $availableLanguages)) {
                    return $choice;
                }
            }
        }

        // If the language is not in the available list, use english as default
        return $default;
    }


    
    /**-------------GESTION DE LA CONNEXION AVEC FACEBOOK--------------- */

     /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */

    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */

    public function handleFacebookCallback()
    {
        $providerUser = Socialite::driver('facebook')->user();
      
        //Si j'ai déjà le provider_id dans la base de donnée
        //je connecte directement l'utilisateur
        
        $user = $this->checkIfRefererFExists($providerUser);
       
                if($user){
                    Auth::guard()->login($user, true);
                    return redirect('/');
                }

               
                
        //Je vérifie si j'ai un email
        if($providerUser->email !== null){
            //Je rajoute le provider_id a l'utilisateur dont le mail
            //correspond et je redirige vers la page appelé
            $user = User::where('email', $providerUser->email)
                        ->where('auth_referer', 'NULL')
                        ->first();
                        
            if($user){
                
                $user->auth_referer='facebook';
                $user->save();
                Auth::guard()->login($user, true); // true pour garder l'utilisateur connecté ( remember me )
                return redirect('/');
            }
            
            $user = User::where('email', $providerUser->email)
                    ->where('auth_referer','<>', 'facebook')
                    ->first();
                    
           
            if($user !== null)
            {
                if($user->auth_referer==='google')
                {
                    Session::flash('error',trans('profile.facebook_error'));
                    return redirect('/login');
                }

            }
        }

        
        //Je crée l'utilisateur si j'arrive jusque là ;)
        $user = User::create([
            'name' => $providerUser->name,
            'email' => $providerUser->email,
            'password'=>$providerUser->token,
            'auth_referer' =>'facebook',
        ]);
            
        if($user){
            Auth::guard()->login($user, true);
            return redirect('/');
        }
    }  



     /**-------------GESTION DE LA CONNEXION AVEC GOOGLE--------------- */

          /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleGoogleCallback()
    {
        $providerUser = Socialite::driver('google')->user();

      
        //Si j'ai déjà le provider_id dans la base de donnée
        //je connecte directement l'utilisateur
        
        $user = $this->checkIfRefererGExists($providerUser);
        
                if($user){
                    Auth::guard()->login($user, true);
                    return redirect('/');
                }


                
        //Je vérifie si j'ai un email
        if($providerUser->email !== null){
            //Je rajoute le provider_id a l'utilisateur dont le mail
            //correspond et je redirige vers la page appelé
            $user = User::where('email', $providerUser->email)
                        ->where('auth_referer', 'NULL')
                        ->first();
            if($user){
                $user->referer='google';
                $user->save();
                Auth::guard()->login($user, true); // true pour garder l'utilisateur connecté ( remember me )
                return redirect('/');
            }

            $user = User::where('email', $providerUser->email)
                ->where('auth_referer','<>', 'google')
                ->first();


            if($user !== null)
            {
                if($user->auth_referer==='facebook')
                {
                    Session::flash('error',trans('profile.google_error'));
                    return redirect('/login');
                }
            }
        }


        //Je crée l'utilisateur si j'arrive jusque là ;)
       
        $user = User::create([
            'auth_referer' =>'google',
            'name' => $providerUser->name,
            'email' => $providerUser->email,
            'password'=>$providerUser->token,
            
        ]);

            
        if($user){
            Auth::guard()->login($user, true);
            return redirect('/');
        }
    }


    /**
     * @param $provider
     * @param $providerId
     * @return mixed
     * Fonction qui vérifie si l'utilisateur à déjà un identifiant
     * venant d'un réseau social
     */
    public function checkIfRefererFExists($provider){
            
        $user = User::where('auth_referer', 'facebook')
                ->where('email', $provider->email)
                ->first();
            
        return $user;
            
    }

          /**
     * @param $provider
     * @param $providerId
     * @return mixed
     * Fonction qui vérifie si l'utilisateur à déjà un identifiant
     * venant d'un réseau social
     */
    public function checkIfRefererGExists($provider){
        
    $user = User::where('auth_referer', 'google')
            ->where('email', $provider->email)
            ->first();
        
    return $user;
        
    }


}
