<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\UserProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Image;
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeEmail;

class UserController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->middleware('auth');
        $profile=new UserProfile();
        // Prepare the countries iso list for the form
        $countriesQuery = file_get_contents("./assets/data/countries.json");
        $data['countries'] = json_decode($countriesQuery, true);
        $data['countriesPrefixes'] = explode('-', $profile['country']);


        $user = User::where('id', Auth::id())->first();

        $profileQuery = Userprofile::where('iduser', Auth::id())->first();
        
        if($profileQuery !== null) {
            $profile = $profileQuery;
        } else {

            // This is the first time a user logs in, send a welcome message
            $user_profile = new UserProfile();
            $user_profile->iduser = Auth::id();
            $user_profile->phone_1 = '';
            $user_profile->phone_2 = '';
            $user_profile->gender = '';
            $user_profile->birth_date = '1970-01-01';
            $user_profile->website_url = '';
            $user_profile->about_me = '';
            $user_profile->language = '';
            $user_profile->address = '';
            $user_profile->city = '';
            $user_profile->region = '';
            $user_profile->country = '';
            $user_profile->postcode = '';
            $user_profile->save();

            $this->__greatUser();
        }

        $data['profile']=$profile;
        $data['user']=$user;


        // ajout des données de l'utilisateur dans la session
        
        session(['name'=>$user->name]);
        session(['email'=>$user->email]);
        session(['phone_1'=>$profile->phone_1]);
        session(['phone_2'=>$profile->phone_2]);
        session(['gender'=>$profile->gender]);

        return view('home',$data);

    }

    public function agents()
    {
        $data = [];
        $data['pageTitle'] = trans('profile.agents_title');
        $data['agents'] = [];

        // Collect all agencies
        $agenciesQuery = User::where('franchise', 1)->get();

        // For every agency find agents
        foreach ($agenciesQuery as $agency)
        {
            // Get all users in the agency
            $agentsQuery = User::where('referer_id', $agency->id)->get();

            if($agentsQuery !== null) {
                $data['agents'][$agency->id]['agency'] = $agency->toArray();
                $data['agents'][$agency->id]['agents'] = $agentsQuery->toArray();
            }
        }

        return view('webapp.agents', $data);
    }


    public function agentsdetail($id)
    {
        $data = [];
        $data['pageTitle'] = "Detail Agents";
        $data['agents'] = [];

        $data['agents']=  User::where('id', $id)->first();

        return view('webapp.agents-detail', $data);
    }

    public function agencies()
    {
        $data = [];
        $data['pageTitle'] = trans('profile.agencies_title');

        // Get all agencies
        $data['agencies'] = User::where('franchise', 1)->get();

        return view('webapp.agencices', $data);
    }


    public function agencyAgents($id)
    {
        $data = [];

        $data['agents'] = [];

        // Collect all agencies
        $agenciesQuery = User::where('id', $id)->first();

        $data['pageTitle'] = trans('profile.agents_of_title') . " " . $agenciesQuery->name;

            $agentsQuery = User::where('referer_id', $id)->get();

            if($agentsQuery !== null) {

                $data['agents'] = $agentsQuery;

            }

        return view('webapp.agents_agency', $data);

    }

    public function updateprofile(Request $request)
    {

        // UPDATE USER
        $user = User::where('id',Auth::id())->first();

        if($request->password!==null)
        {              
            $this->validate($request, [
            'password'=>'required|min:6|confirmed',
            'password_confirmation'=>'required|min:6',
            ]);


            $user->update([           
                'name'=>$request->name,
                'password'=>Hash::make($request->password)
            ]);

        }

        // Upload profile picture if one
        if ($request->hasFile('avatar')) {

            $file = $request->file('avatar');

            $extension = $file->getClientOriginalExtension();

            $path = $this->generateRandomString(22) . '.'.$extension;

            Image::make($file)
                ->resize(450, null, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->save('storage/'.$path);

            $user->avatar = $path ;
            $user->name = $request->name;

            $user->update();

        } else {
            $user->name = $request->name;
            $user->update();
        }

        // UPDATE USER_PROFILE
        $user_profile = UserProfile::where('iduser', Auth::id())->first();

        $birth_date = date_create($request->birth_date);
        $birth_date = date_format($birth_date,"Y-m-d H:i:s");

        if($user_profile !== null) {
            $user_profile->phone_1 = $request->phone_1;
            $user_profile->phone_2 = $request->phone_2;
            $user_profile->gender = $request->gender;
            $user_profile->birth_date = $birth_date;
            $user_profile->website_url = $request->website_url;
            $user_profile->about_me = $request->about_me;
            $user_profile->language = $request->language;
            $user_profile->address = $request->address;
            $user_profile->city = $request->city;
            $user_profile->region = $request->region;
            $user_profile->country = $request->country;
            $user_profile->postcode = $request->postcode;

            $user_profile->update();
        } else {
            $user_profile = new UserProfile();

            $user_profile->iduser = Auth::id();
            $user_profile->phone_1 = $request->phone_1;
            $user_profile->phone_2 = $request->phone_2;
            $user_profile->gender = $request->gender;
            $user_profile->birth_date = $birth_date;
            $user_profile->website_url = $request->website_url;
            $user_profile->about_me = $request->about_me;
            $user_profile->language = $request->language;
            $user_profile->address = $request->address;
            $user_profile->city = $request->city;
            $user_profile->region = $request->region;
            $user_profile->country = $request->country;
            $user_profile->postcode = $request->postcode;

            $user_profile->save();
        }


        return redirect('/userprofile')->with('message', trans('profile.update_success'));

    }

    public function createprofile(Request $request)
    {
        
        $this->validate($request, [
            
        'poste_code'=>'numeric',
        'username'=>'required',]);

      
        $user_profil= new UserProfile();   

        if($request->password!==null)
        {              
            $this->validate($request, [
            'password'=>'required|min:6',
            'retry_password'=>'required|min:6',]);

             // UPDATE USER
            $user = User::where('id',Auth::id());
            $user->update([           
                'name'=>$request->name,
                'username'=>$request->username,
                'password'=>$request->password]);

        }
        else
        {          
            // UPDATE USER
            $user = User::where('id',Auth::id());
            $user->update([           
                'name'=>$request->name,
                'username'=>$request->username]);
    
        }

        //user_profile éléments
        $user_profil->iduser=Auth::id();
        $user_profil->phone_1 = $request->phone_1;
        $user_profil->phone_2 = $request->phone_2;
        $user_profil->gender = $request->gender;
        $user_profil->birth_date = $request->birth_date;
        $user_profil->website_url = $request->website_url;
        $user_profil->about_me = $request->about_me;
        $user_profil->language = $request->language;
        $user_profil->address = $request->address;
        $user_profil->city = $request->city;
        $user_profil->region = $request->region;
        $user_profil->country = $request->country;
        $user_profil->postcode = $request->postcode;
        $user_profil->save();
        return redirect('/');
    }

    private function __greatUser() {

        $user = User::where('id', Auth::id())->first();

        $data = [
            'user_email' => $user->email,
            'user_name' => $user->name
        ];

        Mail::to($user->email, 'ETB Batiment: ' . $user->name)
            ->send(new WelcomeEmail($data));
    }

}
