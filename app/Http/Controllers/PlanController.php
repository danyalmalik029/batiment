<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;

use App\User;
use App\Models\Users;
use App\Models\Plan;
use App\Models\Files;
use App\Models\UserProfile;
use App\Models\PlanMessage;
use App\Models\SubmissionPlanMessage;

use Validator;
use Auth;
use Carbon\Carbon;
use Plupload;
use File;
use Image;
use Storage;

class PlanController extends Controller
{
    /**
	 * Create a new authentication controller instance.
	 *
	 * @return void
	 */
	public function __construct() 
	{
	    parent::__construct();
		$this->middleware('auth', ['except' => ['viewAllPlans', 'details', 'uploadForm', 'uploadZipForm']]);
	}

	/**
     * Create plan form
     *
     * @return \Illuminate\Http\Response
     */
    public function createPlan(Request $request, $id = 0)
    {

        $data = [
            'page' => 'submit-plan',
            'sucess' => 'null',
            'editing' => false,
            'planImages' => []
        ];

        $planId = intval($id);

        // If asked for image deletion, then proceed it
        if($request->action) {
            if($request->action === 'deleteimage' ) {

                // Delete file in filesystem aswell
                $fileQuery = Files::where('id', $request->image_id)
                    ->first();
                $file = base_path("public/storage" . $fileQuery->path . $fileQuery->nom);
                $file2 = base_path($fileQuery->path . $fileQuery->nom);

                if (is_file($file)) {
                    unlink($file);
                } elseif(is_file($file2)) {
                    unlink($file2);
                }
                Files::where('id', $request->image_id)
                        ->delete();
            }
        }

        if($planId) {
            $plan = Plan::where('id',$planId)->first();
            if($plan !== null) {
                $data['editing'] = true;
                $data['plan'] = $plan;

                // Find images related to this property
                $imagesQuery = Files::where('plan_id',$planId)
                                ->get();

                $planImages = ($imagesQuery !== null)?$imagesQuery->toArray():[];

                $data['planImages'] = $planImages;

            } elseif(($plan === null) && ($planId > 0)) {
                // Requestig for an unexisting property, bad ID
                // Initialize with empty values
                return redirect('view-all-properties');
            }
        } else {
            $data['plan'] = new Plan();
        }

        return view('webapp.plan.createForm')->with($data);

    }

    /**
     * Create properties form
     *
     * @return \Illuminate\Http\Response
     */
    public function listMessages($plan_id)
    {
        $ads = Plan::where('id', $plan_id)
            ->where('user_id', Auth::id())
            ->first();

        if($ads === null)
            return redirect('view-all-properties');

        $messages = SubmissionPlanMessage::where('plan_id', $plan_id)
            ->join('users', 'users.id', 'plan_submission_messages.sender_id')
            ->select(
                'plan_submission_messages.*',
                'users.name as senderName',
                'users.email as senderEmail'
            )
            ->orderBy('id', 'des')->get();

        //dd($messages);
        return view('webapp.properties.messages')->with([
            'page' => 'view-properties',
            'sucess' => 'null',
            'ads' => $ads,
            'messages_type' => 'plan',
            'messages' => $messages
        ]);
    }



    /**
     * Save plan form
     *
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {

    	//dd($request->cleaning);
    	//$regex = "/^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/";

    	$validator = Validator::make($request->all(), [
    		'title' => 'required',
			'description' => 'required'
        ]);

        if ($validator->fails()) {

        	//dd(count($validator->errors()->all()));
           return redirect()->back()
                        ->withErrors($validator->errors())
                        ->withInput();
        }

        $plan = new Plan();

        $plan->title =  $request->title;
        $plan->description =  $request->description;
        $plan->slug =  str_slug($request->title, '-');
        $plan->user_id =  Auth::id();
        
        $plan->save();

        //Storage::disk('plans')->makeDirectory(str_slug($plan->slug, '-'));

        $this->__saveAdImages($request, $plan->id);

        return redirect("view-all-plans");
    }

    /**
     * List plans form
     *
     * @return \Illuminate\Http\Response
     */
    public function myPlans()
    {
        //dd(Auth::id());
        $plans = Plan::where('user_id', Auth::id())
            ->where('statut', '<>', '2')
            ->orderBy('id', 'DESC')
            ->paginate(\Config::get('settings.listLength'));

        $user = null;
        if(Auth::check()){
            $user = UserProfile::where('iduser', Auth::id())->first();
        }

        //dd($plans);
        return view('webapp.plan.viewAll')->with([
            'page' => 'my-plans',
            'sucess' => 'null',
            'error' => 'null',
            'plans' => $plans,
            'userProfile' => $user,
            'title' => trans('general.menu_my_plans')
        ]);
    }

    public function viewAllPlans()
    {
        $plans = Plan::where('statut', '1')
            ->orderBy('id', 'DESC')
            ->paginate(\Config::get('settings.listLenght'));

        $user = null;
        if(Auth::check()){
            $user = UserProfile::where('iduser', Auth::id())->first();
        }

        //dd(Auth::User()->ads);

        return view('webapp.plan.viewAll')->with([
            'page' => 'view-all-plans',
            'sucess' => 'null',
            'error' => 'null',
            'plans' => $plans,
            'userProfile' => $user,
            'title' => trans('plan.view_all_plans')
        ]);
    }

    /**
     * Create properties form
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $regex = "/^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/";

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
        ]);

        if ($validator->fails()) {

            //dd(count($validator->errors()->all()));
           return redirect()->back()
                        ->withErrors($validator->errors())
                        ->withInput();
        }

        //dd($request->all());
        $plan = Plan::find($request->plan);

        $plan->title =  $request->title;
        $plan->slug =  str_slug($request->title, '-');
        $plan->description =  $request->description;
        
        $plan->updated_at = Carbon::now();
        $plan->update();

        $this->__saveAdImages($request, $plan->id);

        return redirect("view-all-plans");
    }

    /**
     * Edit Plan form
     *
     * @return \Illuminate\Http\Response
     */
    public function details($id)
    {
        $plan = Plan::find($id);

        $var = Files::where('plan_id', $id)
                ->where('type', '<>', 'cad')
                ->where('type', '<>', 'CAD')
                ->get();

        $list = array();
        $i = 1;
        foreach ($var as $key ) {
            $list[$i] = $key->nom;
            $i++;
        }
        
        $userList = array();
        $messages = array();
        
        if(Auth::check())
            $messages = SubmissionPlanMessage::where('plan_id', $id)->where('receiver_id', Auth::id())->get();
        else
            $messages = array();
            
        $i = 1;
        foreach ($messages as $key) {
            $user = Users::find($key->sender_id);
            if(!in_array($user, $userList)){
                $userList[$i] = $user;
                $i++;
            }
        }

        //dd($userList);
        return view('webapp.plan.details')->with([
            'page' => 'view-plans',
            'sucess' => 'null',
            'plan' => $plan,
            'owner' => User::where('id', $plan->user_id)->first(),
            'list' => $list,
            'pagetitle' => "ETB Batiment, " . $plan->title,
            'userList' => $userList
        ]);
    }

    /**
     * Create properties form
     *
     * @return \Illuminate\Http\Response
     */
    public function sendMessage(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'subject' => 'required',
            'message' => 'required'
        ]);

        if ($validator->fails()) {

            //dd(count($validator->errors()->all()));
           return redirect()->back()
                        ->withErrors($validator->errors())
                        ->withInput();
        }

        //dd($request->all());
        $plan = Plan::find($request->plan);

        if(Auth::id() != $plan->user_id){
            $message = new SubmissionPlanMessage();
            $message->sender_id = Auth::id();
            $message->subject = $request->subject;
            $message->message_body = $request->message;
            $message->receiver_id = $plan->user_id;
            $message->plan_id = $plan->id;
        }else{
            $message = new SubmissionMessage ();
            $message->sender_id = $plan->user_id;
            $message->subject = $request->subject;
            $message->message_body = $request->message;
            $message->receiver_id = Auth::id();
            $message->plan_id = $plan->id;
        } 

        $message->save(); 

        $var = Files::where('plan_id', $request->plan)
                ->where('type', '<>', 'cad')
                ->where('type', '<>', 'CAD')
                ->get();

        $list = array();
        $i = 1;
        foreach ($var as $key ) {
            $list[$i] = $key->nom;
            $i++;
        }
        
        $userList = array();
        $messages = array();

        $messages = SubmissionPlanMessage::where('plan_id', $request->plan)->where('receiver_id', Auth::id())->get();
        
        $i = 1;
        foreach ($messages as $key) {
            $user = Users::find($key->sender_id);
            if(!in_array($user, $userList)){
                $userList[$i] = $user;
                $i++;
            }
        }

//        return view('webapp.plan.details')->with([
//            'page' => 'view-properties',
//            'sucess' => 'Your message have been send to the owner.',
//            'plan' => $plan,
//            'list' => $list,
//            'userList' => $userList
//        ]);

        return redirect('view-details-plan/' . $plan->id) -> with('sucess', 'Your message have been send to the owner.');
    }

    /**
     * Create properties form
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadForm($id)
    {
        // Get the plan details
        $planQuery = Plan::where('id', $id)
            ->where('user_id', Auth::id())
            ->where('statut','=','1')
            ->first();

        // Make sure the current user owns the plan
        if($planQuery === null)
            return redirect('my-plans');

        return view('webapp.plan.uploadForm')->with([
            'page' => 'upload-image-form',
            'sucess' => 'null',
            'plan' => $planQuery,
            'user' => Auth::User()
        ]);
    }

    /**
     * Create properties form
     *
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
        $plan = Plan::find($request->plan);

        // Save source files for the plan

        for ($i=1; $i <= $request->fileNumber; $i++) {
            $var = "f".$i;
            //dd('ici');
            if($request->hasFile($var)){

                $file = $request->file($var);
                $extension = $file->getClientOriginalExtension();

                //dd($file);
                $name = 'plansource-' . $request->plan.'-' . $i . '-' . str_random(10);

                $file->move('../storage/planFiles/', $name.'.'.$extension);

                // Store the same in the database
                $file1 = new Files();
                $file1->nom = $name.'.'.$extension;
                $file1->type = $extension;
                $file1->path = '/storage/planFiles/';
                $file1->plan_id = $request->plan;
                $file1->statut = "1";

                $file1->save();


            }
        }

        return redirect("edit-plan/".$request->plan);
        
    }

    public function readSourceFile($file_name)
    {

        $user_id = Auth::id();

        // Get the current authenticaed user
        $user=  User::where('id', $user_id)->first();

        // If the user is not autenticated
        if($user === null) {
            return redirect('/view-all-plans');
        }

        $file_path = base_path('/storage/planFiles/' . $file_name);

        if (!is_file($file_path)) {

            // File not found with this name
            return redirect('/view-all-plans');
        }

        if (false) {

            // Also check if the iser has the right to view the file, if not, redirect him
            return redirect('/view-all-plans');
        }

        // Get gile Details from the database
        $fileQuery = Files::where('nom', $file_name)->first();

        if ($fileQuery === null) {

            // The file does not exists anymore in the database
            return redirect('/view-all-plans');
        }

        $extension = $fileQuery->type;

        switch ($extension) {
            case 'pdf':
                $type = 'application/pdf';
                break;
            case 'png':
                $type = 'image/*';
                break;
            case 'jpg':
                $type = 'image/*';
                break;
            case 'png':
                $type = 'image/*';
                break;
            case 'jpeg':
                $type = 'image/*';
                break;
            case 'cad':
                $type = 'application/*';
                break;
            case 'CAD':
                $type = 'application/*';
                break;
            default:
                $type = '*';
                break;
        }

        $file = File::get($file_path);
        $response = Response::make($file);
        $response->header('Content-Type', $type);

        return $response;
    }

    /**
     * Create properties form
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadZipForm()
    {
        $plans = Plan::where('user_id', Auth::id())->get();
        //dd($plans);
        return view('webapp.plan.zip')->with([
            'page' => 'plans-zip',
            'sucess' => 'null',
            'plans' => $plans,
            'user' => Auth::User()
        ]);
    }

    /**
     * Create properties form
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadZip(Request $request)
    {
        //dd($request->plan);
        $plan = Plan::find($request->plan);
        //dd($plan);
        $request->request->add(['title', $plan->title]);

        $this->__saveAdImages($request, $request->plan);

        return redirect("edit-plan/".$request->plan);
        
    }

    private function __saveAdImages($request , $plan_id)
    {

        // preprage title information aswell
        $titleSlug = str_slug($request->title, '-').'-'.str_random(10);

        $plan = Plan::find($plan_id);
        //dd($request->fileNumber);
        for ($i=1; $i <= $request->fileNumber; $i++) {
            $var = "f".$i;
            //dd('ici');
            if($request->hasFile($var)){

                $file = $request->file($var);
                $extension = $file->getClientOriginalExtension();

                //dd($file);
                $name = $titleSlug.'-'.$i;

                //check if it's the CAD file
                if (($extension != "CAD") && ($extension != "cad") && ($extension != "zip") && ($extension != "rar")) {

                    $path1 = 'storage/planImages/'.$name.'-real.'.$extension;
                    $path2 = 'storage/planImages/'.$name.'-thumb.'.$extension;

                    //dd($path2);
                    // Upload the image
                    Image::make($file)
                        ->resize(900, null, function ($constraint) {
                            $constraint->aspectRatio();
                        })
                        ->insert('assets/webapp/img/logo_etb_watermark.png', 'bottom-right', 10, 10)
                        ->save('storage/planImages/'.$name.'.'.$extension);

                    $file1 = new Files();
                    $file1->nom = $name.'.'.$extension;
                    $file1->type = $extension;
                    $file1->path = '/planImages/';
                    $file1->plan_id = $plan_id;
                    $file1->statut = "1";

                    $file1->save();

                    // Generate the thumbnail for the newly uploaded image
                    Image::make('storage/planImages/'.$name.'.'.$extension)
                        ->resize(255, null, function ($constraint) {
                            $constraint->aspectRatio();
                        })
                        ->save($path2);

                }else{

                    //dd('ici');
                    $file->move(
                        base_path() . '/public/storage/planImages/'.$name.'.'.$extension
                    );

                    $file1 = new Files();
                    $file1->nom = $name.'.'.$extension;
                    $file1->type = $extension;
                    $file1->path = '/planImages/';
                    $file1->plan_id = $plan_id;
                    $file1->statut = "1";

                    $file1->save();
                }
            }
        }
    }

}
