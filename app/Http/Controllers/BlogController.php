<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Blog;


class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $perpage = \Config::get('settings.listLenght');

        $userQuery = User::where('id', Auth::id())->first();

        if($userQuery === null)
            $userQuery = new User();


        if( ($userQuery->role_id == 1) || ($userQuery->role_id == 3) ) {
            // Query all articles for admin or manager
            $data['articles'] = Blog::orderBy('posts.id', 'desc')
                ->join('categories', 'categories.id', 'posts.category_id')
                ->join('users', 'users.id', 'posts.author_id')
                ->where('categories.slug', 'blog')
                ->select('posts.*', 'users.name as userName', 'users.avatar as userProfil')
                ->paginate($perpage);
        } else {
            // For others, query only published articles
            $data['articles'] = Blog::orderBy('posts.id', 'desc')
                ->join('categories', 'categories.id', 'posts.category_id')
                ->join('users', 'users.id', 'posts.author_id')
                ->where('categories.slug', 'blog')
                ->where('posts.status', 'PUBLISHED')
                ->select('posts.*', 'users.name as userName', 'users.avatar as userProfil')
                ->paginate($perpage);
        }

        $data['pageTitle'] = 'Blog';
        $data['slug'] = 'blog';

        return view('webapp.blog.blog-etb', $data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function guide()
    {
        //

        $perpage = \Config::get('settings.listLenght');
/*
        $data['articles'] = Blog::orderBy('id', 'desc')
            ->paginate($perpage);
*/
        $data['articles'] = Blog::orderBy('posts.id', 'desc')
            ->join('categories', 'categories.id', 'posts.category_id')
            ->join('users', 'users.id', 'posts.author_id')
            ->where('categories.slug', 'guide')
            ->select('posts.*', 'users.name as userName')
            ->paginate($perpage);

        $data['pageTitle'] = 'Guide';
        $data['slug'] = 'guide';

        return view('webapp.blog.blog-etb', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        //

        $details = [];
        $detailsQuery = Blog::where('slug', $slug)
            ->first();

        if($detailsQuery !== null)
            $details = $detailsQuery->toArray();

        //  Recupération du role du user

        $user_id = Auth::id();

        if(isset($user_id)){

            $data['user'] = DB::table('users')->where('id', $user_id)->first();

        }

        $data['details'] = $details;

        return view('webapp.blog.blog-detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
