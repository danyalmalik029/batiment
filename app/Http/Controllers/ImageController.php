<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Models\Image;
use Config;
use Auth;
use Carbon\Carbon;
use App\Models\ShirtGroupImage;

class ImageController extends Controller
{
    protected $image;



    public function getUpload()
    {
        return view('admin.dashboard.setting');
    }

    public function upload(Request $request)
    {
        $input = $request->file('file');
        $bytes = $input->getSize();
        $fileName = Carbon::now()->timestamp .uniqid(). '.jpg';
        $destinationPath=public_path('/uploads/');
        $image['original_name'] = $input->getClientOriginalName();
        $image['path']  = 'uploads/' . $fileName;
        $image['filesize'] = $this->formatSizeUnits($bytes); 
        $image['mimetype'] = $input->getMimeType(); 
        $input->move($destinationPath, $fileName);
        
        $upload_type =  $request->only('upload_type');
        if($upload_type['upload_type']=='shirt'){
            $shirt_group_id =  $request->only('shirt_group_id');
            $image['shirt_group_id']  = !empty($shirt_group_id['shirt_group_id']) ? $shirt_group_id['shirt_group_id'] : 0 ;
            $id=ShirtGroupImage::create($image)->id;
        }elseif($upload_type['upload_type']=='user'){
            
        }else{
           
        }
       

        return $id;

    }

    public function delete(Request $request)
    {
        $input = $request->all();
        $deleteImageID= $request->input('deleteImageID');
        $deleteResult=ShirtGroupImage::find($deleteImageID)->delete();
//        if($type=='shirt'){
//            DealImages::where('file_path_delete',$s3_path)->delete();
//        }else{
//            Image::where('file_path_delete', $s3_path)->delete();
//        }
        if($deleteResult){
            $var['status']='success';
        }else{
            $var['status']='fail';
        }
        return $var;

    }
    public function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
    }
    public function postUploadEditManager(Request $request)
    {
    
        $photo = $request->all();
        $image  =  $photo['file'];
        $userID =  $request->only('user_id');

        $originalName = $image->getClientOriginalName();

        $fileSize = $image->getSize();
        // return $fileSize;

        $imageFileName = uniqid() . '.jpg';

        $filePath = '/uploads/' . $imageFileName;
        
        $filesystem->put($filePath, file_get_contents($image), 'public');

        $filePathFull = Config::get('aws.s3_path').$filePath;

        // $read = $filesystem->read($filePath);

        $image = new Image;
        $image->filename      = $imageFileName;
        $image->original_name = $originalName;
        $image->user_id  = $userID;
        $image->file_path  = $filePathFull;
        $image->file_path_delete = $filePath;
        $image->file_size = $fileSize;
        $image->save();


        return $filePathFull;

    }
    
}