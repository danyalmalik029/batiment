<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BreadcrumbController extends Controller
{
    public static function render($crumbs)
    {
        return self::renderCrumbs($crumbs);
    }

    private static function crumbToUrl($crumb)
    {
        $title = str_replace( ' ', '-', $crumb);
        return strtolower($title);
    }

    private static function renderCrumbs($crumbs)
    {
        $currentPage = array_pop($crumbs);

        $html = '<ol class="breadcrumb p-0">';
        $html .= '<li><a href="'.route('dashboard').'">Dashbaord</a></li>';
        foreach ($crumbs as $index=>$crumb)
        {
            $html .= '<li><a href="'.self::crumbToUrl($crumb['url']).'">'.$crumb['title'].'</a></li>';
        }
        $html .= '<li class="active">'.$currentPage['title'].'</li></ol>';

        return $html;
    }
}