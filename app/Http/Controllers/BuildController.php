<?php

namespace App\Http\Controllers;

use App\Mail\BuildNowEmail;
use App\Models\Ads;
use App\Models\Category;
use App\Models\Projects;
use App\Models\Categories;
use App\Models\City;
use App\Models\Images;
use App\Models\Plan;
use App\Models\Files;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Auth;
use App\User;
use Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Storage;
class BuildController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth', [
            'except' => []]);
    }

    public function index(){
        return view('webapp.build.index')->with([
            'title' => trans('build.build_title')
            ]);
    }


    public function build_now(){
        return view('webapp.build.build_now')
                ->with(['title' =>  trans('build.build_now_title'),
                        'text_1'  =>  trans('build.build_now_text_1'),
                        'text_2'  =>  trans('build.build_now_text_2'),
                        'text_3'  =>  trans('build.build_now_text_3'),
                        'button_text'   => trans('build.lets_go_button')
                    ]);
    }

    /*
    Retrieve data to the table
    */
    public function retrieve(){
        $projects = Projects::all();
        return view('webapp.build.table')->with([
            'projects' => $projects,
            'title' => trans('build.build_title')
            ]);
    }

    /*
    Download Helper
    */

    public function download($id){
        $path = 'storage/files/'.$id;
        return response()->download($path);
    }
    /* 
    Sending logic to the build-form view,
    consist of eloquent for Terrains
    */ 
    public function show(Request $request){


        if($request->ajax() && $request->land==true)
        {
                $category   =   Categories::where('category_slug',$request->category)->first();
                $lands = Ads::where('statut',1)
                    ->where('catid', $category->id)
                    ->where('ad_dtype',$request->type)
                    ->when(!empty($request->area) , function ($query) use($request){
                        return $query->where('area',$request->area);
                    })
                    ->when(!empty($request->price_to) , function ($query) use($request){
                        return $query->whereBetween('price',[$request->price_from, $request->price_to]);
                    })
                    ->when(!empty($request->lat) , function ($query) use($request){
                        return $query->where(['address_lat'=>$request->lat,'address_lng'=>$request->long]);
                    })
                    ->has('images')->paginate(6);



            return view('webapp.build.ajax_lands')->with([
                'title' => trans('banking.banking_title'),
                'lands' => $lands
            ]);
        }
        if($request->ajax() && $request->plan==true)
        {
            $plans = Ads::where('statut',1)
                ->where('catid', 7)
                ->where('ad_dtype','buy')
                ->when(!empty($request->area) , function ($query) use($request){
                    return $query->where('area',$request->area);
                })
                ->when(!empty($request->price_to) , function ($query) use($request){
                    return $query->whereBetween('price',[$request->price_from, $request->price_to]);
                })
                ->has('images')->paginate(6);
            return view('webapp.build.ajax_plan')->with([
                'title' => trans('banking.banking_title'),
                'plans' => $plans
            ]);
        }
        $plans = Ads::where('statut',1)
            ->where('catid',7)
            ->where('ad_dtype','buy')
            ->has('images')
            ->paginate(6);
        $lands = Ads::where('statut',1)
            ->where('catid',4)
            ->has('images')
            ->paginate(6);
        return view('webapp.build.buildform')->with([
            'title' => trans('banking.banking_title'),
            'lands' => $lands,
            'plans' => $plans,
            'categories' => Categories::where('id','!=',7)->get(),
            ]);
    }

    /*
    Store value from view to database
    */

    public function store(Request $request){

        $project = new Projects();
        $project->user_id = Auth::user()->id;
        $project->construction_type = $request->house;
        if($request->askland == "yes") {
           $project->land_id = 0 ;
           $image1 = $request->file('land_permission');

           $ext1=$image1->getClientOriginalExtension();

           $filename1=uniqid().'.'.$ext1;

            // $imagepath1='images/datasheets/'.Auth::id();

           $imagepath1='../storage/files/';

           $uploadsuccesimage1=$image1->move($imagepath1, $filename1);
           $project->land_permission = $filename1;
        }
        else {
            $project->land_id = $request->land;
        } 
        if($request->askplan =="yes"){
            $project->plan_id = 0;
            $image1 = $request->file('plan_permission');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            // $imagepath1='images/datasheets/'.Auth::id();

            $imagepath1='../storage/files/';

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);
            $project->plan_permission = $filename1;
        }
        else {
            $project->plan_id =$request->plan ;
        }
        $project->budget = $request->budget;
        if($request->area <> 0){
        $project->area = $request->area ;
        }

        if($request->permit == "yes"){
            $image1 = $request->file('permit_file');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            // $imagepath1='images/datasheets/'.Auth::id();

            $imagepath1='../storage/files/';

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);
            $project->building_permit = $filename1;

        }
        else {
            $project->building_permit = 0;
        }
//        if($request->askincome =="yes"){
//            $project->income = $request->income;
//        }
//        else {
            $project->income = $request->income;
//        }
        if($request->summary=="yes")
        {
            $project->down_payment = $request->down_payment;
        }
        else {
            $project->down_payment = 0;
        }
        $project->contract=$request->contract_text;
        $project->save();
        $amount=$project->income;
        Mail::to(Auth::user()->email, 'ETB Batiment: ')
            ->send(new BuildNowEmail($amount));
        $response = array(
            'message' => 'success'
            );
        return response()->json($response);

    }

}
