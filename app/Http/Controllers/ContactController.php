<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactEmail;

use Validator;

class ContactController extends Controller
{
    public function show()
    {

        $data = [];

        return view('webapp/contact', $data);
    }


    public function sendMessage(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'your_email' => 'required|email',
            'your_name' => 'required',
            'phone' => 'required',
            'city' => 'required',
            'subject' => 'required',
            'mymessage' => 'required',
        ]);

        if ($validator->fails()) {

            return redirect()->back()
                ->withErrors($validator->errors())
                ->withInput();
        }

        $data = [
            'your_email' => $request->input('your_email'),
            'your_name' => $request->input('your_name'),
            'phone' => $request->input('phone'),
            'city' => $request->input('city'),
            'subject' => $request->input('subject'),
            'mymessage' => $request->input('mymessage')
        ];

        $moreUsers = [
            'eric.palmiste@hotmail.fr'
        ];

        $evenMoreUsers = [
            'hdouanla.ei@gmail.com'
        ];

        Mail::to('info@etb-batiment.com', 'Info ETB')
            ->cc($moreUsers)
            ->bcc($evenMoreUsers)
            ->send(new ContactEmail($data));

        return redirect()->back()->with('message', trans('contact.message_sent'));
    }
}
