<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Instantiate a new new controller instance.
     * Using the constructor to inititialize whatever needed
     *
     * @return void
     */
    public function __construct()
    {
        // If the language is not in the session then we should set one here

        $this->middleware(function ($request, $next) {

            if(session()->has( 'language' )) {
                App::setLocale(session()->get( 'language' ));
            } else {
                $locale = $this->__get_client_language();
                App::setLocale($locale);
                session()->put('language', $locale);
            }

            return $next($request);
        });


    }

    // Get the browser's language
    private function __get_client_language($default = 'fr')
    {

        $availableLanguages = array('fr', 'en');

        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            $langs = explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);

            foreach ($langs as $value) {
                $choice = substr($value, 0, 2);
                if (in_array($choice, $availableLanguages)) {
                    return $choice;
                }
            }
        }

        // If the language is not in the available list, use english as default
        return $default;
    }

    /**
     * generateRandomString: Used to generate random string
     *
     */
    public function generateRandomString($length = 6) {
        $characters = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
