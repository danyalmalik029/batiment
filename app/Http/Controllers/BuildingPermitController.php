<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\BuildingPermit;
use App\User;
use ZanySoft\Zip\ZipManager;
use Zip;
use App\Models\UserProfile;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Carbon;
use ZipArchive;

class BuildingPermitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['building']=BuildingPermit::where('user_id',Auth::id())
            ->orderBy('id', 'desc')
            ->first();


        if(!isset($data['building']))
            $data['building']=new BuildingPermit();


        $user = User::where('id', Auth::id())->first();
        $profile = Userprofile::where('iduser', Auth::id())->first();

        session(['name'=>$user->name]);
        session(['email'=>$user->email]);
        session(['phone_1'=>$profile->phone_1]);
        session(['phone_2'=>$profile->phone_2]);
        session(['gender'=>$profile->gender]);


        return view('webapp.building_permit/make_request',$data);

    }



    public function download()
    {

        /*
            $data['buildings'] = BuildingPermit::orderby('id', 'desc')
                ->get();
         */

        $data['buildings'] = DB::table('building_permits')
            ->join('users', 'users.id','building_permits.user_id')
            ->select('users.email as email',
                'users.name as name',
                'building_permits.user_id as id',
                'building_permits.namefolder',
                'building_permits.id as building_id')->get();

        return view('webapp.building_permit.download_file',$data);
    }


    public function zip_download($id)
    {

        $building_permit = BuildingPermit::where('id', $id)
            ->first();

            $user= User::where('id',$building_permit->user_id)->first();
            $nameFolder=$user->email;
        //      Zip des Fichiers


            $data['files']=json_decode($building_permit->metadata,true);

                $zip = new ZipArchive;
                $zip->open('../storage/files/build/'.$building_permit->user_id.'/'.$nameFolder.'.zip', ZipArchive::CREATE) ;
                if($data['files']['ground_plane']!=null)
                $zip->addFile('../storage/files/build/'.$building_permit->user_id.'/'.$data['files']['ground_plane'], $data['files']['ground_plane']);
                if($data['files']['plan_facade']!=null)
                $zip->addFile('../storage/files/build/'.$building_permit->user_id.'/'.$data['files']['plan_facade'], $data['files']['plan_facade']);
                if($data['files']['photo_ground1']!=null)
                $zip->addFile('../storage/files/build/'.$building_permit->user_id.'/'.$data['files']['photo_ground1'], $data['files']['photo_ground1']);
                if($data['files']['photo_ground2']!=null)
                $zip->addFile('../storage/files/build/'.$building_permit->user_id.'/'.$data['files']['photo_ground2'], $data['files']['photo_ground2']);
                if($data['files']['situation_plan']!=null)
                $zip->addFile('../storage/files/build/'.$building_permit->user_id.'/'.$data['files']['situation_plan'], $data['files']['situation_plan']);
                if($data['files']['recipice']!=null)
                $zip->addFile('../storage/files/build/'.$building_permit->user_id.'/'.$data['files']['recipice'], $data['files']['recipice']);
                $zip->close();
   

        //    Téléchargement des fichiers Zippés

        $public_dir='../storage/files/build/'.$building_permit->user_id;

        //  $zipFileName = Carbon\Carbon::now().'.zip';

        $zipFileName = $nameFolder.'.zip';

        $headers = array(
            'Content-Type' => 'application/octet-stream',
        );
        $filetopath=$public_dir.'/'.$zipFileName;

        if(file_exists($filetopath)){
            return response()->download($filetopath,$zipFileName,$headers);
        }
        return ['status'=>'file does not exist'];

    }

  

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function make_request()
    {

        $building=BuildingPermit::where('user_id',Auth::id())
            ->where('status',0)
            ->first();
        if(isset($building))
        {
            $data['building']=$building;
            $data['files']=json_decode($building->metadata,true);
            $data['editing']=true;

            if($building->amount == 0)
            $data['build_amount'] = 0;

        }
        else
        {
            $building=new BuildingPermit();
            $data['building']=$building;

            $data['build_amount'] = 0;

        }

     //   dd($data['files']['ground_plane']);

        return view('webapp.building_permit.form',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $filesData=[];

        //  Validation Files


        if(!is_null($request->file('ground_plane'))){

            $this->validate($request, [
                'ground_plane' => 'nullable|mimes:jpeg,png,pdf,jpg|max:10240',
            ]);

            $image1 = $request->file('ground_plane');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            $imagepath1='../storage/files/build/'.Auth::id();

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);

            $chemin = $filename1;

            $filesData['ground_plane']=$chemin;

        }

        else
        {

            $filesData['ground_plane']=$request->ground_plane1;
            // delete($request->hground_plane);
        }
        // dd($filesData['ground_plane']);
        if(!is_null($request->file('plan_facade'))){

            $this->validate($request, [
                'plan_facade' => 'nullable|mimes:jpeg,png,pdf,jpg|max:10240',
            ]);

            $image1 = $request->file('plan_facade');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            $imagepath1='../storage/files/build/'.Auth::id();

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);

            $chemin = $filename1;


            $filesData['plan_facade']=$chemin;

        }

        else
        {
            $filesData['plan_facade']=$request->plan_facade1;
            // delete($request->hground_plane);
        }
        if(!is_null($request->file('photo_ground1'))){

            $this->validate($request, [
                'photo_ground1' => 'nullable|mimes:jpeg,png,pdf,jpg|max:10240',
            ]);

            $image1 = $request->file('photo_ground1');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            $imagepath1='../storage/files/build/'.Auth::id();

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);

            $chemin = $filename1;


            $filesData['photo_ground1']=$chemin;


        }

        else
        {
            $filesData['photo_ground1']=$request->photo_ground11;
            // delete($request->hground_plane);
        }
        if(!is_null($request->file('photo_ground2'))){

            $this->validate($request, [
                'photo_ground2' => 'nullable|mimes:jpeg,png,pdf,jpg|max:10240',
            ]);

            $image1 = $request->file('photo_ground2');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            $imagepath1='../storage/files/build/'.Auth::id();

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);

            $chemin = $filename1;


            $filesData['photo_ground2']=$chemin;

        }

        else
        {
            $filesData['photo_ground2']=$request->photo_ground21;
            // delete($request->hground_plane);
        }
        if(!is_null($request->file('situation_plan'))){

            $this->validate($request, [
                'situation_plan' => 'nullable|mimes:jpeg,png,pdf,jpg|max:10240',
            ]);

            $image1 = $request->file('situation_plan');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            $imagepath1='../storage/files/build/'.Auth::id();

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);

            $chemin = $filename1;


            $filesData['situation_plan']=$chemin;

        }

        else
        {
            $filesData['situation_plan']=$request->situation_plan1;
            // delete($request->hground_plane);
        }
        if(!is_null($request->file('recipice'))){

            $this->validate($request, [
                'recipice' => 'nullable|mimes:jpeg,png,pdf,jpg|max:10240',
            ]);

            $image1 = $request->file('recipice');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            $imagepath1='../storage/files/build/'.Auth::id();

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);

            $chemin = $filename1;


            $filesData['recipice']=$chemin;

        }

        else
        {
            $filesData['recipice']=$request->recipice1;
            // delete($request->hground_plane);
        }

        $buildinpermit= new BuildingPermit();
        $buildinpermit->construction_type=$request->house;
        $buildinpermit->user_id=Auth::id();
        $buildinpermit->ground=$request->ground;
        $buildinpermit->building=$request->building;
        $buildinpermit->access=$request->access;
        $buildinpermit->aspect=$request->aspect;
        $buildinpermit->petitionary_name=$request->petitionary_name;
        $buildinpermit->place_construction=$request->place_construction;
        $buildinpermit->characteristics_place=$request->characteristics_place;
        $buildinpermit->surrounding_landscape=$request->surrounding_landscape;
        $buildinpermit->concerned_land=$request->concerned_land;
        $buildinpermit->neighboring_constructions=$request->neighboring_constructions;
        $buildinpermit->metadata = json_encode($filesData);

         $buildinpermit->namefolder = '../storage/files/build/'.Auth::id();

        $buildinpermit->amount=$request->amount;

        $buildinpermit->save();

        $data['building']=BuildingPermit::where('user_id',Auth::id())
            ->orderBy('id', 'desc')
            ->first();


        if(!isset($data['building']))
            $data['building']=new BuildingPermit();

        return view('webapp.building_permit/make_request',$data);
    }


    private function __saveFinanceFiles($request , $buildinpermit) {

        // Collect and email all inquiry file

        $files = $request->file();
        $filesData = [];

        foreach ($files as $key=>$file) {
            $filesData[$key] = $file->getClientOriginalName();
        }

        $buildinpermit->metadata = json_encode($filesData);

        $buildinpermit->update();
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {

        $buildinpermit=BuildingPermit::where('id',$request->id)->first();

        //  Validation Files


        $foldername = $buildinpermit->namefolder;


        if(!is_null($request->file('ground_plane'))){

            $this->validate($request, [
                'ground_plane' => 'nullable|mimes:jpeg,png,pdf,jpg|max:10240',
            ]);

            $image1 = $request->file('ground_plane');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            $imagepath1='../storage/files/build/'.Auth::id();

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);

            $chemin = $filename1;

            $filesData['ground_plane']=$chemin;

        }

        else
        {

            $filesData['ground_plane']=$request->ground_plane1;
            // delete($request->ground_plane);
        }
        // dd($filesData['ground_plane']);
        if(!is_null($request->file('plan_facade'))){

            $this->validate($request, [
                'plan_facade' => 'nullable|mimes:jpeg,png,pdf,jpg|max:10240',
            ]);

            $image1 = $request->file('plan_facade');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            $imagepath1='../storage/files/build/'.Auth::id();

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);

            $chemin = $filename1;


            $filesData['plan_facade']=$chemin;

        }

        else
        {
            $filesData['plan_facade']=$request->plan_facade1;
            // delete($request->ground_plane);
        }
        if(!is_null($request->file('photo_ground1'))){

            $this->validate($request, [
                'photo_ground1' => 'nullable|mimes:jpeg,png,pdf,jpg|max:10240',
            ]);

            $image1 = $request->file('photo_ground1');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            $imagepath1='../storage/files/build/'.Auth::id();

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);

            $chemin = $filename1;


            $filesData['photo_ground1']=$chemin;


        }

        else
        {
            $filesData['photo_ground1']=$request->photo_ground11;
            // delete($request->ground_plane);
        }
        if(!is_null($request->file('photo_ground2'))){

            $this->validate($request, [
                'photo_ground2' => 'nullable|mimes:jpeg,png,pdf,jpg|max:10240',
            ]);

            $image1 = $request->file('photo_ground2');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            $imagepath1='../storage/files/build/'.Auth::id();

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);

            $chemin = $filename1;


            $filesData['photo_ground2']=$chemin;

        }

        else
        {
            $filesData['photo_ground2']=$request->photo_ground21;
            // delete($request->ground_plane);
        }
        if(!is_null($request->file('situation_plan'))){

            $this->validate($request, [
                'situation_plan' => 'nullable|mimes:jpeg,png,pdf,jpg|max:10240',
            ]);

            $image1 = $request->file('situation_plan');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            $imagepath1='../storage/files/build/'.Auth::id();

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);

            $chemin = $filename1;


            $filesData['situation_plan']=$chemin;

        }

        else
        {
            $filesData['situation_plan']=$request->situation_plan1;
            // delete($request->ground_plane);
        }
        if(!is_null($request->file('recipice'))){

            $this->validate($request, [
                'recipice' => 'nullable|mimes:jpeg,png,pdf,jpg|max:10240',
            ]);

            $image1 = $request->file('recipice');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            $imagepath1='../storage/files/build/'.Auth::id();

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);

            $chemin = $filename1;


            $filesData['recipice']=$chemin;

        }

        else
        {
            $filesData['recipice']=$request->recipice1;
            // delete($request->ground_plane);
        }

        $buildinpermit->id=$request->id;
        $buildinpermit->construction_type=$request->house;
        $buildinpermit->user_id=Auth::id();
        $buildinpermit->ground=$request->ground;
        $buildinpermit->building=$request->building;
        $buildinpermit->access=$request->access;
        $buildinpermit->aspect=$request->aspect;
        $buildinpermit->petitionary_name=$request->petitionary_name;
        $buildinpermit->place_construction=$request->place_construction;
        $buildinpermit->characteristics_place=$request->characteristics_place;
        $buildinpermit->surrounding_landscape=$request->surrounding_landscape;
        $buildinpermit->concerned_land=$request->concerned_land;
        $buildinpermit->neighboring_constructions=$request->neighboring_constructions;
        $buildinpermit->metadata = json_encode($filesData);

        $buildinpermit->amount=$request->amount;

        $buildinpermit->update();
        $data['building']=BuildingPermit::where('user_id',Auth::id())
            ->orderBy('id', 'desc')
            ->first();

        if(!isset($data['building']))
            $data['building']=new BuildingPermit();

        return view('webapp.building_permit/make_request',$data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
