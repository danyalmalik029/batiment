<?php

namespace App\Http\Controllers;

use App\Models\Ads;
use App\Models\Categories;
use App\Models\City;
use App\Models\Images;
use App\Models\Plan;
use App\Models\Files;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page'] = 'home';

        $properties = [];
        $plans = [];

        $propertiesQuery = Ads::where('statut', '1')
            ->orderBy('id', 'DESC')
            ->limit(\Config::get('settings.listLenght'))
            ->join('ads_categories','ads_categories.id','=','ads.catid')
            ->select('ads.*', 'ads_categories.category_name')
            ->get();

        if($propertiesQuery !== null) {
            $properties = $propertiesQuery->toArray();
        }

        $plansQuery = Plan::where('statut', '1')
            ->orderBy('id', 'DESC')
            ->limit(\Config::get('settings.listLenght'))
            ->get();

        if($plansQuery !== null) {
            $plans = $plansQuery->toArray();
        }

        foreach ($properties as $key=>$row) {

            $properties[$key]['images'] = [];

            // Find images related to this property
            $imagesQuery = Images::where('ads_id',$properties[$key]['id'])
                ->join('user_image','user_image.id','=','ads_images.image_id')
                ->select('ads_images.*','user_image.image_name as image_name')
                ->get();

            if($imagesQuery !== null) {
                $imagesArray = $imagesQuery->toArray();
                $properties[$key]['images'] = $imagesArray;
            }


        }

        foreach ($plans as $key=>$plan) {

            $plans[$key]['images'] = [];

            // Find images related to this property
            $imagesQuery = Files::where('plan_id', $plan['id'])
                ->where('type', '<>', 'cad')
                ->where('type', '<>', 'CAD')
                ->get();

            if($imagesQuery !== null) {
                $imagesArray = $imagesQuery->toArray();
                $plans[$key]['images'] = $imagesArray;
            }


        }


        //DISPLAY FEATURES
        $data['features']=DB::table('ads')
            ->join('ads_images', 'ads.id', '=', 'ads_images.ads_id')
            ->join('user_image', 'user_image.id', '=', 'ads_images.image_id')
            ->select('ads.id as id','title', 'price', 'area','ad_dtype', 'address_city','image_name')
            ->where('favorite','=',1)
            ->orderBy('ads.id', 'desc')
            ->limit(10)
            ->get();

        // For every single property, get the images
        $data['categories'] = Categories::All();

        $data['properties'] = $properties;
        $data['plans'] = $plans;
        return view('welcome', $data);
    }

    public function mark_noti_read($not_id)
    {
        if($not_id!=null)
        {
            try
            {
                $user=Auth::user();
                $user->unreadNotifications->where('id', $not_id)->markAsRead();
                return 'success';
            }
            catch (Exception $e)
            {
                return 'false';
            }

        }
        return 'false';

    }

}
