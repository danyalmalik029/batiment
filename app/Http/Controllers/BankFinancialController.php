<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Redirect;
use App\Models\FinancialFolder;
use App\Models\FinancialInfos;
use App\Models\Files;
use Illuminate\Support\Facades\Mail;
use App\Mail\BankFinancialEmail;
use ZipArchive;
use Auth;
use App\User;
use App\Models\UserProfile;
use ZanySoft\Zip\ZipManager;
use Zip;
use Carbon;
use Storage;


class BankFinancialController extends Controller
{
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth', [
            'except' => []]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $inqueries = FinancialFolder::where('user_id', Auth::id())
            ->where('status', 0)
            ->first();



        /*
        $inqueries = FinancialFolder::where('user_id', Auth::id())
            ->orderBy('id', 'DESC')->get();
            //->paginate(\Config::get('settings.listLenght'));
*/
        if(!isset($inqueries))
            $inqueries=new FinancialFolder();

        $user = null;

        if(Auth::check()){
            $user = UserProfile::where('iduser', Auth::id())->first();
        }


        $user = User::where('id', Auth::id())->first();
        $profile = Userprofile::where('iduser', Auth::id())->first();

        session(['name'=>$user->name]);
        session(['email'=>$user->email]);
        session(['phone_1'=>$profile->phone_1]);
        session(['phone_2'=>$profile->phone_2]);
        session(['gender'=>$profile->gender]);
        session(['birth_date'=>$profile->birth_date]);
        session(['address'=>$profile->address]);
        session(['city'=>$profile->city]);
        session(['country'=>$profile->country]);
        session(['postcode'=>$profile->postcode]);


        //  dd($inqueries);

        //   dd($inqueries);

        //dd(Auth::User()->ads);

        return view('webapp.banking.viewAll')->with([
            'page' => 'view-bank-financial',
            'sucess' => 'null',
            'error' => 'null',
            'inquiries' => $inqueries,
            'userProfile' => $user,
            'title' => trans('banking.banking_title')
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = null;

        if(Auth::check()){
            $user = UserProfile::where('iduser', Auth::id())->first();
        }

        $user_id = Auth::id();

        //  Récupération des informations financières de l'utilisateur connecté

        $financialinfos = FinancialFolder::where('user_id', $user_id)
            ->where('status', 0)
            ->first();

        //  Récupération des Fichiers sur les informations financières de l'utilisateur connecté

        if($financialinfos){
            // dd("Oui");
            //  $financialfiles = Files::where('financial_infos_id', $financialinfos->id)->get();
            $financialfiles = json_decode($financialinfos->metadata,true);
            $mode = "true";

            //  Recupération de l'ID de la demande de Financement

            //  dd($financialinfos->id);

        }

        else{

            //   dd("Non");

            $financialinfos = new FinancialFolder;

            $financialfiles = new Files;

            $mode = "false";

            //  Recupération de l'ID de la demande de Financement

            //  $id = FinancialFolder::Max("id");

            //  dd($id);



        }

        //  dd($mode);

        // dd($financialfiles);

        return view('webapp.banking.bankingform')->with([
            'page' => 'view-bank-financial',
            'sucess' => 'null',
            'error' => 'null',
            'inquiry' => $financialinfos,
            'files' => $financialfiles,
            'userProfile' => $user,
            'editing' => $mode,
            'title' => trans('banking.banking_title')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  Validation Files

        $user_id=Auth::id();


        if($request->input('editing') == "true" ) {

            $financialinfos = FinancialFolder::where('user_id', $user_id)
                ->where ('status', 0)
                ->first();

            $foldername = $financialinfos->namefolder;

            //  Recupération de l'ID de la demande de Financement

            //    dd($financialinfos);

        }else{

            //  Date de création de la demande de financement

            //   $datecre = Carbon\Carbon::now().'.zip';

            $namefolder = Carbon\Carbon::now();

            //  $namefolder = explode(" ", $datecre);

            $namefolder = str_replace ( ":" , "" , $namefolder );

        //    $namefolder = str_replace ( " " , "" , $namefolder );

            //  dd($datecre);

            $financialinfos = new FinancialFolder;

            //      Nom du dossier dans lequel nous allons stoker les fichiers

            $foldername = $namefolder;

        }

        //  Récupération de l'utilisateur connecté

        $financialinfos->user_id = $user_id;

        //  Recupération des information de la page 1       Identité et Conjoint

        $financialinfos->civility = $request->input('civility');
        $financialinfos->email = $request->input('email');
        $financialinfos->fname = $request->input('fname');
        $financialinfos->lname = $request->input('lname');
        $financialinfos->rep = $request->input('rep');
        $financialinfos->datenais = $request->input('datenais');
        $financialinfos->nat = $request->input('nat');
        $financialinfos->paysnais = $request->input('paysnais');
        $financialinfos->vilcp = $request->input('vilcp');
        $financialinfos->sitfamil = $request->input('sitfamil');
        $financialinfos->nbenfcharg = $request->input('nbenfcharg');
        $financialinfos->civilityconj = $request->input('civilityconj');
        $financialinfos->natconj = $request->input('natconj');
        $financialinfos->fnameconj = $request->input('fnameconj');
        $financialinfos->lnameconj = $request->input('lnameconj');
        $financialinfos->paysnaisconj = $request->input('paysnaisconj');
        $financialinfos->vilcpconj = $request->input('vilcpconj');
        $financialinfos->datenaisconj = $request->input('datenaisconj');
        $financialinfos->coemprunt = $request->input('coemprunt');

        //  Recupération des information de la page 2           Adresse

        $financialinfos->nomvoie = $request->input('nomvoie');
        $financialinfos->resprin = $request->input('resprin');
        $financialinfos->adrcompl = $request->input('adrcompl');
        $financialinfos->vilcpadr = $request->input('vilcpadr');
        $financialinfos->telfixe = $request->input('telfixe');
        $financialinfos->mdhab = $request->input('mdhab');
        $financialinfos->dateresidence = $request->input('dateresidence');
        $financialinfos->telmobile = $request->input('telmobile');

        //  Recupération des information de la page 3           Situation

        $financialinfos->vtprofession = $request->input('vtprofession');
        $financialinfos->vtcontrat = $request->input('vtcontrat');
        $financialinfos->vtancactivite = $request->input('vtancactivite');
        $financialinfos->vtemployeur = $request->input('vtemployeur');
        $financialinfos->vtvilcpemployeur = $request->input('vtvilcpemployeur');
        $financialinfos->saprofession = $request->input('saprofession');
        $financialinfos->sncontrat = $request->input('sncontrat');
        $financialinfos->snancactivite = $request->input('snancactivite');
        $financialinfos->snemployeur = $request->input('snemployeur');
        $financialinfos->snvilcpemployeur = $request->input('snvilcpemployeur');

        //  Recupération des information de la page 4           Budget

        $financialinfos->vtrevmens = $request->input('vtrevmens');
        $financialinfos->vtperioderev = $request->input('vtperioderev');
        $financialinfos->namebanq = $request->input('namebanq');
        $financialinfos->snrevmens = $request->input('snrevmens');
        $financialinfos->saperioderev = $request->input('saperioderev');
        $financialinfos->ancbanque = $request->input('ancbanque');
        $financialinfos->autrerevmens = $request->input('autrerevmens');
        $financialinfos->vtcartbanc = $request->input('vtcartbanc');
        $financialinfos->changbanq = $request->input('changbanq');
        $financialinfos->fichagebanc = $request->input('fichagebanc');
        $financialinfos->loyerresprinc = $request->input('loyerresprinc');
        $financialinfos->creditconso = $request->input('creditconso');
        $financialinfos->pensaliment = $request->input('pensaliment');
        $financialinfos->regroupcredit = $request->input('regroupcredit');

        //  Recupération des information de la page 5               Type de Propriété

        $financialinfos->house = $request->input('house');

        //  Recuperation de l'image

        $filesData=[];



        if(!is_null($request->file('cni'))){

            $this->validate($request, [
                'cni' => 'nullable|mimes:jpeg,png,pdf,jpg|max:10240',
            ]);

            $image1 = $request->file('cni');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            // $imagepath1='images/datasheets/'.Auth::id();

            $imagepath1='../storage/files/banking/'.Auth::id().'/'.$foldername;

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);

            $chemin = $imagepath1."/".$filename1;

            $filesData['cni']=$chemin;

        }

        else
        {

            $filesData['cni']=$request->cni1;

            // delete($request->cni);

        }

        /*
        dd($filesData);

        dd('cni : ' . $request->file('cni'));

        dd('passeport : '.$request->file('passeport'));

        dd('contrat_de_bail : '.$request->file('contrat_de_bail'));

        dd('surfaces_habitables_reno : '.$request->file('surfaces_habitables_reno'));
*/

        if(!is_null($request->file('livret_de_mariage'))){

            $this->validate($request, [
                'livret_de_mariage' => 'nullable|mimes:jpeg,png,pdf,jpg|max:10240',
            ]);

            $image1 = $request->file('livret_de_mariage');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            $imagepath1='../storage/files/banking/'.Auth::id().'/'.$foldername;

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);

            $chemin = $imagepath1."/".$filename1;

            $filesData['livret_de_mariage']=$chemin;

        }

        else
        {

            $filesData['livret_de_mariage']=$request->livret_de_mariage1;
            // delete($request->livret_de_mariage);
        }


        if(!is_null($request->file('certificat_marie'))){

            $this->validate($request, [
                'certificat_marie' => 'nullable|mimes:jpeg,png,pdf,jpg|max:10240',
            ]);

            $image1 = $request->file('certificat_marie');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            $imagepath1='../storage/files/banking/'.Auth::id().'/'.$foldername;

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);

            $chemin = $imagepath1."/".$filename1;

            $filesData['certificat_marie']=$chemin;

        }

        else
        {

            $filesData['certificat_marie']=$request->certificat_marie1;
            // delete($request->certificat_marie);
        }


        if(!is_null($request->file('facture_eau'))){

            $this->validate($request, [
                'facture_eau' => 'nullable|mimes:jpeg,png,pdf,jpg|max:10240',
            ]);

            $image1 = $request->file('facture_eau');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            $imagepath1='../storage/files/banking/'.Auth::id().'/'.$foldername;

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);

            $chemin = $imagepath1."/".$filename1;

            $filesData['facture_eau']=$chemin;

        }

        else
        {

            $filesData['facture_eau']=$request->facture_eau1;
            // delete($request->facture_eau);
        }


        if(!is_null($request->file('releves_de_compte'))){

            $this->validate($request, [
                'releves_de_compte' => 'nullable|mimes:jpeg,png,pdf,jpg|max:10240',
            ]);

            $image1 = $request->file('releves_de_compte');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            $imagepath1='../storage/files/banking/'.Auth::id().'/'.$foldername;

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);

            $chemin = $imagepath1."/".$filename1;

            $filesData['releves_de_compte']=$chemin;

        }

        else
        {

            $filesData['releves_de_compte']=$request->releves_de_compte1;
            // delete($request->releves_de_compte);
        }


        if(!is_null($request->file('prets_familiaux'))){

            $this->validate($request, [
                'prets_familiaux' => 'nullable|mimes:jpeg,png,pdf,jpg|max:10240',
            ]);

            $image1 = $request->file('prets_familiaux');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            $imagepath1='../storage/files/banking/'.Auth::id().'/'.$foldername;

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);

            $chemin = $imagepath1."/".$filename1;

            $filesData['prets_familiaux']=$chemin;

        }

        else
        {

            $filesData['prets_familiaux']=$request->prets_familiaux1;
            // delete($request->prets_familiaux);
        }


        if(!is_null($request->file('donation'))){

            $this->validate($request, [
                'donation' => 'nullable|mimes:jpeg,png,pdf,jpg|max:10240',
            ]);

            $image1 = $request->file('donation');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            $imagepath1='../storage/files/banking/'.Auth::id().'/'.$foldername;

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);

            $chemin = $imagepath1."/".$filename1;

            $filesData['donation']=$chemin;

        }

        else
        {

            $filesData['donation']=$request->donation1;
            // delete($request->donation);
        }


        if(!is_null($request->file('placements'))){

            $this->validate($request, [
                'placements' => 'nullable|mimes:jpeg,png,pdf,jpg|max:10240',
            ]);

            $image1 = $request->file('placements');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            $imagepath1='../storage/files/banking/'.Auth::id().'/'.$foldername;

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);

            $chemin = $imagepath1."/".$filename1;

            $filesData['placements']=$chemin;

        }

        else
        {

            $filesData['placements']=$request->placements1;
            // delete($request->placements);
        }


        if(!is_null($request->file('surfaces_habitables_reno'))){

            $this->validate($request, [
                'surfaces_habitables_reno' => 'nullable|mimes:jpeg,png,pdf,jpg|max:10240',
            ]);

            $image1 = $request->file('surfaces_habitables_reno');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            $imagepath1='../storage/files/banking/'.Auth::id().'/'.$foldername;

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);

            $chemin = $imagepath1."/".$filename1;

            $filesData['surfaces_habitables_reno']=$chemin;

        }

        else
        {

            $filesData['surfaces_habitables_reno']=$request->surfaces_habitables_reno1;
            // delete($request->surfaces_habitables_reno);
        }


        if(!is_null($request->file('dossier_diagnostics_techniques'))){

            $this->validate($request, [
                'dossier_diagnostics_techniques' => 'nullable|mimes:jpeg,png,pdf,jpg|max:10240',
            ]);

            $image1 = $request->file('dossier_diagnostics_techniques');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            $imagepath1='../storage/files/banking/'.Auth::id().'/'.$foldername;

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);

            $chemin = $imagepath1."/".$filename1;

            $filesData['dossier_diagnostics_techniques']=$chemin;

        }

        else
        {

            $filesData['dossier_diagnostics_techniques']=$request->dossier_diagnostics_techniques1;
            // delete($request->dossier_diagnostics_techniques);
        }


        if(!is_null($request->file('passeport'))){

            $this->validate($request, [
                'passeport' => 'nullable|mimes:jpeg,png,pdf,jpg|max:10240',
            ]);

            $image1 = $request->file('passeport');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            $imagepath1='../storage/files/banking/'.Auth::id().'/'.$foldername;

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);

            $chemin = $imagepath1."/".$filename1;

            $filesData['passeport']=$chemin;

        }

        else
        {

            $filesData['passeport']=$request->passeport1;
            // delete($request->passeport);
        }


        if(!is_null($request->file('livret_de_famille'))){

            $this->validate($request, [
                'livret_de_famille' => 'nullable|mimes:jpeg,png,pdf,jpg|max:10240',
            ]);

            $image1 = $request->file('livret_de_famille');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            $imagepath1='../storage/files/banking/'.Auth::id().'/'.$foldername;

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);

            $chemin = $imagepath1."/".$filename1;

            $filesData['livret_de_famille']=$chemin;

        }

        else
        {

            $filesData['livret_de_famille']=$request->livret_de_famille1;
            // delete($request->livret_de_famille);
        }


        if(!is_null($request->file('quittances_edf'))){

            $this->validate($request, [
                'quittances_edf' => 'nullable|mimes:jpeg,png,pdf,jpg|max:10240',
            ]);

            $image1 = $request->file('quittances_edf');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            $imagepath1='../storage/files/banking/'.Auth::id().'/'.$foldername;

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);

            $chemin = $imagepath1."/".$filename1;

            $filesData['quittances_edf']=$chemin;

        }

        else
        {

            $filesData['quittances_edf']=$request->quittances_edf1;
            // delete($request->quittances_edf);
        }


        if(!is_null($request->file('contrat_de_bail'))){

            $this->validate($request, [
                'contrat_de_bail' => 'nullable|mimes:jpeg,png,pdf,jpg|max:10240',
            ]);

            $image1 = $request->file('contrat_de_bail');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            $imagepath1='../storage/files/banking/'.Auth::id().'/'.$foldername;

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);

            $chemin = $imagepath1."/".$filename1;

            $filesData['contrat_de_bail']=$chemin;

        }

        else
        {

            $filesData['contrat_de_bail']=$request->contrat_de_bail1;
            // delete($request->contrat_de_bail);
        }


        if(!is_null($request->file('avis_imposition'))){

            $this->validate($request, [
                'avis_imposition' => 'nullable|mimes:jpeg,png,pdf,jpg|max:10240',
            ]);

            $image1 = $request->file('avis_imposition');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            $imagepath1='../storage/files/banking/'.Auth::id().'/'.$foldername;

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);

            $chemin = $imagepath1."/".$filename1;

            $filesData['avis_imposition']=$chemin;

        }

        else
        {

            $filesData['avis_imposition']=$request->avis_imposition1;
            // delete($request->avis_imposition);
        }


        if(!is_null($request->file('epargne_logement'))){

            $this->validate($request, [
                'epargne_logement' => 'nullable|mimes:jpeg,png,pdf,jpg|max:10240',
            ]);

            $image1 = $request->file('epargne_logement');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            $imagepath1='../storage/files/banking/'.Auth::id().'/'.$foldername;

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);

            $chemin = $imagepath1."/".$filename1;

            $filesData['epargne_logement']=$chemin;

        }

        else
        {

            $filesData['epargne_logement']=$request->epargne_logement1;
            // delete($request->epargne_logement);
        }


        if(!is_null($request->file('benefice_entreprise'))){

            $this->validate($request, [
                'benefice_entreprise' => 'nullable|mimes:jpeg,png,pdf,jpg|max:10240',
            ]);

            $image1 = $request->file('benefice_entreprise');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            $imagepath1='../storage/files/banking/'.Auth::id().'/'.$foldername;

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);

            $chemin = $imagepath1."/".$filename1;

            $filesData['benefice_entreprise']=$chemin;

        }

        else
        {

            $filesData['benefice_entreprise']=$request->benefice_entreprise1;
            // delete($request->benefice_entreprise);
        }


        if(!is_null($request->file('contrat_de_construction'))){

            $this->validate($request, [
                'contrat_de_construction' => 'nullable|mimes:jpeg,png,pdf,jpg|max:10240',
            ]);

            $image1 = $request->file('contrat_de_construction');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            $imagepath1='../storage/files/banking/'.Auth::id().'/'.$foldername;

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);

            $chemin = $imagepath1."/".$filename1;

            $filesData['contrat_de_construction']=$chemin;

        }

        else
        {

            $filesData['contrat_de_construction']=$request->contrat_de_construction1;
            // delete($request->contrat_de_construction);
        }


        if(!is_null($request->file('titre_de_propriete'))){

            $this->validate($request, [
                'titre_de_propriete' => 'nullable|mimes:jpeg,png,pdf,jpg|max:10240',
            ]);

            $image1 = $request->file('titre_de_propriete');

            $ext1=$image1->getClientOriginalExtension();

            $filename1=uniqid().'.'.$ext1;

            $imagepath1='../storage/files/banking/'.Auth::id().'/'.$foldername;

            $uploadsuccesimage1=$image1->move($imagepath1, $filename1);

            $chemin = $imagepath1."/".$filename1;

            $filesData['titre_de_propriete']=$chemin;

        }

        else
        {

            $filesData['titre_de_propriete']=$request->titre_de_propriete1;
            // delete($request->titre_de_propriete);
        }




        $financialinfos->metadata = json_encode($filesData);




        if($request->input('editing') == "true" ) {

            //  Enregistrement des données dans la table  :  financialinfos

            $financialinfos->update();

        }

        elseif($request->input('editing') == "false" ) {

            //  Enregistrement des données dans la table  :  financialinfos

            $financialinfos->namefolder = $namefolder;


            $financialinfos->save();

        }


        //  $inquiry = new FinancialFolder();

        // $inquiry->user_id = Auth::id();
        // $inquiry->status = 0;
        //  $inquiry->save();

        //  $this->__saveFinanceFiles($request, $inquiry);
        //  $this->sendMessage();
        return redirect('bank-financial');
    }

    public function download()
    {

        $data['bank_financials'] = FinancialFolder::orderby('id', 'desc')
            ->get();

        //  dd($data['bank_financials']);

        return view('webapp.banking.download_file',$data);

    }


    public function zip_download($id)
    {

        $financialinfos = FinancialFolder::where('id', $id)
            ->first();


        //  Pour une ancienne demande, on enregistre tous les fichiers dans un nouveau dossier qu'on va par la suite Zipper

        //***************************************************************************************************************************************************************************************************************************

        // création de la demande de financement

        //   $datecre = Carbon\Carbon::now().'.zip';

        $user_id=Auth::id();

        $namefolder = Carbon\Carbon::now();

        //  $namefolder = explode(" ", $datecre);

        $namefolder = str_replace ( ":" , "" , $namefolder );

        $namefolder = str_replace ( " " , "" , $namefolder );

        //      Nom du dossier dans lequel nous allons stoker les fichiers

        $foldername = $namefolder;

     //   dd($financialinfos->metadata);

        //      Zip des Fichiers

        $data['files']=json_decode($financialinfos->metadata,true);

        $user= User::where('id',$financialinfos->user_id)->first();

       // dd($financialinfos->user_id);

       // dd($data['files']);

        $zip = new ZipArchive;
        $zip->open('../storage/files/banking/'.$financialinfos->user_id.'/'.$foldername.'.zip', ZipArchive::CREATE) ;

        if($data['files']['cni']!=null)
            $zip->addFile($data['files']['cni'], $data['files']['cni']);

        if($data['files']['livret_de_mariage']!=null)
            $zip->addFile($data['files']['livret_de_mariage'], $data['files']['livret_de_mariage']);

        if($data['files']['certificat_marie']!=null)
            $zip->addFile($data['files']['certificat_marie'], $data['files']['certificat_marie']);

        if($data['files']['facture_eau']!=null)
            $zip->addFile($data['files']['facture_eau'], $data['files']['facture_eau']);

        if($data['files']['releves_de_compte']!=null)
            $zip->addFile($data['files']['releves_de_compte'], $data['files']['releves_de_compte']);

        if($data['files']['prets_familiaux']!=null)
            $zip->addFile($data['files']['prets_familiaux'], $data['files']['prets_familiaux']);

        if($data['files']['donation']!=null)
            $zip->addFile($data['files']['donation'], $data['files']['donation']);

        if($data['files']['placements']!=null)
            $zip->addFile($data['files']['placements'], $data['files']['placements']);

        if($data['files']['surfaces_habitables_reno']!=null)
            $zip->addFile($data['files']['surfaces_habitables_reno'], $data['files']['surfaces_habitables_reno']);

        if($data['files']['dossier_diagnostics_techniques']!=null)
            $zip->addFile($data['files']['dossier_diagnostics_techniques'], $data['files']['dossier_diagnostics_techniques']);

        if($data['files']['passeport']!=null)
            $zip->addFile($data['files']['passeport'], $data['files']['passeport']);

        if($data['files']['livret_de_famille']!=null)
            $zip->addFile($data['files']['livret_de_famille'], $data['files']['livret_de_famille']);

        if($data['files']['quittances_edf']!=null)
            $zip->addFile($data['files']['quittances_edf'], $data['files']['quittances_edf']);

        if($data['files']['contrat_de_bail']!=null)
            $zip->addFile($data['files']['contrat_de_bail'], $data['files']['contrat_de_bail']);

        if($data['files']['avis_imposition']!=null)
            $zip->addFile($data['files']['avis_imposition'], $data['files']['avis_imposition']);

        if($data['files']['epargne_logement']!=null)
            $zip->addFile($data['files']['epargne_logement'], $data['files']['epargne_logement']);

        if($data['files']['benefice_entreprise']!=null)
            $zip->addFile($data['files']['benefice_entreprise'], $data['files']['benefice_entreprise']);

        if($data['files']['contrat_de_construction']!=null)
            $zip->addFile($data['files']['contrat_de_construction'], $data['files']['contrat_de_construction']);

        if($data['files']['titre_de_propriete']!=null)
            $zip->addFile($data['files']['titre_de_propriete'], $data['files']['titre_de_propriete']);

        $zip->close();

        //***************************************************************************************************************************************************************************************************************************

        //      Zip des Fichiers

      //  $this->createZip('../storage/files/banking/'.$financialinfos->user_id.'/'.$financialinfos->namefolder.'/','../storage/files/banking/'.$financialinfos->user_id.'/'.$financialinfos->namefolder.'/');

        //    Téléchargement des fichiers Zippés

        $public_dir='../storage/files/banking/'.$financialinfos->user_id;

        //  $zipFileName = Carbon\Carbon::now().'.zip';

        /*
         *

        if($financialinfos->namefolder == null)
            $zipFileName = $foldername.'.zip';
        else
            $zipFileName = $financialinfos->namefolder.'.zip';

         */

        $zipFileName = $foldername.'.zip';

      //  $zipFileName = $financialinfos->namefolder.'.zip';

        $headers = array(
            'Content-Type' => 'application/octet-stream',
        );
        $filetopath=$public_dir.'/'.$zipFileName;

        if(file_exists($filetopath)){
            return response()->download($filetopath,$zipFileName,$headers);
        }
        return ['status'=>'file does not exist'];

        

    }

    /**
     * Fonction de création des Zip
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    private function createZip($dirSource,$dirBackup, $reload = null, $oZip = null) {
        // si le dossier existe
        if ($dir = opendir($dirSource)) {
            // on créait le chemin du dossier final
            $pathZip = substr($dirBackup, 0, -1).'.zip';

            //si la fonction est lancé pour la première fois on créait l'objet
            if(!$reload){
                $oZip = new ZipArchive;
                $oZip->open($pathZip, ZipArchive::CREATE);
            }// sinon on récupère l'object passé en param
            else{
                $oZip = $oZip;
            }

            while (($file = readdir($dir)) !== false) {
                // on évite le dossier parent et courant
                if($file != '..'  && $file != '.') {
                    // Si c'est un dossier on relance la fonction
                    if(is_dir($dirSource.$file)) {
                        createZip($dirSource.$file.'/', $dirBackup.$file.'/', 1, $oZip);
                    }// sinon c'est un fichier donc on l'ajoute à l'archive
                    else {
                        $oZip->addFile($dirSource.$file);
                    }
                }
            }

            //    dd($oZip);

            // on ferme l'archive
            if(!$reload){
                return $oZip->close();
            }
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    private function __saveFinanceFiles($request , $inquiry) {

        // Collect and email all inquiry file

        $files = $request->file();
        $filesData = [];

        foreach ($files as $key=>$file) {
            $filesData[$key] = $file->getClientOriginalName();
        }

        $inquiry->metadata = json_encode($filesData);

        $inquiry->update();
    }

    public function sendMessage()
    {


        $data = [

            'your_name' => Auth::User()->name,
            'subject' => 'fichier de financement',
            'mymessage' => trans('banking.message')
        ];

        $moreUsers = [
            'eric.palmiste@hotmail.fr'
        ];

        $evenMoreUsers = [
            'hdouanla.ei@gmail.com'
        ];


        Mail::to('hervebogne@gmail.com', 'Info ETB')
            ->cc($moreUsers)
            ->bcc($evenMoreUsers)
            ->send(new BankFinancialEmail($data));
        // ->attach($zipFileName);

    }
}
