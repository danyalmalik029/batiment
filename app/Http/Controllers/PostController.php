<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Users;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function show(Request $request, $slug) {

        $postQuery = Post::Where('slug',$slug)->first();

        if($request->action) {
            if($request->action == "publish") {
                // Want to publich the post that still in draf

                $postQuery->status = 'publish';
                $postQuery->save();
            }
        }

        $data['content'] = [];

        if($postQuery != null)
            $data['content'] = $postQuery->toArray();

        //  Recupération du role du user

        $user_id = Auth::id();

        if(isset($user_id)){

            $data['user'] = DB::table('users')->where('id', $user_id)->first();

        }

        return view('webapp/page' , $data);

    }
}
