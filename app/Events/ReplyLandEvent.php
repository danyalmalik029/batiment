<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ReplyLandEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $message;
    public $user;
    public $reciever;
    public $count;
    public $notify_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($message,$user,$reciever,$count,$notify_id)
    {
        $this->message=$message;
        $this->user=$user;
        $this->reciever=$reciever;
        $this->count=$count;
        $this->notify_id=$notify_id;
    }


    public function broadcastAs()
    {
        return 'Event';
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('message-'.$this->reciever->id);
    }
}
