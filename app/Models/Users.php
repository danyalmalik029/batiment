<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Users extends Model
{
    use Notifiable;
    protected 	$table = "users";
    protected 	$fillable = [
	    			'id',
					'name',
					'referer_id',
					'reference_code',
					'username',
					'email',
					'password',
					'block',
					'sendEmail',
					'registerDate',
					'lastvisitDate',
					'activation',
					'params',
					'lastResetTGime',
					'resetCount',
					'otpKey',
					'otep',
					'requireReset',
					'remember_token',
					'updated_at',
					'created_at',
					'franchise',
					'subscriptionDate',
					'subscriptionRenewalDate'
				];

	public function ads(){
        return $this->hasMany('App\Models\Ads', 'user_id', 'id');
    }

    public function profile()
    {
        return $this->hasOne('App\Models\UserProfile', 'iduser', 'id');
    }

    public function plans(){
        return $this->hasMany('App\Models\Plan', 'user_id', 'id');
    }

    public function buyedPlans(){
        return $this->belongsToMany('App\Models\Plan', 'users_plan', 'plan_id', 'user_id');
    }
}
