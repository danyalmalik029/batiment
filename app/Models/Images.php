<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    protected 	$table = "ads_images";

    public $timestamps = false;
    
    protected 	$fillable = [
	    			'ads_id',
					'image_id',
					'ordering'
				];

	public function ads(){
        return $this->belongsTo('App\Models\Ads', 'ads_id');
    }

    public function UserImages(){
        return $this->belongsTo('App\Models\UserImage', 'image_id');
    }
}
