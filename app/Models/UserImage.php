<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserImage extends Model
{
    protected 	$table = "user_image";

    public $timestamps = false;
    
    protected 	$fillable = [
	    			'id',  
                    'image_name',  
                    'ad_id'
				];

    public function ads(){
        return $this->belongsToMany('App\Models\Ads', 'ads_images', 'ads_id', 'image_id');
    }

    public function imagesLinked(){
        return $this->hasMany('App\Models\Images', 'image_id', 'id');
    }
}
