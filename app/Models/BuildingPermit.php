<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BuildingPermit extends Model
{
    public      $timestamps = false;
    protected 	$table = "building_permits";
}
