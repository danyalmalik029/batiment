<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected 	$table = "ads_categories";
    protected 	$fillable = [
	    			'id',
					'category_name',
					'category_slug',
					'category_description',
					'category_image'
				];

	public function ads(){
        return $this->hasMany('App\Models\Ads', 'catid', 'id');
    }
}
