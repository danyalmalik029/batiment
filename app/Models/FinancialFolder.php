<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FinancialFolder extends Model
{
    //
    public      $timestamps = false;
    protected 	$table = "financial_folder";
}
