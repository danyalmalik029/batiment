<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

	protected $table = 'blog_categories';
	public $timestamps = false;
	protected $guarded = array('id');

	public function CategoryToPosts()
	{
		return $this->belongsTo('CategoryPost');
	}

}