<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubmissionPlanMessage extends Model
{
    protected 	$table = "plan_submission_messages";

    public $timestamps = false;
    
    protected 	$fillable = [
    				'id',
	    			'sender_id',
					'subject',
					'message_body',
					'receiver_id',
					'plan_id',
                    'updated_at',
                    'created_at'
				];

	public function sender(){
        return $this->belongsTo('App\Models\Users', 'sender_id');
    }

    public function receiver(){
        return $this->belongsTo('App\Models\Users', 'receiver_id');
    }

    public function plan(){
        return $this->belongsTo('App\Models\Plan', 'plan_id');
    }
}
