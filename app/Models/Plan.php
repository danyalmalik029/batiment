<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected 	$table = "plan";
    protected 	$fillable = [
	    			'id',
				  	'title',
				  	'slug',
				  	'description',
				  	'user_id'
	    		];

	public function user(){
        return $this->belongsTo('App\Models\Users', 'user_id', 'id');
    }

	public function files(){
        return $this->hasMany('App\Models\Files', 'plan_id', 'id');
    }

    public function buyers(){
        return $this->belongsToMany('App\Models\Users', 'users_plan', 'plan_id', 'user_id');
    }

}
