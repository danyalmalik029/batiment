<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Files extends Model
{
    protected 	$table = "files";
    protected 	$fillable = [
	    			'id',
				  	'nom',
				  	'type',
				  	'path',
				  	'plan_id',
				  	'statut'
	    		];

	public function plan(){
        return $this->belongsTo('App\Models\Plan', 'plan_id', 'id');
    }

}
