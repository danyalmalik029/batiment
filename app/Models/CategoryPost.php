<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryPost extends Model {

	protected $table = 'blog_category_post';
	public $timestamps = false;

	public function posts()
	{
		return $this->belongsTo('Post', 'post_id');
	}

	public function categories()
	{
		return $this->hasOne('Category');
	}

}