<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Calculator extends Model
{
    protected 	$table = "calculator";

    
    protected 	$fillable = [
	    			'id',
					'cost',
					'pdf',
                    'room',
                    'floor',
                    'created_at',
                    'updated_at',
				];


}
