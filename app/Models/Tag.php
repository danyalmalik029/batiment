<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model {

	protected $table = 'blog_tags';
	public $timestamps = false;
        protected $guarded = array('id');

	public function TagToPost()
	{
		return $this->belongsTo('PostTag');
	}

}