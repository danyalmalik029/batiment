<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Projects extends Model
{
    protected 	$table = "projects";
    protected 	$fillable = [
	    			'id',
	    			'user_id',
	    			'construction_type',
	    			'land_id',
	    			'land_permission',
	    			'plan_id',
	    			'plan_permission',
	    			'area',
	    			'budget',
	    			'building_permit',
	    			'income',
	    			'created_at',
	    			'updated_at',
	    		];
	//relation to ads table, to retrieve land details
	public function land(){
        return $this->belongsTo('App\Models\Ads', 'land_id');
    }
	//relation to plan table, to retrieve plan details
	public function plan(){
        return $this->belongsTo('App\Models\Plan', 'plan_id');
    }
	//relation to user table, to retrieve user details
	public function user(){
        return $this->belongsTo('App\Models\Users', 'user_id');
    }


}
