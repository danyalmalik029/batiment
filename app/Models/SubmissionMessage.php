<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubmissionMessage extends Model
{
    protected 	$table = "submission_message";

    public $timestamps = false;
    
    protected 	$fillable = [
    				'id',
	    			'sender_id',
					'subject',
					'message_body',
					'receiver_id',
					'ads_id',
                    'updated_at',
                    'created_at'
				];

	public function sender(){
        return $this->belongsTo('App\Models\Users', 'sender_id');
    }

    public function receiver(){
        return $this->belongsTo('App\Models\Users', 'receiver_id');
    }

    public function ads(){
        return $this->belongsTo('App\Models\Ads', 'ads_id');
    }
}
