<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
    protected 	$table = "ads";
    protected 	$fillable = [
	    			'id',
				  	'catid',
				  	'title',
				  	'slug',
				  	'description',
				  	'etb01_adscol',
				  	'user_id',
				  	'ad_dtype',
				  	'action',
				  	'price',
				  	'bedrooms',
				  	'bathrooms',
				  	'area',
				  	'legaldocument',
				  	'conditioning',
				  	'cleaning',
				  	'dishwasher',
				  	'grill',
				  	'internet',
				  	'microwave',
				  	'pool',
				  	'terrace',
				  	'balcony',
				  	'cofeepot',
				  	'dvd',
				  	'hairdryer',
				  	'iron',
				  	'oven',
				  	'radio',
				  	'toaster',
				  	'bedding',
				  	'computer',
				  	'fan',
				  	'heating',
				  	'juicer',
				  	'parking',
				  	'roofterrace',
				  	'towelwes',
				  	'cabletv',
				  	'cot',
				  	'fridge',
				  	'hifi',
				  	'lift',
				  	'parquet',
				  	'smoking',
				  	'useofpool',
				  	'statut'
	    		];

	public function categories(){
        return $this->belongsTo('App\Models\Categories', 'catid');
    }

    public function images(){
        return $this->belongsToMany('App\Models\UserImage', 'ads_images', 'ads_id', 'image_id');
    }

    public function user(){
        return $this->belongsTo('App\Models\Users', 'user_id', 'id');
    }

    public function imagesLinked(){
        return $this->hasMany('App\Models\Images', 'ads_id', 'id');
    }
}
